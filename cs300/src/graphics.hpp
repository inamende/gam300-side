/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: graphics.hpp
Purpose: Some graphics utilities.
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#pragma once

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include "mesh.hpp"

namespace graphics
{
	template <typename T>
	unsigned create_buffer(T * data, size_t size ,unsigned type = GL_ARRAY_BUFFER)
	{
		unsigned handle;

		glGenBuffers(1, &handle);
		glBindBuffer(type, handle);

		const auto size_in_bytes = sizeof(T) * size;
		glBufferData(type, size_in_bytes, data, GL_STATIC_DRAW);

		return handle;
	}

	template <typename T>
	unsigned create_buffer(std::vector<T> data, size_t size, unsigned type = GL_ARRAY_BUFFER)
	{
		unsigned handle;

		glGenBuffers(1, &handle);
		glBindBuffer(type, handle);

		const auto size_in_bytes = sizeof(T) * size;
		glBufferData(type, size_in_bytes, data.data(), GL_STATIC_DRAW);

		return handle;
	}

	unsigned create_VAO();
	
	unsigned create_shader_program(const char * vertex_sh, const char * fragment);

	unsigned compile_shader(const char * shader, unsigned type);

	void add_vertex_attribute(unsigned VAO, unsigned VBO, unsigned location, unsigned lenght, unsigned stride = 0, void * offset = nullptr, unsigned type = GL_FLOAT);

	void draw(Object & object, unsigned int texture);

	unsigned load_texture_from_file(std::string path);

	template<typename T>
	unsigned generate_texture(const T data, unsigned width, unsigned height)
	{
		unsigned texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		return texture;
	}

	template<typename T> void set_uniform(unsigned shader_program, const std::string & name, const T &);

	template<>
	inline void set_uniform<bool>(unsigned shader_program, const std::string & name, const bool & mat)
	{
		glUniform1ui(glGetUniformLocation(shader_program, name.c_str()), (unsigned)mat);
	}

	template<> 
	inline void set_uniform<glm::mat4>(unsigned shader_program, const std::string & name, const glm::mat4 & mat)
	{
		glUniformMatrix4fv(glGetUniformLocation(shader_program, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
	}

	template<>
	inline void set_uniform<glm::vec3>(unsigned shader_program, const std::string & name, const glm::vec3 & mat)
	{
		glUniform3fv(glGetUniformLocation(shader_program, name.c_str()), 1, glm::value_ptr(mat));
	}

	template<>
	inline void set_uniform<unsigned>(unsigned shader_program, const std::string & name, const unsigned & mat)
	{
		glUniform1ui(glGetUniformLocation(shader_program, name.c_str()),mat);
	}

	template<>
	inline void set_uniform<int>(unsigned shader_program, const std::string & name, const int & mat)
	{
		glUniform1i(glGetUniformLocation(shader_program, name.c_str()), mat);
	}

	template<>
	inline void set_uniform < float > (unsigned shader_program, const std::string & name, const float & mat)
	{
		glUniform1f(glGetUniformLocation(shader_program, name.c_str()), mat);
	}
};
