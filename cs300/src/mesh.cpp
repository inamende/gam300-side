/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: mesh.cpp
Purpose: Define the mesh class.
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#include "mesh.hpp"
#include "editor.hpp"
#include <GLFW/glfw3.h>
#include <glm/gtx/projection.hpp>
#include <array>
#include <string>
#include <map>

//std::vector<Light *> Light::lights{};

Vertex::Vertex(vec3 pos, vec2 uv, vec3 normal) : position(pos), tex_coords(uv), normal(normal)
{
}

Vertex::Vertex(float x, float y, float z, float uvx, float uvy, float nx, float ny, float nz) : 
	position({ x,y,z }), tex_coords({ uvx, uvy }), normal({nx, ny, nz})
{
}

Object::Object(const char * vertex, const char * fragment) : 
	vertex_sh(vertex), fragment_sh(fragment), m_scale(vec3{ 1,1,1 })
{
}

void Object::set_shaders(const char * vertex, const char * fragment)
{
	vertex_sh = vertex;
	fragment_sh = fragment;
}

vec3 Light::spawn_pos = { light_radius, 0, 0 };
unsigned Light::light_count = 0;

Light::Light(const char * vertex, const char * fragment) :
	Object(vertex, fragment)
{
	//lights.push_back(this);
	ID = light_count;
	light_count++;
	spawn_pos = { light_radius, 0, 0 };
	m_scale = vec3(0.2f, 0.2f, 0.2f);
	m_position = spawn_pos;
	obj_type = ObjType::light;
}

void Light::update(bool pause)
{
	if (pause)
		return;

	m_position.x = (float)cos(glfwGetTime()+ ID) * light_radius;
	m_position.z = (float)sin(glfwGetTime()+ ID) * light_radius;
	m_position.y = (float)sin(glfwGetTime()+ ID * 2);
}

void Light::show_imgui()
{
	std::string name = "Light (" + std::string(std::to_string(ID) + ")");
	
	if (ImGui::CollapsingHeader(name.data()))
	{
		editor::ColorEdit3("Ambient#Ligth", properties.m_ambient);
		editor::ColorEdit3("Diffuse#Ligth", properties.m_diffuse);
		editor::ColorEdit3("Specular#Ligth", properties.m_specular);
		editor::SliderFloat("Outer angle", &properties.m_outer_angle, 0.f, 1.7f);
		editor::SliderFloat("Inner angle", &properties.m_inner_angle, 0.f, properties.m_outer_angle);
		editor::SliderFloat("Fallof", &properties.m_falloff, 0.f, 50.f);
		editor::SliderFloat3("Attenuation", properties.m_attenuation, 0.f, 1.f);
	}
}

// Material properties
vec3 Material::m_diffuse{1, 1, 1};
vec3 Material::m_specular{1, 1, 1};
float Material::m_shininess{50};

void Object::show_imgui()
{
	if (ImGui::CollapsingHeader("Material"))
	{
		editor::ColorEdit3("Diffuse#Object", material.m_diffuse);
		editor::ColorEdit3("Specular#Object", material.m_specular);
		editor::SliderFloat("Shininess", &material.m_shininess, 0, 100);
	}
}

void Object::set_tangents_bitangents()
{
	//std::vector<vec3> tangents;
	//std::vector<vec3> bitangents;
	//
	//typedef std::array<unsigned, 3> face;
	//std::vector<std::array<unsigned, 3>> faces;
	//
	//// for every face
	//for (unsigned i = 0; i < mesh.size(); i+= 3)
	//{
	//	faces.push_back(face{ i, i + 1, i + 2 });
	//
	//	vec3 edge1 = mesh[i + 1].position - mesh[i].position;
	//	vec3 edge2 = mesh[i + 2].position - mesh[i].position;
	//
	//	vec2 deltaUV1 = mesh[i + 1].tex_coords - mesh[i].tex_coords;
	//	vec2 deltaUV2 = mesh[i + 2].tex_coords - mesh[i].tex_coords;
	//
	//	vec3 tangent;
	//
	//	auto div = (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
	//
	//	if (div == 0)
	//		div = 0.00001f;
	//
	//	tangent = (deltaUV2.y * edge1 - deltaUV1.y * edge2) / div;
	//	tangent = glm::normalize(tangent);
	//
	//	//tangent = vec3(1, 0, 0);
	//
	//	// push back each 3 times since for every triangle there's a tangent
	//	tangents.push_back(tangent);
	//	tangents.push_back(tangent);
	//	tangents.push_back(tangent);
	//}
	//
	//// use Gram–Schmidt orthogonalization on Tangents
	//std::vector<vec3> new_tangents;
	//
	//for (unsigned i = 0; i < tangents.size(); i++)
	//{
	//	vec3 new_tangent = (tangents[i] - glm::dot(tangents[i], mesh[i].normal) * mesh[i].normal);
	//	new_tangent = glm::normalize(new_tangent);
	//	new_tangents.push_back(new_tangent);
	//}
	//
	//tangents = new_tangents;
	//
	//for (unsigned i = 0; i < mesh.size(); i++)
	//	mesh[i].tangent = tangents[i];
}

std::vector<vec3> Object::get_positions()
{
	 std::vector<vec3> positions;

	 for (auto & vertex : mesh.vertices)
		 positions.push_back(vertex.position);

	 return positions;
}

std::vector<vec3> Object::get_normals()
{
	std::vector<vec3> normals;

	for (auto & vertex : mesh.vertices)
		normals.push_back(vertex.normal);

	return normals;
}

std::vector<vec2> Object::get_uvs()
{
	std::vector<vec2> uvs;

	for (auto & vertex : mesh.vertices)
		uvs.push_back(vertex.tex_coords);

	return uvs;
}

std::vector<vec3> Object::get_tangents()
{
	std::vector<vec3> tangents;

	for (auto & vertex : mesh.vertices)
		tangents.push_back(vertex.tangent);

	return tangents;
}

std::vector<vec3> Object::get_bitangents()
{
	std::vector<vec3> bitangents;

	for (auto & vertex : mesh.vertices)
		bitangents.push_back(vertex.bitangent);

	return bitangents;
}

Mesh::Mesh()
{
}

Mesh::Mesh(std::vector<Vertex> & vert, std::vector<unsigned> & ind)
{
	vertices = vert;
	indices = ind;
}

size_t Mesh::size()
{
	return vertices.size();
}
