/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: graphics.cpp
Purpose: Some graphics utilities.
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#include "graphics.hpp"
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

unsigned graphics::create_shader_program(const char * vertex_sh, const char * fragment_sh)
{
	unsigned vs_handle = graphics::compile_shader(vertex_sh, GL_VERTEX_SHADER);
	unsigned fs_handle = graphics::compile_shader(fragment_sh, GL_FRAGMENT_SHADER);


	unsigned shader_program = glCreateProgram();

	glAttachShader(shader_program, vs_handle);
	glAttachShader(shader_program, fs_handle);
	glLinkProgram(shader_program);

	int success;
	char infoLog[512];

	glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shader_program, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER PROGRAM CLINK FAILED: \n" << infoLog << std::endl;
	}

	return shader_program;
}

unsigned graphics::compile_shader(const char * shader, unsigned type)
{
	//std::cout << shader << std::endl;

	unsigned handle = glCreateShader(type);

	glShaderSource(handle, 1, &shader, NULL);
	glCompileShader(handle);

	// check errors
	int  success;
	char infoLog[512];
	glGetShaderiv(handle, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(handle, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	return handle;
}


void graphics::add_vertex_attribute(unsigned VAO, unsigned VBO, unsigned location, unsigned lenght, unsigned stride, void * offset, unsigned type)
{
	// stride = byte offset between consecutive generic vertex attributes. 
	// If stride is 0, the generic vertex attributes are understood to be tightly packed in the array
	// source: khronos

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glVertexAttribPointer(location, lenght, type, GL_FALSE, stride, (void *)offset);
	glEnableVertexAttribArray(location);

	glBindVertexArray(0);
}

void graphics::draw(Object & object, unsigned int texture)
{
	glBindTexture(GL_TEXTURE_2D, texture);

	auto error = glGetError();
	(void)error;

	//glDisable(GL_CULL_FACE);
	glDrawArrays(GL_TRIANGLES, 0, (GLsizei)object.mesh.vertices.size());
}

unsigned graphics::load_texture_from_file(std::string path)
{
	int width, height, nrChannels;
	unsigned char * data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);

	unsigned handle = graphics::generate_texture(data, width, height);

	return handle;
}

unsigned graphics::create_VAO()
{
	unsigned int VAO;
	glGenVertexArrays(1, &VAO);

	return VAO;
}
