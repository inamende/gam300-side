/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: color.cpp
Purpose: Some colors I will be using.
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#include "color.hpp"

Color CommonColors::blue{ 0, 0, 255, 255 };
Color CommonColors::lightblue{ 0, 255, 255, 255 };
Color CommonColors::green{ 0, 255, 0, 255 };
Color CommonColors::yellow{ 255, 255, 0, 255 };
Color CommonColors::red{ 255, 0, 0, 255 };
Color CommonColors::pink{ 255, 0, 255, 255 };