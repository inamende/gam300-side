#pragma once

#include <glm/glm.hpp>

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))


using glm::vec3;

struct Axis;

enum Shape
{
	Box = 0,
	Sphere
};

class Collider
{
public:
	vec3 translation;
	Shape shape;
};

class BoxCollider3D : public Collider
{
public:
	BoxCollider3D();

	Axis compute_axis();

	vec3 scale;
	float rotation;
};

class SphereCollider3D : public Collider
{
public:
	SphereCollider3D();

	float radius;
};

struct Axis
{
	vec3 max;
	vec3 min;
};

bool collide_bodies(Collider *, Collider *);
bool collide_boxes(Collider *, Collider *);
bool collide_sphere_box(Collider *, Collider *);
bool collide_spheres(Collider *, Collider *);


// Concrete collision detection
bool aabb_aabb(BoxCollider3D *, BoxCollider3D *);
bool sphere_aabb(SphereCollider3D *, BoxCollider3D *);
bool collide_point_box(vec3*, Collider*);

// not implemented
bool aabb_obb(BoxCollider3D*, BoxCollider3D*);
bool obb_obb(BoxCollider3D*, BoxCollider3D*);
bool sphere_obb(SphereCollider3D *, BoxCollider3D *);
