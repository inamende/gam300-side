/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: main.cpp
Purpose: Draw diferent procedurally generated shapes with a procedurally generated texture in opengl
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#include "camera.hpp"
#include "parser.hpp"
#include "graphics.hpp"
#include "color.hpp"
#include "mesh.hpp"
#include "shader.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <glad/glad.h>  // Initialize with gladLoadGL()
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <stdio.h>
#include <memory>
#include <map>
#include <array>
#include <string>

#include "stb_image.h"

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

const float pi = glm::pi<float>();

Shader basic_vs{ "./shaders/basic.vert" };
Shader basic_fs{ "./shaders/basic.frag" };
Shader light_fs{ "./shaders/light.frag" };

using glm::vec3;
using glm::vec2;
using glm::mat4;

// renderable objects
Object plane(basic_vs.shader.c_str(), light_fs.shader.c_str());
Object cube(basic_vs.shader.c_str(), light_fs.shader.c_str());
Object cone(basic_vs.shader.c_str(), light_fs.shader.c_str());
Object sphere(basic_vs.shader.c_str(), light_fs.shader.c_str());
Object cylinder(basic_vs.shader.c_str(), light_fs.shader.c_str());

Light light1(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light2(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light3(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light4(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light5(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light6(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light7(basic_vs.shader.c_str(), basic_fs.shader.c_str());
Light light8(basic_vs.shader.c_str(), basic_fs.shader.c_str());

unsigned current_lights = 1;

std::array<Light *, 8> lights{ &light1, &light2, &light3, &light4, &light5, &light6, &light7, &light8 };
std::vector<Object *> objects;

Camera camera(vec3(0,0,4));

enum class Shape { plane, cube, cone, sphere, cylinder, ground};
enum RenderMode {normal_map, plain_normal_map, normals, tangent, bitangent, count};
unsigned current_mode;

// some globals
Shape current_shape = Shape::plane;
Object current_object(basic_vs.shader.c_str(), light_fs.shader.c_str());
Object ground(basic_vs.shader.c_str(), light_fs.shader.c_str());

const unsigned width = 1280;
const unsigned height = 720;

GLFWwindow* window;

bool texture_mapping = true;
bool pause = false;

// needed stuff to build some Objects
unsigned slices = 30;

const Color hardcoded_texture[36] =
{
	CommonColors::blue,      CommonColors::lightblue, CommonColors::green,     CommonColors::yellow,    CommonColors::red,       CommonColors::pink,
	CommonColors::lightblue, CommonColors::green,     CommonColors::yellow,    CommonColors::red,       CommonColors::pink,      CommonColors::blue,
	CommonColors::green,     CommonColors::yellow,    CommonColors::red,       CommonColors::pink,      CommonColors::blue,      CommonColors::lightblue,
	CommonColors::yellow,    CommonColors::red,       CommonColors::pink,      CommonColors::blue,      CommonColors::lightblue, CommonColors::green,
	CommonColors::red,       CommonColors::pink,      CommonColors::blue,      CommonColors::lightblue, CommonColors::green,     CommonColors::yellow,
	CommonColors::pink,      CommonColors::blue,      CommonColors::lightblue, CommonColors::green,     CommonColors::yellow,    CommonColors::red
};

std::vector<unsigned> keys_down;
std::vector<unsigned> prev_keys_down;

// glfw glfwGetKey wraper
bool key_down(unsigned key)
{
	bool key_pressed_prev_frame = false;

	if (glfwGetKey(window, key) == GLFW_RELEASE)
		return false;
	else
		keys_down.push_back(key);

	for (auto i : prev_keys_down)
	{
		if (i == key)
			key_pressed_prev_frame = true;
	}

	if (key_pressed_prev_frame)
		return false;
	else
		return true;

}

bool key_pressed(unsigned key)
{
	if (glfwGetKey(window, key) == GLFW_PRESS)
		return true;

	else
		return false;
}

// For sending to the Shader program the data we need
void setup_draw(Object * object)
{
	object->VAO = graphics::create_VAO();
	glBindVertexArray(object->VAO);

	object->pos_VBO = graphics::create_buffer(object->get_positions(), object->mesh.vertices.size());
	object->normal_VBO = graphics::create_buffer(object->get_normals(), object->mesh.vertices.size());
	object->uv_VBO = graphics::create_buffer(object->get_uvs(), object->mesh.vertices.size());
	//object->tangent_VBO = graphics::create_buffer(object->get_tangents(), object->mesh.size());
	//object->bitangent_VBO = graphics::create_buffer(object->get_bitangents(), object->mesh.size());

	object->shader_program = graphics::create_shader_program(object->vertex_sh.data(), object->fragment_sh.data());

	graphics::add_vertex_attribute(object->VAO, object->pos_VBO,		 0, 3);
	graphics::add_vertex_attribute(object->VAO, object->normal_VBO,		 1, 3);
	graphics::add_vertex_attribute(object->VAO, object->uv_VBO,			 2, 2);
	//graphics::add_vertex_attribute(object->VAO, object->tangent_VBO,	 3, 3);
	//graphics::add_vertex_attribute(object->VAO, object->bitangent_VBO,	 4, 3);
}

// build a cone with the given slices (slices --> global scope)
void init_cone()
{
	cone.mesh.vertices.clear();
	cone.mesh.vertices = parser::load_mesh("./obj/cone.obj");
}

// build the plane
void init_plane()
{
	plane.mesh.vertices.clear();
	plane.mesh.vertices = parser::load_mesh("./obj/plane.obj");
	plane.set_tangents_bitangents();
}

// build the cube
void init_cube()
{
	cube.mesh.vertices.clear();
	cube.mesh.vertices = parser::load_mesh("./obj/cube.obj");
	cube.set_tangents_bitangents();
}

// build a sphere with the given slices (slices & radius --> global scope)
void init_sphere()
{
	sphere.mesh.vertices.clear();
	sphere.mesh.vertices = parser::load_mesh("./obj/sphere.obj");
	sphere.set_tangents_bitangents();
}

// build a cylinder with the given slices (slices & radius --> global scope)
void init_cylinder()
{
	cylinder.mesh.vertices.clear();
	cylinder.mesh.vertices = parser::load_mesh("./obj/cylinder.obj");
	cylinder.set_tangents_bitangents();

}

void init_ground()
{
	ground.mesh.vertices.clear();
	ground.mesh.vertices = parser::load_mesh("./obj/cube.obj");
	ground.set_tangents_bitangents();
	ground.m_rotation.x = -glm::half_pi<float>(); // -90 degrees
	ground.m_position = vec3{ 0, -2, 0 };
	ground.m_scale = vec3(10.f, 10.f, 0.1f );
}

// build all shapes
void init_shapes()
{
	init_plane();
	init_cube();
	init_cone();
	init_sphere();
	init_cylinder();
	init_ground();
}

// build the given shape
void init_shapes(Shape shape)
{
	switch (shape)
	{
	case Shape::plane:
		init_plane();
		break;
	case Shape::cube:
		init_cube();
		break;
	case Shape::cone:
		init_cone();
		break;
	case Shape::sphere:
		init_sphere();
		break;
	case Shape::cylinder:
		init_cylinder();
		break;
	default:
		init_shapes();
		break;
	}
}

// change the mesh to draw
void change_mesh(Shape shape)
{
	switch (shape)
	{
	case Shape::plane:
		current_shape = Shape::plane;
		current_object = plane;
		init_plane();
		break;
	case Shape::cube:
		current_shape = Shape::cube;
		current_object = cube;
		init_cube();
		break;
	case Shape::cone:
		current_shape = Shape::cone;
		current_object = cone;
		init_cone();
		break;
	case Shape::sphere:
		current_shape = Shape::sphere;
		current_object = sphere;
		init_sphere();
		break;
	case Shape::cylinder:
		current_shape = Shape::cylinder;
		current_object = cylinder;
		init_cylinder();
		break;
	case Shape::ground:
		current_shape = Shape::ground;
		current_object = ground;
		init_ground();

	default:
		break;
	}

	current_object.set_shaders(basic_vs.shader.c_str(), light_fs.shader.c_str());
	ground.set_shaders(basic_vs.shader.c_str(), light_fs.shader.c_str());

	setup_draw(&current_object);
	setup_draw(&ground);
}

// process the input
void input()
{
	if (key_down(GLFW_KEY_1))
	{
		change_mesh(Shape::plane);
	}

	else if (key_down(GLFW_KEY_2))
	{
		change_mesh(Shape::cube);
		setup_draw(&current_object);
	}

	else if (key_down(GLFW_KEY_3))
	{
		change_mesh(Shape::cone);
	}

	else if (key_down(GLFW_KEY_4))
	{
		change_mesh(Shape::cylinder);
	}

	else if (key_down(GLFW_KEY_5))
	{
		change_mesh(Shape::sphere);
	}

	else if (key_down(GLFW_KEY_Y))
	{
		texture_mapping = !texture_mapping;

		change_mesh(current_shape);
	}

	else if (key_pressed(GLFW_KEY_UP))
		current_object.m_rotation.x += 0.1f;

	else if (key_pressed(GLFW_KEY_DOWN))
		current_object.m_rotation.x -= 0.1f;

	else if (key_pressed(GLFW_KEY_LEFT))
		current_object.m_rotation.y += 0.1f;

	else if (key_pressed(GLFW_KEY_RIGHT))
		current_object.m_rotation.y -= 0.1f;

	else if (key_pressed(GLFW_KEY_A))
	{
		camera.m_angle_x += 0.1f;
		camera.m_angle_z += 0.1f;
	}

	else if (key_pressed(GLFW_KEY_D))
	{
		camera.m_angle_x -= 0.1f;
		camera.m_angle_z -= 0.1f;
	}

	else if (key_pressed(GLFW_KEY_W))
	{
		camera.m_position.y += 1.0f;
	}

	else if (key_pressed(GLFW_KEY_S))
		camera.m_position.y -= 1.0f;

	else if (key_pressed(GLFW_KEY_E))
	{
		camera.m_radius -= 0.1f;

	}

	else if (key_pressed(GLFW_KEY_Q))
		camera.m_radius += 0.1f;

	else if (key_down(GLFW_KEY_P))
		pause = !pause;

	else if (key_pressed(GLFW_KEY_L))
	{
		for (auto & light : lights)
			light->m_type = LightType::pointlight;
	}

	else if (key_pressed(GLFW_KEY_K))
	{
		for (auto & light : lights)
			light->m_type = LightType::spotlight;
	}


	else if (key_pressed(GLFW_KEY_J))
	{
		for (auto & light : lights)
			light->m_type = LightType::directional;
	}


	else if (key_down(GLFW_KEY_T))
	{
		if (current_mode < RenderMode::count)
			current_mode++;
		else
			current_mode = 0;

		if (current_mode == 1)
			texture_mapping = false;

		else
			texture_mapping = true;
	}

	else if (key_down(GLFW_KEY_I))
	{
		if (current_lights < 8)
			current_lights++;
		else
			return;

		lights[current_lights - 1]->m_position = Light::spawn_pos;

		if(current_lights > 1) // light 0 is already in
			objects.push_back(lights[current_lights - 1]);
	}

	else if (key_down(GLFW_KEY_U))
	{
		if (current_lights > 1)
		{
			current_lights--;
			objects.pop_back();
		}
	}

	prev_keys_down = keys_down;
	keys_down.clear();
}

static void glfw_error_callback(int error, const char* description)
{
	fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

void ShowImguiWindow(bool* p_open)
{
	IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing dear imgui context. Refer to examples app!"); // Exceptionally add an extra assert here for people confused with initial dear imgui setup

	static bool show_app_custom_rendering = false;

	// Dear ImGui Apps (accessible from the "Help" menu)
	static bool show_app_metrics = false;
	static bool show_app_style_editor = false;
	static bool show_app_about = false;

	if (show_app_metrics) { ImGui::ShowMetricsWindow(&show_app_metrics); }
	if (show_app_style_editor) { ImGui::Begin("Style Editor", &show_app_style_editor); ImGui::ShowStyleEditor(); ImGui::End(); }
	if (show_app_about) { ImGui::ShowAboutWindow(&show_app_about); }

	// Demonstrate the various window flags. Typically you would just use the default!
	static bool no_titlebar = false;
	static bool no_scrollbar = false;
	static bool no_menu = false;
	static bool no_move = true;
	static bool no_resize = true;
	static bool no_collapse = false;
	static bool no_close = true;
	static bool no_nav = false;
	static bool no_background = false;
	static bool no_bring_to_front = false;

	ImGuiWindowFlags window_flags = 0;
	if (no_titlebar)        window_flags |= ImGuiWindowFlags_NoTitleBar;
	if (no_scrollbar)       window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (!no_menu)           window_flags |= ImGuiWindowFlags_MenuBar;
	if (no_move)            window_flags |= ImGuiWindowFlags_NoMove;
	if (no_resize)          window_flags |= ImGuiWindowFlags_NoResize;
	if (no_collapse)        window_flags |= ImGuiWindowFlags_NoCollapse;
	if (no_nav)             window_flags |= ImGuiWindowFlags_NoNav;
	if (no_background)      window_flags |= ImGuiWindowFlags_NoBackground;
	if (no_bring_to_front)  window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
	if (no_close)           p_open = NULL; // Don't pass our bool* to Begin

										   // We specify a default position/size in case there's no data in the .ini file. Typically this isn't required! We only do it to make the Demo applications a little more welcoming.
	ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_Always);
	ImGui::SetNextWindowSize(ImVec2(400, 720), ImGuiCond_Always);

	// Main body of the Demo window starts here.
	if (!ImGui::Begin("Editor", p_open, window_flags))
	{
		// Early out if the window is collapsed, as an optimization.
		ImGui::End();
		return;
	}

	// Most "big" widgets share a common width settings by default.
	//ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.65f);    // Use 2/3 of the space for widgets and 1/3 for labels (default)
	ImGui::PushItemWidth(ImGui::GetFontSize() * -12);           // Use fixed width for labels (by passing a negative value), the rest goes to widgets. We choose a width proportional to our font size.
	ImGui::Spacing();

	current_object.show_imgui();
	for(unsigned i = 0; i < current_lights; i++)
		lights[i]->show_imgui();

	// End of ShowDemoWindow()
	ImGui::End();
}

void render_scene(unsigned texture, unsigned int shader_program = 0)
{
	// create transformations for the current object
	for (size_t i = 0; i < objects.size(); i++)
	{
		if (shader_program == 0)
			shader_program = objects[i]->shader_program;

		// use the shader program
		glUseProgram(shader_program);

		unsigned error = glGetError();

		// Use this vao for drawing
		glBindVertexArray(objects[i]->VAO);

		error = glGetError();

		glm::mat4 view = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
		glm::mat4 projection = glm::mat4(1.0f);
		projection = glm::perspective(glm::radians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
		view = camera.view;

		glm::mat4 model = glm::identity<mat4>();

		mat4 scale = glm::scale(glm::identity<mat4>(), objects[i]->m_scale);
		mat4 rotation_x = glm::rotate(glm::identity<mat4>(), objects[i]->m_rotation.x, glm::vec3(1, 0, 0));
		mat4 rotation_y = glm::rotate(glm::identity<mat4>(), objects[i]->m_rotation.y, glm::vec3(0, 1, 0));
		mat4 translation = glm::translate(glm::identity<mat4>(), objects[i]->m_position);

		model = translation * rotation_x * rotation_y * scale;

		//graphics::set_uniform(objects[i]->shader_program, "renderMode", (int)current_mode);
		graphics::set_uniform(shader_program, "renderTexture", texture_mapping);

		graphics::set_uniform(shader_program, "textureData", 0);
		//graphics::set_uniform(objects[i]->shader_program, "normalMap", 1);

		//graphics::set_uniform(objects[i]->shader_program, "normalMap", (int)1);
		graphics::set_uniform(shader_program, "model", model);
		graphics::set_uniform(shader_program, "projection", projection);
		graphics::set_uniform(shader_program, "view", view);
		graphics::set_uniform(shader_program, "camPos", camera.m_position);
		graphics::set_uniform(shader_program, "NormalCam", camera.m_viewvec);

		graphics::set_uniform(objects[i]->shader_program, "material.m_diffuse", objects[i]->material.m_diffuse);
		graphics::set_uniform(objects[i]->shader_program, "material.m_shininess", objects[i]->material.m_shininess);
		graphics::set_uniform(objects[i]->shader_program, "material.m_specular", objects[i]->material.m_specular);

		graphics::set_uniform(objects[i]->shader_program, "lights_size", int(lights.size()));

		for (size_t j = 0; j < current_lights; j++)
		{
			lights[j]->properties.m_direction = current_object.m_position - lights[j]->m_position;

			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_ambient", lights[j]->properties.m_ambient);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_attenuation", lights[j]->properties.m_attenuation);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_diffuse", lights[j]->properties.m_diffuse);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_direction", lights[j]->properties.m_direction);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_falloff", lights[j]->properties.m_falloff);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_inner_angle", std::cos(lights[j]->properties.m_inner_angle));
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_outer_angle", std::cos(lights[j]->properties.m_outer_angle));
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_specular", lights[j]->properties.m_specular);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_position", lights[j]->m_position);
			graphics::set_uniform(objects[i]->shader_program, "lights[" + std::to_string(j) + "].m_type", (int)lights[j]->m_type);
		}

		error = glGetError();

		graphics::draw(*objects[i], texture);

		error = glGetError();
	}
}

// where magic happens
int main()
{
	// Setup window
	glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit())
		return 1;

	const char* glsl_version = "#version 130";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

	// Create window with graphics context
	window = glfwCreateWindow(width, height, "Graphics", NULL, NULL);
	if (window == NULL)
		return 1;
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1); // Enable vsync

	// Initialize OpenGL loader
	bool err = gladLoadGL() == 0;

	glEnable(GL_CULL_FACE);

	if (err)
	{
		fprintf(stderr, "Failed to initialize OpenGL loader!\n");
		return 1;
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	bool show_demo_window = true;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	// load and create a texture 
    // -------------------------

    unsigned int texture1;

	// texture 1
	// ---------

	glGenTextures(1, &texture1);
	glBindTexture(GL_TEXTURE_2D, texture1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 6, 6, 0, GL_RGBA, GL_UNSIGNED_BYTE, hardcoded_texture);
	glGenerateMipmap(GL_TEXTURE_2D);

	// depth texture
	// ---------
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);

	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	init_shapes();
	current_object.mesh = sphere.mesh;

	// Prepare to draw
	setup_draw(&current_object);
	setup_draw(&ground);

	for (auto & light : lights)
	{
		light->mesh = sphere.mesh;
		light->m_type = LightType::spotlight;
		setup_draw(light);
	}

	objects.push_back(&ground);
	objects.push_back(&current_object);
	objects.push_back(&light1);

	// Main loop
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
		if (show_demo_window)
			ShowImguiWindow(&show_demo_window);

		// Rendering
		ImGui::Render();
		int display_w, display_h;
		glfwMakeContextCurrent(window);
		glfwGetFramebufferSize(window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		glEnable(GL_DEPTH_TEST);
		glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		input();
		camera.update();

		for(auto & light : lights)
			light->update(pause);

		auto error = glGetError();

		render_scene(texture1);

		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwMakeContextCurrent(window);
		glfwSwapBuffers(window);
	}

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
