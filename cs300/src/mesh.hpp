/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: mesh.hpp
Purpose: Define the mesh class.
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#pragma once

#include <glm/glm.hpp>
#include <vector>

using glm::vec3;
using glm::vec2;

struct Material
{
	static vec3 m_diffuse;
	static vec3 m_specular;
	static float m_shininess;
};

struct LightProperties
{
	vec3 m_ambient;
	vec3 m_diffuse;
	vec3 m_specular;
	vec3 m_attenuation;
	vec3 m_direction;
	float m_outer_angle{0.785f}; // thetha
	float m_inner_angle{0.087f}; // phi
	float m_falloff{5.0f};
};

struct Vertex 
{
	Vertex(vec3 pos, vec2 text_coords, vec3 normal);
	Vertex(float x, float y, float z, float uvx, float uvy, float nx, float ny, float nz);

	vec3 position;
	vec3 normal;
	vec2 tex_coords;
	vec3 tangent;
	vec3 bitangent;
};

class Mesh
{
public:
	Mesh();
	Mesh(std::vector<Vertex> & vert, std::vector<unsigned> & ind);

	size_t size();

	std::vector<Vertex> vertices;
	std::vector<unsigned> indices;
};

enum ObjType {obj, light};

class Object
{
public:
	Object(const char * vertex, const char * fragment);
	Object(const Object &) = delete;
	virtual void show_imgui();

	ObjType obj_type = obj;

	Mesh mesh;

	void set_positions();
	void set_normals();
	void set_text_coords();
	void set_tangents_bitangents();

	std::vector<vec3> get_positions();
	std::vector<vec3> get_normals();
	std::vector<vec2> get_uvs();
	std::vector<vec3> get_tangents();
	std::vector<vec3> get_bitangents();

	vec3 m_position = {0,0,0};
	vec3 m_rotation = { 0,0, 0 };
	vec3 m_scale = { 100,100,100 };

	// Material properties
	Material material;

	unsigned VAO;
	unsigned pos_VBO;
	unsigned normal_VBO;
	unsigned uv_VBO;
	unsigned tangent_VBO;
	unsigned bitangent_VBO;

	unsigned shader_program;
	std::string vertex_sh;
	std::string fragment_sh;

	void set_shaders(const char* vertex, const char * fragment);
};

enum LightType{ pointlight, spotlight, directional }; // 0, 1, 2
const float light_radius = 2;

class Light : public Object
{
public:
	Light(const char * vertex, const char * fragment);
	void update(bool pause);
	void show_imgui() override;

	LightType m_type = LightType::directional;

	unsigned ID;
	float dt;

	LightProperties properties;

	static vec3 spawn_pos;
	//static std::vector<Light *> lights;

private:
	static unsigned light_count;
};
