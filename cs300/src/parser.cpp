#include "parser.hpp"

#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>

using glm::vec3;
using glm::vec2;

namespace parser
{
	std::vector<Vertex> load_mesh(std::string path)
	{
		std::vector<vec3> positions;
		std::vector<vec2> tex_coords;
		std::vector<vec3> normals;

		std::ifstream file(path.data());

		if (!file.is_open())
		{
			std::string error{ "Error opening file: " + path };
			std::cout << error << std::endl;
			return std::vector<Vertex>();
		}

		while (!file.eof())
		{
			std::string line_header;

			file >> line_header;

			if (line_header == "v") // position found
			{
				std::string value;
				vec3 pos;

				file >> value;
				pos.x = static_cast<float>(std::atof(value.c_str()));
				file >> value;
				pos.y = static_cast<float>(std::atof(value.c_str()));
				file >> value;
				pos.z = static_cast<float>(std::atof(value.c_str()));

				positions.push_back(pos);
			}
			else if (line_header == "vt") // texture coordinate found
			{
				std::string value;
				vec2 coord;

				file >> value;
				coord.x = static_cast<float>(std::atof(value.c_str()));
				file >> value;
				coord.y = static_cast<float>(std::atof(value.c_str()));

				tex_coords.push_back(coord);

			}
			else if (line_header == "vn") // normal found
			{
				std::string value;
				vec3 normal;

				file >> value;
				normal.x = static_cast<float>(std::atof(value.c_str()));
				file >> value;
				normal.y = static_cast<float>(std::atof(value.c_str()));
				file >> value;
				normal.z = static_cast<float>(std::atof(value.c_str()));

				normals.push_back(normal);
			}
			else if (line_header == "f") // face found
			{
				
			}
		}


		auto pos_size = positions.size();
		auto text_coords_size = tex_coords.size();
		auto normals_size = normals.size();

		// make sure that the .obj was ok
		if (!(pos_size == text_coords_size && text_coords_size == normals_size && normals_size == pos_size))
		{
			std::string error{ "Error reading file: " + path };
			std::cout << error << std::endl;
			return std::vector<Vertex>();
		}

		std::vector<Vertex> vertices;

		for (size_t i = 0; i < positions.size(); i++)
		{
			vertices.push_back(Vertex{ positions[i], tex_coords[i], normals[i] });
		}

		return vertices;
	}
	std::string load_file(std::string path)
	{
		std::ifstream file(path.data());

		if (!file.is_open())
		{
			std::string error{ "Error opening file: " + path };
			std::cout << error << std::endl;
			return nullptr;
		}

		std::string content, line;

		while (std::getline(file, line))
			content += line + "\n";

		return content;
	}
};