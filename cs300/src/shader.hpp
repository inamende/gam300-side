#pragma once

#include <string>
#include "parser.hpp"

class Shader
{
public:
	Shader(std::string path);
	std::string shader;
};