#include "camera.hpp"
#include <GLFW/glfw3.h>
#include <iostream>

Camera::Camera(vec3 position, vec3 target) : m_position(position), m_target(target)
{
	//m_viewvec = glm::normalize(m_position - m_target);
	m_viewvec = glm::normalize(vec3{0, 0, -1});
	m_right = glm::normalize(glm::cross(generic_up, m_viewvec));
	m_up = glm::cross(m_viewvec, m_right);
	view = glm::lookAt(m_position, m_target, m_up);

	m_angle_x= 0;
	m_angle_z= 0;
}

void Camera::update()
{
	m_position.x = sin(m_angle_x) * m_radius;
	m_position.z = cos(m_angle_z) * m_radius;

	view = glm::lookAt(m_position, m_target, m_up);
}
