#pragma once

#include <glm/glm.hpp>
#include "imgui.h"

namespace editor
{
	bool ColorEdit3(const char * label, glm::vec3 & vector)
	{
		float temp[3] = { vector.x, vector.y, vector.z };
		bool result = ImGui::ColorEdit3(label, temp);
		vector.x = temp[0]; 
		vector.y = temp[1]; 
		vector.z = temp[2];
		return result;
	}

	bool SliderFloat(const char * label, float * number, float min, float max)
	{
		bool result = ImGui::SliderFloat(label, number, min, max);
		return result;
	}

	bool SliderFloat3(const char * label, glm::vec3 vector, float min, float max)
	{
		float temp[3] = { vector.x, vector.y, vector.z };
		bool result = ImGui::SliderFloat3(label, temp, min, max);
		vector.x = temp[0];
		vector.y = temp[1]; 
		vector.z = temp[2];
		return result;
	}
}