#pragma once

#include "mesh.hpp"

namespace parser
{
	std::vector<Vertex> load_mesh(std::string path);
	std::string load_file(std::string path);
};