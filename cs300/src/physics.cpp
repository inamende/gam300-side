#include "physics.h"

BoxCollider3D::BoxCollider3D()
{
	shape = Shape::Box;
}

Axis BoxCollider3D::compute_axis()
{
	Axis result;

	float x1 = translation.x + (scale.x / 2);
	float x2 = translation.x - (scale.x / 2);

	float y1 = translation.y + (scale.y / 2);
	float y2 = translation.y - (scale.y / 2);

	float z1 = translation.z + (scale.z / 2);
	float z2 = translation.z - (scale.z / 2);

	result.max = vec3{ MAX(x1, x2), MAX(y1, y2), MAX(z1,z2) };
	result.min = vec3{ MIN(x1, x2), MIN(y1, y2), MIN(z1,z2) };
}

SphereCollider3D::SphereCollider3D()
{
	shape = Shape::Sphere;
}

bool collide_bodies(Collider * a, Collider * b)
{
	if (a->shape == Shape::Box && b->shape == Shape::Box)
		return collide_boxes(a, b);

	else if (a->shape == Shape::Sphere && b->shape == Shape::Sphere)
		return collide_spheres(a, b);

	else if (a->shape == Shape::Sphere && b->shape == Shape::Box)
		return collide_sphere_box(a, b);

	else if (a->shape == Shape::Box && b->shape == Shape::Sphere)
		return collide_sphere_box(b, a);
}

bool collide_spheres(Collider * a_, Collider * b_)
{
	SphereCollider3D * a = static_cast<SphereCollider3D *>(a_);
	SphereCollider3D * b = static_cast<SphereCollider3D *>(b_);

	// later on I'll need to add the offset within the object
	float distance = glm::distance(a->translation, b->translation);
	float totalradius = a->radius + b->radius;

	if (distance < totalradius)
		return true;

	else 
		return false;
}


bool collide_boxes(Collider * a_, Collider* b_)
{
	BoxCollider3D * a = static_cast<BoxCollider3D *>(a_);
	BoxCollider3D * b = static_cast<BoxCollider3D *>(b_);

	return aabb_aabb(a, b);


	// turns out we don't needs OBB for now

	//// AABB vs OBB
	//else if (a->rotation == 0.0f && b->rotation != 0.0f)
	//	return aabb_obb(a, b);
	//
	//// OBB vs AABB
	//else if (a->rotation != 0.0f && b->rotation == 0.0f)
	//	return aabb_obb(b, a);
	//
	//else // OBB vs OBB
	//	return obb_obb(a, b);
}

bool collide_sphere_box(Collider * a_, Collider * b_)
{
	SphereCollider3D * a = static_cast<SphereCollider3D *>(a_);
	BoxCollider3D * b = static_cast<BoxCollider3D *>(b_);

	// sphere vs AABB
	return sphere_aabb(a, b);
	
	// turns out we don't needs OBB for now

	//else // sphere vs OBB
	//	return sphere_obb(a, b);
}

bool aabb_aabb(BoxCollider3D * a, BoxCollider3D * b)
{
	Axis a_axis = a->compute_axis();
	Axis b_axis = b->compute_axis();

	return	(a_axis.min.x <= b_axis.max.x && a_axis.max.x >= b_axis.min.x) &&
			(a_axis.min.y <= b_axis.max.y && a_axis.max.y >= b_axis.min.y) &&
			(a_axis.min.z <= b_axis.max.z && a_axis.max.z >= b_axis.min.z);
}

bool collide_point_box(vec3 * point, Collider* collider)
{
	// for now there's only point vs aabb implemented
	BoxCollider3D * box = static_cast<BoxCollider3D*>(collider);

	Axis axis = box->compute_axis();

	return	(point->x >= axis.min.x && point->x <= axis.max.x) &&
			(point->y >= axis.min.y && point->y <= axis.max.y) &&
			(point->z >= axis.min.z && point->z <= axis.max.z);
}