#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


using glm::vec3;
using glm::vec2;
using glm::mat4;

class Camera
{
public:
	Camera(vec3 position = vec3{ 0,0,0 }, vec3 target = vec3{ 0,0,0 });
	void update();

	vec3 m_position;
	vec3 m_right = glm::normalize(glm::cross(generic_up, m_viewvec));
	vec3 m_up = glm::cross(m_viewvec, m_right);
	mat4 view = glm::lookAt(m_position, m_target, m_up);

	float m_angle_x = 0;
	float m_angle_z = 0;

	vec3 m_target = glm::vec3(0.0f, 0.0f, 0.0f);
	vec3 m_viewvec = glm::normalize(m_position - m_target);
	float m_radius = 4.0f;

private:
	vec3 generic_up{ 0.0f, 1.0f, 0.0f };
};
