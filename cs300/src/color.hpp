/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: color.hpp
Purpose: Some colors I will be using
Language: C++, Visual Studio 2015
Platform: Windows
Project: CS300_ignacio.mendezona_0
Author: Ignacio Mendezona --- ignacio.mendezona@digipen.edu
----------------------------------------------------------------------------------------------------------*/

#pragma once
#include <glm/glm.hpp>

using Color = glm::vec<4, unsigned char>;

struct CommonColors
{
	static Color blue;
	static Color lightblue;
	static Color green;
	static Color yellow;
	static Color red;
	static Color pink;
};
