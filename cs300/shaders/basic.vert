#version 450 core

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal; // in model space
layout(location = 2) in vec2 aTexCoords;

out vec2 TexCoords;
out vec3 FragPos;

// in world space
out vec3 in_normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	FragPos = vec3(model * vec4(aPos, 1.0));  
    TexCoords = aTexCoords;
		
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	in_normal = normalize(normalMatrix * aNormal);
	
    gl_Position = projection * view * vec4(FragPos, 1.0);
}
