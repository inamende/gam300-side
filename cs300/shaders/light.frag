#version 450 core

out vec3 FragColor;

in vec2 TexCoords;
in vec3 FragPos;

// in world space
in vec3 in_normal;

uniform sampler2D textureData;

uniform bool renderTexture;

uniform vec3 camPos;

struct Material
{
	vec3 m_diffuse;
	vec3 m_specular;
	float m_shininess;
};

uniform Material material;

struct Light
{
	vec3 m_ambient;
	vec3 m_diffuse;
	vec3 m_specular;
	vec3 m_attenuation;
	vec3 m_direction;
	vec3 m_position;
	float m_outer_angle; // thetha
	float m_inner_angle; // phi
	float m_falloff;
	int m_type;
};

uniform int lights_size;
uniform Light lights[8];																																			 																																																																							 

float atenuation_x = 1.0f;
float atenuation_y = 0.09f;
float atenuation_z = 0.032f;
																																 
vec3 pointlight(in vec3 Normal, in vec3 textureColor, in Light light, vec3 V)																			 
{																																				 
	// ambient
    vec3 ambient = light.m_ambient * textureColor;
  	
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize((light.m_position - FragPos));
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.m_diffuse * diff * material.m_diffuse * textureColor;  
    
    // specular
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(V, reflectDir), 0.0), material.m_shininess);
    vec3 specular = light.m_specular * spec * material.m_specular;  
    
    // attenuation
    float distance    = length(light.m_position - FragPos);
    float attenuation = 1.0 / (atenuation_x + atenuation_y * distance + atenuation_z * (distance * distance));    
 
    diffuse  *= attenuation;
	specular *= attenuation;   
        
	return ambient + diffuse + specular;																							 
}																																				 
																																				 
vec3 spotlight(vec3 Normal, vec3 textureColor, Light light, vec3 V)																			 
{	
	// ambient
    vec3 ambient = light.m_ambient * textureColor;
    
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.m_position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.m_diffuse * diff * material.m_diffuse * textureColor;  
    
    // specular
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(V, reflectDir), 0.0), material.m_shininess);
    vec3 specular = light.m_specular * spec * material.m_specular; 
    
    // spotlight (soft edges)
    float alpha = dot(-lightDir, normalize(light.m_direction));
	float epsilon = (light.m_inner_angle - light.m_outer_angle);
	float intensity = pow(clamp((alpha - light.m_outer_angle) / epsilon, 0.0, 1.0), light.m_falloff);
    diffuse  *= intensity;
    specular *= intensity;
    
    // attenuation
    float distance    = length(light.m_position - FragPos);
	float attenuation = 1.0 / (atenuation_x + atenuation_y * distance + atenuation_z * (distance * distance));    
    
	ambient  *= attenuation; 
    diffuse  *= attenuation;
    specular *= attenuation;   
        
    return diffuse + specular + ambient;																																			 																										 
}

vec3 directionallight(vec3 Normal, vec3 textureColor, Light light, vec3 V)
{
	// ambient
    vec3 ambient = light.m_ambient * textureColor;
	
	// diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(-light.m_direction);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.m_diffuse * diff * material.m_diffuse * textureColor;  
	
	// specular
	vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(V, reflectDir), 0.0), material.m_shininess);
    vec3 specular = light.m_specular * spec * material.m_specular;
	
	return diffuse + specular + ambient;
}

void main()
{
	vec3 finalColor = vec3(0);
	vec3 V = normalize(camPos - FragPos);
	
	vec3 texture_color = texture(textureData, TexCoords).rgb;
	
	vec2 temp_uv;
	temp_uv.x =  1 - TexCoords.x;
	temp_uv.y =  1 - TexCoords.y;
	
	//vec3 normal = 2.0 * texture( normalMap, temp_uv ).xyz - 1.0;
	vec3 world_normal = normalize(in_normal);
	vec3 normal = normalize(in_normal);
	
	vec3 color;
	if(renderTexture)
		color = texture(textureData, temp_uv).rgb;
	else
		color = vec3(1);	
	
	for (int i = 0; i < lights_size; i++)																											 
	{						
		if (lights[i].m_type == 0)																												 
			finalColor += pointlight(normal, color, lights[i], V);		
			
		else if (lights[i].m_type == 1)																											 
			finalColor += spotlight(normal, color, lights[i], V);	
			
		else if (lights[i].m_type == 2)																			 
			finalColor += directionallight(normal, color, lights[i], V);	
	}	
	
	FragColor = finalColor;
}
