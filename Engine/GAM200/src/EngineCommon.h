#ifndef ENGINE_COMMON_H
#define ENGINE_COMMON_H

#include <vector>
#include <list>
#include <map>
#include <array>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

//FIND MEMORY LEAKS
/*****************************************************************/
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#else
#define DBG_NEW new
#endif
/*****************************************************************/

#include "json/json.h"
#include <glm/glm.hpp>

#define EDITOR // no time to make another solution
extern Json::Value commonValue;
extern bool possesed_knight;

using namespace glm;
using std::cout;

extern std::string editor_loader;
extern bool first_play;

#define JOYSTICK_DEADZONE  6000

//this will be the volume that gets modified by menues and other events
//1 is max, 0 is muted

inline vec2 safe_normalize(vec2 vec) {
	return vec == vec2() ? vec2() : normalize(vec);
}

namespace MasterVolume
{
	extern float MASTER_VOLUME;
	extern float EFFECTS_VOLUME;
	extern float MUSIC_VOLUME;
//modify and access the master volume
	void SetMasterVolume(float v);
	float GetMasterVolume();
	float & GetMasterVolumeReference();
	void DebugReduceVolume();
	void MaxOutVolume();
	//modify and get the music volume
	void SetMusicVolume(float v);
	float GetMusicMasterVolume();
	float & GetMusicMasterVolumeReference();
	void DebugReduceMusicVolume();
	void MaxOutMusicVolume();
	//modify and get the effects volume
	void SetEffectsVolume(float v);
	float GetEffectsMasterVolume();
	float & GetEffectsMasterVolumeReference();
	void DebugReduceEffectsVolume();
	void MaxOutEffectsVolume();
};

#endif