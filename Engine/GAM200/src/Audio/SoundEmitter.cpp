#include "SoundEmitter.h"
#include "../EngineCommon.h"
#include "../Editor/ImGuiEditor.h"
#include "../ObjectManager/GameObject.h"

#include "../Gfx/Gfx.h"
#include "../Gfx/Comp/Transform.h"
#include "../Gfx/Reso/Camera.h"
#include "../Input/Input.h"
#include "../../src/ResourceManager/ResoManager.h"
#include "../Gfx/Reso/SoundFile.h"
#include "MusicEmitter.h"
#include "../Input/Input.h"
#include "../Gfx/Comp/Transform.h"
#include "../ObjectManager/ObjectManager.h"
SoundEmitter::SoundEmitter() : sound(NULL), voice(NULL), volume(0.5f), PointerToVoiceArray(NULL), IsInit(false){}

SoundEmitter::~SoundEmitter()
{
	gAudioMgr.FreeThisVoice(voice);
	voice = NULL;
	//gAudioMgr.FreeSound(sound);
}
void SoundEmitter::Initialize()
{
	//gets a list of the voices
	//PointerToVoiceArray = gAudioMgr.GetVoiceArrayPointer();
	IsInit = true;
	MusicEmitter * m = NULL;
	m = GetOwner()->GetComponent<MusicEmitter>();
	if (m)
	{
		AttatchedToStrawss = true;
	}
	else
	{
		AttatchedToStrawss = false;
	}
	mObjectTransform = GetOwner()->GetComponent<Transform>();
}

void SoundEmitter::SetSpecificVolume(float f)
{
	mSpecificVolume = f;
}
void SoundEmitter::Update()
{
	if (voice && voice->IsValid())
	{
		if (AttatchedToStrawss)
		{
			volume = 1.0f;
			//std::cout << "Strawss: Voulme in sound emitter is: " << voice->GetVolume() << "   voice: " << voice << std::endl;
		}
		else
		{
			if (VolumeUsesDistance)
			{
				camDistance = glm::distance(glm::vec2(mObjectTransform->position.x, mObjectTransform->position.y), glm::vec2(graphics->GetMainCamera()->position.x, graphics->GetMainCamera()->position.y));
				
				float camDistNormalized = camDistance / camMaxDistance;
				camDistNormalized = clamp(camDistNormalized, 0.0f, 1.0f);
				volume = 1.0f - camDistNormalized;
			}
			else
			{
				volume = 1.0f;
			}
			//std::cout << "Other: Voulme in sound emitter is: " << voice->GetVolume() << "   voice: " << voice << std::endl;

		}
		volume *= MasterVolume::GetMasterVolume();
		volume *= MasterVolume::GetEffectsMasterVolume();
		volume *= mSpecificVolume;
		voice->SetVolume(volume);
	}
	
}
void SoundEmitter::Terminate(){}

void SoundEmitter::load(Json::Value & val)
{
	active = val["SoundEmitter"].get("active", true).asBool();
	mInitialSound = val["SoundEmitter"].get("mInitialSound", "Kalimba.mp3").asString();
	VolumeUsesDistance = val["SoundEmitter"].get("VolumeUsesDistance", true).asBool();
	mSpecificVolume = val["SoundEmitter"].get("mSpecificVolume", 1).asFloat();
	AttatchedToStrawss = val["SoundEmitter"].get("AttatchedToStrawss", false).asBool();
	camMaxDistance = val["SoundEmitter"].get("camMaxDistance", 500.0f).asFloat();
}
void SoundEmitter::save(Json::Value & val)
{
	val["SoundEmitter"]["active"] = active;
	val["SoundEmitter"]["mInitialSound"] = mInitialSound;
	val["SoundEmitter"]["VolumeUsesDistance"] = VolumeUsesDistance;
	val["SoundEmitter"]["mSpecificVolume"] = mSpecificVolume;
	val["SoundEmitter"]["AttatchedToStrawss"] = AttatchedToStrawss;
	val["SoundEmitter"]["camMaxDistance"] = camMaxDistance;
}
void SoundEmitter::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("SoundEmitter"))
	{
		ImGui::Checkbox("active##SoundEmitter", &active); //no effect for now
		ImGuiEditor::InputText("Initial Sound##SoundEmitter", mInitialSound);
		//this will determine if the distance from the camera needs to be taken into account while setting the volume
		ImGui::Checkbox("UsingDistanceForVolume##SoundEmitter", &VolumeUsesDistance);
		ImGui::Checkbox("AttatchedToStrawss##SoundEmitter", &AttatchedToStrawss);
		ImGui::InputFloat("Specific Volume##SoundEmitter", &mSpecificVolume);
		ImGui::InputFloat("Cam Max Distance ##SoundEmitter", &camMaxDistance);

		if (ImGui::Button("DebugPauseSound##SoundEmitter"))
		{
			if (sound)
			{
				voice->SetPause(true);
			}
		}
		if (ImGui::Button("DebugHalveVolume##SoundEmitter"))
		{
			MasterVolume::DebugReduceVolume();
		}
		if (ImGui::Button("DebugMAXVolume##SoundEmitter"))
		{
			MasterVolume::MaxOutVolume();
		}
		if (ImGui::Button("DebugHalveEffectsVolume##SoundEmitter"))
		{
			MasterVolume::DebugReduceEffectsVolume();
		}
		if (ImGui::Button("DebugMAXVolumeEffects##SoundEmitter"))
		{
			MasterVolume::MaxOutEffectsVolume();
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##SoundEmitter"))
			GetOwner()->detach(this);
	}
}
void SoundEmitter::PlayCurrentSound()
{
	this->PlayOnce(mInitialSound, false);
}
void SoundEmitter::StopVoice()
{
	if (voice)
	{
		voice->Stop();

		gAudioMgr.FreeThisVoice(voice);
		//gAudioMgr.FreeSound(sound);
		voice = NULL;
	}
}
void SoundEmitter::PlayOnce(std::string SoundName, bool init_pause)
{
	if (voice && voice->IsValid())
		return;

	//std::cout << "Playing Sound: "<< SoundName << "With State = "<< SoundState << std::endl;
	Sound * tempSound = resoManager->getReso<SoundFile>(SoundName)->ReturnSoundPointer();
	if (tempSound == NULL)
		return;

	//free voice
	if (voice != NULL)
		gAudioMgr.FreeThisVoice(voice);
	voice = NULL;
	//auto debug = gAudioMgr.Play(tempSound, false);
	voice = gAudioMgr.Play(tempSound, init_pause);
	//if (init_pause == false)
  	//std::cout << "Sound is: " << SoundName << " , and volume is: " << voice->GetVolume() <<"   voice is: "<<voice << std::endl;
	
}
void SoundEmitter::PlayLooped(std::string SoundName, bool init_pause)
{
	//auto s = SoundName;
	//std::cout << s << std::endl;
	if (voice && voice->IsValid())
		return;

	//std::cout << "Playing Sound: "<< SoundName << "With State = "<< SoundState << std::endl;
	Sound * tempSound = resoManager->getReso<SoundFile>(SoundName)->ReturnSoundPointer();
	if (tempSound == NULL)
		return;

	//free voice
	if (voice != NULL)
		gAudioMgr.FreeThisVoice(voice);
	voice = NULL;
	//auto debug = gAudioMgr.Play(tempSound, false);
	voice = gAudioMgr.Loop(tempSound, init_pause);
}
void SoundEmitter::PauseVoice()
{
	voice->SetPause(true);
}