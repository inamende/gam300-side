#include "MusicEmitter.h"
#include "../EngineCommon.h"
#include "../Editor/ImGuiEditor.h"
#include "../ObjectManager/GameObject.h"
#include "../Level/Scene.h"

#include "../Gfx/Gfx.h"
#include "../Gfx/Window.h"
#include "../Gfx/Comp/Transform.h"
#include "../Gfx/Reso/Camera.h"
#include "../Input/Input.h"
#include "../../src/ResourceManager/ResoManager.h"
#include "../Gfx/Reso/SoundFile.h"
#include "../Input/Input.h"
#include "../ObjectManager/ObjectManager.h"
#include "../Utils/Utilities.h"

MusicEmitter::MusicEmitter() : mVoice(NULL), mSong("NULL"), mSpecificVolume(1.0f)
{
	//std::cout << "Music Constructor" << std::endl;
}
MusicEmitter::~MusicEmitter()
{
	//std::cout << "Music Destructor" << std::endl;
	gAudioMgr.FreeThisVoice(mVoice); 
	mVoice = NULL;
}
void MusicEmitter::Initialize()
{
	//std::cout << "Music Initialize" << std::endl;
	//std::cout << "Is music already playing?" << std::endl;

	mSound = gAudioMgr.GetSound(mSong.c_str());
	
	//free before playing
	gAudioMgr.FreeThisVoice(mVoice);
	mVoice = NULL;
	mVoice = gAudioMgr.Loop(mSound, true);

	t = GetOwner()->GetComponent<Transform>();
	p = GetOwner()->GetComponent<PlayerController>();
	//check if this is attatched to strawss
	if (p)
	{
		IsAProp = false;
	}
	else
	{
		IsAProp = true;
	}

	mVoice->SetPause(false);

	gAudioMgr.pFMOD->createDSPByType(FMOD_DSP_TYPE_LOWPASS, &dsp);
	dsp->setParameterFloat(FMOD_DSP_LOWPASS_CUTOFF, 2000.f);
}
void MusicEmitter::Terminate()
{
	if (mVoice)
	{
		mVoice->SetPause(true);
		gAudioMgr.FreeThisVoice(mVoice);
		mVoice = NULL;
	}
}
void MusicEmitter::Update()
{
	//Voice *	AudioManager::Loop(Sound * pSound, bool paused)
	//mVoice = gAudioMgr.Loop(mSound);
	//mVoice->SetVolume(master_vol);
	float temp = MasterVolume::GetMasterVolume();

	if (IsAProp)
	{
		
		//make the distance calculations
		//float camDistance = glm::distance(glm::vec2(mObjectTransform->position.x, mObjectTransform->position.y), glm::vec2(graphics->GetMainCamera()->position.x, graphics->GetMainCamera()->position.y));
		float camDistance = glm::distance(glm::vec2(t->position.x, t->position.y), glm::vec2(graphics->GetMainCamera()->position.x, graphics->GetMainCamera()->position.y));
		//bless the magic number
		float camDistNormalized = camDistance / 500;
		camDistNormalized = clamp(camDistNormalized, 0.0f, 1.0f);
		float volume = 1.0f - camDistNormalized;

		temp = temp * MasterVolume::GetEffectsMasterVolume();
		temp = temp * volume;

		if (mVoice)
		{
			mVoice->SetVolume(temp);
			if (mVoice->IsPaused() && !mWindow->window_is_missing())
			{
				mVoice->SetPause(false);
			}
		}
	}
	else
	{
		temp = temp * MasterVolume::GetMusicMasterVolume();
		//debug, remove
		//temp *= 0.2;
		//
		if (mVoice)
		{
			mVoice->SetVolume(temp);
			if (mVoice->IsPaused() && !mWindow->window_is_missing())
			{
				mVoice->SetPause(false);
			}
		}
	}
}
void MusicEmitter::load(Json::Value & val)
{
	mSong = val["MusicEmitter"].get("mSong", "Kalimba").asString();
	Playing = val["MusicEmitter"].get("Playing", true).asBool();
	mSpecificVolume = val["MusicEmitter"].get("mSpecificVolume", 1.0f).asFloat();
}
void MusicEmitter::save(Json::Value & val)
{
	val["MusicEmitter"]["mSong"] = mSong;
	val["MusicEmitter"]["Playing"] = Playing;
	val["MusicEmitter"]["mSpecificVolume"] = mSpecificVolume;
}
void MusicEmitter::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("MusicEmitter"))
	{
		ImGui::Checkbox("active##MusicEmitter", &active); //no effect for now
		ImGui::Checkbox("Playing##MusicEmitter", &Playing);
		ImGui::InputFloat("SpecificVolume##MusicEmitter", &mSpecificVolume);
		ImGuiEditor::InputText("Song Name##MusicEmitter", mSong);
		
		if (ImGui::Button("DebugPlayEnd##MusicEmitter"))
		{
			global_handler.handle(ReachedEndOfLevelEvent());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##MusicEmitter"))
			GetOwner()->detach(this);
	}
}

void MusicEmitter::StopVoice(bool v) {
	if (mVoice) {
		mVoice->SetPause(v);
	}
}

void  MusicEmitter::addDSP() {
	mVoice->pChannel->addDSP(0, dsp);
}

void  MusicEmitter::removeDSP() {
	mVoice->pChannel->removeDSP(dsp);
}


