// ---------------------------------------------------------------------------
// Project Name		:	Alpha Engine
// File Name		:	AudioManager.cpp
// Author			:	Thomas Komair
// Creation Date	:	2012/02/15
// Purpose			:	main implementation file for the AudioManager
// History			:
// - 2012/03/29		:	- initial implementation
// ---------------------------------------------------------------------------
#include "Audio.h"
//oscars additions
#include "../ResourceManager/ResoManager.h"
#include "../Gfx/Reso/SoundFile.h"

#define MAX_VOICE_COUNT 2000	// ! CANNOT BE ZERO

#pragma region// Voice

Voice::Voice(): pChannel(NULL), isLooping(false), isPaused(true), volume(1.0f)
{
	
}
Voice::~Voice()
{
	
}
void Voice::SetVolume(float vol)
{
	if(pChannel == NULL)
		return;
	
	volume = vol;

	if(volume < 0.0f)
		volume = 0.0f;
	else if (volume > 1.0f)
		volume = 1.0f;

	pChannel->setVolume(volume);
}
float  Voice::GetVolume()
{
	return volume;
}
void Voice::Stop()
{
	if (pChannel == NULL)
		return;
	pChannel->stop();
}
void Voice::SetPause(bool paused)
{
	
	if(pChannel == NULL)
		return;
	isPaused = paused;
	pChannel->setPaused(paused);
}
bool Voice::IsPaused()
{
	return isPaused;
}
void Voice::SetLoop(bool isLoop)
{
	
	if(pChannel == NULL)
		return;

	isLooping = isLoop;
	if(isLooping)
		pChannel->setLoopCount(-1);
	else
		pChannel->setLoopCount(0);
}
bool Voice::IsValid()
{
	
	if (pChannel == NULL)
	{
		//std::cout << "VOICE IS NOT VALID" << std::endl;
		return false;
	}

	bool pl;
	pChannel->isPlaying(&pl);
	return pl;
} 
bool Voice::IsLooping()
{
	
	return isLooping;
}
#pragma endregion

// ----------------------------------------------------------------------------
#pragma region// Sound

Sound::Sound() : pSound(NULL)
{
	
}
#pragma endregion

// ----------------------------------------------------------------------------
#pragma region// Audio Manager

// Global 
AudioManager gAudioMgr;

// ----------------------------------------------------------------------------
AudioManager::AudioManager()
	: pFMOD(NULL)
	, soundCount(0)
	, voiceCount(MAX_VOICE_COUNT)
	, voices(0)
{
	
}
AudioManager::~AudioManager()
{	
	Shutdown();
}
bool	AudioManager::Initialize()
{
	FMOD_RESULT result;

	result = FMOD::System_Create(&pFMOD);
	if (result!= FMOD_OK)
	{
		//AE_ASSERT_MESG(result == FMOD_OK, "AUDIO: Error creating FMOD system! (%d) %s\n", result, FMOD_ErrorString(result));
		return false;
	}

	// Initialize FMOD
	result = pFMOD->init(MAX_VOICE_COUNT, FMOD_INIT_NORMAL, 0);
	if (result  != FMOD_OK)
	{
		pFMOD->release();
		pFMOD = NULL;
		return false;
	}

	AllocVoices();
	//this will create new resource objects of type soundfile
	InitAudioMap();
	return true;
}
//return a Sound Pointer based on the string given from the resource manager
Sound * AudioManager::GetSound(std::string s)
{
	if (s == "NULL")
	{
		//std::cout << "GetSound: the sound string is null" << std::endl;
		return NULL;
	}
	Sound * tempSound;
	tempSound = resoManager->getReso<SoundFile>(s)->ReturnSoundPointer();
	return tempSound;
}
//returns a pointer the the sound array for use in the sound emitter
Voice * AudioManager::GetVoiceArrayPointer()
{
	return voices;
}
void	AudioManager::InitAudioMap()
{
	resoManager->AddReso(DBG_NEW SoundFile, "DrawBridge.mp3", "./Data/Sounds/DrawBridge.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "StrawssDirtJump.mp3", "./Data/Sounds/StrawssDirtJump.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "StrawssDrinkJuice.mp3", "./Data/Sounds/StrawssDrinkJuice.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "StrawssSpecterReturn.mp3", "./Data/Sounds/StrawssSpecterReturn.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "MetalGrateRustyOpen.mp3", "./Data/Sounds/MetalGrateRustyOpen.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "LeverActivate.mp3", "./Data/Sounds/LeverActivate.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "KnightRunning.mp3", "./Data/Sounds/KnightRunning.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "KnightWalking.mp3", "./Data/Sounds/KnightWalking.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "KnightSwordSwing.mp3", "./Data/Sounds/KnightSwordSwing.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "BowmanWalk.mp3", "./Data/Sounds/BowmanWalk.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "SteamWhistle.mp3", "./Data/Sounds/SteamWhistle.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "Menu_Accept.mp3", "./Data/Sounds/Menu_Accept.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "Menu_Click22.mp3", "./Data/Sounds/Menu_Click22.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "Menu_Select16.mp3", "./Data/Sounds/Menu_Select16.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "Whoosh.mp3", "./Data/Sounds/Whoosh.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "IronClang.mp3", "./Data/Sounds/IronClang.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "WoodenDoorBreak.mp3", "./Data/Sounds/WoodenDoorBreak.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "Grunt.mp3", "./Data/Sounds/Grunt.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "BowmanCrossbowShootReload.mp3", "./Data/Sounds/BowmanCrossbowShootReload.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "PressurePlate.mp3", "./Data/Sounds/PressurePlate.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "StrawssDirtRun.mp3", "./Data/Sounds/StrawssDirtRun.mp3");
	//NewMusic
	resoManager->AddReso(DBG_NEW SoundFile, "BOSS_Music.mp3", "./Data/Sounds/BOSS_Music.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "LEVEL_1_Music.mp3", "./Data/Sounds/LEVEL_1_Music.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "LEVEL_2_Music.mp3", "./Data/Sounds/LEVEL_2_Music.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "LEVEL_3_Music.mp3", "./Data/Sounds/LEVEL_3_Music.mp3");
	resoManager->AddReso(DBG_NEW SoundFile, "MENU_Music.mp3", "./Data/Sounds/MENU_Music.mp3");

}
void	AudioManager::Shutdown()
{
	
	if(NULL == pFMOD)
		return;

	pFMOD->release();
	pFMOD = NULL;
	FreeVoices();
}
void	AudioManager::Update()
{
	//if (usedVoices.size() >= MAX_VOICE_COUNT)
	//{
	//	std::cout << "Reached the max" << std::endl;
	//}
	//if (freeVoices.size() <= 0)
	//{
	//	std::cout << "Reached the min" << std::endl;
	//
	//}
	if(NULL == pFMOD)
		return;

	// Update FMOD -> play audio
	pFMOD->update();

	//With great power, comes great responsibility

	// loop through the voices
	//for (Voice::PTR_LIST::iterator it = usedVoices.begin(); it != usedVoices.end(); ++it)
	//{
	//	// this voice is no longer playing
	//	if (!(*it)->IsValid())
	//	{
	//		// erase and push to free list
	//		freeVoices.push_back(*it);
	//		it = usedVoices.erase(it);
	//		if (it == usedVoices.end())
	//			break;
	//	}
	//}

}
FMOD::System * AudioManager::GetFMOD()
{
	//if (this->pFMOD == nullptr)
	//{
	//	std::cout << "pFMOD is NULL" << std::endl;
	//}
	return this->pFMOD;
}
// ----------------------------------------------------------------------------

void AudioManager::FreeThisVoice(Voice *pVoice)
{
	
	if(NULL == pFMOD)
		return;
	if(pVoice == NULL)
		return;

	pVoice->pChannel->stop();
	usedVoices.remove(pVoice);
	freeVoices.remove(pVoice);
	freeVoices.push_back(pVoice);
}
//ITS NOTE HERE
/*******************************************************************************/
Voice *	AudioManager::GetFreeVoice()
{
	if (NULL == pFMOD)
	{
		//std::cout << "CANNOT GET FREE VOICE: pFMOD IS NULL" << std::endl;
		return NULL;
	}
	if (freeVoices.empty())
	{
		//std::cout << "CANNOT GET FREE VOICE: freeVoices IS EMPTY" << std::endl;
		return NULL;
	}
	
	Voice * pv = freeVoices.back();
	freeVoices.pop_back();
	usedVoices.push_back(pv);

	return pv;
}
/*******************************************************************************/

void	AudioManager::AllocVoices()
{
	
	if(NULL == pFMOD)
		return;

	// free the voices if necessary
	FreeVoices();

	// allocate new array of voices
	voiceCount = MAX_VOICE_COUNT;
	voices = new Voice[voiceCount];

	// push all onto the free voices
	for(unsigned int i = 0;i < voiceCount; ++i)
		freeVoices.push_back(voices +i);
}
void	AudioManager::FreeVoices()
{
	
	if(NULL == pFMOD)
		return;
	if(voices)
	{
		delete [] voices;
		voiceCount = 0;
		freeVoices.clear();
		usedVoices.clear();
	}
}

// ----------------------------------------------------------------------------
Sound *	AudioManager::CreateSound(const char * filename)
{
	
	if(NULL == pFMOD)
		return NULL;

	// Allocate new memory for the sound
	Sound * pSound = new Sound();
	pFMOD->createSound(filename, FMOD_LOOP_NORMAL | FMOD_2D, 0, &pSound->pSound);

	// save the name of the 
	pSound->filename = filename;

	// error check
	if(pSound->pSound == NULL)
	{
		// make sure to delete the sound pointer
		delete pSound;
		return NULL;
	}

	// All is good
	++soundCount;	// Stats update
	return pSound;
}
void		AudioManager::FreeSound(Sound * pSound)
{
	
	if(NULL == pFMOD)
		return;
	if(!pSound)
		return;
	
	if(pSound->pSound)
	{
		pSound->pSound->release();
		pSound->pSound = 0;
	}

	// Stats update
	--soundCount;
	delete pSound;
}
Voice *	AudioManager::Play(Sound * pSound, bool paused)
{
	
	// make sure we can actually play the sound
	if( pFMOD == NULL || pSound == NULL)
		return NULL;
	
	// look for a free voice
	Voice * pVoice = GetFreeVoice();

	// this voice is valid
	if(pVoice)
	{
		// we found an available voice
		pFMOD->playSound(pSound->pSound, 0, paused, &pVoice->pChannel);
		pVoice->SetPause(paused);
		pVoice->SetLoop(false);
	}
	else
	{
		//printf("No voice available for play \n");
	}
	
	// Return the voice (either NULL or correct)
	return pVoice;
}
Voice *	AudioManager::Loop(Sound * pSound, bool paused)
{
	
	Voice * pv = Play(pSound, paused);
	if (pv)
		pv->SetLoop(true);
	return pv;
}
void		AudioManager::StopAll()
{
	if(NULL == pFMOD)
		return;

	// loop through the voices
	while(!usedVoices.empty())
	{
		// erase and push to free list
		usedVoices.back()->pChannel->stop();
		freeVoices.push_back(usedVoices.back());
		usedVoices.pop_back();
	}
}

#pragma endregion
