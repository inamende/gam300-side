#pragma once
#include "../Component/Component.h"
#include "Audio.h"
#include <vector>
#include "../Events/event.h"
#include "../Gfx/Comp/Transform.h"
#include "../Gameplay/Components/Strawss/PlayerController.h"

enum SongStates
{
	//volume goes from 0 to 1
	begining = 1,
	//the main loop of the song
	loop = 2,
	//volume goes from 1 to 0
	end = 3
};
//an event that this MusicEmitter will receive when Strawss reaches the end of the level
class ReachedEndOfLevelEvent : public Event
{
public:
	ReachedEndOfLevelEvent() {}
	~ReachedEndOfLevelEvent() {}
private:
};
//an event this musicEmitter will send once it has completed the volume interpolation
//signalling that we can switch levels
class CanChangeLevels : public Event
{
public:
	CanChangeLevels() {}
	~CanChangeLevels() {}
private:
};
//this is a component to be added on to the camera, to constantly produce music
class MusicEmitter : public Component
{
public:
	MusicEmitter();
	~MusicEmitter();
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	void StopVoice(bool);
	void addDSP();
	void removeDSP();

private:
	Voice * mVoice;
	Sound * mSound;
	FMOD::DSP *dsp;

	std::string		mSong;
	bool mEditorPlayBool;
	bool Playing = true;
	float mSpecificVolume;
	Transform * t = nullptr;
	PlayerController * p = nullptr;
	bool IsAProp = false;
};