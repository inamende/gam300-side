#pragma once
#include "../Component/Component.h"
#include "Audio.h"
#include <vector>
#include "../Events/event.h"

class Transform;
//this is a component placed on objects that will generate sounds
class SoundEmitter : public Component
{
public:
	SoundEmitter();
	~SoundEmitter();
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void PlayCurrentSound();
	bool IsInit = false;
	Voice * GetVoice()
	{
		return voice;
	}
	void SetSpecificVolume(float f);
	
	void StopVoice();
	
	void PlayOnce(std::string SoundName, bool init_pause);
	void PlayLooped(std::string SoundName, bool init_pause);
	void PauseVoice();
private:
	Sound * sound;
	Voice * voice;
	float volume;
	float camDistance;
	float camMaxDistance = 500.0f; // TODO: Make this tweakable
	std::string mInitialSound;
	unsigned debug_counter = 0;
	unsigned max_debug_counter = 100;
	//trying to use my own voice array
	Voice * PointerToVoiceArray;
	bool VolumeUsesDistance = true;
	//number between 0 and 1 to determine if a particular object needs to have its volume manually specified
	float mSpecificVolume;
	bool AttatchedToStrawss = false;
	Transform * mObjectTransform;
};
