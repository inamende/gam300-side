// ---------------------------------------------------------------------------
// Project Name		:	Alpha Engine
// File Name		:	AEAudio.h
// Author			:	Thomas Komair
// Creation Date	:	2013/06/13
// Purpose			:	main header file for the AudioManager
// History			:
// - 2012/03/29		:	- initial implementation
// ---------------------------------------------------------------------------

#ifndef AE_AUDIO_MANAGER_H_
#define AE_AUDIO_MANAGER_H_

#include <fmod/fmod.hpp>
#include <list>
#include <vector>
#include <map>
// Forward declare the FMOD classes
// so that I don't have to include fmod.hpp 
// in Audio.h, and therefore create an unnecessary 
// dependency. 

//oscars additions for debugging purposes
void PrintDebugInfo();

namespace FMOD
{
	class Sound;
	class Channel;
	class System;
}

struct Sound
{
	FMOD::Sound		*pSound;
	std::string		 filename;

	Sound();
};

class Voice
{
	// friends have access to privates
	friend class AudioManager;

public:

	Voice();
	~Voice();

	// Setters and Getters
	void SetVolume(float volume);
	void SetPause(bool paused);
	void SetLoop(bool isLoop);
	bool IsPaused();
	bool IsValid(); 
	bool IsLooping();
	float  GetVolume();
	void Stop();
	
	// STL
	typedef std::list<Voice *> PTR_LIST;

//private:
	
	FMOD::Channel * pChannel;
	float			volume;
	bool			isLooping;
	bool			isPaused;
};

class AudioManager
{
public:

	AudioManager();
	~AudioManager();

	bool Initialize();
	void Update();
	void Shutdown();

	// Sound Management
	Sound * CreateSound(const char * filename);
	void	FreeSound(Sound * pSound);
	Voice * Play(Sound * pSound, bool paused = false);
	Voice * Loop(Sound * pSound, bool paused = false);

	FMOD::System * GetFMOD();

	// Stop All
	void StopAll();
	void FreeThisVoice(Voice *pVoice);
	void InitAudioMap();
	//oscars additions
	//return a Sound Pointer based on the string given
	Sound * GetSound(std::string s);
	Voice * GetVoiceArrayPointer();
	void FreeVoicesPublic()
	{
		this->FreeVoices();
	}
//private:
	
	// Helper methods
	void						AllocVoices();
	void						FreeVoices();

	// Voice manager... adds to free and removes from used
	Voice *						GetFreeVoice();

	// FMOD System
	FMOD::System*				pFMOD;

	// keep track of the sounds created 
	unsigned int				soundCount;
	unsigned int				voiceCount;
	// Voice Management List (equivalent to dead list and free list)
	Voice *						voices;
	Voice::PTR_LIST				freeVoices;
	Voice::PTR_LIST				usedVoices;

};

// GLOBAL Audio Manager
extern AudioManager gAudioMgr;

// ----------------------------------------------------------------------------
#endif