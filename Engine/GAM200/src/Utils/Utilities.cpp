#include "Utilities.h"

#include "../Gfx/Comp/Transform.h"

float RandomFloat(float a, float b)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

void Grow(Transform * out, unsigned x, unsigned y)
{
	out->position.x -= (x - out->scale.x) * 0.5f;
	out->position.y += (y - out->scale.y) * 0.5f;

	out->scale.x = x;
	out->scale.y = y;
}

bool CompareFloat(float a, float b, float epsilon) {
	return fabs(a - b) < epsilon;
}