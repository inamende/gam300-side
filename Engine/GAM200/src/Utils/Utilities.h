#ifndef UTILITIES_H
#define UTILITIES_H

#include "../EngineCommon.h"

class Transform;

float RandomFloat(float a, float b);

template<typename T>
T Lerp(T start, T end, float dT)
{
	return (start + dT * (end - start));
}

bool CompareFloat(float a, float b, float epsilon);

// make a box grow like a life bar, it can be used for other things too (see the knight -> KChase.cpp)
void Grow(Transform * out, unsigned x, unsigned y);

#endif