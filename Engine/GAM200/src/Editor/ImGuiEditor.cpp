#include "ImGuiEditor.h"

#include <windows.h>

#include <GL/glew.h>

#include "../Factory/Factory.h"
#include "../ObjectManager/ObjectManager.h"
#include "../Input/Input.h"

#include "../Level/Scene.h"

#include "../Gfx/Gfx.h"
#include "../Gfx/Reso/Camera.h"
#include "../Gfx/Reso/TileMap.h"
#include "../Gfx/Reso/Texture.h"
#include "../Gfx/Reso/TexturePacker.h"
#include "../Gfx/Reso/AtlasData.h"
#include "../Gfx/Reso/AnimationData.h"
#include "../Gfx/Comp/Renderable.h"
#include "../Gfx/Window.h"

#include "../Physics/Physics.h"
#include "../Physics/Collisions.h"

#include "../Actions/Actions.h"
#include "../Actions/TransformAction.h"
#include "../Actions/CreateAction.h"
#include "../Actions/DestroyAction.h"

#include <stdio.h>

#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL_image.h>

#include "../Physics/Comp/BoxCollider.h"

// this is for ctrl z
std::vector<Action*> pickingActions;
// this is for ctrl c
std::vector<Json::Value> copyValues; std::vector<glm::vec3> copyPositions;

std::vector<Transform> gizmos; // the little yellow square for moving

// the arrows to make stuff... you know... the ones to move things...
std::vector<Transform> arrowsUp;
std::vector<Transform> arrowsRight;

// helper function to know which gizmo has been selected
bool SelectedHelper(std::vector<Transform> & tr, glm::vec3 & p);

bool PickerLogicFunction(const glm::vec2 & pt, GameObject * obj);
bool PickerALotLogicFunction(const glm::vec2 & pt, GameObject * obj); // sugested by Thomas
bool PickerTileMapLogicFunction(const glm::vec2 & pt, GameObject * obj);

ImGuiEditor::ImGuiEditor() :
selectedObj(NULL),
cursorIsOnImGui(false),
pickingLayer("default"),
showTileMapWindow(false),
selectedTileMap(NULL),
propEditMode(false),
toPlaceObj(NULL)
{
	editState = EditState::Idle;
	transformState = TransformState::TranslatePlease;
}

ImGuiEditor::~ImGuiEditor() {}

ImGuiEditor * ImGuiEditor::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW ImGuiEditor();

	return instance;
}

void ImGuiEditor::InitImGui(SDL_Window * handle)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	ImGui_ImplSdlGL3_Init(handle);
	ImGui::StyleColorsDark();
}

void ImGuiEditor::Render()
{
	ImGui::Render();
	ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
}

void ImGuiEditor::EditorWindow(SDL_Window * handle)
{
	ImGui_ImplSdlGL3_NewFrame(handle);

#ifdef EDITOR
	// ImGui::ShowDemoWindow();

	// *******************************
	// MENU BAR
	// *******************************
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("Menu"))
		{
			ShowMenuSettings();
			ImGui::EndMenu();
		}
		ImGui::Separator();

		if (ImGui::Checkbox("Prop Editor", &propEditMode))
		{
			if (propEditMode)
			{
				float z = GetLastZ();

				std::vector<std::string> jsonNames = graphics->GetFiles("./Data/Jsons/");
				toPlaceObj = scene->CreateGameObject();
				toPlaceObj->LoadJson("./Data/Jsons/" + jsonNames[0]);
				toPlaceObj->mTransform->position.z = z + 0.1f;
				Renderable * rend = toPlaceObj->GetComponent<Renderable>();
				if (rend)
					rend->layer = pickingLayer;
				toPlaceObj->Initialize();
			}
			else
			{
				scene->DeleteGameObject(toPlaceObj);
				toPlaceObj = NULL;
			}
		}

		ImGui::Separator();
		std::string picc = "Picking : " + pickingLayer;
		if (ImGui::BeginMenu(picc.c_str()))
		{
			for (unsigned i = 0; i < layerNames.size(); i++)
				if (ImGui::MenuItem(layerNames[i].c_str()))
				{
					pickingLayer = layerNames[i];
					graphics->Flush(pickingLayer);
				}
			ImGui::EndMenu();
		}

		if (ImGui::Button("[ + ]"))
			transformState = TransformState::TranslatePlease;
		if (ImGui::Button("[ x ]"))
			transformState = TransformState::ScalePlease;
		if (ImGui::Button("[ o ]"))
			transformState = TransformState::RotatePlease;

		std::string edit = scene->isEditing ? "[  |>  ]" : "[  ||  ]";

		ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor(255, 0, 255, 255));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor(255, 200, 255, 255));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor(255, 0, 255, 255));
		if (ImGui::Button(edit.c_str()))
			scene->ChangeEditMode();
			
		ImGui::PopStyleColor(3);

		if (ImGui::BeginMenu("Help"))
		{
			ShowHelp();
			ImGui::EndMenu();
		}

		ImGui::EndMainMenuBar();
	}

	// *******************************
	// EDITOR
	// *******************************
	if (!ImGui::Begin("Editor", NULL))
	{
		ImGui::End();
		return;
	}

	ImGui::SetWindowSize(ImVec2(300, 500), ImGuiCond_FirstUseEver);

	static bool autoSave = objectManager->AutosaveEnabled();
	if (ImGui::Checkbox("AUTO SAVE", &autoSave))
		objectManager->SetAutoSave(autoSave);

	mWindow->WriteToImguiWindow();
	objectManager->WriteToImguiWindow();
	graphics->WriteToImguiWindow();
	physics->WriteToImguiWindow();
	scene->WriteToImguiWindow();

	cursorIsOnImGui = ImGui::GetIO().WantCaptureMouse; // this tells when the cursor is hovering an ImGui::whatever
	
	static int counter = 0;
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

	ImGui::End();

	// *******************************
	// INSPECTOR
	// *******************************
	if (!ImGui::Begin("Inspector", NULL))
	{
		ImGui::End();
		return;
	}

	ShowObjectComponents(selectedObj);

	ImGui::End();

	// *******************************
	// TILEMAP :(
	// *******************************
	if(showTileMapWindow)
		ShowTileMap(selectedTileMap);

	if (KeyPressed(SDL_SCANCODE_1))
		transformState = TransformState::TranslatePlease;
	else if (KeyPressed(SDL_SCANCODE_2))
		transformState = TransformState::ScalePlease;
	else if (KeyPressed(SDL_SCANCODE_3))
		transformState = TransformState::RotatePlease;

	// *******************************
	// PROP EDITOR
	// *******************************
	if (KeyPressed(Keyboard::LEFT_SHIFT) && KeyDown(Keyboard::E))
	{
		propEditMode = !propEditMode;

		if (propEditMode)
		{
			float z = GetLastZ();

			std::vector<std::string> jsonNames = graphics->GetFiles("./Data/Props/");
			toPlaceObj = scene->CreateGameObject();
			toPlaceObj->LoadJson("./Data/Props/" + jsonNames[0]);
			toPlaceObj->mTransform->position.z = z + 0.1f;
			Renderable * rend = toPlaceObj->GetComponent<Renderable>();
			if (rend)
				rend->layer = pickingLayer;
			toPlaceObj->Initialize();
		}
		else
		{
			scene->DeleteGameObject(toPlaceObj);
			toPlaceObj = NULL;
		}
	}
		
	if (propEditMode)
		PropEditMode();
	else
	{
		Picking();
		RenderPicking();
		ModifyObjects();
	}
	
	// Rendering
	glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT);
#endif
}

void ImGuiEditor::Terminate()
{
	if (toPlaceObj != NULL)
	{
		objectManager->aliveObjects.remove(toPlaceObj);
		toPlaceObj->Terminate();
		delete toPlaceObj;
	}
	
	delete instance;
}

void ImGuiEditor::ShowMenuSettings()
{
	if (ImGui::BeginMenu("New"))
	{
		if (ImGui::MenuItem("Empty Object"))
		{
			CreateObject("./Data/Jsons/EmptyObject.json");
		}

		if (ImGui::MenuItem("Dummy"))
		{
			CreateObject("./Data/Jsons/DefaultObject.json");
		}

		if (ImGui::BeginMenu("Archetype"))
		{
			ImGui::Text("Jsons");
			ImGui::Separator();

			static ImGuiTextFilter filter;
			filter.Draw("search##Archetype");
			std::vector<std::string> jsonNames = graphics->GetFiles("./Data/Jsons");
			for (unsigned i = 0; i < jsonNames.size(); i++)
			{
				if (filter.PassFilter(jsonNames[i].c_str()))
				{
					if (ImGui::Selectable(jsonNames[i].c_str()))
					{
						CreateObject("./Data/Jsons/" + jsonNames[i]);
					}
				}
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Buttons"))
		{
			ImGui::Text("Jsons");
			ImGui::Separator();

			static ImGuiTextFilter filter;
			filter.Draw("search##Buttons");
			std::vector<std::string> jsonNames = graphics->GetFiles("./Data/Jsons/Buttons");
			for (unsigned i = 0; i < jsonNames.size(); i++)
			{
				if (filter.PassFilter(jsonNames[i].c_str()))
				{
					if (ImGui::Selectable(jsonNames[i].c_str()))
					{
						CreateObject("./Data/Jsons/Buttons" + jsonNames[i]);
					}
				}
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Props"))
		{
			ImGui::Text("Jsons");
			ImGui::Separator();

			static ImGuiTextFilter filter;
			filter.Draw("search##Props");
			std::vector<std::string> jsonNames = graphics->GetFiles("./Data/Props");
			for (unsigned i = 0; i < jsonNames.size(); i++)
			{
				if (filter.PassFilter(jsonNames[i].c_str()))
				{
					if (ImGui::Selectable(jsonNames[i].c_str()))
					{
						CreateObject("./Data/Props/" + jsonNames[i]);
					}
				}
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("TileMap"))
		{
			static int w = 5;
			ImGui::InputInt("width##TileMap", &w);

			static int h = 5;
			ImGui::InputInt("height##TileMap", &h);

			std::string tilemapname = "TileMap_";

			if (ImGui::Button("Generate Tile Map"))
			{
				GameObject * newEditorObj = scene->CreateGameObject();
				newEditorObj->LoadJson("./Data/Jsons/DefaultTileMap.json");

				newEditorObj->SetName(tilemapname + std::to_string(graphics->tileMaps.size()));

				newEditorObj->GetComponent<TileMap>()->SetWidth(w);
				newEditorObj->GetComponent<TileMap>()->SetHeight(h);

				newEditorObj->Initialize();

				editState = EditState::TileMapPlease;
				showTileMapWindow = true;
				selectedTileMap = newEditorObj->GetComponent<TileMap>();
			}

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Atlas"))
		{
			static int w = 1024;
			ImGui::InputInt("width##Atlas", &w);

			static int h = 1024;
			ImGui::InputInt("height##Atlas", &h);

			static std::string outputFile = "default_atlas";
			ImGuiEditor::InputText("Output File", outputFile);

			if (ImGui::Button("Generate Atlas"))
			{
				imGuiEditor->Packaged("./Data/Packaged/", outputFile, w, h);
				AutomaticPropGenerator("./Data/Result/" + outputFile + ".json");
			}

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Animation"))
		{
			static int r = 10;
			ImGui::InputInt("rows##Animation", &r);

			static int c = 10;
			ImGui::InputInt("columns##Atlas", &c);

			static std::string outputFile = "default_animaion";
			ImGuiEditor::InputText("Output File", outputFile);

			if (ImGui::Button("Generate Amimation"))
			{
				std::string fullPath = outputFile + ".meta";
				AnimationData * newAnim = DBG_NEW AnimationData(outputFile, r, c);
				newAnim->Writer("./Data/Animations/" + fullPath);

				resoManager->AddReso(newAnim, fullPath, "./Data/Animations/");
				imGuiEditor->animationNames.push_back(fullPath);
			}


			ImGui::EndMenu();
		}

		ImGui::EndMenu();
	}
}

void ImGuiEditor::ShowHelp()
{
	if (ImGui::TreeNode("Shortcuts"))
	{
		ImGui::Text("Camera :");
		ImGui::Text("Middle mouse / arrow keys	-> move camera");
		ImGui::Text("Ctrl + Mouse scroll	-> zoom in / out");
		ImGui::Text("I don't have time for this, figure it out yourself!");

		ImGui::TreePop();
	}
}

float ImGuiEditor::GetLastZ()
{
	//find the game object with the biggest z
	auto find = std::max_element(objectManager->aliveObjects.begin(), objectManager->aliveObjects.end(), 
		[](GameObject * first, GameObject * second)
		{
			/*if (first->mTransform->position.z < 100 && second->mTransform->position.z < 100) //lights have 500 of z so don't take em into account
				return first->mTransform->position.z < second->mTransform->position.z;
			else
				return false;*/

			return first->mTransform->position.z < second->mTransform->position.z;
		});

	return (find != objectManager->aliveObjects.end()) ? (*find)->mTransform->position.z : -50.0f;
}

void ImGuiEditor::ShowObjectComponents(GameObject * obj)
{
	static int selected_component = -1;

	if (obj == NULL)
		return;

	// with this is clear when the engine takes keyboard input and when ImGui does
	//mInput->editorIsActive = (ImGui::IsWindowHovered() || ImGui::IsItemHovered() || ImGui::IsWindowFocused() || ImGui::IsAnyItemHovered() || ImGui::IsAnyItemFocused()) ? true : false;
	mInput->editorIsActive = (ImGui::IsWindowFocused() || ImGui::IsAnyItemHovered() || ImGui::IsAnyItemFocused()) ? true : false;

	// set the name

	//a bit hacky, but needed
	auto collider = obj->GetComponent<BoxCollider>();
	
	if (collider != nullptr && collider->has_been_deleted_in_this_frame())
	{
		collider = nullptr;
		return;
	}

	std::string newName = obj->GetName();
	ImGuiEditor::InputText("name", newName);

	int id = obj->GetId();
	ImGui::Text("Unique ID: %i", id);

	if (obj) obj->SetName(newName);

	// show the components
	ImGui::Text("Components : ");
	obj->OpenComponentWindows();

	// allow to attach and detach components
	ImGui::NewLine();
	if (ImGui::Button("Attach"))
		ImGui::OpenPopup("ComponentList");

	if (ImGui::BeginPopup("ComponentList"))
	{
		ImGui::Text("Components");
		ImGui::Separator();

		static ImGuiTextFilter filter;

		filter.Draw("search##Attach");
		for (unsigned i = 0; i < componentNames.size(); i++)
		{
			if (filter.PassFilter(componentNames[i].c_str()))
			{
				if (ImGui::Selectable(componentNames[i].c_str()))
				{
					selected_component = i;

					Component * comp = dynamic_cast<Component*>(mFactory->Create((componentNames[selected_component]).c_str()));

					obj->attach(comp);
					comp->Initialize();
				}
			}
		}
			
		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("createarchetype"))
	{
		static std::string archetypeFile = "DefaultArchetype";
		ImGuiEditor::InputText("Archeotype Name", archetypeFile);

		if (ImGui::Button("Create Archetype"))
			obj->SaveJson("./Data/Jsons/" + archetypeFile + ".json");

		ImGui::EndPopup();
	}

	// delete the selectedObjs
	ImGui::NewLine();
	if (ImGui::Button("Delete"))
	{
		selectedObj = NULL;
		for (auto it = selectedObjs.begin(); it != selectedObjs.end(); it++)
			scene->DeleteGameObject(*it);
		selectedObjs.clear();
	}

	// allow to create an archeotype from this
	ImGui::NewLine();
	if (ImGui::Button("Create Archetype"))
		ImGui::OpenPopup("createarchetype");
}

void ImGuiEditor::ShowTileMap(TileMap * tilemap)
{
	if (!ImGui::Begin("Tile Map Editor", &showTileMapWindow))
	{
		ImGui::End();
		return;
	}

	if (!showTileMapWindow)
		editState = EditState::Idle;

	mInput->editorIsActive = (ImGui::IsWindowHovered() || ImGui::IsItemHovered() || ImGui::IsWindowFocused() || ImGui::IsAnyItemHovered() || ImGui::IsAnyItemFocused()) ? true : false;

	if (ImGui::Checkbox("Move##TileMapEditor", &tilemap->isMoving))

		ImGui::NewLine();

	if (tilemap->GetMoving())
	{
		DrawColorBox(glm::vec3(tilemap->GetOwner()->mTransform->position.x, tilemap->GetOwner()->mTransform->position.y, 99.f), 200.f, 200.f, glm::vec4(0.f, 0.f, 1.f, 1.f));

		std::list<GameObject*> temp;
		temp.push_back(tilemap->GetOwner());
		if (MousePressed(SDL_BUTTON_LEFT) && graphics->GetCameraByRenderLayer(pickingLayer)->PickGameObject(glm::vec3(mousePosition.x, mousePosition.y, 1.f), temp, PickerTileMapLogicFunction))
		{
			tilemap->Reload();

			glm::vec2 mouse = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePosition);
			glm::vec2 mousePrev = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePrevPosition);

			glm::vec2 pos = glm::vec2(temp.front()->mTransform->position);
			glm::vec2 mov = glm::vec2(mouse.x - mousePrev.x, mouse.y - mousePrev.y);
			pos += mov;
			temp.front()->mTransform->position.x = pos.x;
			temp.front()->mTransform->position.y = pos.y;
		}
	}

	tilemap->EditWindow();

	ImGui::End();
}

void ImGuiEditor::RenderPicking()
{
	if (editState == EditState::Select && MousePressed(SDL_BUTTON_LEFT))
	{
		// draw the selection box, pretty straight-foward
		selectionBox.midpoint = (selectionBox.sider_0 + selectionBox.sider_1) * 0.5f;
		DrawColorBox(glm::vec3(selectionBox.midpoint, 99.f), (selectionBox.sider_1.x - selectionBox.sider_0.x) * 0.5f, (selectionBox.sider_1.y - selectionBox.sider_0.y) * 0.5f, glm::vec4(0.f, 1.f, 0.f, 1.f));
	}

	for (auto it : gizmos)
		DrawColorBox(glm::vec3(it.position.x, it.position.y, 10000.f), it.scale.x * 0.5f, it.scale.y * 0.5f, glm::vec4(1.f, .7f, 0.f, 1.f));

	if (transformState == TransformState::TranslatePlease)
	{
		for (auto it : arrowsUp)
		{
			DrawArrowCute(glm::vec3(it.position.x, it.position.y, 99.f), it.scale.x, it.scale.y, true, glm::vec4(0.f, 1.f, 0.f, 1.f));
			//DrawColorBox(glm::vec3(it.position.x, it.position.y, 99.f), it.scale.x * 0.5f, it.scale.y * 0.5f, glm::vec4(0.f, 1.f, 0.f, 1.f));
		}
			
		for (auto it : arrowsRight)
		{
			DrawArrowCute(glm::vec3(it.position.x, it.position.y, 99.f), it.scale.x, it.scale.y, false, glm::vec4(1.f, 0.f, 0.f, 1.f));
			//DrawColorBox(glm::vec3(it.position.x, it.position.y, 99.f), it.scale.x * 0.5f, it.scale.y * 0.5f, glm::vec4(1.f, 0.f, 0.f, 1.f));
		}
			
	}

	if (transformState == TransformState::ScalePlease)
	{
		for (auto it : arrowsUp)
		{
			DrawBoxCute(it.position, it.scale.x, it.scale.y, true, glm::vec4(0.f, 1.f, 0.f, 1.f));
			//DrawColorBox(glm::vec3(it.position.x, it.position.y, 99.f), it.scale.x * 0.5f, it.scale.y * 0.5f, glm::vec4(0.f, 1.f, 0.f, 1.f));
		}

		for (auto it : arrowsRight)
		{
			DrawBoxCute(it.position, it.scale.x, it.scale.y, false, glm::vec4(1.f, 0.f, 0.f, 1.f));
			//DrawColorBox(glm::vec3(it.position.x, it.position.y, 99.f), it.scale.x * 0.5f, it.scale.y * 0.5f, glm::vec4(1.f, 0.f, 0.f, 1.f));
		}

	}
	
	if (selectedObj)
		DrawFancySelector(selectedObj, glm::vec4(0.f, 0.f, 1.f, 1.f));

	for (auto it : selectedObjs)
		DrawFancySelector(it, glm::vec4(0.f, 1.f, 0.f, 1.f));
}

// just like zero engine!
void ImGuiEditor::Picking()
{
	static bool gizmoSelected = false;
	static bool upSelected = false;
	static bool rightSelected = false;
	Camera * cam = graphics->GetCameraByRenderLayer(pickingLayer);
	glm::vec2 mouse = cam->ViewportToWorld(mousePosition);
	static int index = 0;
	static int dir0 = 0;
	static bool zChange = false;

	switch (editState)
	{
	case EditState::Select:
	case EditState::Idle:
		if (!KeyPressed(SDL_SCANCODE_LCTRL))
		{
			selectedObjs.clear();
			//selectedObj = NULL;
		}
		if (MouseDown(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
		{
			selectionBox.sider_0 = selectionBox.sider_1 = graphics->GetMainCamera()->ViewportToWorld(mousePosition);
			editState = EditState::Select;
		}
		else if (MousePressed(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
		{
			selectionBox.sider_1 = graphics->GetMainCamera()->ViewportToWorld(mousePosition);
		}
		else if (!MousePressed(SDL_BUTTON_LEFT) && editState == EditState::Select && !cursorIsOnImGui)
		{
			index = 0;

			std::list<GameObject*> picked;
			
			// pick single object
			GameObject * possible = graphics->GetCameraByRenderLayer(pickingLayer)->PickGameObject(glm::vec3(mousePosition.x, mousePosition.y, 1.f), objectManager->aliveObjects, PickerLogicFunction);
			if (possible)
			{
				selectedObj = possible;
				picked.push_back(possible);
			}
			else
			{
				selectedObj = NULL;
				// pick multiple objects
				graphics->GetCameraByRenderLayer(pickingLayer)->PickGameObjects(glm::vec3(mousePosition.x, mousePosition.y, 1.f), objectManager->aliveObjects, picked, PickerALotLogicFunction);
				if (!picked.empty())
					selectedObj = picked.front();
			}

			if (KeyPressed(SDL_SCANCODE_LCTRL) && !picked.empty())
			{
				for (auto it : picked)
				{
					selectedObjs.remove(it);
					selectedObjs.push_back(it);
				}
				editState = EditState::TransformPlease;
			}
			else if (!KeyPressed(SDL_SCANCODE_LCTRL) && !picked.empty())
			{
				selectedObjs = picked;
				editState = EditState::TransformPlease;
			}
			else if (!KeyPressed(SDL_SCANCODE_LCTRL) && picked.empty())
			{
				selectedObjs.clear();
				editState = EditState::Idle;
			}
		}
		break;

	case EditState::TransformPlease:
	{
		if (MouseWheel(dir0) && !KeyPressed(SDL_SCANCODE_LCTRL) && !cursorIsOnImGui)
		{
			std::list<GameObject*> possible;
			graphics->GetCameraByRenderLayer(pickingLayer)->PickGameObjects(glm::vec3(mousePosition.x, mousePosition.y, 1.f), objectManager->aliveObjects, possible, PickerLogicFunction);

			if (!possible.empty())
			{
				auto finder0 = std::find_if(selectedObjs.begin(), selectedObjs.end(), [&](const GameObject * first) { return first == selectedObj; });
				if (finder0 != selectedObjs.end()) selectedObjs.remove(selectedObj);

				index = index + dir0;

				if (index < 0) index = possible.size() - 1;
				if (index > possible.size() - 1) index = 0;

				selectedObj = *std::next(possible.begin(), index);
				selectedObjs.push_back(selectedObj);
				zChange = true;
			}
		}

		if (MouseDown(SDL_BUTTON_LEFT) && zChange && !cursorIsOnImGui)
		{
			zChange = false;
			editState = EditState::TransformPlease;
		}

		gizmos.clear(); arrowsUp.clear(); arrowsRight.clear();
		std::vector<Transform> originTrans;
		for (auto it : selectedObjs)
		{
			gizmos.push_back(Transform(it->mTransform->position, it->mTransform->rotation, it->mTransform->scale * 0.5f));

			glm::vec3 upPos = glm::vec3(it->mTransform->position.x, it->mTransform->position.y + abs(it->mTransform->scale.y * 0.5f), 1.f);
			glm::vec3 rightPos = glm::vec3(it->mTransform->position.x + abs(it->mTransform->scale.x * 0.5f), it->mTransform->position.y, 1.f);

			glm::vec3 upScl = glm::vec3(20.f, abs(it->mTransform->scale.y * 0.5f), 1.f);
			glm::vec3 rightScl = glm::vec3(abs(it->mTransform->scale.x * 0.5f), 20.f, 1.f);

			arrowsUp.push_back(Transform(upPos, it->mTransform->rotation, upScl));
			arrowsRight.push_back(Transform(rightPos, it->mTransform->rotation, rightScl));
		}

		originTrans = gizmos;

		if (MouseDown(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
		{
			// change the selected object between the selected objects
			GameObject * possible = graphics->GetCameraByRenderLayer(pickingLayer)->PickGameObject(glm::vec3(mousePosition.x, mousePosition.y, 1.f), selectedObjs, PickerLogicFunction);
			if (possible)
				selectedObj = possible;

			glm::vec3 temp = glm::vec3(mouse, 1.f);
			upSelected = SelectedHelper(arrowsUp, temp);
			rightSelected = SelectedHelper(arrowsRight, temp);
			gizmoSelected = SelectedHelper(gizmos, temp);

			if (!gizmoSelected && !upSelected && !rightSelected)
			{
				gizmos.clear(); arrowsUp.clear(); arrowsRight.clear();
				editState = EditState::Idle;
				break;
			}
			selectionBox.sider_0 = selectionBox.sider_1 = graphics->GetMainCamera()->ViewportToWorld(mousePosition);

			// ctrl z stuff... (current pos)
			pickingActions.clear();
			for (auto it : selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				pickingActions.push_back(move);
			}
		}

		if (gizmoSelected || upSelected || rightSelected)
		{
			unsigned mode = 2;
			if (upSelected) mode = 0;
			else if (rightSelected) mode = 1;

			switch (transformState)
			{
			case TransformState::TranslatePlease:	TransformSelected(gizmos, originTrans, mode);	break;
			case TransformState::ScalePlease:		ScaleSelected(gizmos, originTrans, mode);		break;

			case TransformState::RotatePlease:
				if (gizmoSelected)
					RotateSelected(gizmos, originTrans); break;
			}

			auto is = gizmos.begin();
			for (auto it = selectedObjs.begin(); it != selectedObjs.end(); it++, is++)
			{
				(*it)->mTransform->position = is->position;
				(*it)->mTransform->scale = is->scale * 2.f;
				(*it)->mTransform->rotation = is->rotation;
			}
		}
		// ctrl z stuff... (destination pos)
		if (MouseUp(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
		{
			for (auto it : selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				pickingActions.insert(pickingActions.begin(), move);
			}
			if (!pickingActions.empty())
				mActions->PushAction(pickingActions);
		}
		break;
	}
	case EditState::TileMapPlease: break;
	}
}

void ImGuiEditor::ModifyObjects()
{
	// delete ALL the selectedObjs
	if (KeyDown(SDL_SCANCODE_DELETE))
	{
		std::vector<Action*> deleteAction;
		for (auto it = selectedObjs.begin(); it != selectedObjs.end(); it++)
		{
			if (*it == NULL)
				continue;

			DestroyAction * del = DBG_NEW DestroyAction(*it);
			deleteAction.push_back(del);
			del->ExecuteAction();
		}
		/*while (selectedObjs.size())
		{
			DestroyAction * del = DBG_NEW DestroyAction(*selectedObjs.begin());
			deleteAction.push_back(del);
			del->ExecuteAction();

			//selectedObjs.pop_front();
		}*/
		
		selectedObj = NULL;
		DumpSelected();

		mActions->PushAction(deleteAction);
	}

	// ctrl c
	if (KeyPressed(SDL_SCANCODE_LCTRL) && KeyDown(SDL_SCANCODE_C))
	{
		copyValues.clear(); copyPositions.clear();

		for (auto it = selectedObjs.begin(); it != selectedObjs.end(); it++)
		{
			Json::Value v = (*it)->GetValue();
			v["name"] = (*it)->GetName();
			(*it)->SaveComponents(v);
			copyValues.push_back(v);
			copyPositions.push_back((*it)->mTransform->position);
		}
	}

	// ctrl v
	if (KeyPressed(SDL_SCANCODE_LCTRL) && KeyDown(SDL_SCANCODE_V))
	{
		selectedObjs.clear();

		std::vector<Action*> createAction;

		unsigned i = 0;
		for (auto it : copyValues)
		{
			//glm::vec3 pos = graphics->GetMainCamera()->position + copyPositions[i];
			glm::vec3 pos = copyPositions[i];

			CreateAction * create = DBG_NEW CreateAction(it, pos, pickingLayer);
			createAction.push_back(create);
			create->ExecuteAction();
			i++;

			selectedObjs.push_back(create->GetActor());
		}
		if (!copyValues.empty())
			mActions->PushAction(createAction);
	}
}

bool SelectedHelper(std::vector<Transform> & tr, glm::vec3 & p)
{
	for (auto it : tr)
	{
		bool selected = StaticPointToStaticRect(p, it.position, fabs(it.scale.x), fabs(it.scale.y));
		if (selected)
			return true;
	}
	return false;
}

void ImGuiEditor::DrawFancySelector(GameObject * obj, glm::vec4 color)
{
	// p0 _    _ p1
	//  |        |
	//
	//  |        |
	// p2 _    _ p3

	glm::vec3 horizontal(fabs(obj->mTransform->scale.x * 0.5f), 0.0f, 0.0f);
	glm::vec3 vertical(0.0f, fabs(obj->mTransform->scale.y * 0.5f), 0.0f);

	glm::vec3 p0 = obj->mTransform->position - horizontal + vertical;
	glm::vec3 p1 = obj->mTransform->position + horizontal + vertical;
	glm::vec3 p2 = obj->mTransform->position - horizontal - vertical;
	glm::vec3 p3 = obj->mTransform->position + horizontal - vertical;

	DrawColorLine(glm::vec3(p0.x, p0.y, 99.f), glm::vec3(p0.x + 30.f, p0.y, 99.f), color);
	DrawColorLine(glm::vec3(p0.x, p0.y, 99.f), glm::vec3(p0.x, p0.y - 30.f, 99.f), color);

	DrawColorLine(glm::vec3(p1.x, p1.y, 99.f), glm::vec3(p1.x - 30.f, p1.y, 99.f), color);
	DrawColorLine(glm::vec3(p1.x, p1.y, 99.f), glm::vec3(p1.x, p1.y - 30.f, 99.f), color);

	DrawColorLine(glm::vec3(p2.x, p2.y, 99.f), glm::vec3(p2.x + 30.f, p2.y, 99.f), color);
	DrawColorLine(glm::vec3(p2.x, p2.y, 99.f), glm::vec3(p2.x, p2.y + 30.f, 99.f), color);

	DrawColorLine(glm::vec3(p3.x, p3.y, 99.f), glm::vec3(p3.x - 30.f, p3.y, 99.f), color);
	DrawColorLine(glm::vec3(p3.x, p3.y, 99.f), glm::vec3(p3.x, p3.y + 30.f, 99.f), color);
}

void ImGuiEditor::DrawArrowCute(glm::vec3 & pos, float x, float y, bool vert, glm::vec4 color)
{
	if (vert)
	{
		glm::vec3 side0(x * 0.5f, -y * 0.2f, 0.0f);
		glm::vec3 side1(-x * 0.5f, -y * 0.2f, 0.0f);

		glm::vec3 vertical(0.0f, fabs(y), 0.0f);

		glm::vec3 p0 = pos + vertical * 0.5f;
		glm::vec3 p1 = pos - vertical * 0.5f;

		DrawColorLine(p1, p0, color);
		DrawColorLine(p0, p0 + side0, color);
		DrawColorLine(p0, p0 + side1, color);
		DrawColorLine(p0 + side0, p0 + side1, color);
	}
	else
	{
		glm::vec3 side0(-x * 0.2f, y * 0.5f, 0.0f);
		glm::vec3 side1(-x * 0.2f, -y * 0.5f, 0.0f);

		glm::vec3 horizontal(fabs(x), 0.0f, 0.0f);

		glm::vec3 p0 = pos + horizontal * 0.5f;
		glm::vec3 p1 = pos - horizontal * 0.5f;

		DrawColorLine(p1, p0, color);
		DrawColorLine(p0, p0 + side0, color);
		DrawColorLine(p0, p0 + side1, color);
		DrawColorLine(p0 + side0, p0 + side1, color);
	}
}

void ImGuiEditor::DrawBoxCute(glm::vec3 & pos, float x, float y, bool vert, glm::vec4 color)
{
	if (vert)
	{
		glm::vec3 side0(x * 0.5f, -y * 0.2f, 0.0f);
		glm::vec3 side1(-x * 0.5f, -y * 0.2f, 0.0f);

		glm::vec3 vertical(0.0f, fabs(y), 0.0f);

		glm::vec3 p0 = pos + vertical * 0.5f;
		glm::vec3 p1 = pos - vertical * 0.5f;

		glm::vec3 temp(p0.x, p0.y - x * 0.5f, p0.z);

		DrawColorLine(p1, p0, color);
		DrawColorBox(temp, x * 0.5f, x * 0.5f, color);
	}
	else
	{
		glm::vec3 side0(-x * 0.2f, y * 0.5f, 0.0f);
		glm::vec3 side1(-x * 0.2f, -y * 0.5f, 0.0f);

		glm::vec3 horizontal(fabs(x), 0.0f, 0.0f);

		glm::vec3 p0 = pos + horizontal * 0.5f;
		glm::vec3 p1 = pos - horizontal * 0.5f;

		glm::vec3 temp(p0.x - y * 0.5f, p0.y, p0.z);

		DrawColorLine(p1, p0, color);
		DrawColorBox(temp, y * 0.5f, y * 0.5f, color);
	}
}

bool ImGuiEditor::ListBox(const char* label, int* currIndex, std::vector<std::string>& values)
{
	if (values.empty()) { return false; }
	return ImGui::ListBox(label, currIndex, [](void* vec, int idx, const char** outText)
	{
		auto& vector = *static_cast<std::vector<std::string>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*outText = vector.at(idx).c_str();
		return true;
	}, static_cast<void*>(&values), values.size());
}

bool ImGuiEditor::ListBoxHeader(const char * label, int * currIndex, std::vector<std::string>& values, const char * search_text)
{
	static ImGuiTextFilter filter;

	filter.Draw(search_text);

	int size = values.size();
	if (!ImGui::ListBoxHeader(label, values.size()))
		return false;

	// Assume all items have even height (= 1 line of text). If you need items of different or variable sizes you can create a custom version of ListBox() in your code without using the clipper.
	bool value_changed = false;
	ImGuiListClipper clipper(values.size(), ImGui::GetTextLineHeightWithSpacing()); // We know exactly our line height here so we pass it as a minor optimization, but generally you don't need to.
	while (clipper.Step())
		for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++)
		{
			const bool item_selected = (i == *currIndex);
			const char* item_text = values[i].c_str();

			ImGui::PushID(i);
			if (filter.PassFilter(item_text))
			{
				if (ImGui::Selectable(item_text, item_selected))
				{
					*currIndex = i;
					value_changed = true;
				}
			}
			if (item_selected)
				ImGui::SetItemDefaultFocus();
			ImGui::PopID();
		}
	ImGui::ListBoxFooter();
	return value_changed;
}

bool ImGuiEditor::InputFloat2(const char * label, glm::vec2 & vector)
{
	float temp[2] = { vector.x, vector.y };
	bool result = ImGui::InputFloat2(label, temp);
	vector.x = temp[0]; vector.y = temp[1];
	return result;
}

bool ImGuiEditor::InputFloat3(const char * label, glm::vec3 & vector)
{
	float temp[3] = { vector.x, vector.y, vector.z };
	bool result = ImGui::InputFloat3(label, temp);
	vector.x = temp[0]; vector.y = temp[1]; vector.z = temp[2];
	return result;
}

void ImGuiEditor::InputInt(const char * label, unsigned & integral)
{
	int temp = integral;
	ImGui::InputInt(label, &temp);
	integral = (temp >= 0) ? temp : 0;
}

void ImGuiEditor::ColorEdit3(const char * label, glm::vec3 & color)
{
	float temp[3] = { color.x, color.y, color.z };
	ImGui::ColorEdit3(label, temp);
	color.x = temp[0]; color.y = temp[1]; color.z = temp[2];
}

void ImGuiEditor::ColorEdit4(const char * label, glm::vec4 & color)
{
	float temp[4] = { color.x, color.y, color.z, color.w };
	ImGui::ColorEdit4(label, temp);
	color.x = temp[0]; color.y = temp[1]; color.z = temp[2]; color.w = temp[3];
}

bool ImGuiEditor::InputText(const char * label, std::string & message)
{
	bool result = false;

	char temp[225];

	message.resize(225);

	unsigned i;
	for (i = 0; i < message.size() - 1; i++) //this is soooooo stupid
		temp[i] = message[i];

	temp[i] = 0;

	result = ImGui::InputText(label, temp, 225);

	int j;
	for (j = 0; temp[j] != 0; j++)
		message[j] = temp[j];

	message.resize(j);

	return result;
}

SelectionBox & ImGuiEditor::GetSBox() { return selectionBox;  }

void ImGuiEditor::DumpSelected() { selectedObjs.clear(); }

void ImGuiEditor::SetSelected(GameObject * obj)
{ 
	selectedObj = obj; 
	if (selectedObj != NULL)
		selectedObjs.push_back(selectedObj);
}

GameObject * ImGuiEditor::GetSelected() { return selectedObj; }

std::string ImGuiEditor::GetPickingLayer() { return pickingLayer; }

bool ImGuiEditor::HoveringImGui() { return cursorIsOnImGui; }


bool PickerLogicFunction(const glm::vec2 & pt, GameObject * obj)
{
	glm::vec3 temp = glm::vec3(pt.x, pt.y, 1.f);

	// we only pick the object if it belongs to the same render layer as the current picking layer
	Renderable * rend = obj->GetComponent<Renderable>();

	return (rend != NULL) ?
		StaticPointToStaticRect(temp, obj->mTransform->position, fabsf(obj->mTransform->scale.x), fabsf(obj->mTransform->scale.y)) && obj->mRenderable->layer == imGuiEditor->GetPickingLayer() :
		StaticPointToStaticRect(temp, obj->mTransform->position, fabsf(obj->mTransform->scale.x), fabsf(obj->mTransform->scale.y));
}

bool PickerALotLogicFunction(const glm::vec2 & pt, GameObject * obj)
{
	glm::vec3 temp = glm::vec3(pt.x, pt.y, 1.f);

	// don't pick objects which are too small
	if (obj->mTransform->scale.x == 0.0f || obj->mTransform->scale.y == 0.0f)
		return false;

	glm::vec3 horizontal(obj->mTransform->scale.x * 0.5f, 0.0f, 0.0f);
	glm::vec3 vertical(0.0f, obj->mTransform->scale.y * 0.5f, 0.0f);

	glm::vec3 p0 = obj->mTransform->position - horizontal + vertical;
	glm::vec3 p1 = obj->mTransform->position + horizontal + vertical;
	glm::vec3 p2 = obj->mTransform->position - horizontal - vertical;
	glm::vec3 p3 = obj->mTransform->position + horizontal - vertical;

	glm::vec2 mid = imGuiEditor->GetSBox().midpoint;
	glm::vec2 s0 = imGuiEditor->GetSBox().sider_0; glm::vec2 s1 = imGuiEditor->GetSBox().sider_1;

	// we only pick the object if it belongs to the same render layer as the current picking layer
	Renderable * rend = obj->GetComponent<Renderable>();

	return (rend != NULL) ?
		//return true if the four vertices of the sprite are inside the selector
		StaticPointToStaticRect(p0, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) &&
		StaticPointToStaticRect(p1, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) &&
		StaticPointToStaticRect(p2, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) &&
		StaticPointToStaticRect(p3, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) && obj->mRenderable->layer == imGuiEditor->GetPickingLayer() :

		StaticPointToStaticRect(p0, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) &&
		StaticPointToStaticRect(p1, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) &&
		StaticPointToStaticRect(p2, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y))) &&
		StaticPointToStaticRect(p3, glm::vec3(mid.x, mid.y, 1.f), fabs((s1.x - s0.x)), fabs((s1.y - s0.y)));
}

bool PickerTileMapLogicFunction(const glm::vec2 & pt, GameObject * obj)
{
	glm::vec3 temp = glm::vec3(pt.x, pt.y, 1.f);

	// we only pick the object if it belongs to the same render layer as the current picking layer
	Renderable * rend = obj->GetComponent<Renderable>();

	return (rend != NULL) ?
		StaticPointToStaticRect(temp, obj->mTransform->position, 400.f, 400.f) && obj->mRenderable->layer == imGuiEditor->GetPickingLayer() :
		StaticPointToStaticRect(temp, obj->mTransform->position, 400.f, 400.f);
}

void ImGuiEditor::TransformSelected(std::vector<Transform> & gz, std::vector<Transform> & tr, unsigned mode)
{
	glm::vec2 mouse = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePosition);
	glm::vec2 mousePrev = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePrevPosition);
	glm::vec2 mov = glm::vec2(mouse.x - mousePrev.x, mouse.y - mousePrev.y);

	if (mode == 0)
		mov.x = 0.f;
	if (mode == 1)
		mov.y = 0.f;

	if (MousePressed(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
	{
		auto is = tr.begin();
		for (auto it = gz.begin(); it != gz.end(); it++, is++)
			it->position = is->position + glm::vec3(mov, 0.f);
	}

	static int dir0 = 0;
	if (MouseWheel(dir0) && KeyPressed(Keyboard::LEFT_SHIFT) && !cursorIsOnImGui && !KeyPressed(Keyboard::LEFFT_CTRL))
	{
		auto is = tr.begin();
		for (auto it = gz.begin(); it != gz.end(); it++, is++)
			it->position.z = is->position.z += dir0;	
	}
}

void ImGuiEditor::ScaleSelected(std::vector<Transform> & gz, std::vector<Transform> & tr, unsigned mode)
{
	glm::vec2 mouse = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePosition);
	glm::vec2 mousePrev = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePrevPosition);
	glm::vec2 mov = glm::vec2(mouse.x - mousePrev.x, mouse.y - mousePrev.y);

	if (MousePressed(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
	{
		auto is = tr.begin();
		for (auto it = gz.begin(); it != gz.end(); it++, is++)
		{
			if (mode == 1)
				it->scale.x = is->scale.x + mov.x;
			else if (mode == 0)
				it->scale.y = is->scale.y + mov.y;
			else
			{
				it->scale.x = is->scale.x * (1.f + mov.x / 200.f);
				it->scale.y = is->scale.y * (1.f + mov.x / 200.f);
			}
		}
	}
}

void ImGuiEditor::RotateSelected(std::vector<Transform> & gz, std::vector<Transform> & tr)
{
	glm::vec2 mouse = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePosition);
	glm::vec2 mousePrev = graphics->GetCameraByRenderLayer(pickingLayer)->ViewportToWorld(mousePrevPosition);
	glm::vec2 mov = glm::vec2(mouse.x - mousePrev.x, mouse.y - mousePrev.y);

	if (MousePressed(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
	{
		auto is = tr.begin();
		for (auto it = gz.begin(); it != gz.end(); it++, is++)
			it->rotation = is->rotation - mov.y * EDIT_ROT_VEL;
	}
}

void ImGuiEditor::PropEditMode()
{
	static int dir0 = 0;
	static int index = 0;
	static int zPlacer = 0;

	std::vector<std::string> jsonNames = graphics->GetFiles("./Data/Props/");

	if (MouseWheel(dir0) && !cursorIsOnImGui && !KeyPressed(Keyboard::LEFFT_CTRL))
	{
		if (KeyPressed(Keyboard::LEFT_SHIFT))
		{
			if (toPlaceObj != NULL)
			{
				zPlacer += dir0;
				toPlaceObj->mTransform->position.z = zPlacer;
			}
		}
		else
		{
			index = index + dir0;

			if (index < 0) index = jsonNames.size() - 1;
			if (index > jsonNames.size() - 1) index = 0;

			float z = GetLastZ();

			scene->DeleteGameObject(toPlaceObj);
			toPlaceObj = scene->CreateGameObject();
			toPlaceObj->LoadJson("./Data/Props/" + jsonNames[index]);
			toPlaceObj->mTransform->position.z = z + 0.1f;
			Renderable * rend = toPlaceObj->GetComponent<Renderable>();
			if (rend)
				rend->layer = pickingLayer;
			toPlaceObj->Initialize();
		}
	}

	if (MouseDown(SDL_BUTTON_LEFT) && !cursorIsOnImGui)
	{
		// toPlaceObj->mTransform->position.x = graphics->GetMainCamera()->ViewportToWorld(mousePosition).x;
		// toPlaceObj->mTransform->position.y = graphics->GetMainCamera()->ViewportToWorld(mousePosition).y;
		// 
		// int fixer = 10;
		// 
		// int fixY = round(toPlaceObj->mTransform->position.y / fixer);
		// int final = fixY * fixer;
		// 
		// toPlaceObj->mTransform->position.y = final;
		// 
		// toPlaceObj = scene->CreateGameObject();
		// toPlaceObj->LoadJson("./Jsons/" + jsonNames[index]);
		// toPlaceObj->Initialize();

		float z = GetLastZ();

		glm::vec3 pos = glm::vec3(graphics->GetMainCamera()->ViewportToWorld(mousePosition).x, graphics->GetMainCamera()->ViewportToWorld(mousePosition).y, 1.f);
		pos.z = z + 0.1f;

		GameObject temp;
		temp.LoadJson("./Data/Props/" + jsonNames[index]);

		Json::Value v = temp.GetValue();

		
		std::vector<Action*> createAction;
		CreateAction * create = DBG_NEW CreateAction(v, pos, pickingLayer);
		createAction.push_back(create);
		create->ExecuteAction();
		mActions->PushAction(createAction);
	}

	if (toPlaceObj != NULL)
	{
		toPlaceObj->mTransform->position.x = graphics->GetMainCamera()->ViewportToWorld(mousePosition).x;
		toPlaceObj->mTransform->position.y = graphics->GetMainCamera()->ViewportToWorld(mousePosition).y;
	}
}

void ImGuiEditor::CreateObject(std::string path)
{
	float z = GetLastZ();

	glm::vec3 pos = glm::vec3(graphics->GetMainCamera()->position);
	pos.z = z + 0.1f;

	GameObject temp;
	temp.LoadJson(path.c_str());

	Json::Value v = temp.GetValue();

	std::vector<Action*> createAction;
	CreateAction * create = DBG_NEW CreateAction(v, pos, pickingLayer);
	createAction.push_back(create);
	create->ExecuteAction();
	mActions->PushAction(createAction);
}

void ImGuiEditor::Packaged(std::string filePath, std::string outPath, int width, int height)
{
	struct TempTexture
	{
		SDL_Surface * surf;
		GLuint hand;
		std::string name;
	};

	Json::Value atlasMeta;
	std::vector<std::string> textureFiles = graphics->GetFiles(filePath);
	std::vector<TempTexture> textures;

	for (auto files : textureFiles)
	{
		SDL_Surface * tex0 = IMG_Load((filePath + files).c_str());

		//std::cout << files << std::endl;

		GLuint handle0;
		glGenTextures(1, &handle0);
		glBindTexture(GL_TEXTURE_2D, handle0);

		int pixelFormat = GL_RGB;

		if (tex0->format->BytesPerPixel == 4)
			pixelFormat = GL_RGBA;

		float color[] = { 1.0f , 0.0f , 1.0f , 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
		glTexImage2D(GL_TEXTURE_2D, 0, pixelFormat, tex0->w, tex0->h, 0, pixelFormat, GL_UNSIGNED_BYTE, tex0->pixels);

		//filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		textures.push_back({ tex0 , handle0 , files });
	}

	// CONTAINER TEXTURE
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	GLuint shelfHandle;

	SDL_Surface * shelfSurf = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, SDL_PIXELFORMAT_RGBA32);

	glGenTextures(1, &shelfHandle);
	glBindTexture(GL_TEXTURE_2D, shelfHandle);

	int pixelFormat = GL_RGB;

	if (shelfSurf->format->BytesPerPixel == 4)
		pixelFormat = GL_RGBA;

	float color[] = { 1.0f , 0.0f , 1.0f , 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
	glTexImage2D(GL_TEXTURE_2D, 0, pixelFormat, shelfSurf->w, shelfSurf->h, 0, pixelFormat, GL_UNSIGNED_BYTE, shelfSurf->pixels);

	//filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	// ORDER THE TEXTURES FROM BIGGER TO SMALLER
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	std::sort(textures.begin(), textures.end(), [](TempTexture first, TempTexture second) { return first.surf->w > second.surf->w; });
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	// FIT THE TEXTURES IN THE CONTAINER
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	Shelf * container = DBG_NEW Shelf;
	container->shelf = Box(0, width, height, 0);

	atlasMeta["name"] = outPath;
	atlasMeta["width"] = width;
	atlasMeta["height"] = height;

	for (auto tex = textures.begin(); tex != textures.end(); tex++)
	{
		Shelf * inserter = Insert(tex->surf, container);
		std::string textureName = tex->name;

		if (inserter == NULL)
		{
			//
			//<< "ATLAS IGNORE" << std::endl;
			continue;
		}

		SaveAtlas(atlasMeta, textureName, glm::vec2(inserter->shelf.left / width, inserter->shelf.top / height), glm::vec2(inserter->shelf.width / width, inserter->shelf.height / height));

		Json::StreamWriterBuilder builder;
		Json::StreamWriter * writer = builder.newStreamWriter();
		std::ofstream jsonFileWrite("./Data/Result/" + outPath + ".json");
		writer->write(atlasMeta, &jsonFileWrite);
		jsonFileWrite.close();


		inserter->empty = false;

		glBindTexture(GL_TEXTURE_2D_ARRAY, shelfHandle);
		glCopyImageSubData(tex->hand, GL_TEXTURE_2D, 0, 0, 0, 0, shelfHandle, GL_TEXTURE_2D, 0, inserter->shelf.left, inserter->shelf.top, 0, inserter->shelf.width, inserter->shelf.height, 1);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////

	char * pixel_data = DBG_NEW char[shelfSurf->w * shelfSurf->h * 4];

	glBindTexture(GL_TEXTURE_2D_ARRAY, shelfHandle);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixel_data);

	SDL_Surface * result = SDL_CreateRGBSurfaceWithFormatFrom((void*)pixel_data, shelfSurf->w, shelfSurf->h, 4 * shelfSurf->w, shelfSurf->pitch, SDL_PIXELFORMAT_RGBA32);

	IMG_SavePNG(result, ("./Data/Result/" + outPath + ".png").c_str());
	SDL_FreeSurface(shelfSurf);

	for (auto tex = textures.begin(); tex != textures.end(); tex++)
		SDL_FreeSurface(tex->surf);
}

void ImGuiEditor::SaveAtlas(Json::Value & val, std::string index, glm::vec2 & position, glm::vec2 & scale)
{
	val[index]["position"]["x"] = position.x;
	val[index]["position"]["y"] = position.y;
	val[index]["width"] = scale.x;
	val[index]["height"] = scale.y;
}

void ImGuiEditor::AutomaticPropGenerator(std::string jsonPath)
{
	AtlasData meta;
	meta.Loader(jsonPath);

	for (auto it : meta.atlasObj)
	{
		GameObject * temp = scene->CreateGameObject();
		temp->LoadJson("./Data/Jsons/DefaultObject.json");


		Json::Value val = temp->GetValue();

		val["Renderable"]["shader"] = "spriteSheet";
		val["Renderable"]["texture"] = meta.GetName() + ".png";
		val["Renderable"]["atlas_index"] = it.first;
		val["name"] = it.first;

		val["Transform"]["scale"]["x"] = (it.second.scale.x * meta.width) * 0.5f;
		val["Transform"]["scale"]["y"] = (it.second.scale.y * meta.height) * 0.5f;

		Json::StreamWriterBuilder builder;
		Json::StreamWriter * writer = builder.newStreamWriter();

		std::ofstream jsonFileWrite("./Data/Result/" + it.first + ".json");

		writer->write(val, &jsonFileWrite);

		jsonFileWrite.close();

		scene->DeleteGameObject(temp);
	}
}

ImGuiEditor * ImGuiEditor::instance = 0;