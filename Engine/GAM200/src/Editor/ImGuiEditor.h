#ifndef IMGUI_EDITOR_H
#define IMGUI_EDITOR_H

#include "../Component/Entity.h"
#include <SDL2/SDL.h>

#include "imGui/imgui.h"
#include "imGui/imgui_impl_sdl_gl3.h"

#include "../Gfx/Comp/Transform.h"

#define EDIT_ROT_VEL 0.05f

#pragma warning(disable:4996)

class GameObject;
class TileMap;
class Action;
class TileMap;

struct SelectionBox
{
	glm::vec2 sider_0;
	glm::vec2 sider_1;
	glm::vec2 midpoint;
};

class ImGuiEditor : public Entity
{
public:
	enum class EditState { Idle, Select, TransformPlease, TileMapPlease } editState;
	enum class TransformState {  TranslatePlease, ScalePlease, RotatePlease } transformState;

private:
	static ImGuiEditor * instance;
	ImGuiEditor();

public:
	std::vector<std::string> textureNames;
	std::vector<std::string> spriteNames;
	std::vector<std::string> componentNames;
	std::vector<std::string> animationNames;
	std::vector<std::string> shaderNames;
	std::vector<std::string> fontNames;
	std::vector<std::string> layerNames;

	std::vector<std::string> enemytypeNames;

	virtual ~ImGuiEditor();
	static ImGuiEditor * getInstance();

	virtual void InitImGui(SDL_Window * handle);
	virtual void Render();
	virtual void EditorWindow(SDL_Window * handle);
	virtual void Terminate();

	void ShowMenuSettings();
	void ShowHelp();

	float GetLastZ();

	void ShowObjectComponents(GameObject * obj);
	void ShowTileMap(TileMap * tilemap);

	void RenderPicking();
	void Picking();
	void ModifyObjects();

	void DrawFancySelector(GameObject * obj, glm::vec4 color = glm::vec4(0.f, 1.f, 0.f, 1.f));
	void DrawArrowCute(glm::vec3 & pos, float x, float y, bool vertical = true, glm::vec4 color = glm::vec4(0.f, 1.f, 0.f, 1.f));
	void DrawBoxCute(glm::vec3 & pos, float x, float y, bool vertical = true, glm::vec4 color = glm::vec4(0.f, 1.f, 0.f, 1.f));

	static bool ListBox(const char * label, int * currIndex, std::vector<std::string>& values);
	static bool ListBoxHeader(const char * label, int * currIndex, std::vector<std::string>& values, const char * search_text);
	static bool InputFloat2(const char * label, glm::vec2 & vector);
	static bool InputFloat3(const char * label, glm::vec3 & vector);
	static void InputInt(const char * label, unsigned & integral);
	static void ColorEdit3(const char * label, glm::vec3 & color);
	static void ColorEdit4(const char * label, glm::vec4 & color);
	static bool InputText(const char * label, std::string & message);

	void TransformSelected(std::vector<Transform> & gz, std::vector<Transform> & tr, unsigned mode);
	void ScaleSelected(std::vector<Transform> & gz, std::vector<Transform> & tr, unsigned mode);
	void RotateSelected(std::vector<Transform> & gz, std::vector<Transform> & tr);
	
	void PropEditMode();

	void CreateObject(std::string path);

	SelectionBox & GetSBox();
	void DumpSelected();
	void SetSelected(GameObject * obj);
	GameObject * GetSelected();
	std::string GetPickingLayer();
	bool HoveringImGui();

	void Packaged(std::string filePath, std::string outPath, int width = 1024, int height = 1024);
	void SaveAtlas(Json::Value & val, std::string index, glm::vec2 & position, glm::vec2 & scale);

	void AutomaticPropGenerator(std::string jsonPath);

	bool showTileMapWindow;
	TileMap * selectedTileMap;

	std::list<GameObject*> selectedObjs; // bunch of objects selected with selectionBox or ctrl

private:
	std::string pickingLayer;

	SelectionBox selectionBox;

	GameObject * selectedObj;			 //selected object in the editor
	bool cursorIsOnImGui;
	bool propEditMode;
	GameObject * toPlaceObj;
};

#define imGuiEditor  ImGuiEditor::getInstance()

#endif