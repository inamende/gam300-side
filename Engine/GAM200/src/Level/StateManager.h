#ifndef STATE_MANAGER_H
#define STATE_MANAGER_H

#include "../EngineCommon.h"

namespace StateManager
{
	std::vector<std::string>  GetAllLevels(std::string path);
}

#endif
