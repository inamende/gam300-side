#include "Scene.h"

#include "../Gfx/Reso/Camera.h"
#include "../Gfx/Reso/Texture.h"
#include "../Gfx/Gfx.h"
#include "../Gfx/Window.h"

#include "../Editor/ImGuiEditor.h"

#include "../Level/StateManager.h"

#include "../ObjectManager/ObjectManager.h"
#include "../Input/Input.h"
#include "../Factory/Factory.h"

#include "../Physics/Comp/BoxCollider.h"
#include "../Physics/Collisions.h"
#include "../Physics/Physics.h"
#include "../Physics/ContactCollisions.h"

#include "../Audio/Audio.h"

#include "../Actions/Actions.h"

#include "../Gfx/Comp/Renderable.h"
#include "../Gfx/Reso/Texture.h"

#include "../Menu/Menu.h"
#include "PauseMenu.h"

#include "../Audio/SoundEmitter.h"
#include "../Audio/MusicEmitter.h"

#include "../Utils/Utilities.h"

Json::Value commonValue;

bool PointObjAABB(const glm::vec2 & pt, GameObject * obj);

MusicEmitter *bg_musicc = nullptr;

void free_all_events()
{
	auto obj_mngr = objectManager;

	Physics::getInstance()->prev_contacts.clear();

	// delete all the events
	for (auto & i : obj_mngr->deadObjects)
		i->local_handler.Terminate();

	for (auto & i : obj_mngr->aliveObjects)
		i->local_handler.Terminate();

	global_handler.Terminate();
}

Scene * Scene::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW Scene();

	return instance;
}

Scene::Scene() : isPaused(false), isEditing(true), restartSignal(false), fadeoutSignal(false), dt(0.f)
{
	cursor = nullptr;
	mainCam = DBG_NEW Camera();
	cam0 = DBG_NEW Camera();
	cam1 = DBG_NEW Camera();
	cam2 = DBG_NEW Camera();
	dynamicCam = DBG_NEW Camera();
	postDefaultCam = DBG_NEW Camera();
	frontCam = DBG_NEW Camera();
	backgroundCam = DBG_NEW Camera();
	backCam = DBG_NEW Camera();

	levelNames = StateManager::GetAllLevels("./Data/Levels");
	currLevel = levelNames[0];
}

Scene::~Scene() {}

void Scene::Initialize()
{
	Load();
	prevLevel = currLevel;

	pause_boi = DBG_NEW pause_menu("Pause", pause_position, std::vector<std::pair<std::string, vec2> > {
		{ "ResumeButton", vec2(0, 90)},
		{ "RestartButton", vec2(0, 10) },
		{ "OptionsButton", vec2(0, -60) },
		{ "BackToMainButton", vec2(0, -140) }
	});

	options_boi = DBG_NEW pause_menu("Options", options_position, std::vector<std::pair<std::string, vec2> > {
		{ "GraphicsButton", vec2(0, 90)},
		{ "SoundOptionsButton", vec2(0, 10) },
		{ "BackToPauseButton", vec2(0, -100) }
	});

	graphics_boi = DBG_NEW pause_menu("Graphics", graphicc_options_position, std::vector<std::pair<std::string, vec2> > {
		{ "FullScreenButton", vec2(0, 90)},
		{ "VSyncButton", vec2(0, 10) },
		{ "ResolutionButton", vec2(0, -60) },
		{ "BackToGraphiccButton", vec2(0, -140) }
	});

	sound_boi = DBG_NEW pause_menu("Sound", sound_options_position, std::vector<std::pair<std::string, vec2> > {
		{ "MasterVolumeButton", vec2(0, 10) },
		{ "BackToSoundButton", vec2(0, -140) }
	});

	main_sure_boi = DBG_NEW pause_menu("Are You Sure?", main_sure_position, std::vector<std::pair<std::string, vec2> > {
		{ "YesToMainButton", vec2(0, 10) },
		{ "NoToPauseButton", vec2(0, -100) }
	});

	restart_sure_boi = DBG_NEW pause_menu("Are You Sure?", restart_sure_position, std::vector<std::pair<std::string, vec2> > {
		{ "YesToRestartButton", vec2(0, 10) },
		{ "NoToPauseFromRestartButton", vec2(0, -100) }
	});
	
	//CAMERAS 
	backgroundCam->SetName("BackgroundCam");
	backCam->SetName("BackCam");
	mainCam->SetName("MainCamera");
	postDefaultCam->SetName("PostDefaultCam");
	dynamicCam->SetName("DynamicCam");
	cam0->SetName("cam0");
	cam1->SetName("cam1");
	cam2->SetName("cam2");
	
	graphics->SetMainCamera(mainCam);

	graphics->AddRenderLayer("background");
	graphics->AddRenderLayer("back");
	graphics->AddRenderLayer("default");
	graphics->AddRenderLayer("dynamic");
	graphics->AddRenderLayer("postdefault");
	graphics->AddRenderLayer("front");
	graphics->AddRenderLayer("UI");
	graphics->AddRenderLayer("PostUI");
	graphics->AddRenderLayer("PostPostUI");

	graphics->AddCamera(backgroundCam, "background");
	graphics->AddCamera(backCam, "back");
	graphics->AddCamera(mainCam, "default");
	graphics->AddCamera(dynamicCam, "dynamic");
	graphics->AddCamera(postDefaultCam, "postdefault");
	graphics->AddCamera(frontCam, "front");
	graphics->AddCamera(cam0, "UI");
	graphics->AddCamera(cam1, "PostUI");
	graphics->AddCamera(cam2, "PostPostUI");

	//OBJECTS
	objectManager->LoadObjectsFromFile("./Data/Levels/" + currLevel);
	objectManager->Initialize();
	cam0->position.x = 0.f;
	cam0->position.y = 0.f;
	cam0->view.x = cam0->position.x;
	cam0->view.y = cam0->position.y;

	create_sound_stuff();
}

void Scene::Update()
{
	// mark the (0,0)
	DrawFancyCursor(glm::vec3(0.f, 0.f, 98.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	if (currLevel != prevLevel) {
		imGuiEditor->DumpSelected();
		imGuiEditor->SetSelected(NULL);

		// save the level when we exit it
		if(objectManager->AutosaveEnabled())
			if (isEditing)
				objectManager->SaveObjectsToFile("./Data/Levels/" + prevLevel);

		free_all_events();
		objectManager->UnloadObjects();
		prevLevel = currLevel;
		objectManager->LoadObjectsFromFile("./Data/Levels/" + currLevel);
		objectManager->Initialize();
		fadeoutSignal = true;
		graphics->global_ambient_lighting = vec4(1.0);

		if (valid_level())
		{
			pause_boi->initialize();
			options_boi->initialize();
			graphics_boi->initialize();
			sound_boi->initialize();
			main_sure_boi->initialize();
			restart_sure_boi->initialize();
		}
		else {
			mainCam->position.x = 0.f;
			mainCam->position.y = 0.f;
			mainCam->view.x = cam0->position.x;
			mainCam->view.y = cam0->position.y;
		}
		create_sound_stuff();
		cursor = create_cursor();

		GameObject *obj = scene->FindObject("Strawss");
		if (obj) {
			for (auto comp : obj->components) {
				if (comp->GetName() == "MusicEmitter") {
					bg_musicc = dynamic_cast<MusicEmitter*>(comp);
				}
			}
		}
	}
	if (!fadeoutSignal) {
		dt = 0;
	}
	else {
		dt += ImGui::GetIO().DeltaTime;
		fadeoutSignal = !graphics->FadeOut(0, dt);
	}

#ifdef EDITOR
	mWindow->window_state = SDL_GetWindowFlags(mWindow->windowHandle);
	if (mWindow->window_is_missing() && mWindow->prev_window_state != mWindow->window_state) {
		for (auto obj : objectManager->aliveObjects) {
			if (!obj->alive || !obj->active) continue;
			for (auto comp : obj->components) {
				if (!comp->active) continue;
				if (comp->GetName() == "MusicEmitter") {
					MusicEmitter *e = dynamic_cast<MusicEmitter*>(comp);
					if (e)
						e->StopVoice(true);
				}
			}
		}
		/*if (!isPaused && valid_level()) {
			isPaused = true;
			pause_logic();
		}*/
	}

	if ((KeyDown(SDL_SCANCODE_ESCAPE) || ButtonDown(SDL_CONTROLLER_BUTTON_START)) && valid_level()) {
		isPaused = !isPaused;
		pause_logic();
	}
#endif
#ifndef EDITOR
	mWindow->window_state = SDL_GetWindowFlags(mWindow->windowHandle);
	if (mWindow->window_is_missing() && mWindow->prev_window_state != mWindow->window_state) {
		for (auto obj : objectManager->aliveObjects) {
			if (!obj->alive || !obj->active) continue;
			for (auto comp : obj->components) {
				if (!comp->active) continue;
				if (comp->GetName() == "MusicEmitter") {
					bg_musicc = dynamic_cast<MusicEmitter*>(comp);
					if (bg_musicc)
						bg_musicc->StopVoice(true);
				}
			}
		}
		if (!isPaused && valid_level()) {
			isPaused = true;
			pause_logic();
		}
	}
	if ((KeyDown(SDL_SCANCODE_ESCAPE) || ButtonDown(SDL_CONTROLLER_BUTTON_START)) && valid_level()) {
		isPaused = !isPaused;
		pause_logic();
	}
#endif
	/*if (KeyDown(Keyboard::F5))
		ChangeEditMode();*/
		
	/*if (!isEditing && KeyPressed(SDL_SCANCODE_LCTRL, false) && KeyDown(Keyboard::R, false))
		Restart();*/

	/*if (KeyPressed(SDL_SCANCODE_LCTRL, false) && KeyDown(Keyboard::F, false))
		mWindow->FullScreen(!mWindow->GetFullScreen());*/

	/*if (KeyPressed(SDL_SCANCODE_LCTRL, false) && KeyDown(Keyboard::G, false))
	{
		mWindow->UpdateResolution();
		mWindow->SetResolution();
	}*/
#ifdef EDITOR
	if (KeyPressed(SDL_SCANCODE_LCTRL, false) && KeyDown(Keyboard::S, false))
	{
		std::cout << "saving " << currLevel << "..." << " don't remove the memory card" << std::endl;
		objectManager->SaveObjectsToFile("./Data/Levels/" + currLevel);
		std::cout << "saving completed" << std::endl;
	}

	// will this work? who knows!
	if (KeyPressed(SDL_SCANCODE_LCTRL, false) && KeyDown(Keyboard::Z, false))
		mActions->Undo();
		
	if (KeyPressed(SDL_SCANCODE_LCTRL, false) && KeyDown(Keyboard::Y, false))
		mActions->Redo();

	if (isEditing) {
		mainCam->Update();
		frontCam->Mimic(mainCam);
		backCam->Mimic(mainCam);

		objectManager->FreeDeadObjects();
		objectManager->AutoSave(5.0f); //save every 5 sec
	}
	else {
		if (!isPaused) {
			objectManager->Update();
		}
		else {
			// ignore this please uwu
			update_pause_stuff();
			objectManager->FreeDeadObjects();
			for (auto obj : objectManager->aliveObjects)
				if (obj->alive && obj->active)
					for (auto comp : obj->components)
						if (comp->active) {
							if (comp->GetName() == "MusicEmitter") comp->Update();
							else if (comp->GetName() == "SoundEmitter") {
								SoundEmitter *e = dynamic_cast<SoundEmitter*>(comp);
								if (e)
									if (e->IsInit)
										e->StopVoice();
							}
						}
			if (cursor) cursor->Update();
		}
	}

#endif
#ifndef EDITOR
	static float dt_pause = 0.f;
	if (!isPaused) {
		objectManager->Update();
		dt_pause = 0.f;
	}
	else {
		// ignore this please uwu
		update_pause_stuff();
		objectManager->FreeDeadObjects();
		if (cursor) cursor->Update();
		for (auto obj : objectManager->aliveObjects)
			if (obj->alive && obj->active)
				for (auto comp : obj->components)
					if (comp->active) {
						if (comp->GetName() == "MusicEmitter") {
							bg_musicc = dynamic_cast<MusicEmitter*>(comp);
							comp->Update();
						}
						if (comp->GetName() == "SoundEmitter") {
							SoundEmitter *e = dynamic_cast<SoundEmitter*>(comp);
							if (e)
								if (e->IsInit)
									e->StopVoice();
						}
					}


	}
#endif
	
	cam0->width = mainCam->width;
	cam0->height = mainCam->height;

	cam2->width = mainCam->width;
	cam2->height = mainCam->height;

	dynamicCam->Mimic(mainCam);
	postDefaultCam->Mimic(mainCam);
	cam1->Mimic(cam0);

	if (restartSignal) {
		restartSignal = false;
		Restart();
	}

	if (scene->prevLevel != "intro.json") mWindow->prev_window_state = mWindow->window_state;
}

void Scene::Terminate()
{
	auto obj_mngr = objectManager;

	free_all_events();

	Save();

	gAudioMgr.StopAll();
	obj_mngr->Terminate();
	delete pause_boi;
	delete options_boi;
	delete graphics_boi;
	delete sound_boi;
	delete main_sure_boi;
	delete restart_sure_boi;
	delete mainCam;
	delete cam0;
	delete cam1;
	delete cam2;
	delete dynamicCam;
	delete frontCam;
	delete instance;
}

void Scene::Load()
{
	std::ifstream jsonFile(editor_loader);
	Json::Reader reader;
	bool ok = reader.parse(jsonFile, commonValue);
	//if (!ok)
	//	std::cout << "ERROR READING JSON" << std::endl;
	
	mainCam->Load(commonValue["main_cam"]);

	//isPaused = commonValue.get("pause", false).asBool();

	std::string tempCurr = commonValue.get("current_level", levelNames[0]).asString();
	// as the level may have been deleted, check if it is between the new levels
	auto find = std::find_if(levelNames.begin(), levelNames.end(), [&](std::string str) { return str == tempCurr; });
	currLevel = (find != levelNames.end()) ? tempCurr : levelNames[0];

	jsonFile.close();
}

void Scene::Save()
{
#ifdef EDITOR
	mainCam->Save(commonValue["main_cam"]);
	cam0->Save(commonValue["cam0"]);

	//commonValue["pause"] = isPaused;

	commonValue["current_level"] = currLevel;

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();
	std::ofstream jsonFileWrite(editor_loader);
	writer->write(commonValue, &jsonFileWrite);
	jsonFileWrite.close();
#endif
}

void Scene::WriteToImguiWindow()
{
	static int selected_level = 0;

	if (ImGui::CollapsingHeader("Scene"))
	{
		if (ImGui::Button("Reset Camera Rotation"))
			mainCam->rotation = 0.f;

		if (ImGui::Button("Reset Camera Position"))
		{
			mainCam->position = glm::vec3(0.f, 0.f, 1.f);
			mainCam->view = glm::vec3();
		}

		glm::vec2 mouse = mainCam->ViewportToWorld(mousePosition);

		ImGui::Text("Cursor World Position : %.2f , %.2f", mouse.x, mouse.y);

		ImGui::Text("Main Canera Position  : %.2f , %.2f , %.2f", mainCam->position.x, mainCam->position.y, mainCam->position.z);

		if (ImGui::Button("Levels##Scene"))
			ImGui::OpenPopup("levels##Scene");

		ImGui::SameLine();
		ImGui::Text(currLevel.c_str());

		ImGui::NewLine();

		if (ImGui::Button("Create Level"))
			ImGui::OpenPopup("createlevel");
	}

	if (ImGui::BeginPopup("levels##Scene"))
	{
		ImGui::Text("Levels");
		ImGui::Separator();

		for (unsigned i = 0; i < levelNames.size(); i++)
			if (ImGui::Selectable(levelNames[i].c_str()))
			{
				selected_level = i;
				currLevel = levelNames[selected_level];
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("createlevel"))
	{
		static std::string levelName = "DefaultLevel";
		ImGuiEditor::InputText("Level Name", levelName);

		if (ImGui::Button("Create Level"))
		{
			std::string extensionName = levelName + ".json";

			auto find = std::find_if(levelNames.begin(), levelNames.end(), [&](std::string n) { return n == extensionName; });

			if (find == levelNames.end())
			{
				std::ofstream levelFile("./Data/Levels/" + extensionName);

				if (levelFile.is_open())
					levelNames.push_back(extensionName);

				levelFile.close();
			}
		}

		ImGui::EndPopup();
	}
}

void Scene::Restart()
{
	graphics->global_ambient_lighting = vec4(1.0);
	free_all_events();
	imGuiEditor->DumpSelected();
	imGuiEditor->SetSelected(NULL);

	objectManager->UnloadObjects();
	currLevel = prevLevel;
	objectManager->LoadObjectsFromFile("./Data/Levels/" + currLevel);
	objectManager->Initialize();
	fadeoutSignal = true;
	if (valid_level())
	{
		pause_boi->initialize();
		options_boi->initialize();
		graphics_boi->initialize();
		sound_boi->initialize();
		main_sure_boi->initialize();
		restart_sure_boi->initialize();
	}
	else
	{
		mainCam->position = glm::vec3(0.f, 0.f, 1.f);
		mainCam->view = glm::vec3();
	}
	create_sound_stuff();
	cursor = create_cursor();

	GameObject *obj = scene->FindObject("Strawss");
	if (obj) {
		for (auto comp : obj->components) {
			if (comp->GetName() == "MusicEmitter") {
				bg_musicc = dynamic_cast<MusicEmitter*>(comp);
			}
		}
	}
}

void Scene::ChangeEditMode()
{
	if (isEditing)
	{
		isPaused = false;
		objectManager->SaveObjectsToFile("./Data/Levels/" + currLevel);
	}
		
	isEditing = !isEditing;
	scene->Restart();
}


GameObject * Scene::CreateGameObject(const char * name)
{
	GameObject * newGameObject = mFactory->Create<GameObject>();
	objectManager->AddGameObject(newGameObject, name);
	return newGameObject;
}

void Scene::DeleteGameObject(GameObject * deleteMe)
{
#ifndef EDITOR
	deleteMe->alive = false;
#endif
#ifdef EDITOR
	if (isEditing) {
		objectManager->RemoveGameObject(deleteMe);
	}
	else {
		deleteMe->alive = false;
	}
#endif
}

GameObject * Scene::FindObject(std::string name)
{
	return objectManager->FindObjectByName(name);
}

void Scene::DrawFancyCursor(glm::vec3 position, glm::vec4 color)
{
	DrawColorLine(glm::vec3(position.x - 5.0f, position.y, position.z), glm::vec3(position.x - 20.0f, position.y, position.z), color);
	DrawColorLine(glm::vec3(position.x + 5.0f, position.y, position.z), glm::vec3(position.x + 20.0f, position.y, position.z), color);
	DrawColorLine(glm::vec3(position.x, position.y - 5.0f, position.z), glm::vec3(position.x, position.y - 20.0f, position.z), color);
	DrawColorLine(glm::vec3(position.x, position.y + 5.0f, position.z), glm::vec3(position.x, position.y + 20.0f, position.z), color);
}

void Scene::update_pause_stuff() {
	// please ignore this uwu
	if (vec2(cam0->position) == pause_position) {
		pause_boi->activate(true);
		options_boi->activate(false);
		graphics_boi->activate(false);
		sound_boi->activate(false);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(false);
	}
	else if (vec2(cam0->position) == options_position) {
		pause_boi->activate(false);
		options_boi->activate(true);
		graphics_boi->activate(false);
		sound_boi->activate(false);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(false);
	}
	else if (vec2(cam0->position) == graphicc_options_position) {
		pause_boi->activate(false);
		options_boi->activate(false);
		graphics_boi->activate(true);
		sound_boi->activate(false);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(false);
	}
	else if (vec2(cam0->position) == sound_options_position) {
		pause_boi->activate(false);
		options_boi->activate(false);
		graphics_boi->activate(false);
		sound_boi->activate(true);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(false);
	}
	else if (vec2(cam0->position) == main_sure_position) {
		pause_boi->activate(false);
		options_boi->activate(false);
		graphics_boi->activate(false);
		sound_boi->activate(false);
		main_sure_boi->activate(true);
		restart_sure_boi->activate(false);
	}
	else if (vec2(cam0->position) == restart_sure_position) {
		pause_boi->activate(false);
		options_boi->activate(false);
		graphics_boi->activate(false);
		sound_boi->activate(false);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(true);
	}

	pause_boi->update();
	options_boi->update();
	graphics_boi->update();
	sound_boi->update();
	main_sure_boi->update();
	restart_sure_boi->update();
}

void Scene::create_sound_stuff() {
	GameObject *new_obj = scene->CreateGameObject();
	new_obj->LoadJson("./Data/Jsons/GeneralPurposeSoundEmitter.json");
	new_obj->save = false;
	new_obj->Initialize();
}

GameObject *Scene::create_cursor() {
	GameObject *new_obj = scene->CreateGameObject();
	new_obj->LoadJson("./Data/Jsons/cursor.json");
	new_obj->save = false;
	new_obj->Initialize();
	return new_obj;
}

bool Scene::valid_level() {
	return currLevel != "MainMenu.json" && currLevel != "Options.json" && currLevel != "intro.json" && currLevel != "Credits.json" && currLevel != "AreYouSure.json" &&
	currLevel != "HowToPlay.json" && currLevel != "TheEnd.json";
}

void Scene::pause_logic() {
	std::cout << "okkk" << std::endl;
	float y = 0.f;
	if (isPaused) {
		bg_musicc->addDSP();
		y = pause_position.y;
		pause_boi->activate(true);
		options_boi->activate(false);
		graphics_boi->activate(false);
		sound_boi->activate(false);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(false);
	}
	else {
		bg_musicc->removeDSP();
		y = 0.f;
		pause_boi->activate(false);
		options_boi->activate(false);
		graphics_boi->activate(false);
		sound_boi->activate(false);
		main_sure_boi->activate(false);
		restart_sure_boi->activate(false);
	}
	cam0->position.x = 0.f;
	cam0->position.y = y;
	cam0->view.x = cam0->position.x;
	cam0->view.y = cam0->position.y;
}

Scene * Scene::instance = 0;

bool PointObjAABB(const glm::vec2 & pt, GameObject * obj)
{
	glm::vec3 temp = glm::vec3(pt.x, pt.y, 1.f);
	return StaticPointToStaticRect(temp, obj->mTransform->position, fabsf(obj->mTransform->scale.x), fabsf(obj->mTransform->scale.y));
}