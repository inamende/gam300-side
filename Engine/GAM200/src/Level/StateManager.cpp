#include "StateManager.h"
#include <windows.h>



std::vector<std::string> StateManager::GetAllLevels(std::string path)
{
	std::string file = path + "//*";

	WIN32_FIND_DATA data;
	HANDLE handle;

	std::vector<std::string> files;

	std::wstring temp = std::wstring(file.begin(), file.end());
	LPCWSTR lpcFile = temp.c_str();

	if ((handle = FindFirstFile(lpcFile, &data)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (data.cFileName[0] != '.') //ignore parent directories
			{
				std::wstring tempstr = data.cFileName;
				files.emplace_back(std::string(tempstr.begin(), tempstr.end()));
			}

		} while (FindNextFile(handle, &data) != 0);
		FindClose(handle);
	}
	return files;
}
