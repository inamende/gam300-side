#ifndef SCENE_H
#define SCENE_H

#include "../EngineCommon.h"
#include <glm/glm.hpp>

class Camera;
class GameObject;
class pause_menu;

class Scene 
{
	friend class LevelTransitioner;
private:

	static Scene * instance;
	Scene();

public: 

	virtual ~Scene();

	static Scene * getInstance();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void Load();
	virtual void Save();

	void WriteToImguiWindow();

	void Restart();
	void ChangeEditMode();

	GameObject * CreateGameObject(const char * name = NULL);
	void DeleteGameObject(GameObject * deleteMe);
	GameObject * FindObject(std::string name);
	void DrawFancyCursor(glm::vec3 position, glm::vec4 color = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f)); //nice for testing space conversions
	void pause_logic();
	bool valid_level();

	std::string currLevel;
	bool isPaused;
	bool isEditing;
	bool restartSignal;
	bool fadeoutSignal;
	float dt;

	vec2 pause_position = vec2(0.f, 50000.f);
	vec2 options_position = vec2(50000.f, 50000.f);
	vec2 graphicc_options_position = vec2(50000.f, 0.f);
	vec2 sound_options_position = vec2(-50000.f, -50000.f);
	vec2 main_sure_position = vec2(0.f, -50000.f);
	vec2 restart_sure_position = vec2(-50000.f, 0.f);

	GameObject *cursor;
private:
	void update_pause_stuff();
	void create_sound_stuff();
	GameObject *create_cursor();
	pause_menu* pause_boi;
	pause_menu* options_boi;
	pause_menu* graphics_boi;
	pause_menu* sound_boi;
	pause_menu* main_sure_boi;
	pause_menu* restart_sure_boi;

	Camera * backgroundCam;
	Camera * backCam;
	Camera * mainCam;
	Camera * postDefaultCam;
	Camera * dynamicCam;
	Camera * frontCam;
	Camera * cam0;
	Camera * cam1;
	Camera * cam2;

	std::vector<std::string> levelNames;
	std::string prevLevel;
};

#define scene Scene::getInstance()

#endif