#ifndef PAUSE_MENU_H
#define PAUSE_MENU_H

#include "../EngineCommon.h"

class GameObject;

class pause_menu {
public:
	pause_menu() = default;
	pause_menu(const char*, vec2, std::vector<std::pair<std::string, vec2>>);
	void initialize();
	void update();
	void activate(bool);
//private:
	std::string text;
	bool active;
	vec2 position;
	GameObject *title;
	std::array<GameObject*, 3> decorations;
	std::vector<GameObject*> buttons;
	GameObject *menu = nullptr;
	GameObject *background = nullptr;
	std::vector<std::pair<std::string, vec2>> copy;
};

#endif