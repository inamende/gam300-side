#include "PauseMenu.h"
#include "../ObjectManager/GameObject.h"
#include "../Gfx/Comp/Transform.h"
#include "../Gfx/Comp/FontRenderable.h"
#include "../Menu/Menu.h"
#include "Scene.h"

GameObject* create_something(vec2 position, std::string path);

pause_menu::pause_menu(const char* name, vec2 pos, std::vector<std::pair<std::string, vec2>> b) :
	text(name),
	position(pos),
	copy(b),
	active(false)
{}

void pause_menu::initialize() {
	buttons.clear();

	background = create_something(position, "./Data/Jsons/DefaultObject.json");
	background->mTransform->scale = vec3(2000, 1000, 1);
	background->mTransform->position.z = 0.f;
	Renderable* rend = background->GetComponent<Renderable>();
	if (rend) {
		rend->layer = "UI";
		rend->texture = nullptr;
		rend->color.a = 0.7f;
	}
	background->Initialize();
	
	title = create_something(vec2(position.x, position.y + 210.f), "./Data/Jsons/Buttons/pause_text.json");
	FontRenderable* ft = title->GetComponent<FontRenderable>();
	ft->displayText = text;
	unsigned t_size = text.size();
	ft->offset.x = -9.f * t_size;

	title->Initialize();

	decorations[0] = create_something(vec2(position.x, position.y + 280.f), "./Data/Jsons/Buttons/decoration.json");
	decorations[1] = create_something(vec2(position.x, position.y - 230.f), "./Data/Jsons/Buttons/decoration.json");
	decorations[1]->mTransform->FlipHorizontal();
	decorations[0]->Initialize();
	decorations[1]->Initialize();

	decorations[2] = create_something(vec2(position.x, position.y + 170.f), "./Data/Jsons/Buttons/smol_decoration.json");
	decorations[2]->Initialize();

	menu = create_something(position, "./Data/Jsons/Buttons/pause_menu.json");
	menu->active = false;

	Menu* comp = menu->GetComponent<Menu>();
	for (auto it : copy) {
		GameObject* obj = create_something(position + it.second, "./Data/Jsons/Buttons/" + it.first + ".json");
		obj->active = false;
		obj->Initialize();
		buttons.push_back(obj);
		comp->OwnedButtons.push_back(it.first);
	}
	menu->Initialize();
}

void pause_menu::update() {
	if (active) {
		if (menu) {
			menu->Update();
		}
		for (auto it : buttons) {
			if (it) {
				it->Update();
			}
		}
	}
}

void pause_menu::activate(bool ok) {
	active = ok;
	menu->active = ok;
	for (auto it : buttons) {
		it->active = ok;
	}
}

GameObject* create_something(vec2 position, std::string path) {
	GameObject *new_obj = scene->CreateGameObject();
	new_obj->LoadJson(path);
	new_obj->mTransform->position.x = position.x;
	new_obj->mTransform->position.y = position.y;
	new_obj->mTransform->position.z = 1000.f;
	new_obj->save = false;
	//new_obj->Initialize();
	return new_obj;
}