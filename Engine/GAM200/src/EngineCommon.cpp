#pragma once
#include "EngineCommon.h"

#ifdef EDITOR
std::string editor_loader = "./Data/Load/remember.json";
#endif
#ifndef EDITOR
std::string editor_loader = "./Data/Load/build.json";
#endif

bool first_play = true;
bool possesed_knight = false;

float MasterVolume::MASTER_VOLUME = 1.0f;
float MasterVolume::EFFECTS_VOLUME = 1.0f;
float MasterVolume::MUSIC_VOLUME = 1.0f;
//glm::clamp(mCurrTime, 0.0f, mDuration);
void MasterVolume::SetMasterVolume(float v)
{
	float t = clamp(v, 0.0f, 1.0f);
	MASTER_VOLUME = t;
}
float MasterVolume::GetMasterVolume() { return MASTER_VOLUME; }
void MasterVolume::DebugReduceVolume() 
{
	SetMasterVolume(0.1); 
	std::cout << "Volume is now: " << GetMasterVolume() << std::endl;
}
void MasterVolume::MaxOutVolume() 
{
	SetMasterVolume(1.0); 
	std::cout << "Volume is now: " << GetMasterVolume() << std::endl;
}
float & MasterVolume::GetMasterVolumeReference()
{
	return MASTER_VOLUME;
}
float MasterVolume::GetMusicMasterVolume()
{
	return MUSIC_VOLUME;
}
void MasterVolume::SetMusicVolume(float v)
{
	float t = clamp(v, 0.0f, 1.0f);
	MUSIC_VOLUME = t;
}
float & MasterVolume::GetMusicMasterVolumeReference()
{
	return MUSIC_VOLUME;
}
void MasterVolume::DebugReduceMusicVolume()
{
	SetMusicVolume(0.1);
	std::cout << "Volume is now: " << GetMusicMasterVolume() << std::endl;
}
void MasterVolume::MaxOutMusicVolume()
{
	SetMusicVolume(1.0);
	std::cout << "Volume is now: " << GetMusicMasterVolume() << std::endl;
}
/*EFFECTS*/
void MasterVolume::SetEffectsVolume(float v)
{
	float t = clamp(v, 0.0f, 1.0f);
	EFFECTS_VOLUME = t;
}
float MasterVolume::GetEffectsMasterVolume()
{
	return EFFECTS_VOLUME;
}
float & MasterVolume::GetEffectsMasterVolumeReference()
{
	return EFFECTS_VOLUME;
}
void MasterVolume::DebugReduceEffectsVolume()
{
	SetEffectsVolume(0.1);
	std::cout << "Volume is now: " << GetEffectsMasterVolume() << std::endl;
}
void MasterVolume::MaxOutEffectsVolume()
{
	SetEffectsVolume(1.0);
	std::cout << "Volume is now: " << GetEffectsMasterVolume() << std::endl;
}