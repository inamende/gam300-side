#ifndef ATTACK_H
#define ATTACK_H

#include "../../../Component/Component.h"

class Attack : public Component
{
public:
	Attack();

	int direction;
	float speed;
	float time;

	GameObject * Owner;
};

#endif
