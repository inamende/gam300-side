#ifndef SWORD_ATTACK_H
#define SWORD_ATTACK_H

#include "Attack.h"

class Transform;
class Scene;

class SwordAttack : public Attack
{
public:
	SwordAttack();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

private:
	Transform * tr;
	Scene * sc;
};

#endif
