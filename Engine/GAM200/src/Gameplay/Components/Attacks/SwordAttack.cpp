#include "SwordAttack.h"

#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Level/Scene.h"
#include "imGui/imgui.h"

SwordAttack::SwordAttack() : Attack::Attack(), tr(NULL), sc(NULL) {}

void SwordAttack::Initialize()
{
	tr = GetOwner()->mTransform;
	sc = scene;
}

void SwordAttack::Terminate() {}

void SwordAttack::Update()
{
	static float timer = 0.f;
	timer += ImGui::GetIO().DeltaTime;
	tr->position.x += direction * speed;

	if (timer >= time)
	{
		timer = 0.f;
		sc->DeleteGameObject(GetOwner());
	}
}

void SwordAttack::load(Json::Value & val)
{
	active = val["SwordAttack"].get("active", true).asBool();
	speed = val["SwordAttack"].get("speed", 2.f).asFloat();
	time = val["SwordAttack"].get("time", 2.f).asFloat();
}

void SwordAttack::save(Json::Value & val)
{
	val["SwordAttack"]["active"] = active;
	val["SwordAttack"]["speed"] = speed;
	val["SwordAttack"]["time"] = time;
}

void SwordAttack::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("SwordAttack"))
	{
		ImGui::InputFloat("Attack Speed##Attack", &speed);
		ImGui::InputFloat("Attack Time##Attack", &time);
	}
}