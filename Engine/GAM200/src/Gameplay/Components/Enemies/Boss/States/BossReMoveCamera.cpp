#include "BossReMoveCamera.h"
#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Gfx.h"
#include "../../../../../Gfx/Reso/Camera.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gameplay/Components/Strawss/CameraLogic.h"

BossReMoveCamera::BossReMoveCamera(const char *name, Transform *t, RigidBody *r, Boss *b) :
	BossState::BossState(name, t, r, b)
{}

void BossReMoveCamera::Enter() {
	cam = graphics->GetMainCamera();
}

void BossReMoveCamera::Update() {
	if (!strawss) strawss = scene->FindObject("Strawss");
	if (strawss) {
		log = strawss->GetComponent<CameraLogic>();
	}
	if (!cam || !log) return;

	log->move = false;
	log->focus = false;
	m_owner->ChangeState("Idle");
}