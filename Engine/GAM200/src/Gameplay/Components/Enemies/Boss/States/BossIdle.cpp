#include "BossIdle.h"
#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"

BossIdle::BossIdle(const char *name, Transform *t, RigidBody *r, Boss *b) : 
	BossState::BossState(name, t, r, b)
{}

void BossIdle::Enter() {
	switch (boss->age) {
	case young	: m_actor->mAnimator->ChangeAnimation("king_young");							break;
	case medium: m_actor->mAnimator->ChangeAnimation("king_medium");	boss->scale_medium();	break;
	case old: m_actor->mAnimator->ChangeAnimation("king_old");			boss->scale_old();		break;
	case dead	: return;
	}
	m_actor->mAnimator->Loop(true);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();
}

void BossIdle::Update() {
	if (boss->drain_life) {
		boss->drain_life = false;
		m_owner->ChangeState("MoveCamera");
	}
}