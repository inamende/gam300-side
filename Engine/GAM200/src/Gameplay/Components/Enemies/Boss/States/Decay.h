#ifndef DECAY_H
#define DECAY_H

#include "../Boss.h"

class Decay : public BossState {
public:
	Decay(const char*, Transform*, RigidBody*, Boss*);
	void Enter();
	void Update();
private:
	bool change_level;
	float dt;
	GameObject *strawss;
};

#endif
