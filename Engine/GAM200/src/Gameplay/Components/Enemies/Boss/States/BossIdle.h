#ifndef BOSSIDLE_H
#define BOSSIDLE_H

#include "../Boss.h"

class BossIdle : public BossState {
public:
	BossIdle(const char*, Transform*, RigidBody*, Boss*);
	void Enter();
	void Update();
};

#endif
