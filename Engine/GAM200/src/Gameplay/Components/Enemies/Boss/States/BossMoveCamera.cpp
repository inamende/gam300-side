#include "BossMoveCamera.h"
#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Gfx.h"
#include "../../../../../Gfx/Reso/Camera.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gameplay/Components/Strawss/CameraLogic.h"

BossMoveCamera::BossMoveCamera(const char *name, Transform *t, RigidBody *r, Boss *b) :
	BossState::BossState(name, t, r, b)
{}

void BossMoveCamera::Enter() {
	cam = graphics->GetMainCamera();
}

void BossMoveCamera::Update() {
	if (!strawss) strawss = scene->FindObject("Strawss");
	if (strawss) {
		log = strawss->GetComponent<CameraLogic>();
	}
	if (!cam || !log) return;

	log->move = true;
	log->focus = true;

	if (cam->MoveOver(vec2(m_actor->mTransform->position), m_time_in_state * 0.05f)) {
		m_owner->ChangeState("Decay");
	}
}