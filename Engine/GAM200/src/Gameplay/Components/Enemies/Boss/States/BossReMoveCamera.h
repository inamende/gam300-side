#ifndef BOSSREMOVECAMERA_H
#define BOSSREMOVECAMERA_H

#include "../Boss.h"

class Camera;
class CameraLogic;
class GameObject;

class BossReMoveCamera : public BossState {
public:
	BossReMoveCamera(const char*, Transform*, RigidBody*, Boss*);
	void Enter();
	void Update();
	Camera *cam;
	CameraLogic *log;
	GameObject *strawss;
};

#endif
