#ifndef BOSSMOVECAMERA_H
#define BOSSMOVECAMERA_H

#include "../Boss.h"

class Camera;
class CameraLogic;
class GameObject;

class BossMoveCamera : public BossState {
public:
	BossMoveCamera(const char*, Transform*, RigidBody*, Boss*);
	void Enter();
	void Update();
	Camera *cam;
	CameraLogic *log;
	GameObject *strawss;
};

#endif
