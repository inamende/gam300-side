#include "Decay.h"
#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Postprocessor.h"
#include "../../../../../Gfx/Reso/RenderLayer.h"
#include "../../../../../Gfx/Gfx.h"
#include "../../../../../Level/Scene.h"
#include "../../../Strawss/States/StrawssParam.h"
#include "imGui/imgui.h"

Decay::Decay(const char *name, Transform *t, RigidBody *r, Boss *b) :
	BossState::BossState(name, t, r, b)
{}

void Decay::Enter() {
	strawss = nullptr;
	change_level = false;
	dt = 0.f;
	switch (boss->age) {
	case young: m_actor->mAnimator->ChangeAnimation("king_decay_medium");	boss->scale_decay_medium();	break;
	case medium: m_actor->mAnimator->ChangeAnimation("king_decay_old");		boss->scale_decay_old();	break;
	case old: m_actor->mAnimator->ChangeAnimation("king_die");				boss->scale_decay_die();	break;
	}
	m_actor->mAnimator->Loop(false);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();
}

void Decay::Update() {
	if (m_actor->mAnimator->finished) {
		if (boss->age != dead) {
			m_owner->ChangeState("ReMoveCamera");
			return;
		}
		else {
			change_level = true;
		}
	}
	if (change_level) {
		if (!strawss) strawss = scene->FindObject("Strawss");
		strawss->active = false;
		dt += ImGui::GetIO().DeltaTime;
		if (graphics->FadeIn(-100, dt * 0.3f)) {
			for (auto layer : graphics->renderLayers) {
				if (layer->GetName() == "dynamic" || layer->GetName() == "UI" || layer->GetName() == "PostUI" || layer->GetName() == "PostPostUI") continue;
				layer->postprocess.postVar.hue = Strawss::GetParam().HUE;
				layer->postprocess.postVar.saturation = Strawss::GetParam().SAT;
			}
			scene->currLevel = "TheEnd.json";
		}
	}
}