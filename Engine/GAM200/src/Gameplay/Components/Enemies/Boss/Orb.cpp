#include "Orb.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../Physics/Physics.h"
#include "../../../../Level/Scene.h"
#include "../../../../Gfx/Comp/Renderable.h"
#include "../../../../Gfx/Reso/Texture.h"
#include "../../../../ResourceManager/ResoManager.h"
#include "../../Juice/JuiceBar.h"
#include "Boss.h"
#include "imgui/imgui.h"
//oscars additions
#include "../../../../Audio/SoundEmitter.h"
Orb::Orb() {}
Orb::~Orb() {}

void Orb::Initialize() {
	GetOwner()->register_handler(*this, &Orb::OnCollisionStarted);
	boss = scene->FindObject("big_boss");
	strawss = scene->FindObject("Strawss");
	broken = false;
}

void Orb::Update() {
	if (!boss) {
		boss = scene->FindObject("big_boss");
	}
	if (!strawss) {
		strawss = scene->FindObject("Strawss");
	}
}

void Orb::Terminate() {
	GetOwner()->unregister_handler(*this, &Orb::OnCollisionStarted);
}

void Orb::load(Json::Value & val) {
	active = val["Orb"].get("active", true).asBool();
}

void Orb::save(Json::Value & val) {
	val["Orb"]["active"] = active;
}

void Orb::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("Orb")) {
		ImGui::NewLine();
		if (ImGui::Button("Detach##Orb"))
			GetOwner()->detach(this);
	}
}

void Orb::OnCollisionStarted(const CollisionStartedEvent & eve) {
	if (!broken && boss && (eve.other_object->GetName() == "sword_attack" || eve.other_object->GetName() == "Chandelier")) {
		SoundEmitter * e = NULL;
		GameObject * g = objectManager->FindObjectByName("GeneralPurposeSoundEmitter");
		e = g->GetComponent<SoundEmitter>();
		if (e) {
			e->PlayOnce("WoodenDoorBreak.mp3", false);
		}
		boss->GetComponent<Boss>()->drain_life = true;
		broken = true;
		GetOwner()->GetComponent<Renderable>()->texture = resoManager->getReso<Texture>("broken_orb.png");
		JuiceBar *j = strawss->GetComponent<JuiceBar>();
		if (j) {
			j->RestoreJuice(j->get_max_juice());
		}
	}
}