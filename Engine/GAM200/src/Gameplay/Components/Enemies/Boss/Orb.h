#ifndef ORB_H
#define ORB_H

#include "../../../../Component/Component.h"

class GameObject;
class CollisionStartedEvent;

class Orb : public Component
{
public:
	Orb();
	~Orb();
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	void OnCollisionStarted(const CollisionStartedEvent & eve);
	GameObject *boss, *strawss;
	bool broken;
};

#endif