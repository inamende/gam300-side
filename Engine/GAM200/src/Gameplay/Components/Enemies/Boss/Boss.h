#ifndef BOSS_H
#define BOSS_H

#include "../../../../StateMachine/StateMachine.h"
#include "../Enemy.h"

class Transform;
class RigidBody;
class Boss;

class BossState : public State {
public:
	BossState(const char*, Transform*, RigidBody*, Boss*);
	virtual void Enter() = 0;
	virtual void Update() = 0;
	Transform *trans;
	RigidBody *rigid;
	Boss *boss;
};

enum age_time {young, medium, old, dead};

class Boss : public Enemy {
public:
	Boss();
	~Boss();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void EnemyAttack();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void scale_decay_medium();
	void scale_medium();
	void scale_decay_old();
	void scale_old();
	void scale_decay_die();

	age_time age;
	bool drain_life;
};

#endif

