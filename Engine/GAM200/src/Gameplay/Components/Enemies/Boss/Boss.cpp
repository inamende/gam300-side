#include "Boss.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../Input/Input.h"
#include "States/BossIdle.h"
#include "States/BossMoveCamera.h"
#include "States/Decay.h"
#include "States/BossReMoveCamera.h"
#include "imGui/imgui.h"

BossState::BossState(const char *name, Transform *t, RigidBody *r, Boss *b) :
	State::State(name),
	trans(t),
	rigid(r),
	boss(b)
{}

Boss::Boss() :
	age(young),
	drain_life(false)
{}

Boss::~Boss() {}

void Boss::Initialize() {
	m_actor = GetOwner();
	AddState(DBG_NEW BossIdle("Idle", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BossMoveCamera("MoveCamera", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW Decay("Decay", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BossReMoveCamera("ReMoveCamera", m_actor->mTransform, m_actor->mRigidBody, this));
	SetInitState("Idle");
	mInitialState->InternalEnter();
}

void Boss::Update() {
	if (KeyDown(Keyboard::Y)) {
		drain_life = true;
	}
	StatusUpdate();
}

void Boss::Terminate() {}

void Boss::EnemyAttack() {}

void Boss::load(Json::Value & val) {
	active = val["Boss"].get("active", true).asBool();
}

void Boss::save(Json::Value & val) {
	val["Boss"]["active"] = active;
}

void Boss::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("Boss"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Detach##Boss"))
			GetOwner()->detach(this);
	}
}

void Boss::scale_decay_medium() {
	age = medium;
	GetOwner()->mTransform->scale.x -= 47;
	GetOwner()->mTransform->scale.y -= 47;
	GetOwner()->mTransform->position.y -= 21;
}

void Boss::scale_medium() {
	age = medium;
	GetOwner()->mTransform->scale.x -= 45;
	GetOwner()->mTransform->scale.y -= 45;
	GetOwner()->mTransform->position.y -= 28;
}

void Boss::scale_decay_old() {
	age = old;
	GetOwner()->mTransform->scale.x += 52;
	GetOwner()->mTransform->scale.y += 52;
	GetOwner()->mTransform->position.y += 24;
}

void Boss::scale_old() {
	age = old;
	GetOwner()->mTransform->scale.x += 52;
	GetOwner()->mTransform->scale.y += 52;
	GetOwner()->mTransform->position.y += 25.2;
}

void Boss::scale_decay_die() {
	age = dead;
	GetOwner()->mTransform->scale.x -= 97;
	GetOwner()->mTransform->scale.y -= 97;
	GetOwner()->mTransform->position.y -= 72;
}