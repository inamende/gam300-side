#ifndef CHAMAN
#define CHAMAN

#include "../../../../Component/Component.h"
#include "../Enemy.h"

class GameObject;
class Forms;

class Transform;
class RigidBody;
class Knight;
class Chaman;

class CState : public State
{
public:
	CState(const char * name, Transform * t, RigidBody * r, Chaman * cha);

	virtual void Enter() = 0;
	virtual void Update() = 0;

	Transform * trans;
	RigidBody * rigid;
	Chaman * chaman;
};

class Chaman : public Enemy
{
public:
	//constructor
	Chaman();
	//values
	virtual void Initialize();
	//update, loop
	virtual void Update();
	//load-> we initialize the values
	virtual void load(Json::Value & val);
	//save->we save the values
	virtual void save(Json::Value & val);
	//write thing to show in the component window
	virtual void WriteToImguiWindow();

	virtual void EnemyAttack();

	void Meditate();

	GameObject * Player = nullptr;
	float Distance_2_denied;
	float DragForce;

private:


	std::vector<std::string> skins{ "default" };
};

#endif

