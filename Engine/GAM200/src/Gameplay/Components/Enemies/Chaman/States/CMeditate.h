#ifndef CMEDITATE_H
#define CMEDITATE_H

#include "../Chaman.h"

class PlayerController;

class CMeditate : public CState
{
public:
	CMeditate(const char * name, Transform * t, RigidBody * r, Chaman * k);

	void Enter();
	void Exit();
	void Update();


	bool StopMeditation;
	PlayerController * strawss = nullptr;
};

#endif
