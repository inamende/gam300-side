	#include "CMeditate.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../Strawss/PlayerController.h"
#include "../../../Strawss/States/StrawssParam.h"
#include "../../../Strawss/States/Spectre.h"
#include <spine/Bone.h>
#include <spine/Skeleton.h>
#include <glm/vec2.hpp>

CMeditate::CMeditate(const char * name, Transform * t, RigidBody * r, Chaman * cha) : CState::CState(name, t, r, cha) {}

void CMeditate::Enter()
{
	Strawss::GetParam().Denied = true;
	strawss = chaman->Player->GetComponent<PlayerController>();
	//std::cout << "meditating \n";
	StopMeditation = false;

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimationWithOutLoop("Wololo");
}

void CMeditate::Exit()
{
	Strawss::GetParam().Denied = false;
	//std::cout << " stop meditating \n";
}

void CMeditate::Update()
{

	float DistancePlayer;
	float DistanceSpecter = 0;

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	if(!StopMeditation)
		rend->LoopBetweenEvents("WololoStart", 2, "WololoFinish", 3);
	else
	{
		if(rend->DoesThisEnd())
			m_owner->ChangeState("CIdle");
	}


	glm::vec2 Stra(chaman->Player->mTransform->position.x, chaman->Player->mTransform->position.y);

	spBone * bone = nullptr;
	//float worldX, worldY;

	if (!rend->skel)
	{
		//std::cout << "the skeleton is null \n";
		return;
	}

	//spSkeleton_updateWorldTransform(rend->skel);
	bone = spSkeleton_findBone(rend->skel, "HereGoesTheFireParticle");
	if (!bone)
		return;
	//spBone_updateWorldTransform(bone); 
	//spBone_localToWorld(bone, bone->x, bone->y, &worldX, &worldY);
	auto transform = chaman->GetOwner()->GetComponent<Transform>();
	glm::vec2 finalPos = transform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);

	glm::vec2 Cha(finalPos.x, finalPos.y);

	DistancePlayer = glm::distance(Cha, Stra);

	if (strawss->PossEnemy == nullptr)
		DistanceSpecter = 0;
	else
	{
		glm::vec2 PossEnemy(strawss->PossEnemy->GetOwner()->mTransform->position.x, strawss->PossEnemy->GetOwner()->mTransform->position.y);
		DistanceSpecter = glm::distance(PossEnemy, Cha);
	}

	if (DistancePlayer > chaman->Distance_2_denied || DistanceSpecter > chaman->Distance_2_denied)
	{
		StopMeditation = true;
		//m_owner->ChangeState("CIdle");
	}

	
	if (dynamic_cast<Spectre *>(strawss->mCurrentState) != nullptr)
	{

		auto param = Strawss::GetParam();
		float Distance = glm::distance(chaman->GetOwner()->mTransform->position, chaman->Player->mTransform->position);

		//the further you are the stronger it will pull you
		float lengthPercent = (chaman->Distance_2_denied / Distance)* chaman->DragForce;

		if (lengthPercent <= 1.0f)
		{
			lengthPercent = 1.0f;
		}
		else if (lengthPercent >= 5.0f)
		{
			lengthPercent = 5.0f;
		}

		glm::vec3 vector_to_circle = chaman->GetOwner()->mTransform->position - chaman->Player->mTransform->position;

		chaman->Player->mTransform->position.x -= vector_to_circle.x / (param.GhostDrag / lengthPercent);
		chaman->Player->mTransform->position.y -= vector_to_circle.y / (param.GhostDrag / lengthPercent);
	}
}
