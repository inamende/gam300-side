#include "HeDe.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../.././../Strawss/PlayerController.h"


CHeDe::CHeDe(const char * name, Transform * t, RigidBody * r, Chaman * cha) : CState::CState(name, t, r, cha) {}

void CHeDe::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	
	rend->ChangeAnimationWithOutLoop("Dissapear");

	rend->AddInfiniteAnimationToQueue("IdleDeath");
}

void CHeDe::Update()
{
	//HE REST IN PEPERONI, R.I.P.
}
