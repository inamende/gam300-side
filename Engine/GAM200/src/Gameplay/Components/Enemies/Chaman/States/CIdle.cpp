#include "CIdle.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../.././../Strawss/PlayerController.h"
#include <spine/Bone.h>
#include <spine/Skeleton.h>
#include <glm/vec2.hpp>


CIdle::CIdle(const char * name, Transform * t, RigidBody * r, Chaman * cha) : CState::CState(name, t, r, cha) {}

void CIdle::Enter()
{

	if(chaman->Player)
		strawss = chaman->Player->GetComponent<PlayerController>();

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("Idle");
}

void CIdle::Update()
{
	float DistancePlayer;
	float DistanceSpecter = chaman->Distance_2_denied+100;

	glm::vec2 Stra(chaman->Player->mTransform->position.x, chaman->Player->mTransform->position.y);
	
	SkeletonRenderable * rend = chaman->GetOwner()->GetComponent<SkeletonRenderable>();
	spBone * bone = nullptr;
	//float worldX, worldY;

	if (!rend->skel)
	{
		//std::cout << "the skeleton is null \n";
		return;
	}

	//spSkeleton_updateWorldTransform(rend->skel);
	bone = spSkeleton_findBone(rend->skel, "HereGoesTheFireParticle");
	if (!bone)
		return;
	//spBone_updateWorldTransform(bone); 
	//spBone_localToWorld(bone, bone->x, bone->y, &worldX, &worldY);
	auto transform = chaman->GetOwner()->GetComponent<Transform>();
	glm::vec2 finalPos = transform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);

	glm::vec2 Cha (finalPos.x, finalPos.y );

	DistancePlayer = glm::distance(Cha, Stra);

	if (strawss == nullptr)
		return;

	if (strawss->PossEnemy == nullptr)
		DistanceSpecter = glm::distance(Cha, Stra);
	else
	{
		glm::vec2 PossEnemy(strawss->PossEnemy->GetOwner()->mTransform->position.x, strawss->PossEnemy->GetOwner()->mTransform->position.y);
		DistanceSpecter = glm::distance(PossEnemy, Cha);
	}


	if (DistancePlayer <= chaman->Distance_2_denied || DistanceSpecter <= chaman->Distance_2_denied)
	{
		m_owner->ChangeState("CMeditate");
	}
}
