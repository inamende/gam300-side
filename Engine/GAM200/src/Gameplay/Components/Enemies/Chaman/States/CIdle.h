#ifndef CIDLE_H
#define CIDLE_H

#include "../Chaman.h"

class PlayerController;

class CIdle : public CState
{
public:
	CIdle(const char * name, Transform * t, RigidBody * r, Chaman * k);

	void Enter() override;
	void Update();

	PlayerController * strawss = nullptr;
};

#endif