#include "Chaman.h"

#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Editor/ImGuiEditor.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../Gfx/Reso/Camera.h"
#include "../../../../Input/Input.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"
#include <spine/Bone.h>
#include <spine/Skeleton.h>
#include <glm/vec2.hpp>

#include "States\CIdle.h"
#include "States\CMeditate.h"
#include "States\HeDe.h"

Chaman::Chaman() {}

CState::CState(const char * name, Transform * t, RigidBody * r, Chaman * k) : State::State(name), trans(t), rigid(r), chaman(k) {}

void Chaman::Initialize()
{
	Enemy::Initialize();

	Player = objectManager->FindObjectByName("Strawss");

	m_actor = GetOwner();
	AddState(DBG_NEW CIdle    ("CIdle"      , m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW CMeditate("CMeditate", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW CHeDe	  ("HeDe" , m_actor->mTransform, m_actor->mRigidBody, this));


	SetInitState("CIdle");
	mInitialState->InternalEnter();

	SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
	if (rend)
		rend->skin = skins[0];

}

void Chaman::Update()
{
	//Meditate();
	
	StatusUpdate();



	SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
	spBone * bone = nullptr;
	float worldX, worldY;

	if (!rend->skel)
	{
		//std::cout << "the skeleton is null \n";
		return;
	}

	//spSkeleton_updateWorldTransform(rend->skel);
	bone = spSkeleton_findBone(rend->skel, "HereGoesTheFireParticle");
	if (!bone)
	{
		//std::cout << "the bone is null \n";
		return;
	}
	//spBone_updateWorldTransform(bone); 
	spBone_localToWorld(bone, bone->x, bone->y, &worldX, &worldY);
	auto transform = GetOwner()->GetComponent<Transform>();
	glm::vec2 finalPos = transform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);


	graphics->DrawACircle(glm::vec3(finalPos, 5), 10, 10);

	graphics->DrawACircle(glm::vec3(finalPos, 5), Distance_2_denied, 100);
}

void Chaman::load(Json::Value & val)
{
	Distance_2_denied = val["Chaman"].get("Distance_2_denied", 100.0f).asFloat();
	DragForce		  = val["Chaman"].get("DragForce", 100.0f).asFloat();
}
void Chaman::save(Json::Value & val)
{
	val["Chaman"]["Distance_2_denied"] = Distance_2_denied;//we get the value float 
	val["Chaman"]["DragForce"] = DragForce;
}
void Chaman::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Chaman"))
	{
		ImGui::InputFloat("Distance he will denied", &(float)Distance_2_denied);

		ImGui::InputFloat("Drag force", &(float)DragForce);

		ImGui::NewLine();
		if (ImGui::Button("Detach##Chaman"))
			GetOwner()->detach(this);
	}
}

void Chaman::EnemyAttack()
{
}

void Chaman::Meditate()
{
	/*
	if (Transformation->IsPossesing)
	{
		float Distance_2_possessed_enemy;
		Distance_2_possessed_enemy = glm::distance(GetOwner()->mTransform->position, Transformation->Possesed_one->GetOwner()->mTransform->position);
		
		if (Distance_2_possessed_enemy <= Distance_2_denied)
		{
			Transformation->Chaman_power = true;
		}
		
	}
	else
	{
		float Distance;

		Distance = glm::distance(GetOwner()->mTransform->position, Player->mTransform->position);

		if (Transformation->IsGhost && (Distance <= Distance_2_denied))
		{
			//std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA" << std::endl;
			//dragearlo pa atras 
			//Player->mTransform->position += (GetOwner()->mTransform->position - Player->mTransform->position);
		}
		if (Distance <= Distance_2_denied)
		{
			Transformation->Chaman_power = true;
		}
		if (Distance >= Distance_2_denied && Transformation->Chaman_power)
		{
			Transformation->Chaman_power = false;
		}
	}
	*/
}
