#include "ArrowDirection.h"

#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../ObjectManager/ObjectManager.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Physics/Physics.h"
#include "../../../../../Physics/Comp/BoxColliderOriented.h"
#include "../../../../../Physics/Comp/BoxCollider.h"
#include "../../Enemy.h"
#include "../../Bowman/Bowman.h"
#include "../../../Strawss/PlayerController.h"

#include <glm/gtx/vector_angle.hpp>

ArrowDirection::ArrowDirection()
{
}

ArrowDirection::~ArrowDirection()
{
	this->GetOwner()->unregister_handler((*this), &ArrowDirection::ArrowHasHitSomething);
}

void ArrowDirection::Initialize()
{
	Transf = GetOwner()->mTransform;
	RigidBody * Body = GetOwner()->GetComponent<RigidBody>();

	Shoot_at = (Direction - GetOwner()->mTransform->position);

	Shoot_at = glm::normalize(Shoot_at); //we normalize the direction

	if (Body == nullptr)
	{
		//std::cout << "NO RIGID BODY IN ARROW \n";
		return;
	}

	Shoot_at.y += 0.3f;
	Shoot_at.z = 0.0f;

	this->GetOwner()->register_handler((*this), &ArrowDirection::ArrowHasHitSomething);

	glm::vec3 horizontal(1.0, 0.0, 0.0);
	
	Transf->rotation = Transf->position.y > Direction.y ? glm::angle(Shoot_at, horizontal) : -glm::angle(Shoot_at, horizontal);

	GetOwner()->mRigidBody->velocity = Shoot_at * velocity;
}

void ArrowDirection::Terminate()
{
}

void ArrowDirection::Update()
{	
	glm::vec3 horizontal(1.0, 0.0, 0.0);

	glm::vec3 NewDirection = ((Transf->position - GetOwner()->mRigidBody->prevPosition));
	NewDirection = glm::normalize(NewDirection);

	Transf->rotation = Transf->position.y < GetOwner()->mRigidBody->prevPosition.y ? glm::angle(NewDirection, horizontal) : -glm::angle(NewDirection, horizontal);

}

void ArrowDirection::load(Json::Value & val)
{
	active = val["ArrowDirection"].get("active", true).asBool();
	velocity = val["ArrowDirection"].get("velocity", 100.0).asFloat();
}
void ArrowDirection::save(Json::Value & val)
{
	val["ArrowDirection"]["active"] = active;
	val["ArrowDirection"]["velocity"] = velocity;
}
void ArrowDirection::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("ArrowDirection"))
	{
		ImGui::InputFloat("Velocity", &(float)velocity);

		//before the detach button we put a new line
		ImGui::NewLine();
		if (ImGui::Button("Detach##ArrowDirection"))
			GetOwner()->detach(this);
	}

}

void ArrowDirection::ArrowHasHitSomething(const CollisionStartedEvent & rhs)
{
	if (Owner == nullptr)
		return;

	if (Owner->GetComponent<Enemy>()->possesed && rhs.other_object->GetComponent<PlayerController>())
		return;

	//THIS WILL REPRESENMT THE HITBOX OF THE ENEMIES AND I HOT THE EVERYTHING PRRRA
	BoxCollider * Box1 = rhs.other_object->GetComponent<BoxCollider>("BoxCollider_0");
	//if its not the boxcollider thjat we want

	if(Box1)
		if (!CollideAABBs(GetOwner()->GetComponent<BoxCollider>(), Box1, NULL))
			return;

	if (rhs.other_object != Owner)
		objectManager->RemoveGameObject(GetOwner());
}