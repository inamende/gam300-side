#ifndef ARROWDIRECTION_H
#define ARROWDIRECTION_H

#include "../../../Attacks/Attack.h"

class GameObject;
class CollisionStartedEvent;
class Transform;

class ArrowDirection : public Attack
{
public:

	ArrowDirection();
	~ArrowDirection();

	virtual void Initialize();
	virtual void Terminate();
	virtual void Update();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void ArrowHasHitSomething(const CollisionStartedEvent & rhs);

	glm::vec3 Direction;
	//GameObject * ArrowOwner;

private:

	float velocity = 10;
	glm::vec3 Shoot_at;
	Transform * Transf;
};


#endif
