#include "Bowman.h"

#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Editor/ImGuiEditor.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../Gfx/Reso/Camera.h"
#include "../../../../Input/Input.h"
#include "../../../../Level/Scene.h"
#include "Arrow\ArrowDirection.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../../../Physics/Physics.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../InteractableObjects/Lever.h"
#include "../../InteractableObjects/PlayerInteractable.h"
#include "../../../../Menu/Cursor.h"
#include <glm/gtx/vector_angle.hpp>
#include <spine/Bone.h>
#include <spine/Skeleton.h>
#include <glm/vec2.hpp>

#include "States/BIdle.h"
#include "States/BMove.h"
#include "States/BPatrol.h"
#include "States/BReload.h"
#include "States/BShoot.h"
#include "States/BStunned.h"
#include "States/BHeDe.h"
#include "States/BLeverPull.h"
#include "States/BPossessed.h"
#include "States/BFalling.h"
#include "States/BActivateLever.h"

void Create_arrow_at_position(const glm::vec3 Spawn_position, const std::string Name, const glm::vec3 ArrowDirection, GameObject * owner);

Bowman::Bowman() : Distance_2_shoot(500.0f) , Time2Reload(2.0f), Time2Shoot(2.0f), Position2Shoot(0.0f, 0.0f, 0.0f), BmaxSpeed(500.0f),
					Bspeed(100.0f), BmovementDrag(100.0f), DistanceLever(50.0f), PatrolTime(5.0f){}

Bowman::~Bowman()
{
	GetOwner()->unregister_handler(*this, &Bowman::OnCollisionStarted);
}

BState::BState(const char * name, Transform * t, RigidBody * r, Bowman * k) : State::State(name), trans(t), rigid(r), bowman(k) {}
//-1 = right
// 1 = left
void Bowman::Initialize()
{
	Enemy::Initialize();

	direction = 1;

	BRigidBody = GetOwner()->mRigidBody;

	Player = objectManager->FindObjectByName(target);

	initialScale = fabs(GetOwner()->mTransform->scale.x);
	trans = GetOwner()->mTransform;

	//we set the actor to use it in the states 
	m_actor = GetOwner();
	//we add the states that we use in the bowman
	AddState(DBG_NEW BPatrol("BPatrol", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BShoot ("BShoot" , m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BReload("BReload", m_actor->mTransform, m_actor->mRigidBody, this));
	//possession movement
	AddState(DBG_NEW BIdle("BIdle", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BMove("BMove", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BStunned("BStunned", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BPossessed("BPossessed", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BFalling("BFalling", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BActivateLever("BActivateLever", m_actor->mTransform, m_actor->mRigidBody, this));
	//when he needs to die DO NOT CHANGE NAME
	AddState(DBG_NEW BHeDe("HeDe", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW BLeverPull("BLeverPull", m_actor->mTransform, m_actor->mRigidBody, this));
	//we set the first state we are going to use
	SetInitState("BPatrol");
	//we enter to the initial state
	mInitialState->InternalEnter();

	GetOwner()->register_handler(*this, &Bowman::OnCollisionStarted);

	rend = GetOwner()->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	spSkeleton_setSkinByName(rend->skel, "bowman1");
	bone = spSkeleton_findBone(rend->skel, "AMING");
}

void Bowman::Update()
{
	if (!rend)
		return;
	//we update the states
	if (Player != nullptr)
		StatusUpdate();
	else
		return;

	if(BRigidBody->velocity.y < -100)
		ChangeState("BFalling");

	if (!p_in) p_in = GetOwner()->GetComponent<PlayerInteractable>();
	if (p_in && p_in->can_activate) {
		ChangeState("BLeverPull");
		p_in->can_activate = false;
		if (lever) {
			direction = (lever->mTransform->position.x - GetOwner()->mTransform->position.x > 0.f) ? -1 : 1;
		}
	}

	trans->scale.x = (direction > 0) ? initialScale : -initialScale;

		/*
	SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	spBone * bone = nullptr;

	if (!rend->skel)
		std::cout << "the skeleton is null \n";

	bone = spSkeleton_findBone(rend->skel, "AMING");

	auto transform = GetOwner()->GetComponent<Transform>();
	glm::vec2 finalPos = transform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);


	//glm::vec2 finalPos = transform->Trans * glm::vec4(worldX, worldY, 0, 1);

	graphics->DrawACircle(glm::vec3(finalPos, 5), 20, 100);
	*/

	if(!bone)
		return;

	if (possesed && mCurrentState != GetState("BFalling"))
	{
		glm::vec3 NewDirection;
		glm::vec3 Mouse_position;
		Transform * OurTransform = GetOwner()->mTransform;
		glm::vec3 horizontal(1.0, 0.0, 0.0);
		glm::vec2 BonePosition = OurTransform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);


		Camera * cam = graphics->GetCameraByRenderLayer("default");
		glm::vec2 mouse = cam->ViewportToWorld(Input::getInstance()->GetMousePosition());
		Mouse_position = mInput->using_controller ? vec3(scene->cursor->GetComponent<Cursor>()->get_camera_coord("default"), 1.f) : vec3( mouse.x, mouse.y, 1 );
		//we saet the new direction
		NewDirection = { glm::vec2(Mouse_position.x - BonePosition.x, Mouse_position.y - BonePosition.y), 0.0 };

		//as the player can shoot behing him, we need to change where he looks at 
		//-1 = right
		// 1 = left
		float Direction = Mouse_position.x - OurTransform->position.x;

		if(Direction > 0)
			horizontal = { -1.0, 0.0, 0.0 };

		if (Direction < 0 && direction == -1)
		{
			GetOwner()->mTransform->scale.x *= -1;
			direction = 1;
			if(mCurrentState == GetState("BMove"))
				if (rend->currentanimation != "MoonWalkingPos")
				{
					//GetOwner()->mTransform->scale.x *= -1;
					rend->ChangeAnimation("MoonWalkingPos");
				}

		}
		else if (Direction > 0 && direction == 1)
		{
			GetOwner()->mTransform->scale.x *= -1;
			direction = -1;
			if (mCurrentState == GetState("BMove"))
				if (rend->currentanimation != "MoonWalkingPos")
				{
					//GetOwner()->mTransform->scale.x *= -1;
					rend->ChangeAnimation("MoonWalkingPos");
				}
		}
		else
			if (mCurrentState == GetState("BMove"))
				if (rend->currentanimation != "WalkingPos")
				{
					GetOwner()->mTransform->scale.x *= -1;
					rend->ChangeAnimation("WalkingPos");
				}

		NewDirection = glm::normalize(NewDirection);

		float test_angle = glm::angle(NewDirection, horizontal) * 180 / 3.1416;

		test_angle = BonePosition.y < Mouse_position.y ? test_angle : -test_angle;

		//std::cout << "angle aplied to aim: " << test_angle << "\n";
		//we put the angle that we want ot in to the data of the bone
		bone->data->rotation = spBone_localToWorldRotation(bone, test_angle);

		if (possesed && (MouseUp(SDL_BUTTON_LEFT) || ButtonDown(SDL_CONTROLLER_BUTTON_X)))
		{
			//we call here to the shoot state, and we set the target if we need to 
			Position2Shoot = Mouse_position;
			//we change variables that we need to 
			Reloading = 0.0f;
			//we change the state
			ChangeState("BReload");
		}
	}

}

void Bowman::EnemyAttack()
{
	SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
	spBone * bone = nullptr;

	if (!rend)
		return;

	if (!rend->skel)
	{
		//std::cout << "the skeleton is null \n";
		return;
	}

	//spSkeleton_updateWorldTransform(rend->skel);
	bone = spSkeleton_findBone(rend->skel, "AMING");
	if (!bone)
		return;
	//spBone_updateWorldTransform(bone); 
	auto transform = GetOwner()->GetComponent<Transform>();
	glm::vec2 finalPos = transform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);

	if (bone)
	{
		Create_arrow_at_position(glm::vec3(finalPos, GetOwner()->mTransform->position.z), "Arrow", Position2Shoot, GetOwner());
	}
	else
	{ 
		//std::cout << "we didnt get thebone \n";
	}
}

void Bowman::OnCollisionStarted(const CollisionStartedEvent & object)
{
	/*
	//get the component 
	Attack * damage = object.other_object->GetComponent<Attack>();


	if (damage != nullptr)
	{

		if (Enemy * enemy = damage->Owner->GetComponent<Enemy>())
		{
			if (enemy->GetOwner() == GetOwner())
				return;

			if (enemy->possesed)
				objectManager->RemoveGameObject(GetOwner());

		}
	}
	*/

}

void Create_arrow_at_position(const glm::vec3 Spawn_position, const std::string Name, const glm::vec3 ArrowDi, GameObject * owner)
{
	float z = imGuiEditor->GetLastZ();

	GameObject * newEditorObj = scene->CreateGameObject();
	newEditorObj->LoadJson("./Data/Jsons/" + Name + ".json");

	newEditorObj->mTransform->position.x = Spawn_position.x;
	newEditorObj->mTransform->position.y = Spawn_position.y;
	newEditorObj->mTransform->position.z = Spawn_position.z + 0.1;

	if (newEditorObj->mArrowDi)
	{
		newEditorObj->mArrowDi->Direction = ArrowDi;
		newEditorObj->mArrowDi->Owner = owner;
	}

	newEditorObj->Initialize();
}

void Bowman::load(Json::Value & val)
{
	Time2Reload		 = val["Bowman"].get("Time2Reload", 0.0f).asFloat();
	Time2Shoot		 = val["Bowman"].get("Time2Shoot", 0.0f).asFloat();
	Distance_2_shoot = val["Bowman"].get("Distance_2_shoot", 0.0f).asFloat();
	target			 = val["Bowman"].get("target", "Set_Target_Please").asString();
	BmaxSpeed		 = val["Bowman"].get("BmaxSpeed", 0.0f).asFloat();
	Bspeed			 = val["Bowman"].get("Bspeed", 0.0f).asFloat();
	BmovementDrag	 = val["Bowman"].get("BmovementDrag", 0.0f).asFloat(); 
	DistanceLever	 = val["Bowman"].get("DistanceLever", 0.0f).asFloat();
	PatrolTime		 = val["Bowman"].get("PatrolTime", 0.0f).asFloat();
	
}
void Bowman::save(Json::Value & val)
{
	val["Bowman"]["Time2Reload"]	  = Time2Reload;//we get the value float 
	val["Bowman"]["Time2Shoot"]		  = Time2Shoot;
	val["Bowman"]["Distance_2_shoot"] = Distance_2_shoot;
	val["Bowman"]["target"]			  = target;
	val["Bowman"]["Bspeed"]		      = Bspeed;
	val["Bowman"]["BmaxSpeed"]		  = BmaxSpeed;
	val["Bowman"]["BmovementDrag"]    = BmovementDrag;
	val["Bowman"]["DistanceLever"]	  = DistanceLever;
	val["Bowman"]["PatrolTime"]		  = PatrolTime;
}
void Bowman::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Bowman"))
	{
		ImGui::InputFloat("Reloading Time"		  , &(float)Time2Reload);
		ImGui::InputFloat("Shooting Time"		  , &(float)Time2Shoot);
		ImGui::InputFloat("Distance he will shoot", &(float)Distance_2_shoot);

		//if (ImGuiEditor::InputText("Target", target))
		//{
		//	Player = objectManager->FindObjectByName(target);
		//	if (Player)
		//		Transformation = Player->mForms;
		//}

		ImGui::NewLine();

		ImGui::InputFloat("Changing direction time" , &(float)PatrolTime);
		ImGui::InputFloat("MaxSpeed "				, &(float)BmaxSpeed);
		ImGui::InputFloat("SpeedWalking"			, &(float)Bspeed);
		ImGui::InputFloat("Drag"					, &(float)BmovementDrag);
		ImGui::InputFloat("Distance to Lever"		, &(float)DistanceLever);


		ImGui::NewLine();
		if (ImGui::Button("Detach##Bowman"))
			GetOwner()->detach(this);
	}
}