#ifndef BOWMAN
#define BOWMAN

#include "../Enemy.h"
#include "../../../../StateMachine/StateMachine.h"

class GameObject;
class Forms;
class Bowman;
class Transform;
class Lever;
class PlayerInteractable;
class RigidBody;
class spBone;
class SkeletonRenderable;

class CollisionStartedEvent;

class BState : public State
{
public:
	BState(const char * name, Transform * t, RigidBody * r, Bowman * k);

	virtual void Enter() = 0;
	virtual void Update() = 0;

	Transform * trans;
	RigidBody * rigid;
	Bowman * bowman;
};

class Bowman : public Enemy
{
public:

	Bowman();
	~Bowman();

	virtual void Initialize();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	virtual void EnemyAttack();

	void OnCollisionStarted(const CollisionStartedEvent & spot);

//private:

	//variable to look at the sides
	float PatrolTime;
	//variables we use in some states
	float Reloading;
	glm::vec3 Position2Shoot;

	//variables showed in the editor
	float Distance_2_shoot;
	float Time2Reload;
	float Time2Shoot;
	std::string target;
	//variables of possession
	float BmaxSpeed;
	float Bspeed;
	float BmovementDrag;

	//to activae the lever
	float DistanceLever;

	GameObject * Player = nullptr;
	Transform* trans = nullptr;
	Transform* Target = nullptr;
	Forms * Transformation = nullptr;
	RigidBody * BRigidBody = nullptr;
	spBone * bone;
	SkeletonRenderable * rend;

	std::vector<std::string> skins{ "bowman1" };

};

#endif
