#include "BStunned.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"

#include "../Arrow/ArrowDirection.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

BStunned::BStunned(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BStunned::Enter()
{

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("Stun");
	SoundEmitter * e = m_owner->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->PlayOnce("Grunt.mp3", false);
	}
}

void BStunned::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{
		if (bowman->possesed)
			m_owner->ChangeState("BPossessed");
		else
			m_owner->ChangeState("BPatrol");
	}
}
void BStunned::Exit()
{
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
			e->StopVoice();

	}
}
