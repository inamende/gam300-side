#ifndef BACTIVATELEVER_H
#define BACTIVATELEVER_H

#include "../Bowman.h"

class Transform;

class BActivateLever : public BState
{
public:
	BActivateLever(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();

private:
	Transform * BowmanPoition;
	Transform * LeverPoition;

};

#endif

