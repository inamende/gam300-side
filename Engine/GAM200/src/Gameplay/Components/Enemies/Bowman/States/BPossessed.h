#ifndef BPOSSES_H
#define BPOSSES_H

#include "../Bowman.h"

class BPossessed : public BState
{
public:
	BPossessed(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
};

#endif

