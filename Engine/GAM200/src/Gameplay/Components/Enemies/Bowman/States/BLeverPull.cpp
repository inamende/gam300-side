#include "BLeverPull.h"
#include "../../Knight/States/KnightParam.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../ObjectManager/GameObject.h"

BLeverPull::BLeverPull(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BLeverPull::Enter() {
	KnightP::GetParam().pulling_lever = true;
	rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend)
		rend->ChangeAnimationWithOutLoop("Lever1");
	if (rigid) {
		rigid->velocity = vec3();
	}
}

void BLeverPull::Update() {
	if (!rend) rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend) {
		if (rend->DoesThisEnd()) {
			m_owner->ChangeState("BIdle");
			KnightP::GetParam().pulling_lever = false;
		}
	}
}

void BLeverPull::Exit() {

}