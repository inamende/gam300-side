#ifndef BHEDE_H
#define BHEDE_H

#include "../Bowman.h"

class BHeDe : public BState
{
public:
	BHeDe(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
	void Exit();
};

#endif

