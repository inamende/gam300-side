#ifndef BLEVERPULL_H
#define BLEVERPULL_H

#include "../Bowman.h"

class Transform;
class RigidBody;
class Bowman;
class SkeletonRenderable;

class BLeverPull : public BState {
public:
	BLeverPull(const char * name, Transform * t, RigidBody * r, Bowman * k);
	void Enter();
	void Update();
	void Exit();
private:
	SkeletonRenderable* rend;
};

#endif
