#include "BHeDe.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"

//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

BHeDe::BHeDe(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BHeDe::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;


	rend->ChangeAnimationWithOutLoop("Stun");
}

void BHeDe::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->LoopBetweenEvents("StartStun", 5, "EndStun", 6);
}

void BHeDe::Exit()
{
}
