#include "BIdle.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Reso/Camera.h"
#include "../../../../../Gfx/Gfx.h"
#include "../../../../../Input/Input.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"

BIdle::BIdle(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BIdle::Enter()
{
	//std::cout << "WE ENTERED TO THE POSSESSED IDLE STATEEE!! \n";

	rigid = m_actor->mRigidBody;

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("IdlePossesed");



	//std::cout << "enter into the IDLE BOWMAN  \n";
}

void BIdle::Update()
{
	/*SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->LoopBetweenEvents("StartIdle", 1, "EndIdle", 2);*/

	//if its not possessed anymore
	if (!bowman->possesed)
	{
		m_owner->ChangeState("BStunned");
		return;
	}

	Decelerate();

	Camera * cam = graphics->GetCameraByRenderLayer("default");
	glm::vec2 mouse = cam->ViewportToWorld(Input::getInstance()->GetMousePosition());

	glm::vec3 Mouse_position(mouse.x, mouse.y, 1);

	//if he shoots
	/*
	if (MouseDown(SDL_BUTTON_LEFT))
	{
		//we call here to the shoot state, and we set the target
		bowman->Position2Shoot = Mouse_position;
		m_owner->ChangeState("BShoot");
	}
	*/


	if (KeyPressed(SDL_SCANCODE_D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		//bowman->direction = -1;
		m_owner->ChangeState("BMove");
	}
	else if (KeyPressed(SDL_SCANCODE_A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		//bowman->direction = 1;
		m_owner->ChangeState("BMove");
	}
}

void BIdle::Decelerate()
{
	if (abs(rigid->velocity.x) > 0)
		rigid->velocity.x -= (rigid->velocity.x) / bowman->BmovementDrag;
}