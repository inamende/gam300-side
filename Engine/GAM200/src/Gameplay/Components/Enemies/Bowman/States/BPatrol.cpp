#include "BPatrol.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../Strawss/PlayerController.h"

#include "../../../../../Level/Scene.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"


BPatrol::BPatrol(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k), Distance(0.0f) {}

void BPatrol::Enter()
{
	Time = 0.0f;

	rigid->velocity.x = 0;
	//SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	//
	//if (e)
	//{
	//	if (e->IsInit)
	//		e->PlayLooped("BowmanWalk.mp3", false);
	//}

	PlayerTrans = bowman->Player->mTransform;

	rend = m_actor->GetComponent<SkeletonRenderable>();

	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("IdleNotAiming");

	EndAnimation = false;
	Distance = glm::distance(glm::vec2(trans->position.x, trans->position.y), glm::vec2(PlayerTrans->position.x, PlayerTrans->position.y));
	if ((Distance <= bowman->Distance_2_shoot))
	{
		bowman->Target = PlayerTrans;
		m_owner->ChangeState("BShoot");
		//we chnage the variables that we need to
		bowman->Reloading = 0.0f;
	}
	//std::cout << "enter into the patrol \n";
}

void BPatrol::Update()
{
	if (!rend)
		return;

	if(bowman->Player)
	{ 
		PlayerController * PlayerControll = bowman->Player->GetComponent<PlayerController>();
		if (PlayerControll)
			if (PlayerControll->PossEnemy)
			{
				if(!EnemiTrans)
					EnemiTrans = PlayerControll->PossEnemy->GetOwner()->mTransform;
			}
	}
	else
	{
		if(EnemiTrans)
			EnemiTrans = nullptr;
	}

	if(!EndAnimation)
		rend->LoopBetweenEvents("StartIdle", 1, "EndIdle", 2);

	Time += ImGui::GetIO().DeltaTime;

	if (Time >= bowman->PatrolTime && !EndAnimation)
	{
		bowman->direction *= -1;
		trans->scale.x *= -1;
		Time = 0;
	}

	float PlayerDirection = PlayerTrans->position.x - trans->position.x;
	float EnemiDirection;
	if(EnemiTrans)
		EnemiDirection = EnemiTrans->position.x - trans->position.x;

	if(((PlayerDirection > 0) && bowman->direction == -1) || ((PlayerDirection < 0) && bowman->direction == 1) || EndAnimation
		|| (EnemiTrans && (((EnemiDirection > 0) && bowman->direction == -1) || ((EnemiDirection < 0) && bowman->direction == 1))))
	{
		float DistanceToEnemi = INFINITY;
		if (EnemiTrans)
			DistanceToEnemi = glm::distance(glm::vec2(trans->position.x, trans->position.y), glm::vec2(EnemiTrans->position.x, EnemiTrans->position.y));

		Distance = glm::distance(glm::vec2(trans->position.x, trans->position.y), glm::vec2(PlayerTrans->position.x, PlayerTrans->position.y));
		//std::cout<<"distance to enemi:  "<< DistanceToEnemi<<"\n";
		if ((Distance <= bowman->Distance_2_shoot) || (DistanceToEnemi <= bowman->Distance_2_shoot))
		{
			//to shoot if he is looing you
				//if ((bowman->Player->mTransform->position.x - bowman->GetOwner()->mTransform->position.x) < 0)
				//{
				//	if(bowman->direction == -1)
				//}
				//else
				//{
				//	if (bowman->direction == -1)
				//}

			//if (m_owner->mPreviousState == m_owner->GetState("BReload"))
			//{
			//	m_owner->ChangeState("BShoot");
			//	return;
			//}

			if (!EndAnimation)
			{
				EndAnimation = true;
			}
			else if(rend->DoesThisEnd())
			{
				if((Distance <= bowman->Distance_2_shoot))
					bowman->Target = PlayerTrans;
				else
					bowman->Target = EnemiTrans;

				m_owner->ChangeState("BShoot");
				//we chnage the variables that we need to
				bowman->Reloading = 0.0f;
			}
		}
		else if (EndAnimation)
			EndAnimation = false;
	}
	if(bowman->possesed)
		m_owner->ChangeState("BPossessed");

}
void BPatrol::Exit()
{
	//oscars additions
	if (rigid)
	{
		////NOTE, here the input is without the .mp3
		//SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		//if (e)
		//{
		//	if (e->IsInit)
		//		e->StopVoice();
		//}

	}
}
