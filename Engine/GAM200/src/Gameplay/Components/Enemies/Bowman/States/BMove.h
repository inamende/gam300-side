#ifndef BMOVE_H
#define BMOVE_H

#include "../Bowman.h"

class Transform;
class RigidBody;
class Bowman;

class BMove : public BState
{
public:
	BMove(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
	void Exit();
	void MakeMove();

	RigidBody * rigid = NULL;
};

#endif
