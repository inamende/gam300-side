#include "BPossessed.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"


BPossessed::BPossessed(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BPossessed::Enter()
{

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("BeingPossesed");
}

void BPossessed::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{
		m_owner->ChangeState("BIdle");
	}
}
