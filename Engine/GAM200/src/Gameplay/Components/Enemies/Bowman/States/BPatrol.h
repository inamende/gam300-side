#ifndef BPATROL_H
#define BPATROL_H
 
#include "../Bowman.h"

class SkeletonRenderable;
class Transform;

class BPatrol : public BState
{
public:
	BPatrol(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
	void Exit();
private:
	float Distance;
	float Time;
	bool EndAnimation;

	SkeletonRenderable * rend;
	Transform * PlayerTrans;
	Transform * EnemiTrans = nullptr;
};

#endif
