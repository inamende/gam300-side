#include "BReload.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Physics/Physics.h"
#include "../../../../../Physics/Comp/BoxCollider.h"

#include "../Arrow/ArrowDirection.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

BReload::BReload(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BReload::Enter()
{
	PossState = bowman->possesed;
	//std::cout << "WE ENTERED TO THE RELOAD STATEEE!! \n";
	bowman->Reloading = 0.0f; 
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->PlayOnce("BowmanCrossbowShootReload.mp3", false);
	}

		
	//physics->SwapContainer(m_actor->GetComponent<BoxCollider>(), true);

	bowman->EnemyAttack();

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	if(bowman->possesed)
		rend->ChangeAnimationWithOutLoop("AtackPossesed");
	else
		rend->ChangeAnimationWithOutLoop("Attack");
}

void BReload::Update()
{

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{
		if (bowman->possesed)
		{
			if (PossState != bowman->possesed)
				m_owner->ChangeState("BeingPossesed");
			else
			m_owner->ChangeState("BIdle");
		}
		else
		{
			m_owner->ChangeState("BPatrol");
		}
	}
}
void BReload::Exit()
{
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			if (e->IsInit)
				e->StopVoice();
		}
	}

	physics->SwapContainer(m_actor->GetComponent<BoxCollider>(), false);
}
