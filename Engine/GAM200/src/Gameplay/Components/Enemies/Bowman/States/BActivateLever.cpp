#include "BActivateLever.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Physics/Comp/RigidBody.h"
#include "../../../InteractableObjects/Lever.h"


BActivateLever::BActivateLever(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BActivateLever::Enter()
{

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	
	BowmanPoition = m_owner->GetOwner()->mTransform;
	LeverPoition = bowman->TargettedLever->GetOwner()->mTransform;

	float DistanceToLever = glm::distance(vec2{ BowmanPoition->position.x, BowmanPoition->position.y}, vec2{ LeverPoition->position.x, LeverPoition->position.y });

	if(abs(DistanceToLever) < 50)
		rend->ChangeAnimation("Lever1");
	else
		m_owner->ChangeState("BIdle");

}

void BActivateLever::Update()
{
		
}
