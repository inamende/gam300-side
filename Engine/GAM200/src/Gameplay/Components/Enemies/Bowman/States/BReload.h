#ifndef BRELOAD_H
#define BRELOAD_H

#include "../Bowman.h"

class BReload : public BState
{
public:
	BReload(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
	void Exit();

	bool PossState;
};

#endif

