#ifndef BSTUNNED_H
#define BSTUNNED_H

#include "../Bowman.h"

class BStunned : public BState
{
public:
	BStunned(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
	void Exit();
};

#endif

