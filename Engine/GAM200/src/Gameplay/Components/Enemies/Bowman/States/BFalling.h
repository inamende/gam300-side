#ifndef BFALLING_H
#define BFALLING_H

#include "../Bowman.h"


class CollisionStartedEvent;
class SkeletonRenderable;

class BFalling : public BState
{
public:
	BFalling(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Exit();
	void Update();

	private:
	bool EndFalling;
	bool StartAnim;
	void OnCollisionStarted(const CollisionStartedEvent & object);
	SkeletonRenderable * rend;
};

#endif

