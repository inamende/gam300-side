#ifndef BSHOOT_H
#define BSHOOT_H

#include "../Bowman.h"

class Tranfrorm;
class spBone;
class SkeletonRenderable;

class BShoot : public BState
{
public:
	BShoot(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();
	void Exit();
private:
	float Shooting;
	float StartingAngle;
	//bool LookingRight;

	Transform * PlayerTransform;
	Transform * OurTransform;
	spBone * bone;
	SkeletonRenderable * rend;

	void  UpdateDirection();

	bool PossState;
};

#endif

