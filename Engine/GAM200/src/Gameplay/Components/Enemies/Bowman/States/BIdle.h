#ifndef BIDLE_H
#define BIDLE_H

#include "../Bowman.h"

class RigidBody;

class BIdle : public BState
{
public:
	BIdle(const char * name, Transform * t, RigidBody * r, Bowman * k);

	void Enter();
	void Update();

	void Decelerate();

	RigidBody * rigid = NULL;
};

#endif

