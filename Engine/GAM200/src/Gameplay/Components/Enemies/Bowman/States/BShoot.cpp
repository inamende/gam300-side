#include "BShoot.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
//spine
#include "../../../../../Gfx/Reso/Camera.h"
#include "../../../../../Input/Input.h"
#include "../../../../../Gfx/Gfx.h"
#include <spine/Bone.h>
#include <spine/Skeleton.h>
#include <glm/vec2.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <math.h>

#include "../Arrow/ArrowDirection.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

BShoot::BShoot(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k), Shooting(0.0f) {}

void BShoot::Enter()
{
	PossState = bowman->possesed;
	//std::cout << "WE ENTERED TO THE SHOOT STATEEE!! \n";
	//PlayerTransform = bowman->Player->mTransform;
	PlayerTransform = bowman->Target;

	OurTransform = m_owner->GetOwner()->mTransform;

	rigid->velocity.x = 0;
	Shooting = 0.0f;
	
	rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("IdleAiming");
	if (!bowman->possesed)
	{
		if ((OurTransform->position.x - PlayerTransform->position.x) < -1)
		{
			OurTransform->scale.x = abs(OurTransform->scale.x)*-1;
			bowman->direction = -1;
		}
		else
		{
			bowman->direction = 1;
			OurTransform->scale.x = abs(OurTransform->scale.x);
		}
	}
	else//it aims to the direction of the mouse
	{
		//std::cout << "excuse u wtf \n";
	}

	if (!rend->skel)
	{
		//std::cout << "the skeleton is null \n";
		return;
	}


	bone = spSkeleton_findBone(rend->skel, "AMING");
	//we get the position of the bone
	glm::vec2 BonePosition = OurTransform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);
	//and the angle of the bone 
	StartingAngle = spBone_getWorldRotationX(bone);

	//horizontal 
	glm::vec3 horizontal(1.0, 0.0, 0.0);
	if (bowman->direction == -1)
	{
		horizontal = { -1.0, 0.0, 0.0 };
	}
	//we compute the trajectory of the arrow depending if its posssesed or not
	glm::vec3 NewDirection;
	glm::vec3 Mouse_position;
	if(bowman->possesed)
	{
		Camera * cam = graphics->GetCameraByRenderLayer("default");
		glm::vec2 mouse = cam->ViewportToWorld(Input::getInstance()->GetMousePosition());
		Mouse_position = {mouse.x, mouse.y, 1};

		//we saet the new direction, but with the bone to get the correct angle
		NewDirection = {glm::vec2(Mouse_position.x - BonePosition.x, Mouse_position.y - BonePosition.y), 0.0};

		//as the player can shoot behing him, we need to change where he looks at 
		float Direction = Mouse_position.x - OurTransform->position.x;
		
		if (Direction < 0 && bowman->direction == -1)
		{
			OurTransform->scale.x = OurTransform->scale.x*-1;
			bowman->direction = 1;
			horizontal = { 1.0, 0.0, 0.0 };
		}
		if (Direction > 0 && bowman->direction == 1)
		{
			OurTransform->scale.x = OurTransform->scale.x*-1;
			bowman->direction = -1;
			horizontal = { -1.0, 0.0, 0.0 };
		}
		
	}
	else
	{ 
		NewDirection = {glm::vec2(PlayerTransform->position.x - BonePosition.x, PlayerTransform->position.y - BonePosition.y) , 0.0};
	}
	NewDirection = glm::normalize(NewDirection);//normalise
	//we compute the angle 
	float test_angle = glm::angle(NewDirection, horizontal) * 180 / 3.1416;

	if (bowman->possesed)
		test_angle = BonePosition.y < Mouse_position.y ? test_angle : -test_angle;
	else
		test_angle = BonePosition.y < PlayerTransform->position.y ? test_angle : -test_angle;

	//we put the angle that we want ot in to the data of the bone
	bone->data->rotation = spBone_localToWorldRotation(bone, test_angle);

}

void BShoot::Update()
{
	Shooting += ImGui::GetIO().DeltaTime;

	if (PossState != bowman->possesed)
	{
		if(bowman->possesed)
			m_owner->ChangeState("BeingPossesed");
		else
			m_owner->ChangeState("BIdle");
		return;
	}
		

	if (!rend)
		return;
	if (!rend->skel)
	{
		//std::cout << "the skeleton is null \n";
		return;
	}


	//bone = spSkeleton_findBone(rend->skel, "AMING");

	glm::vec3 horizontal(1.0, 0.0, 0.0);
	if (bowman->direction == -1)
	{
		horizontal = { -1.0, 0.0, 0.0 };
	}
	//we get the position of the bone
	glm::vec2 BonePosition = OurTransform->GetTransform() * glm::vec4(bone->worldX, bone->worldY, 0, 1);

	glm::vec3 NewDirection;
	glm::vec3 Mouse_position;
	if (bowman->possesed)
	{
		Camera * cam = graphics->GetCameraByRenderLayer("default");
		glm::vec2 mouse = cam->ViewportToWorld(Input::getInstance()->GetMousePosition());
		Mouse_position = { mouse.x, mouse.y, 1 };
		//we saet the new direction
		NewDirection = { glm::vec2(Mouse_position.x - BonePosition.x, Mouse_position.y - BonePosition.y), 0.0 };

		//as the player can shoot behing him, we need to change where he looks at 
		float Direction = Mouse_position.x - OurTransform->position.x;

		if (Direction < 0 && bowman->direction == -1)
		{
			OurTransform->scale.x = OurTransform->scale.x*-1;
			bowman->direction = 1;
			horizontal = { 1.0, 0.0, 0.0 };
		}
		if (Direction > 0 && bowman->direction == 1)
		{
			OurTransform->scale.x = OurTransform->scale.x*-1;
			bowman->direction = -1;
			horizontal = { -1.0, 0.0, 0.0 };
		}

	}
	else
	{ 
		NewDirection = {glm::vec2(PlayerTransform->position.x - BonePosition.x, PlayerTransform->position.y - BonePosition.y), 0.0};
	}
	NewDirection = glm::normalize(NewDirection);

	float test_angle = glm::angle(NewDirection, horizontal) * 180 / 3.1416;

	if (bowman->possesed)
		test_angle = BonePosition.y < Mouse_position.y ? test_angle : -test_angle;
	else
		test_angle = BonePosition.y < PlayerTransform->position.y ? test_angle : -test_angle;

	//std::cout << "angle aplied to aim: " << test_angle << "\n";
	//we put the angle that we want ot in to the data of the bone
	bone->data->rotation = spBone_localToWorldRotation(bone, test_angle);


	if ((Shooting > bowman->Time2Shoot && !bowman->possesed) || (bowman->possesed && MouseUp(SDL_BUTTON_LEFT)))
	{
		//we call here to the shoot state, and we set the target if we need to 
		if (!bowman->possesed)
			bowman->Position2Shoot = PlayerTransform->position;
		else//we set the target if the bowman is possessed
			bowman->Position2Shoot = Mouse_position;


		//bowman->EnemyAttack();
		//we change variables that we need to 
		bowman->Reloading = 0.0f;
		//we change the state
		m_owner->ChangeState("BReload");
	}

	if(!bowman->possesed)
		UpdateDirection();
}
void BShoot::Exit()
{
	if (!rend)
		return;
	
	bone = spSkeleton_findBone(rend->skel, "AMING");
	bone->data->rotation = spBone_localToWorldRotation(bone, StartingAngle);
}

void BShoot::UpdateDirection()
{
	if (((OurTransform->position.x - PlayerTransform->position.x) < 0) && bowman->direction == 1)
	{
		OurTransform->scale.x = OurTransform->scale.x*-1;
		bowman->direction = -1;
	}
	if (((OurTransform->position.x - PlayerTransform->position.x) > 0) && bowman->direction == -1)
	{
		OurTransform->scale.x = OurTransform->scale.x*-1;
		bowman->direction = 1;
	}
}