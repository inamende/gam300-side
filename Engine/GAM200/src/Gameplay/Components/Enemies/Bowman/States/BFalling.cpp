#include "BFalling.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Physics/Comp/RigidBody.h"
#include "../../../../../Physics/Comp/BoxCollider.h"
#include "../../../../../Physics/Comp/BoxColliderOriented.h"
#include "../../../../../Physics/Physics.h"


BFalling::BFalling(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BFalling::Enter()
{

	rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("Fall"); 
	EndFalling = false;
	StartAnim = false;

	m_owner->GetOwner()->register_handler(*this, &BFalling::OnCollisionStarted);
}

void BFalling::Exit()
{
	m_owner->GetOwner()->unregister_handler(*this, &BFalling::OnCollisionStarted);
}

void BFalling::Update()
{
	if (rigid->velocity.y < 10)
		StartAnim = true;

	if(!EndFalling)
		rend->LoopBetweenEvents("StartFalling", 10, "EndFalling", 11);

	if(StartAnim)
	{
		if(!EndFalling)
		{ 
			EndFalling = true;
			rend->ToEndEventLoopBetweenEvents();
		}
		else
		{ 
			//we hit the ground
			if(rend->DoesThisEnd())
				if(bowman->possesed)
					m_owner->ChangeState("BIdle");
				else
					m_owner->ChangeState("BPatrol");
		}
	}
}

void BFalling::OnCollisionStarted(const CollisionStartedEvent & object)
{
	if (!m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"))
		return;
	if(object.other_object->GetComponent<BoxCollider>())
	{
		if (CollideAABBs(m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"), object.other_object->GetComponent<BoxCollider>(), NULL))
		{
			StartAnim = true;
		}
	}
	else
	{
		if (CollideAABBs(m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"), object.other_object->GetComponent<BoxColliderOriented>(), NULL))
		{
			StartAnim = true;
		}
	}
	
}