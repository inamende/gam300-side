#include "BMove.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Reso/Camera.h"
#include "../../../../../Gfx/Gfx.h"
#include "../../../../../Input/Input.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"

//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

BMove::BMove(const char * name, Transform * t, RigidBody * r, Bowman * k) : BState(name, t, r, k) {}

void BMove::Enter()
{
	//std::cout << "WE ENTERED TO THE POSSESSED MOVING STATEEE!! \n";

	rigid = m_actor->mRigidBody;
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
			e->PlayLooped("BowmanWalk.mp3", false);
	}
	else
	{
		//std::cout << "No SoundEmitter found" << std::endl;
	}

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	
	rend->ChangeAnimation("WalkingPos");

	//std::cout << "enter into the MOVING \n";

}

void BMove::Update()
{

	//if its not possessed anymore, we pass to the stunned
	if (!bowman->possesed)
	{
		m_owner->ChangeState("BStunned");
		return;
	}



	//if he shoots
	/*
	if (MouseDown(SDL_BUTTON_LEFT))
	{

		Camera * cam = graphics->GetCameraByRenderLayer("default");
		glm::vec2 mouse = cam->ViewportToWorld(Input::getInstance()->GetMousePosition());

		glm::vec3 Mouse_position(mouse.x, mouse.y, 1);

		//we call here to the shoot state, and we set the target
		bowman->Position2Shoot = Mouse_position;
		m_owner->ChangeState("BShoot");
	}
	*/

	//MOVING 
	if (KeyPressed(Keyboard::D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		bowman->direction = -1;
	}
	else if (KeyPressed(Keyboard::A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		bowman->direction = 1;
	}
	//if we need to change to idle
	else
		m_owner->ChangeState("BIdle");

	MakeMove();

}

void BMove::MakeMove()
{
	if ((rigid->velocity.x * bowman->direction) < 0)
		rigid->velocity.x += 50 * bowman->direction*-1;
	
	if (abs(rigid->velocity.x) <= bowman->BmaxSpeed)
		rigid->velocity.x += bowman->Bspeed * bowman->direction*-1;
	
	if (abs(rigid->velocity.x) > bowman->BmaxSpeed)
		rigid->velocity.x = bowman->BmaxSpeed * bowman->direction*-1;
}
void BMove::Exit()
{
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
			e->StopVoice();
	}
}