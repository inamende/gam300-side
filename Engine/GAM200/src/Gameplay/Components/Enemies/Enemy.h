#ifndef ENEMY_H
#define ENEMY_H

#include "../../../StateMachine/StateMachine.h"

class Collider;
class CollisionStartedEvent;
class CollisionPersistedEvent;
class Lever;
class PlayerInteractable;
class GameObject;

class Enemy : public StateMachine
{
public:

	Enemy();

	virtual void EnemyAttack() = 0;
	virtual void Initialize() override;
	virtual void Terminate() override;

	float initialScale;
	int direction;
	bool possesed;
	bool playerSpotted;

	GameObject * strawss;
	Collider * viewFrustum;

	void OnCollisionStartedEnemy(const CollisionStartedEvent & object);
	void CollisionPersistedEnemy(const CollisionPersistedEvent & object);

	Lever * TargettedLever = nullptr;
	PlayerInteractable* p_in;
	GameObject* lever;
};

void CreateAtack(std::string json, glm::vec2 & position, int direction, GameObject * owner);

#endif
