#include "Enemy.h"

#include "../../../Editor/ImGuiEditor.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../Level/Scene.h"
#include "../Attacks/Attack.h"
#include "../Traps/DamagingObj.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "Knight/Knight.h"
#include "../Traps/DamagingObj.h"
#include "../../../ObjectManager/ObjectManager.h"
#include "../../../Physics/Physics.h"
#include "Bowman\Arrow\ArrowDirection.h"
#include "Bowman/Bowman.h"
#include "../../../Gfx/Comp/SkeletonRenderable.h"
//to activate the lever
#include "../InteractableObjects/Lever.h"
#include "../../../Input/Input.h"
#include "../Attacks/SwordAttack.h"
//audio
#include "../../../Audio/SoundEmitter.h"
#include "../../../Gameplay/Components/Enemies/Knight/Box/BoxLogic.h"

Enemy::Enemy() : initialScale(0.f), direction(1), possesed(false), playerSpotted(false), strawss(NULL), viewFrustum(NULL), lever(nullptr), p_in(nullptr) {}

void Enemy::Initialize()
{
	//std::cout << "registered enemy collision started! \n";
	GetOwner()->register_handler(*this, &Enemy::OnCollisionStartedEnemy);
	GetOwner()->register_handler(*this, &Enemy::CollisionPersistedEnemy);
}

void Enemy::Terminate()
{
	//std::cout << "unregistered enemy collision event!!!!! \n";
	GetOwner()->unregister_handler(*this, &Enemy::OnCollisionStartedEnemy);
	GetOwner()->unregister_handler(*this, &Enemy::CollisionPersistedEnemy);
}

void Enemy::OnCollisionStartedEnemy(const CollisionStartedEvent & object)
{
	DamagingObj * obj = object.other_object->GetComponent<DamagingObj>();
	Knight * enemy = GetOwner()->GetComponent<Knight>();
	SwordAttack * attack = object.other_object->GetComponent<SwordAttack>();
	

	if (enemy)
	{
		if (enemy->mCurrentState == enemy->GetState("HeDe"))
			return;
		BoxCollider * Box = GetOwner()->GetComponent<BoxCollider>("BoxCollider_0");
		if (Box)
		{
			if(obj)
				if (CollideAABBs(Box, object.other_object->GetComponent<BoxCollider>(), NULL))
				{
					RigidBody * Velocity = object.other_object->GetComponent<RigidBody>();
					if (Velocity)//sanity check 
						if (abs(Velocity->velocity.y) > 300)
						{
							//oscars additions
							SoundEmitter * e = NULL;
							GameObject * g = objectManager->FindObjectByName("GeneralPurposeSoundEmitter");
							e = g->GetComponent<SoundEmitter>();
							if (e)
							{
								BoxLogic * b = NULL;
								//object.other_object.GetOwner()->GetComponent<BoxLogic>();
								b = object.other_object->GetComponent<BoxLogic>();
								if (b != nullptr)
								{
									//e->PlaySound("WoodenDoorBreak.mp3", 1);
									e->PlayOnce("WoodenDoorBreak.mp3", false);
								}
								else
								{
									//e->PlaySound("IronClang.mp3", 1);
									e->PlayOnce("IronClang.mp3", false);
								}
							}


							objectManager->RemoveGameObject(object.other_object);
							GetOwner()->GetComponent<Enemy>()->ChangeState("HeDe");

							//we dont want the death body to move so, we set the body to static
							RigidBody * Body = GetOwner()->GetComponent<RigidBody>();
							Body->autoComputeMass = false;
							Body->invMass = 0;
							physics->SwapContainer(GetOwner()->GetComponent<BoxCollider>(), true);


							SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
							if(rend)
							{ 
								if((GetOwner()->mTransform->position.x - obj->GetOwner()->mTransform->position.x) > 0)
								{ 
									if(enemy->direction == -1)
										rend->ChangeAnimationWithOutLoop("StunReceiveDamageFront");
									else
										rend->ChangeAnimationWithOutLoop("StunReceiveDamageBack");
								}
								else//rigth part
								{
									if (enemy->direction == 1)
										rend->ChangeAnimationWithOutLoop("StunReceiveDamageFront");
									else
										rend->ChangeAnimationWithOutLoop("StunReceiveDamageBack");
								}
							}
							return;
						}
				}
		}
	}
	else if (obj)
	{
		
		DamagingObj * Velocity = object.other_object->GetComponent<DamagingObj>();
		if (Velocity)//sanity check 
			if (abs(Velocity->PreviousYVelocity) > 300)
			{
				//oscars additions
				SoundEmitter * e = NULL;
				GameObject * g = objectManager->FindObjectByName("GeneralPurposeSoundEmitter");
				e = g->GetComponent<SoundEmitter>();
				if (e)
				{
					BoxLogic * b = NULL;
					//object.other_object.GetOwner()->GetComponent<BoxLogic>();
					b = object.other_object->GetComponent<BoxLogic>();
					if (b != nullptr)
					{
						//e->PlaySound("WoodenDoorBreak.mp3", 1);
						e->PlayOnce("WoodenDoorBreak.mp3", false);
					}
					else
					{
						//e->PlaySound("IronClang.mp3", 1);
						e->PlayOnce("IronClang.mp3", false);
					}
				}
				objectManager->RemoveGameObject(object.other_object);
				GetOwner()->GetComponent<Enemy>()->ChangeState("HeDe");

				//we dont want the death body to move so, we set the body to static
				RigidBody * Body = GetOwner()->GetComponent<RigidBody>();
				Body->autoComputeMass = false;
				Body->invMass = 0;
				physics->SwapContainer(GetOwner()->GetComponent<BoxCollider>(), true);

				return;
			}
	}
	if (attack && !enemy)
	{
		GetOwner()->GetComponent<Enemy>()->ChangeState("HeDe");

		//we dont want the death body to move so, we set the body to static
		RigidBody * Body = GetOwner()->GetComponent<RigidBody>();
		Body->autoComputeMass = false;
		Body->invMass = 0;
		physics->SwapContainer(GetOwner()->GetComponent<BoxCollider>(), true);

		return;
	}

	ArrowDirection * arrow = object.other_object->GetComponent<ArrowDirection>();

	if (arrow)
	{
		if (!CollideAABBs(GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"), object.other_object->GetComponent<BoxCollider>("BoxCollider_0"), NULL))
		{
			return;
		}
		//if we are the bowman that shoot or the bowman that shoot was not possess
		if (GetOwner() == arrow->Owner || !arrow->Owner->GetComponent<Bowman>()->possesed)
			return;
		//if the bowman was possessed and we were hit by an arrow, we DE SON, lul copy paste, but w8 in this case we dont need to delete the arrow we gud
		GetOwner()->GetComponent<Enemy>()->ChangeState("HeDe");

		//we dont want the death body to move so, we set the body to static
		RigidBody * Body = GetOwner()->GetComponent<RigidBody>();
		Body->autoComputeMass = false;
		Body->invMass = 0;
		physics->SwapContainer(GetOwner()->GetComponent<BoxCollider>(), true);


		if (enemy)
		{
			SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
			if ((GetOwner()->mTransform->position.x - arrow->GetOwner()->mTransform->position.x) > 0)
			{
				if (enemy->direction == -1)
					rend->ChangeAnimationWithOutLoop("StunReceiveDamageFront");
				else
					rend->ChangeAnimationWithOutLoop("StunReceiveDamageBack");
			}
			else//rigth part
			{
				if (enemy->direction == 1)
					rend->ChangeAnimationWithOutLoop("StunReceiveDamageFront");
				else
					rend->ChangeAnimationWithOutLoop("StunReceiveDamageBack");
			}
		}
	}


}

void Enemy::CollisionPersistedEnemy(const CollisionPersistedEvent & object)
{
	if (object.other_object->GetComponent<Lever>())
		lever = object.other_object;
}

void CreateAtack(std::string json, glm::vec2 & position, int direction, GameObject * owner)
{
	float z = imGuiEditor->GetLastZ();

	GameObject * newEditorObj = scene->CreateGameObject();

	if (!newEditorObj)
		return;

	bool nice = newEditorObj->LoadJson("./Data/Jsons/" + json + ".json");

	if (!nice)
	{
		scene->DeleteGameObject(newEditorObj);
		return;
	}

	Transform * tr = newEditorObj->mTransform;

	if (!tr) 
	{
		scene->DeleteGameObject(newEditorObj);
		return;
	}

	tr->position.x = position.x;
	tr->position.y = position.y;
	tr->position.z = z + 0.1f;

	Attack * attack = newEditorObj->GetComponent<Attack>();

	if (!attack)
	{
		scene->DeleteGameObject(newEditorObj);
		return;
	}

	attack->direction = direction;
	attack->Owner = owner;

	newEditorObj->Initialize();
}