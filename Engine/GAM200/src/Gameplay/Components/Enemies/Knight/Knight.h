#ifndef KNIGHT_H
#define KNIGHT_H

#include "../../../../StateMachine/StateMachine.h"
#include "../Enemy.h"

class Transform;
class RigidBody;
class Knight;
class PlayerInteractable;
class GameObject;
class BoxCollider;

class CollisionStartedEvent;
class CollisionPersistedEvent;
class CollisionEndedEvent;

class KState : public State
{
public:
	KState(const char * name, Transform * t, RigidBody * r, Knight * k);

	virtual void Enter() = 0;
	virtual void Update() = 0;

	Transform * trans;
	RigidBody * rigid;
	Knight * knight;
};

class Knight : public Enemy
{
public:
	Knight();
	~Knight();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	virtual void EnemyAttack();

	void OnCollisionStarted(const CollisionStartedEvent & spot);
	void OnCollisionPersisted(const CollisionPersistedEvent & spot);
	void OnCollisionEnded(const CollisionEndedEvent & spot);

	float idleTime = 2;
	float patrolTime = 3;
	glm::vec2 visionRange = glm::vec2(400.f, 300.f);
	float retreatPoint = 0.f;
	bool const_retreat_point = false;

	std::string target;
	std::string spawnAttack;
	Transform * strawssTrans;
	Transform * trans;

	unsigned currSkin = 1;
	std::vector<std::string> skins{ "Knight1", "Knight2" };

	//variables of possession
	float KmaxSpeed;
	float Kspeed;
	float KmovementDrag;
	float TakingBox;
	float TimeStunned;
	float NoticeTime;
	float DeathTime;
	//float 
	bool  TakeBox;
	bool  LeaveBox;
	bool  WithBox;
	int BoxDirection;

	//
	unsigned int KnightType;
	//bool to make sure that the whistle sound effect only plays when it needs to play
	bool WhistleSoundEffectHasPlayed = false;
	//litle box to check if he is gonna fall or not
	BoxCollider * FallCheck;
	RigidBody * KRigidBody = nullptr;
};



#endif
