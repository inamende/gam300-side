#include "BoxLogic.h"
#include "../Knight.h"

#include "../../../../../Physics/Collisions.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../ObjectManager/ObjectManager.h"
//#include "../../Gfx/Comp/Transform.h"

BoxLogic::BoxLogic() : parented(false), carrierKnight(NULL) , Player(nullptr), LookingAt(1.0f){}

void BoxLogic::Initialize()
{
	GetOwner()->register_handler(*this, &BoxLogic::OnBoxEvent);

	if (GetOwner()->mRigidBody != NULL && !GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"))
	{
		GetOwner()->mRigidBody->invMass = 0.000001f;
		GetOwner()->mRigidBody->gravity.y = -0.001f;
	}
}

void BoxLogic::Terminate()
{
	GetOwner()->unregister_handler(*this, &BoxLogic::OnBoxEvent);
}

void BoxLogic::Update()
{
	//static float prevPos = GetOwner()->mTransform->position.x;

	if (parented)
	{
		/*
		if (prevPos != carrierKnight->mTransform->position.x)
		{
			float movement = carrierKnight->mTransform->position.x - prevPos;
			GetOwner()->mTransform->position.x += movement;

			prevPos = GetOwner()->mTransform->position.x;
		}
		*/
		/*vec3 pos = carrierKnight->mTransform->position;
		if (Player_last_position != pos)
		{
			glm::vec3 Movement = pos - Player_last_position;
			Movement.z = 0;
			GetOwner()->mRigidBody->acceleration = { 0.0,0.0 };
			float dist = distance(pos.x, GetOwner()->mTransform->position.x);
			std::cout << dist << std::endl;
			if (dist > 200) {
				GetOwner()->mTransform->position += Movement;
			}	
			Player_last_position = pos;
		}*/
		int d = (carrierKnight->mTransform->scale.x < 0) ? 1 : -1;
		vec3 pos = carrierKnight->mTransform->position;
		GetOwner()->mRigidBody->acceleration = { 0.0,0.0 };
		if (Player_last_position != pos) {
			GetOwner()->mTransform->position.x = pos.x + d * knight_separation;
		}
		//if (LookingAt != carrierKnight->GetComponent<Knight>()->direction)
		//{
		//	LookingAt = carrierKnight->GetComponent<Knight>()->direction;
		//	GetOwner()->mTransform->position.x += DistanceBetw * LookingAt*2;
		//}

		if (!(reinterpret_cast<Knight * >(carrierKnight)->possesed))
			parented = false;

	}
	else
	{
		carrierKnight = NULL;
		GetOwner()->mRigidBody->isStatic = false;
	}
}


void BoxLogic::OnBoxEvent(const BoxEvent & other)
{
	DistanceBetw = GetOwner()->mTransform->position.x - other.Boxowner->mTransform->position.x;
	if(!other.Boxowner->GetComponent<Knight>()->WithBox)
		if ((DistanceBetw < 0 && other.Boxowner->GetComponent<Knight>()->direction > 0) || (DistanceBetw > 0 && other.Boxowner->GetComponent<Knight>()->direction < 0))
			return;
	if (StaticRectToStaticRect(glm::vec2(GetOwner()->mTransform->position.x, GetOwner()->mTransform->position.y), fabs(GetOwner()->mTransform->scale.x ), fabs(GetOwner()->mTransform->scale.y),
		glm::vec2(other.Boxowner->mTransform->position.x, other.Boxowner->mTransform->position.y), fabs(other.Boxowner->mTransform->scale.x), fabs(other.Boxowner->mTransform->scale.y)))
	{//we enter here when in needs to be parented or not 
		if (!parented)
		{
			Player_last_position = other.Boxowner->mTransform->position;
			GetOwner()->mRigidBody->isStatic = true;
			carrierKnight = other.Boxowner;
			carrierKnight->GetComponent<Knight>()->TakeBox = true;
			carrierKnight->GetComponent<Knight>()->WithBox = true;
			carrierKnight->GetComponent<Knight>()->BoxDirection = (DistanceBetw > 1) ? 1 : -1;
			parented = other.carry;
			LookingAt = other.Boxowner->GetComponent<Knight>()->direction;
			adjust_knight_position(other.Boxowner);
			//DistanceBetw = abs(DistanceBetw);
		}
		else
		{
			parented = false;
			carrierKnight->GetComponent<Knight>()->LeaveBox = true;
			carrierKnight->GetComponent<Knight>()->WithBox = false;
			carrierKnight->GetComponent<Knight>()->direction = carrierKnight->GetComponent<Knight>()->BoxDirection;
			GetOwner()->mTransform->position.y += 5;
		}
	}
}

void BoxLogic::load(Json::Value & val)
{
	active = val["BoxLogic"].get("active", true).asBool();
}

void BoxLogic::save(Json::Value & val)
{
	val["BoxLogic"]["active"] = active;
}

void BoxLogic::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("BoxLogic"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Detach##BoxLogic"))
			GetOwner()->detach(this);
	}
}

void BoxLogic::adjust_knight_position(GameObject* knight) {
	//knight->mTransform->position.x = GetOwner()->mTransform->position.x + 400;
}

#if 0	
BoxLogic::BoxLogic() {}

void BoxLogic::Initialize()
{
	Player = objectManager->FindObjectByName("Player");
	//Reload = false;
	Transformation = Player->mForms;
	GetOwner()->register_handler(*this, &BoxLogic::OnBoxEvent);
	y_changed = 0;
}

void BoxLogic::Update()
{
	if (Parented)
	{
		if (Player_last_position != Player->mForms->Possesed_one->GetOwner()->mTransform->position)
		{
			glm::vec3 Movement = Player->mForms->Possesed_one->GetOwner()->mTransform->position - Player_last_position;
			GetOwner()->mTransform->position += Movement;

			Player_last_position = Player->mForms->Possesed_one->GetOwner()->mTransform->position;
			y_changed += abs(Movement.y);
		}
		if (!Player->mForms->IsPossesing)
			Parented = false;
		if (y_changed >= 10)
			Parented = false;
	}
}

void BoxLogic::load(Json::Value & val)
{
	active = val["BoxLogic"].get("active", true).asBool();//we load that the object is active(??)
	Parented = val["BoxLogic"].get("Parented", false).asBool();
	//Time2Reload = val["Bowman"].get("Time2Reload", 0.0f).asFloat();
	//Distance_2_shoot = val["Bowman"].get("Distance_2_shoot", 0.0f).asFloat();
}
void BoxLogic::save(Json::Value & val)
{
	val["BoxLogic"]["active"] = active;
	val["BoxLogic"]["Parented"] = Parented;
	//val["Bowman"]["Time2Reload"] = Time2Reload;//we get the value float 
	//val["Bowman"]["Distance_2_shoot"] = Distance_2_shoot;//we get the value float 
}
void BoxLogic::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("BoxLogic"))
	{
		//ImGui::InputFloat("Rotation", &(float)rotation);

		//ImGui::Checkbox("Moving", &Parented);

		ImGui::NewLine();
		if (ImGui::Button("Detach##BoxLogic"))
			GetOwner()->detach(this);
	}
}

void BoxLogic::OnBoxEvent(const BoxEvent &)
{
	//std::cout << "hola!"<<std::endl;
	float distance_from_player = abs(Player->mForms->Possesed_one->GetOwner()->mTransform->position.x - GetOwner()->mTransform->position.x);
	if (distance_from_player <= 150)
	{
		if (!Parented)
		{
			Parented = true;
			Player_last_position = Player->mForms->Possesed_one->GetOwner()->mTransform->position;
			y_changed = 0;
		}
		else
			Parented = false;
	}
}

#endif
