#ifndef BOX_LOGIC_H
#define BOX_LOGIC_H

#include "../../../../../Component/Component.h"
#include "../../../../../Events/event.h"

class BoxEvent;
class RigidBody;
class GameObject;

class BoxLogic : public Component
{
public:

	BoxLogic();
	virtual void Terminate() override;

	virtual void Initialize();
	virtual void Update();

	void OnBoxEvent(const BoxEvent &);

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

private:
	void adjust_knight_position(GameObject* knight);
	bool parented;
	glm::vec3 Player_last_position;
	GameObject * Player;
	GameObject * carrierKnight;
	int LookingAt;
	float DistanceBetw;

	float knight_separation = 191;
};

class BoxEvent : public Event
{
public:
	BoxEvent(GameObject * actor, bool c) : Boxowner(actor), carry(c) {}

	GameObject * Boxowner;
	bool carry;
};

#endif
