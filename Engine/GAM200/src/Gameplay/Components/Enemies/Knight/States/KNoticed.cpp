#include "KNoticed.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Input/Input.h"


KRealise::KRealise(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}

void KRealise::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("Stun");

	Time = 0.0f;

	//std::cout << "Noticed \n";
}
void KRealise::Exit()
{
	//std::cout << "Not anymore \n";
}

void KRealise::Update()
{
	Time += ImGui::GetIO().DeltaTime;

	if ((Time > knight->NoticeTime))
	{
		//if its not possessed anymore
		if (!knight->possesed)
		{
			m_owner->ChangeState("Chase");
			return;
		}
		else//if they possessed
		{
			m_owner->ChangeState("KPossIdle");
			return;
		}

	}
}
