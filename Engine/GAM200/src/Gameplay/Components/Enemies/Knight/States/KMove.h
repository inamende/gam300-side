#ifndef KMOVE_H
#define KMOVE_H

#include "../Knight.h"

class RigidBody;

class KMove : public KState
{
public:
	KMove(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
	void Exit();
	void MakeMove();

	RigidBody * rigid = NULL;
	std::string mSoundWhileWalkingPossessed;
	std::string mSoundWhileRunningNormally;
	std::string mSoundToPlay;

	bool Pulling;
	bool Pushing;

	std::array<std::string, 2> pull_anim{"Pull", "Pull2"};
	std::array<std::string, 2> push_anim{ "Push", "Push2" };
};

#endif
