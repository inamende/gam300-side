#ifndef KPATROL_H
#define KPATROL_H

#include "../Knight.h"

class KPatrol : public KState
{
public:
	KPatrol(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
	void Exit();
};

#endif
