#ifndef KFALLPREV_H
#define KFALLPREV_H

#include "../Knight.h"


class KFallPrevent : public KState
{
public:
	KFallPrevent(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Exit();
	void Update();

	float Time;
};

#endif
