#include "KPossIdle.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Input/Input.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gameplay/Components/Strawss/PlayerController.h"


KPossIdle::KPossIdle(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}

void KPossIdle::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	int i = (knight->BoxDirection < 0) ? 0 : 1;
	if(knight->WithBox)
		rend->ChangeAnimation(idle_anim[i]);
	else
		rend->ChangeAnimation("IdleSpectred");

	pc = nullptr;
}

void KPossIdle::Update()
{
	if (!strawss) {
		strawss = scene->FindObject("Strawss");
	}
	if (strawss) {
		pc = strawss->GetComponent<PlayerController>();
	}
	if (pc) {
		if (pc->GetPossEnemy() != knight) {
			m_owner->ChangeState("KStun");
		}
	}

	Decelerate();

	//if its not possessed anymore
	if (!knight->possesed)
	{
		if (knight->WithBox)
		{
			m_owner->ChangeState("KTakeBox");
			return;
		}

		m_owner->ChangeState("KStun");
		return;
	}
	if (KeyPressed(SDL_SCANCODE_D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		m_owner->ChangeState("KMove");
	}
	else if (KeyPressed(SDL_SCANCODE_A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		m_owner->ChangeState("KMove");
	}

	//if he shoots
	if (MouseDown(SDL_BUTTON_LEFT) || ButtonDown(SDL_CONTROLLER_BUTTON_X))
	{
		switch (knight->KnightType)
		{
		case 0:
			m_owner->ChangeState("Attack");
			break;
		case 1:
			m_owner->ChangeState("KTakeBox");
			break;
		default:
			//std::cout << "Attack not found, the type of knigth dont exist? \n";
			break;
		}
	}
}

void KPossIdle::Decelerate()
{
	if (abs(rigid->velocity.x) > 0)
		rigid->velocity.x -= (rigid->velocity.x) / knight->KmovementDrag;
}
