#include "KAttack.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "imGui/imgui.h"

#include "../../../../../Level/Scene.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

KAttack::KAttack(const char * name, Transform * t, RigidBody * r, Knight * k) : KState(name, t, r, k) {}

void KAttack::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	if(knight->possesed)
		rend->ChangeAnimation("AttackSpectred");
	else
		rend->ChangeAnimation("Attack");

	rigid->velocity.x = 0;

	if (!knight->possesed)
		knight->direction = (trans->position.x - knight->strawssTrans->position.x < 0) ? 1 : -1;

	attacked = false;
	
	PlaySoundNow = true;
	PossAttack = knight->possesed;
}

void KAttack::Update()
{
	if (PlaySoundNow)
	{
		PlaySoundNow = false;
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			//std::cout << "Playing attack" << std::endl;
			e->PlayOnce("KnightSwordSwing.mp3", false);
		}
	}
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->CheckEvent("Knigh1HandHit") == 7 && !attacked) {
		knight->EnemyAttack();
		//std::cout << rend->CheckEvent("Knigh1HandHit") << std::endl;
		attacked = true;
	}
	
	if (rend->CheckEvent("Knigh1HandHit") != 7) {
		attacked = false;
	}

	if (m_time_in_state >= KnightP::GetParam().attackTime)
	{
		PlaySoundNow = true;
		if (knight->possesed)
		{
			if(PossAttack)
				m_owner->ChangeState("KPossIdle");
			else
				m_owner->ChangeState("KPossessing");

			return;
		}
		if (glm::distance(glm::vec2(trans->position.x, trans->position.y), glm::vec2(knight->strawssTrans->position.x, knight->strawssTrans->position.y)) >= KnightP::GetParam().knightAttackStart)
		{
			if (knight->WithBox)
			{
				m_owner->ChangeState("KTakeBox");
				return;
			}
			m_owner->ChangeState("Chase");
		}
		else
			m_time_in_state = 0.f;
	}

	scene->DrawFancyCursor(glm::vec3(trans->position.x + KnightP::GetParam().knightAttackStart, trans->position.y, 99.0f), glm::vec4(1.0f, 0.f, .0f, 1.0f));
	scene->DrawFancyCursor(glm::vec3(trans->position.x - KnightP::GetParam().knightAttackStart, trans->position.y, 99.0f), glm::vec4(1.0f, 0.f, .0f, 1.0f));
}
void KAttack::Exit()
{
	//oscars additions
	if (rigid)
	{
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			e->StopVoice();
		}

	}
}
