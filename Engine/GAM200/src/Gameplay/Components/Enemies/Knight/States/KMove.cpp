#include "KMove.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Reso/Camera.h"
#include "../../../../../Gfx/Gfx.h"
#include "../../../../../Input/Input.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
KMove::KMove(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}

void KMove::Enter()
{
	Pulling = false;

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	if (knight->WithBox)
	{
		int i = (knight->BoxDirection < 0) ? 0 : 1;
		rend->ChangeAnimation(pull_anim[i]);
		Pulling = true;
	}
	else
		rend->ChangeAnimation("PossesedWalk");

	rigid = m_actor->mRigidBody;
	//oscars additions
	if (rigid)
	{
		mSoundWhileRunningNormally = "KnightRunning.mp3";
		mSoundWhileWalkingPossessed = "KnightWalking.mp3";
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			if (knight->possesed)
			{
				mSoundToPlay = mSoundWhileWalkingPossessed;

			}
			else
			{
				mSoundToPlay = mSoundWhileRunningNormally;
			}
			
		}
		////set it initially to pause
		//e->PlaySound(mSoundToPlay, 2);
		//e->GetVoice()->SetPause(true);
		e->PlayLooped(mSoundToPlay, false);
	}
}

void KMove::Update()
{
	int i = (knight->BoxDirection < 0) ? 0 : 1;
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();

	if (Pulling)//looping forever
		rend->LoopBetweenEvents("StartPulling", 10, "EndPulling", 1);



	//if its not possessed anymore
	if (!knight->possesed)
	{
		if (knight->WithBox)
		{
			m_owner->ChangeState("KTakeBox");
			return;
		}

		m_owner->ChangeState("KStun");
		return;
	}

	if (KeyPressed(Keyboard::D) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		if (knight->WithBox) {

			if (knight->BoxDirection == 1)
			{
				if (rend->currentanimation != push_anim[i])
					rend->ChangeAnimation(push_anim[i]);
			}
			else
				if (rend->currentanimation != pull_anim[i])
				{
					rend->ChangeAnimation(pull_anim[i]);
					Pulling = true;
				}
		}
		knight->direction = 1;
	}

	else if (KeyPressed(Keyboard::A) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		if (knight->WithBox) {
			SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();

			if (knight->BoxDirection == -1)
			{ 
				if (rend->currentanimation != push_anim[i])
					rend->ChangeAnimation(push_anim[i]);
			}
			else
				if (rend->currentanimation != pull_anim[i])
				{ 
					rend->ChangeAnimation(pull_anim[i]);
					Pulling = true;
				}
		}
		knight->direction = -1;
	}
	else
	{
		m_owner->ChangeState("KPossIdle");
	}

	MakeMove();

	//if he attacks
	if (MouseDown(SDL_BUTTON_LEFT) || ButtonDown(SDL_CONTROLLER_BUTTON_X))
	{
		switch (knight->KnightType)
		{
		case 0:
			m_owner->ChangeState("Attack");
			break;
		case 1:
			m_owner->ChangeState("KTakeBox");
			break;
		default:
			//std::cout << "Attack not found, the type of knigth dont exist? \n";
			break;
		}
	}
}
void KMove::Exit()
{
	//oscars additions
	if (rigid)
	{
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
			e->StopVoice();

	}
}
void KMove::MakeMove()
{
	if ((rigid->velocity.x * knight->direction) < 0)
		rigid->velocity.x += 50 * knight->direction;

	if (abs(rigid->velocity.x) <= knight->KmaxSpeed)
		rigid->velocity.x += knight->Kspeed * knight->direction;

	if (abs(rigid->velocity.x) > knight->KmaxSpeed)
		rigid->velocity.x = knight->KmaxSpeed * knight->direction;
}