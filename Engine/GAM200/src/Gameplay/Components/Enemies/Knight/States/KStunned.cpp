#include "kStunned.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Input/Input.h"

#include "spine/spine.h"

KStun::KStun(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}

void KStun::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("Stun");
	
	Time = 0.0f;
	m_IsStunLooping = false;
	//std::cout << "stunned \n";
	e = this->m_actor->GetComponent<SoundEmitter>();
	if (e)
	{
		//e->PlaySound("KnightWalking.mp3", 2);
		//
		//e->PlaySound("Grunt.mp3", 1);
		PlaySoundOnUpdate = true;
		e->PlayOnce("Grunt.mp3", true);
	}
}
void KStun::Exit()
{
	//std::cout << "Not anymore \n";
	e->StopVoice();
}
void KStun::Update()
{
	if (PlaySoundOnUpdate)
	{
		if (e && e->GetVoice())
			e->GetVoice()->SetPause(false);
	}
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();

	/*
	if (rend->CheckEvent("StunStart") == 13 && m_IsStunLooping == false)
	{
		m_IsStunLooping = true;

		// get the time in the animation, where the stun start event is. 
		m_StunStartTime = spTrackEntry_getAnimationTime(rend->animState->tracks[0]);

		//std::cout << "stun start " << spTrackEntry_getAnimationTime(rend->animState->tracks[0])  << "\n";
	}

	if (m_IsStunLooping)
	{

		//std::cout << "stun loop " << spTrackEntry_getAnimationTime(rend->animState->tracks[0]) << "\n";

		// update timer for the loop
		Time += ImGui::GetIO().DeltaTime;

		// Stun loop is finished, we should exit the state
		// but first wait for animation to finish
		if ((Time > knight->TimeStunned))
		{
			m_IsStunLooping = false;
			//std::cout << "stun time over -> wait for animation to finish \n";
		}

		// make sure that we haven't gotten to the stun end event
		else
		{
			if (rend->CheckEvent("StunEnd") == 12)
			{

				// assume that tracks[0] is the stun animation
				rend->animState->tracks[0]->trackTime = m_StunStartTime;
				//std::cout << "stun end-> LOOOP \n";

			}
		}
	}

	else if (Time != 0) // waiting for stun animation to finish
	{
		float currentTime = spTrackEntry_getAnimationTime(rend->animState->tracks[0]);
		if (currentTime == rend->animState->tracks[0]->animationEnd)
		{	
			//std::cout << "animation finished -> change state \n";

			//if its not possessed anymore
			if (!knight->possesed)
			{
				m_owner->ChangeState("Retreat");
				return;
			}
			else//if they possessed while stunned
			{
				m_owner->ChangeState("KPossIdle");
				return;
			}
		}
	}
	*/

	if (rend->LoopBetweenEvents("StunStart", 13, "StunEnd", 12, knight->TimeStunned))
	{
		if (!knight->possesed)
		{
			m_owner->ChangeState("Retreat");
			return;
		}
		else//if they possessed while stunned
		{
			m_owner->ChangeState("KPossessing");
			return;
		}
	}
	
	
}
