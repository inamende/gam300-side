#ifndef KTAKEBOX_H
#define KTAKEBOX_H

#include "../Knight.h"


class KTakeBox : public KState
{
public:
	KTakeBox(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
	
private:
	float Time;
	std::array<std::string, 2> grab_anim{"Grab", "Grab2"};
	std::array<std::string, 2> let_anim{ "LetTheBox", "LetTheBox2" };
};

#endif
