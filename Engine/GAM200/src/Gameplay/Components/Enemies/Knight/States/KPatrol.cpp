#include "KPatrol.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

KPatrol::KPatrol(const char * name, Transform * t, RigidBody * r, Knight * k) : KState(name, t, r, k) {}

void KPatrol::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("Walking");
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->PlayLooped("KnightWalking.mp3", true);
	}
}

void KPatrol::Update()
{
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->GetVoice() && e->GetVoice()->IsPaused())
			e->GetVoice()->SetPause(false);
	}
	if (knight->possesed)
	{
		m_owner->ChangeState("KPossessing");
		return;
	}

	if (knight && m_time_in_state >= knight->patrolTime)
		m_owner->ChangeState("Idle");

	if (knight->strawss)
		if (knight->playerSpotted)
			m_owner->ChangeState("KRealise");

	trans->position.x += knight->direction * KnightP::GetParam().patrolSpeed;
}
void KPatrol::Exit()
{
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			e->StopVoice();
		}

	}
}