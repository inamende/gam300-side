#ifndef KNIGHT_PARAM_H
#define KNIGHT_PARAM_H

struct KnightP
{
	static KnightP & GetParam()
	{
		static KnightP knight;
		return knight;
	}

	float patrolSpeed = 2.f;
	float retreatSpeed = 2.f;
	float knightAttackRate = 2.f;
	glm::vec2 visionLoss = glm::vec2(1000.f, 300.f);
	float knightAttackStart = 300.f;
	float chaseSpeed = 3.f;
	int attackFarme = 24;
	float attackTime = 1.5f;
	bool pulling_lever = false;
};

#endif
