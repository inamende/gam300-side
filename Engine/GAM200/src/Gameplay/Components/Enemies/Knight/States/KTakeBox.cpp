#include "KTakeBox.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Input/Input.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../Box/BoxLogic.h"

KTakeBox::KTakeBox(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k),
	Time(0.0f) {}

void KTakeBox::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	//update the data 
	Time = 0.0f;
	rigid->velocity.x = 0;

	//seee if we take the box
	global_handler.handle(BoxEvent(m_actor, true));

	int i = (knight->BoxDirection < 0) ? 0 : 1;
	if (knight->LeaveBox)
	{
		rend->ChangeAnimationWithOutLoop(let_anim[i]);
		knight->LeaveBox = false;
		return;
	}

	else if (knight->TakeBox)
	{
		rend->ChangeAnimationWithOutLoop(grab_anim[i]);
		knight->TakeBox = false;
	}
	else
	{
		if (knight->possesed)
			m_owner->ChangeState("KPossIdle");
		else
			m_owner->ChangeState("Patrol");
	}


}

void KTakeBox::Update()
{

	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{

		if (knight->possesed)
			m_owner->ChangeState("KPossIdle");
		else
			m_owner->ChangeState("KStun");
	}
}