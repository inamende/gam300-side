#ifndef KHEDE_H
#define KHEDE_H

#include "../Knight.h"


class KHeDe : public KState
{
public:
	KHeDe(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Exit();
	void Update();
};

#endif
