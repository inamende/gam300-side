#ifndef KIDLE_H
#define KIDLE_H

#include "../Knight.h"


class KIdle : public KState
{
public:
	KIdle(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
};

#endif
