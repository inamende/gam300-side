#include "KChase.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Utils/Utilities.h"
#include "../../../../../Physics/Comp/Collider.h"
#include "imGui/imgui.h"

#include "../../../../../Level/Scene.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

KChase::KChase(const char * name, Transform * t, RigidBody * r, Knight * k) : KState(name, t, r, k) {}

void KChase::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("Running");

	if (knight)
		Grow(knight->viewFrustum->GetTransform(), KnightP::GetParam().visionLoss.x, KnightP::GetParam().visionLoss.y);

	rigid->velocity.x = 0;
	//oscars additions
	CurrentTimer = 0.0f;
	WhistleDuration = 2.0f;
	//make sure that whistle sound effect is only played sparingly
	if (knight->WhistleSoundEffectHasPlayed == false)
	{
		PlayingWhistle = true;
	}
	else
	{
		PlayingWhistle = false;
	}
	
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			if (PlayingWhistle)
			{
				//e->PlaySound("SteamWhistle.mp3", 1);
				knight->WhistleSoundEffectHasPlayed = true;
				e->PlayOnce("SteamWhistle.mp3", false);
			}
			else
			{
				e->StopVoice();
				//e->PlaySound("KnightRunning.mp3", 2);
				e->PlayLooped("KnightRunning.mp3", false);
			}		
			e->GetVoice()->SetPause(true);
		}
	}
}

void KChase::Update()
{
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->GetVoice()->IsPaused())
		{
			e->GetVoice()->SetPause(false);
		}
	}
	if (PlayingWhistle)
	{
		//update timer
		CurrentTimer += ImGui::GetIO().DeltaTime;
		if (CurrentTimer >= WhistleDuration)
		{
			if (e)
			{
				e->StopVoice();
				e->PlayLooped("KnightRunning.mp3", false);
				PlayingWhistle = false;
			}
		}
	}
	
	if (!knight || !knight->strawss)
		return;

	if (!knight->playerSpotted)
		m_owner->ChangeState("Retreat");
		

	if (glm::distance(glm::vec2(trans->position.x, trans->position.y), glm::vec2(knight->strawssTrans->position.x, knight->strawssTrans->position.y)) < KnightP::GetParam().knightAttackStart)
		m_owner->ChangeState("Attack");

	trans->position.x += knight->direction * KnightP::GetParam().chaseSpeed;

	scene->DrawFancyCursor(glm::vec3(trans->position.x + KnightP::GetParam().knightAttackStart, trans->position.y, 99.0f), glm::vec4(1.0f, 0.f, .0f, 1.0f));
	scene->DrawFancyCursor(glm::vec3(trans->position.x - KnightP::GetParam().knightAttackStart, trans->position.y, 99.0f), glm::vec4(1.0f, 0.f, .0f, 1.0f));
}
void KChase::Exit()
{
	//oscars additions
	if (rigid)
	{
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			if (e->IsInit)
				e->StopVoice();
		}

	}
}
