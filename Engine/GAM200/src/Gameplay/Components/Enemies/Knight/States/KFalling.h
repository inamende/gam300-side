#ifndef KFALLING_H
#define KFALLING_H

#include "../Knight.h"


class CollisionStartedEvent;
class SkeletonRenderable;
class RigidBody;

class KFalling : public KState
{
public:
	KFalling(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Exit();
	void Update();

private:
	bool EndFalling;
	bool StartAnim;
	void OnCollisionStarted(const CollisionStartedEvent & object);
	SkeletonRenderable * rend;

};

#endif

