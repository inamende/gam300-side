#ifndef KSTUN_H
#define KSTUN_H

#include "../Knight.h"
//oscars additions
#include "../../../../../Audio/SoundEmitter.h"
#include "../../../../../ObjectManager/ObjectManager.h"

class KStun : public KState
{
public:
	KStun(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Exit();
	void Update();

	float Time;
	bool m_IsStunLooping;
	float m_StunStartTime;
	SoundEmitter * e = NULL;
	bool PlaySoundOnUpdate = true;
};

#endif
