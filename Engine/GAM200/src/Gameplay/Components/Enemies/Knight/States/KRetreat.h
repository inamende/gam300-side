#ifndef KRETREAT_H
#define KRETREAT_H

#include "../Knight.h"

class KRetreat : public KState
{
public:
	KRetreat(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
	void Exit();

};

#endif
