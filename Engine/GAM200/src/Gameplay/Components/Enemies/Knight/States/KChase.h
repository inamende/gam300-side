#ifndef KCHASE_H
#define KCHASE_H

#include "../Knight.h"

class KChase : public KState
{
public:
	KChase(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
	void Exit();
	//note this should be roughly the length of the whistle sound effect
	float WhistleDuration = 2;
	float CurrentTimer;
	bool PlayingWhistle;
};

#endif
