#include "KPossessing.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Input/Input.h"


KPossessing::KPossessing(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}
void KPossessing::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("BeingPossesed");

	Time = 0.0f;

	//std::cout << "Being possessed \n";
	possesed_knight = true;

}
void KPossessing::Exit()
{
	//std::cout << "Not anymore \n";
	possesed_knight = false;
}

void KPossessing::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if(rend->DoesThisEnd())
		m_owner->ChangeState("KPossIdle");
}
