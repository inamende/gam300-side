#ifndef KREACT_H
#define KREACT_H

#include "../Knight.h"


class KRealise : public KState
{
public:
	KRealise(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Exit();
	void Update();

	float Time;
};

#endif
