#ifndef KPIDLE_H
#define KPIDLE_H

#include "../Knight.h"

class PlayerController;

class KPossIdle : public KState
{
public:
	KPossIdle(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();

	void Decelerate();

	std::array<std::string, 2> idle_anim{ "IdleBox", "IdleBox2" };
private:
	GameObject *strawss;
	PlayerController *pc;
};

#endif
