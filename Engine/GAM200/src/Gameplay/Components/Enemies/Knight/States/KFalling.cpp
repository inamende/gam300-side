#include "KFalling.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/Animator.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Level/Scene.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Physics/Comp/RigidBody.h"
#include "../../../../../Physics/Comp/BoxCollider.h"
#include "../../../../../Physics/Comp/BoxColliderOriented.h"
#include "../../../../../Physics/Physics.h"


KFalling::KFalling(const char * name, Transform * t, RigidBody * r, Knight * k) : KState(name, t, r, k) {}

void KFalling::Enter()
{
	rend = m_actor->GetComponent<SkeletonRenderable>();

	if (!rend)
		return;

	rend->ChangeAnimationWithOutLoop("Fall");
	EndFalling = false;
	StartAnim = false;

	m_owner->GetOwner()->register_handler(*this, &KFalling::OnCollisionStarted);
}

void KFalling::Exit()
{
	m_owner->GetOwner()->unregister_handler(*this, &KFalling::OnCollisionStarted);
}

void KFalling::Update()
{
	if(rigid->velocity.y < 10)
		StartAnim = true;


	if (!EndFalling)
		rend->LoopBetweenEvents("FallLoopStart", 3, "FallLoopEnd", 2);

	if (StartAnim)
	{
		if (!EndFalling)
		{
			EndFalling = true;
			rend->ToEndEventLoopBetweenEvents();
		}
		else
		{
			//we hit the ground
			if (rend->DoesThisEnd())
				if (knight->possesed)
					m_owner->ChangeState("KPossIdle");
				else
					m_owner->ChangeState("Patrol");
		}
	}
}

void KFalling::OnCollisionStarted(const CollisionStartedEvent & object)
{
	if (!m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_3"))
		return;

	if (object.other_object->GetComponent<BoxCollider>())
	{
		if (CollideAABBs(m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_3"), object.other_object->GetComponent<BoxCollider>(), NULL))
		{
			StartAnim = true;
		}
	}
	else
	{
		if (CollideAABBs(m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_3"), object.other_object->GetComponent<BoxColliderOriented>(), NULL))
		{
			StartAnim = true;
		}
	}

}