#include "KIdle.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"


KIdle::KIdle(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}

void KIdle::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("Idle");
}

void KIdle::Update()
{
	if (!knight)
		return;


	if (knight->possesed)
	{
		m_owner->ChangeState("KPossessing");
		return;
	}

	if (m_time_in_state >= knight->idleTime)
	{
		m_owner->ChangeState("Patrol");
		knight->direction = -knight->direction;
	}

	if (knight->strawss)
		if (knight->playerSpotted)
			m_owner->ChangeState("KRealise");
}
