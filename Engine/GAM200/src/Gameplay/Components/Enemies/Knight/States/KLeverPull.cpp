#include "KLeverPull.h"
#include "KnightParam.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../ObjectManager/GameObject.h"

KLeverPull::KLeverPull(const char * name, Transform * t, RigidBody * r, Knight * k) :
	KState::KState(name, t, r, k)
{}

void KLeverPull::Enter() {
	KnightP::GetParam().pulling_lever = true;
	rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend)
		rend->ChangeAnimationWithOutLoop("Switch1");
	if (rigid) {
		rigid->velocity = vec3();
	}
}

void KLeverPull::Update() {
	if (!rend) rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend) {
		if (rend->DoesThisEnd()) {
			m_owner->ChangeState("KPossIdle");
			KnightP::GetParam().pulling_lever = false;
		}
	}
}

void KLeverPull::Exit() {

}