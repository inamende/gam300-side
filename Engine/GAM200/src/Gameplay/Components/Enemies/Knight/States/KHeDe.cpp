#include "KHeDe.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Editor/ImGuiEditor.h"
#include "../../../../../Input/Input.h"
#include "../../../../../Physics/Physics.h"
#include "../../../../../Physics/Comp/BoxCollider.h"


KHeDe::KHeDe(const char * name, Transform * t, RigidBody * r, Knight * k) : KState::KState(name, t, r, k) {}

void KHeDe::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->AddAnimationToQueue("Stun");
	//rend->ChangeAnimationWithOutLoop("Stun");

}
void KHeDe::Exit()
{
	RigidBody * Body = m_actor->GetComponent<RigidBody>();
	Body->autoComputeMass = true;
	physics->SwapContainer(m_actor->GetComponent<BoxCollider>(), false);
}

void KHeDe::Update()
{
	//ANOTHER REST IN PEPERONI
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();

	if(rend->LoopBetweenEvents("StunStart", 13, "StunEnd", 12, knight->DeathTime))
		m_owner->ChangeState("Patrol");
		
}
