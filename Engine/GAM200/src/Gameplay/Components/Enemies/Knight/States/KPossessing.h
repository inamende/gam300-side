#ifndef KPOSSESSING_H
#define KPOSSESSING_H

#include "../Knight.h"


class KPossessing : public KState
{
public:
	KPossessing(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Exit();
	void Update();

	float Time;
};

#endif
