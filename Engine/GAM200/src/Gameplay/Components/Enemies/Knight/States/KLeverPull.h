#ifndef KLEVERPULL_H
#define KLEVERPULL_H

#include "../Knight.h"

class Transform;
class RigidBody;
class Knight;
class SkeletonRenderable;

class KLeverPull : public KState {
public:
	KLeverPull(const char * name, Transform * t, RigidBody * r, Knight * k);
	void Enter();
	void Update();
	void Exit();
private:
	SkeletonRenderable * rend;

};

#endif
