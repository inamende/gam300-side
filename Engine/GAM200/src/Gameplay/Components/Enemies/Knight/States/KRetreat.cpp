#include "KRetreat.h"

#include "KnightParam.h"

#include "../../../../../ObjectManager/GameObject.h"
#include "../../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../../../../Gfx/Comp/Transform.h"
#include "../../../../../Utils/Utilities.h"
#include "../../../../../Physics/Comp/Collider.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
KRetreat::KRetreat(const char * name, Transform * t, RigidBody * r, Knight * k) : KState(name, t, r, k) {}

void KRetreat::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;

	rend->ChangeAnimation("Walking");

	if (knight)
		Grow(knight->viewFrustum->GetTransform(), knight->visionRange.x, knight->visionRange.y);

	rigid->velocity.x = 0;
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			e->PlayLooped("KnightRunning.mp3", true);
		}
	}
}

void KRetreat::Update()
{
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->GetVoice() && e->GetVoice()->IsPaused())
		{
			e->GetVoice()->SetPause(false);
		}
	}
	if (!knight)
		return;

	if (knight->possesed)
	{
		m_owner->ChangeState("KPossessing");
		return;
	}

	knight->direction = (trans->position.x - knight->retreatPoint < 0) ? 1 : -1;

	if (glm::distance(trans->position.x, knight->retreatPoint) <= 1.5f)
		m_owner->ChangeState("Idle");


	trans->position.x += knight->direction * KnightP::GetParam().retreatSpeed;

	if (knight->strawss)
		if (knight->playerSpotted)
			m_owner->ChangeState("Chase");
}
void KRetreat::Exit()
{
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			e->StopVoice();
		}
		knight->WhistleSoundEffectHasPlayed = false;
	}
}
