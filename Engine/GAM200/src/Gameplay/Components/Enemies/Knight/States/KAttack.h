#ifndef KATTACK_H
#define KATTACK_H

#include "../Knight.h"

class KAttack : public KState
{
public:
	KAttack(const char * name, Transform * t, RigidBody * r, Knight * k);

	void Enter();
	void Update();
	void Exit();

	float attacked = false;
	bool PossAttack;
	bool PlaySoundNow = false;
};

#endif
