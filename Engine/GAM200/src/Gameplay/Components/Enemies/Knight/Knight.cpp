#include "Knight.h"

#include "../../../../Editor/ImGuiEditor.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../../../Utils/Utilities.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../InteractableObjects/PlayerInteractable.h"
#include "../../InteractableObjects/Lever.h"

#include "States/KIdle.h"
#include "States/KPatrol.h"
#include "States/KRetreat.h"
#include "States/KChase.h"
#include "States/KAttack.h"
//possessed ones
#include "States/KMove.h"
#include "States/KStunned.h"
#include "States/KTakeBox.h"
#include "States/KNoticed.h"
#include "States/KPossIdle.h"
#include "States/KPossessing.h"
#include "States/KFalling.h"
#include "States/KHeDe.h"
#include "States/KLeverPull.h"
#include "../../Attacks/Attack.h"
#include "../../../../Input/Input.h"

#include "Box/BoxLogic.h"


KState::KState(const char * name, Transform * t, RigidBody * r, Knight * k) : State::State(name), trans(t), rigid(r), knight(k) {}

Knight::Knight() : Enemy::Enemy(), target("Strawss"), spawnAttack("sword_attack"), strawssTrans(NULL), trans(NULL)
					, KmaxSpeed(500.0f), Kspeed(100.0f), KmovementDrag(100.0f), KnightType(0), TakingBox(0.0f), TimeStunned(0.0f)
					, TakeBox(false), LeaveBox(false), WithBox(false), NoticeTime(0.0f), BoxDirection(1.0)
{
}
Knight::~Knight() {}

void Knight::Initialize()
{
	Enemy::Initialize();
	initialScale = fabs(GetOwner()->mTransform->scale.x);

	viewFrustum = GetOwner()->GetComponent<BoxCollider>("BoxCollider_1");
	KRigidBody = GetOwner()->GetComponent<RigidBody>();

	m_actor = GetOwner();
	AddState(DBG_NEW KIdle("Idle", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KPatrol("Patrol", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KRetreat("Retreat", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KChase("Chase", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KAttack("Attack", m_actor->mTransform, m_actor->mRigidBody, this));

	//possession 
	AddState(DBG_NEW KPossessing("KPossessing", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KMove("KMove", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KPossIdle("KPossIdle", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KTakeBox("KTakeBox", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KStun("KStun", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KRealise("KRealise", m_actor->mTransform, m_actor->mRigidBody, this));
	AddState(DBG_NEW KFalling("KFalling", m_actor->mTransform, m_actor->mRigidBody, this));
	//he he, siempre me olvido xdd
	AddState(DBG_NEW KHeDe("HeDe", m_actor->mTransform, m_actor->mRigidBody, this));

	AddState(DBG_NEW KLeverPull("KLeverPull", m_actor->mTransform, m_actor->mRigidBody, this));


	SetInitState("KStun");
	mInitialState->InternalEnter();

	trans = m_actor->mTransform;

	GetOwner()->register_handler(*this, &Knight::OnCollisionStarted);
	GetOwner()->register_handler(*this, &Knight::OnCollisionPersisted);
	GetOwner()->register_handler(*this, &Knight::OnCollisionEnded);

	SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
	if (rend)
		rend->skin = skins[currSkin];
	//oscars additions
	WhistleSoundEffectHasPlayed = false;

	//fall check box
	FallCheck = GetOwner()->GetComponent<BoxCollider>("BoxCollider_3");
}

void Knight::Update()
{
	///if (KeyDown(Keyboard::Q)) {
	///	ChangeState("KLeverPull");
	///}
	if (!p_in) p_in = GetOwner()->GetComponent<PlayerInteractable>();
	if (p_in && p_in->can_activate) {
		ChangeState("KLeverPull");
		p_in->can_activate = false;
		if (lever) {
			direction = (lever->mTransform->position.x - GetOwner()->mTransform->position.x > 0.f) ? 1 : -1;
		}
	}


	//cheat to test the possession 
	/*
	if (KeyDown(Keyboard::ONE))
	{
		if (possesed)
			possesed = false;
		else
			possesed = true;
	}*/

	if (!strawss)
	{
		strawss = objectManager->FindObjectByName(target);

		if (strawss)
			strawssTrans = strawss->mTransform;
	}

	if (!this->WithBox)
		trans->scale.x = (direction > 0) ? -initialScale : initialScale;

	//if (KeyPressed(Keyboard.J))
	//{
	//	ChangeState("KStun");
	//}

	if (KRigidBody->velocity.y < -100 && (mCurrentState != GetState("KFalling")))
	{
		if (WithBox)
			global_handler.handle(BoxEvent(m_actor, true));
		LeaveBox = false;

		ChangeState("KFalling");
	}

	StatusUpdate();
}

void Knight::Terminate() {
	GetOwner()->unregister_handler(*this, &Knight::OnCollisionStarted);
	GetOwner()->unregister_handler(*this, &Knight::OnCollisionPersisted);
	GetOwner()->unregister_handler(*this, &Knight::OnCollisionEnded);
}

void Knight::load(Json::Value & val)
{
	active = val["Knight"].get("active", true).asBool();

	idleTime = val["Knight"].get("idle_time", idleTime).asFloat();
	patrolTime = val["Knight"].get("patrol_time", patrolTime).asFloat();
	retreatPoint = val["Knight"].get("retreat_point", 0.f).asFloat();
	visionRange.x = val["Knight"]["vision_range"].get("x", visionRange.x).asFloat();
	visionRange.y = val["Knight"]["vision_range"].get("y", visionRange.y).asFloat();

	target = val["Knight"].get("target", "Strawss").asString();
	currSkin = val["Knight"].get("currSkin", 0).asInt();
	KmaxSpeed = val["Knight"].get("KmaxSpeed", 0.0f).asFloat();
	Kspeed = val["Knight"].get("Kspeed", 0.0f).asFloat();
	KmovementDrag = val["Knight"].get("KmovementDrag", 0.0f).asFloat();
	TakingBox = val["Knight"].get("TakingBox", 0.0f).asFloat();
	TimeStunned = val["Knight"].get("TimeStunned", 0.0f).asFloat();
	KnightType = val["Knight"].get("Knight_type", 0.0f).asUInt();
	NoticeTime = val["Knight"].get("NoticeTime", 0.0f).asFloat();
	DeathTime = val["Knight"].get("DeathTime", 0.0f).asFloat();
	const_retreat_point = val["Knight"].get("const_retreat", false).asBool();
	
}

void Knight::save(Json::Value & val)
{
	val["Knight"]["active"] = active;

	val["Knight"]["idle_time"] = idleTime;
	val["Knight"]["patrol_time"] = patrolTime;
	val["Knight"]["retreat_point"] = retreatPoint;
	val["Knight"]["vision_range"]["x"] = visionRange.x;
	val["Knight"]["vision_range"]["y"] = visionRange.y;

	val["Knight"]["target"] = target;
	val["Knight"]["currSkin"] = currSkin;
	val["Knight"]["Kspeed"] = Kspeed;
	val["Knight"]["KmaxSpeed"] = KmaxSpeed;
	val["Knight"]["KmovementDrag"] = KmovementDrag;
	val["Knight"]["TakingBox"] = TakingBox;
	val["Knight"]["TimeStunned"] = TimeStunned;
	val["Knight"]["NoticeTime"] = NoticeTime;
	val["Knight"]["DeathTime"] = DeathTime;

	val["Knight"]["Knight_type"] = KnightType;
	val["Knight"]["const_retreat"] = const_retreat_point;
}

void Knight::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Knight"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Type##Knight"))
			ImGui::OpenPopup("type");
		ImGui::SameLine();
		ImGui::Text(imGuiEditor->enemytypeNames[KnightType].c_str());

		if (ImGui::BeginPopup("type"))
		{
			ImGui::Text("Type");
			ImGui::Separator();

			for (unsigned i = 0; i < imGuiEditor->enemytypeNames.size(); i++)
				if (ImGui::Selectable(imGuiEditor->enemytypeNames[i].c_str()))
				{
					KnightType = i;
				}

			ImGui::EndPopup();
		}
		ImGui::NewLine();

		ImGui::Checkbox("const retreat", &const_retreat_point);
		if (ImGui::Button("Set Retreat Point"))
			retreatPoint = trans->position.x;
		ImGui::SameLine();
		ImGui::InputFloat("Retreat Point##KnightBreakerMachine", &retreatPoint);
		ImGui::NewLine();
		ImGui::InputFloat("Idle Time##KnightBreakerMachine", &idleTime);
		ImGui::NewLine();
		ImGui::InputFloat("Patrol Time##KnightBreakerMachine", &patrolTime);
		ImGui::NewLine();
		ImGuiEditor::InputFloat2("Vision Range##KnightBreakerMachine", visionRange);
		ImGui::NewLine();

		if (ImGuiEditor::InputText("Target##KnightBreakerMachine", target))
			strawss = objectManager->FindObjectByName(target);


		ImGui::NewLine();

		ImGui::InputFloat("MaxSpeed "	    , &(float)KmaxSpeed);
		ImGui::InputFloat("SpeedWalking"    , &(float)Kspeed);
		ImGui::InputFloat("Drag"		    , &(float)KmovementDrag);
		ImGui::NewLine();
		ImGui::InputFloat("Time taking box" , &(float)TakingBox);
		ImGui::InputFloat("Stunned time"	, &(float)TimeStunned);
		ImGui::InputFloat("Notice time"		, &(float)NoticeTime);
		ImGui::InputFloat("Time death"		, &(float)DeathTime);

		ImGui::NewLine();
		static int sk = currSkin;
		if (ImGui::InputInt("skin", &sk))
		{
			if (sk >= skins.size())
				sk = 0;

			currSkin = sk;

			SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
			if (rend)
			{
				rend->skin = skins[currSkin];
				rend->UploadData();
			}
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##KnightBreakerMachine"))
			GetOwner()->detach(this);
	}
}

void Knight::EnemyAttack()
{
	Transform * tr = m_actor->mTransform;
	Collider * col = m_actor->GetComponent<BoxCollider>();

	if (col && tr)
		CreateAtack(spawnAttack, glm::vec2(tr->position.x + col->GetTransform()->scale.x * 0.7f * direction, tr->position.y + col->GetTransform()->scale.y * 0.4), direction, GetOwner());
}

void Knight::OnCollisionStarted(const CollisionStartedEvent & object)
{
	// check the collision started with any collider the knight owns
	if (object.other_object->GetName() == target)
		playerSpotted = true;
}

void Knight::OnCollisionPersisted(const CollisionPersistedEvent & spot) {
	
}

void Knight::OnCollisionEnded(const CollisionEndedEvent & spot)
{
	// check the collision end with the frustrum, not with the body or whatever
	if (spot.other_object->GetName() == target)
		if (!CollideAABBs(viewFrustum, spot.other_object->GetComponent<BoxCollider>(), NULL))
			playerSpotted = false;
	lever = nullptr;
}