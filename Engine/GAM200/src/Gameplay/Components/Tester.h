#ifndef TESTER_H
#define TESTER_H

//#include "../../Component/Component.h"

#include "../../Gfx/Comp/Renderable.h"

class Tester : public Renderable
{
public:
	Tester();
	~Tester();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

#endif
