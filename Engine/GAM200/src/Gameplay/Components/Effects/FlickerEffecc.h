#ifndef FLICKER_EFECC_H
#define FLICKER_EFECC_H

#include "../../../Component/Component.h"

class FlickerEfecc : public Component
{
 public:

	 virtual void Initialize();
	 virtual void Update();

	 virtual void load(Json::Value & val);
	 virtual void save(Json::Value & val);
	 virtual void WriteToImguiWindow();

private:
	float count = 0;
	float timmer = 0;
	float start = 0;
	float end = 0;

	float init_phi;
	float phi;
	float from = 0.1f;
	float to = 0.3f;

};

#endif