#include "FogMove.h"

#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Gfx/Comp/Renderable.h"
#include "../../../Utils/Utilities.h"
#include "imgui/imgui.h"

FogMove::FogMove() :
	rend(nullptr)
{}
FogMove::~FogMove() {}

void FogMove::Initialize() {
	dt = 0.f;
	initial_pos = GetOwner()->mTransform->position.x;
}

void FogMove::Update() {
	if (!rend) rend = GetOwner()->GetComponent<Renderable>();
	if (!rend) return;
	static float start = 1.0;
	static float end = 0.0;
	if (rend->color.a < 0.f) {
		start = 0.0f;
		end = 1.0f;
		dt = 0;
		GetOwner()->mTransform->position.x = initial_pos;
	}
	else if (rend->color.a > 1.f) {
		start = 1.0f;
		end = 0.0f;
		dt = 0;
	}
	rend->color.a = Lerp<float>(start,end, dt);
	GetOwner()->mTransform->position.x -= dt;
	dt += speed * ImGui::GetIO().DeltaTime;
}

void FogMove::Terminate() {}

void FogMove::load(Json::Value & val) {
	active = val["FogMove"].get("active", true).asBool();
}

void FogMove::save(Json::Value & val) {
	val["FogMove"]["active"] = active;
}

void FogMove::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("FogMove")) {
		ImGui::NewLine();
		if (ImGui::Button("Detach##FogMove"))
			GetOwner()->detach(this);
	}
}