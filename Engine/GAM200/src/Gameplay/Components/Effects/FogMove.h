#ifndef FOG_MOVE_H
#define FOG_MOVE_H

#include "../../../Component/Component.h"

class Renderable;

class FogMove : public Component {
public:
	FogMove();
	~FogMove();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	float initial_pos;
	Renderable *rend;
	float speed = 0.1;
	float dt;
};

#endif
