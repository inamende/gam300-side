#include "FlickerEffecc.h"

#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Light.h"
#include "../../../Utils/Utilities.h"
#include "imGui/imgui.h"

void FlickerEfecc::Initialize() {
	init_phi = acos(GetOwner()->GetComponent<Light>()->spotCosTheta);
	phi = GetOwner()->GetComponent<Light>()->spotCosTheta;
}

void FlickerEfecc::Update()
{
	if (active)
	{
		if (timmer > 0.04f)
		{
			timmer = 0;
			count = 0;
			start = RandomFloat(0.1f, from);
			end = RandomFloat(from, to);
		}

		GetOwner()->GetComponent<Light>()->spotCosTheta = Lerp(cos(init_phi + start), cos(init_phi + end), count);
		count += 0.01f;

		timmer += 0.01f;
	}
}

void FlickerEfecc::load(Json::Value & val)
{
	active = val["FlickerEfecc"].get("active", true).asBool();

	from = val["FlickerEfecc"].get("from", 0.104).asFloat();
	to = val["FlickerEfecc"].get("to", 0.3).asFloat();
}

void FlickerEfecc::save(Json::Value & val) 
{
	GetOwner()->GetComponent<Light>()->spotCosTheta = phi;
	val["FlickerEfecc"]["active"] = active;

	val["FlickerEfecc"]["from"] = from;
	val["FlickerEfecc"]["to"] = to;
}

void FlickerEfecc::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("FlickerEfecc"))
	{
		ImGui::Checkbox("active##FlickerEfecc", &active);

		ImGui::SliderFloat("from##FlickerEfecc", &from, 0.0, 0.0);
		ImGui::SliderFloat("to##FlickerEfecc", &to, 0.0, 0.0);

		ImGui::NewLine();
		if (ImGui::Button("Detach##FlickerEfecc"))
			GetOwner()->detach(this);
	}	
}