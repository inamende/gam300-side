#include "CheckpointAnimatorController.h"
#include "States\AppearHierba.h"
#include "States\AppearPiedra.h"
#include "States\AppearTierra.h"
#include "States\IdleEnterradoHierba.h"
#include "States\IdleEnterradoPiedra.h"
#include "States\IdleEnterradoTierra.h"
#include "States\IdleHierba.h"
#include "States\IdlePiedra.h"
#include "States\IdleTierra.h"
#include "../../../Gfx/Gfx.h"
#include "../../../Editor/ImGuiEditor.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Animator.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "../../../Gfx/Comp/SkeletonRenderable.h"
#include "../../Components/Strawss/PlayerController.h"

CheckpointAnimatorController::CheckpointAnimatorController(): myType(Hierba){}
CheckpointAnimatorController::~CheckpointAnimatorController(){}

void CheckpointAnimatorController::Initialize()
{
	activated = false;
	m_actor = GetOwner();
	//AddState(DBG_NEW Underground("Underground"));
	//SetInitState("Underground");
	//mInitialState->InternalEnter();
	SkeletonRenderable * rend = GetOwner()->GetComponent<SkeletonRenderable>();
	if (rend)
	{
		//rend->skin = skins[currSkin];
		rend->skin = "default";
	}
	//AddState(DBG_NEW Underground("Underground"));
	AddState(DBG_NEW AppearHierba("AppearHierba"));
	AddState(DBG_NEW AppearPiedra("AppearPiedra"));
	AddState(DBG_NEW AppearTierra("AppearTierra"));

	AddState(DBG_NEW IdleEnterradoHierba("IdleEnterradoHierba"));
	AddState(DBG_NEW IdleEnterradoPiedra("IdleEnterradoPiedra"));
	AddState(DBG_NEW IdleEnterradoTierra("IdleEnterradoTierra"));

	AddState(DBG_NEW IdleHierba("IdleHierba"));
	AddState(DBG_NEW IdlePiedra("IdlePiedra"));
	AddState(DBG_NEW IdleTierra("IdleTierra"));

	//add states based on the variable
	if (myType == Hierba)
	{
		
		SetInitState("IdleEnterradoHierba");
		mInitialState->InternalEnter();

	}
	else if (myType == Piedra)
	{
		//AddState(DBG_NEW Underground("Underground"));

		SetInitState("IdleEnterradoPiedra");
		mInitialState->InternalEnter();

	}
	else
	{
		//AddState(DBG_NEW Underground("Underground"));

		SetInitState("IdleEnterradoTierra");
		mInitialState->InternalEnter();
	}
	
	//register events
	GetOwner()->register_handler(*this, &CheckpointAnimatorController::OnCollisionStarted);

}
void CheckpointAnimatorController::Update()
{
	StatusUpdate();
}
void CheckpointAnimatorController::Terminate()
{
	GetOwner()->unregister_handler(*this, &CheckpointAnimatorController::OnCollisionStarted);
}

void CheckpointAnimatorController::load(Json::Value & val)
{
	active = val["CheckpointAnimatorController"].get("active", true).asBool();
	myType = (AnimType)val["CheckpointAnimatorController"].get("myType", true).asInt();

}
void CheckpointAnimatorController::save(Json::Value & val)
{
	val["CheckpointAnimatorController"]["active"] = active;
	val["CheckpointAnimatorController"]["myType"] = myType;

}
void CheckpointAnimatorController::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("CheckpointAnimatorController"))
	{
		//ImGui::InputInt("stateToPlay", &stateToPlay);
		int type = (int)myType;
		if (ImGui::InputInt("myType", &type))
		{
			myType = (AnimType)type;
		}

		if (ImGui::Button("Detach##JuiceBar"))
			GetOwner()->detach(this);
	}
}

void CheckpointAnimatorController::OnCollisionStarted(const CollisionStartedEvent & e)
{
	BoxCollider * StrawssCollider = NULL;
	//lets see if the object we collided with is strawss
	if (e.other_object->GetName() == "Strawss")
	{
		StrawssCollider = e.other_object->GetComponent<BoxCollider>();
	}
	//sanity check
	if (!StrawssCollider)
	{
		return;
	}
	//second sanity check to make sure strawss is in the correct state
	PlayerController * p = NULL;
	p = e.other_object->GetComponent<PlayerController>();
	if (p != NULL)
	{
		if (p->mCurrentState == NULL)
		{
			return;
		}
		else
		{
			//this makes sure that straws is in the correct state to activate the animation
			if ((p->mCurrentState->m_name == "Spectre") || (p->mCurrentState->m_name == "Dash")
				|| (p->mCurrentState->m_name == "ChargeDash") || (p->mCurrentState->m_name == "DashRecovery")
				|| (p->mCurrentState->m_name == "Die") || (p->mCurrentState->m_name == "FlyAway")
				|| (p->mCurrentState->m_name == "GoBackToHumanForm"))
			{
				return;
			}
		}
	}
	//the big detector collider, NOTE, it must be collider 1, not collider 0
	//collider 0 is for the actual checkpoint for the juicebar
	BoxCollider * Detector = GetOwner()->GetComponent<BoxCollider>("BoxCollider_1");
	//2nd sanity check
	if (!Detector)
	{
		return;
	}
	//check for collision
	if (CollideAABBs(StrawssCollider, Detector, NULL))
	{
		StartAppearAnim();
	}

}
void CheckpointAnimatorController::StartAppearAnim()
{
	if (activated == false)
	{
		activated = true;
		if (myType == Hierba)
		{
			ChangeState("AppearHierba");
		}
		else if (myType == Piedra)
		{
			ChangeState("AppearPiedra");
		}
		else
		{
			ChangeState("AppearTierra");
		}
	}
	
}