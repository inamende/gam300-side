#pragma once
#include "../../../StateMachine/StateMachine.h"

class CollisionStartedEvent;

enum AnimType
{
	Hierba = 1,
	Piedra = 2,
	Tierra = 3
};
class CheckpointAnimatorController : public StateMachine
{
public:
	CheckpointAnimatorController();
	~CheckpointAnimatorController();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	
	void OnCollisionStarted(const CollisionStartedEvent & e);
	void StartAppearAnim();
private:
	int myType;
	bool activated = false;
};