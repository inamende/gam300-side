#include "IdleEnterradoTierra.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

IdleEnterradoTierra::IdleEnterradoTierra(const char * name): State(name){}

void IdleEnterradoTierra::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimation("IdleEnterradoTierra");
}
void IdleEnterradoTierra::Update()
{

}
void IdleEnterradoTierra::Exit()
{

}