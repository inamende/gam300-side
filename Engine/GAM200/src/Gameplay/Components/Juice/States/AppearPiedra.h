#pragma once
#include "../../../../StateMachine/StateMachine.h"

class AppearPiedra : public State
{
public:
	AppearPiedra(const char * name);
	void Enter();
	void Update();
	void Exit();
};