#include "AppearPiedra.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

AppearPiedra::AppearPiedra(const char * name): State(name){}

void AppearPiedra::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	//this will only loop once and change once it is done
	rend->ChangeAnimationWithOutLoop("AppearPiedra");
}
void AppearPiedra::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{
		rend->ChangeAnimation("IdlePiedra");
	}
}
void AppearPiedra::Exit()
{

}