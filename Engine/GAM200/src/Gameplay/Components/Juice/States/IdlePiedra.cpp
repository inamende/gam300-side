#include "IdlePiedra.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

IdlePiedra::IdlePiedra(const char * name): State(name){}

void IdlePiedra::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimation("IdlePiedra");
}
void IdlePiedra::Update()
{

}
void IdlePiedra::Exit()
{

}