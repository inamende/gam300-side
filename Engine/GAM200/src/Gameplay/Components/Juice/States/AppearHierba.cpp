#include "AppearHierba.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"
AppearHierba::AppearHierba(const char * name): State(name){}

void AppearHierba::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if(!rend)
		return;
	//this will only loop once and change once it is done
	rend->ChangeAnimationWithOutLoop("AppearHierba");
}
void AppearHierba::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{
		rend->ChangeAnimation("IdleHierba");
	}
}
void AppearHierba::Exit()
{

}