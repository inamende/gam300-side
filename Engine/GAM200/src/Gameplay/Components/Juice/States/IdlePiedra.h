#pragma once
#include "../../../../StateMachine/StateMachine.h"

class IdlePiedra : public State
{
public:
	IdlePiedra(const char * name);
	void Enter();
	void Update();
	void Exit();
};