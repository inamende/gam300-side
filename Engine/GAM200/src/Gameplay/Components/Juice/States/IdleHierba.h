#pragma once
#include "../../../../StateMachine/StateMachine.h"

class IdleHierba : public State
{
public:
	IdleHierba(const char * name);
	void Enter();
	void Update();
	void Exit();
};