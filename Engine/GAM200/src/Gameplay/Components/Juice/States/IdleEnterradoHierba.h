#pragma once
#include "../../../../StateMachine/StateMachine.h"

class IdleEnterradoHierba : public State
{
public:
	IdleEnterradoHierba(const char * name);
	void Enter();
	void Update();
	void Exit();
};