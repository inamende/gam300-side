#pragma once
#include "../../../../StateMachine/StateMachine.h"

class AppearHierba : public State
{
public:
	AppearHierba(const char * name);
	void Enter();
	void Update();
	void Exit();
};