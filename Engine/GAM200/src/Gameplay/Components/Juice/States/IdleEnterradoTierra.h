#pragma once
#include "../../../../StateMachine/StateMachine.h"

class IdleEnterradoTierra : public State
{
public:
	IdleEnterradoTierra(const char * name);
	void Enter();
	void Update();
	void Exit();
};