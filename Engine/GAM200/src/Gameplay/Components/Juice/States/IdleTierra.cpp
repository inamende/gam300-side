#include "IdleTierra.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

IdleTierra::IdleTierra(const char * name): State(name){}

void IdleTierra::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimation("IdleTierra");
}
void IdleTierra::Update()
{

}
void IdleTierra::Exit()
{

}