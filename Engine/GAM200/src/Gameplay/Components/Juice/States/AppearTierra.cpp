#include "AppearTierra.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

AppearTierra::AppearTierra(const char * name): State(name){}

void AppearTierra::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	//this will only loop once and change once it is done
	rend->ChangeAnimationWithOutLoop("AppearTierra");
}
void AppearTierra::Update()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (rend->DoesThisEnd())
	{
		rend->ChangeAnimation("IdleTierra");
	}
}
void AppearTierra::Exit()
{

}