#pragma once
#include "../../../../StateMachine/StateMachine.h"

class IdleEnterradoPiedra : public State
{
public:
	IdleEnterradoPiedra(const char * name);
	void Enter();
	void Update();
	void Exit();
};