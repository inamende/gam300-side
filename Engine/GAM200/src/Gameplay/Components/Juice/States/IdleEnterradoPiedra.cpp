#include "IdleEnterradoPiedra.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

IdleEnterradoPiedra::IdleEnterradoPiedra(const char * name): State(name){}

void IdleEnterradoPiedra::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimation("IdleEnterradoPiedra");
}
void IdleEnterradoPiedra::Update()
{

}
void IdleEnterradoPiedra::Exit()
{

}