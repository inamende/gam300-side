#include "IdleHierba.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"
IdleHierba::IdleHierba(const char * name): State(name){}

void IdleHierba::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimation("IdleHierba");
}
void IdleHierba::Update()
{

}
void IdleHierba::Exit()
{

}