#pragma once
#include "../../../../StateMachine/StateMachine.h"

class AppearTierra : public State
{
public:
	AppearTierra(const char * name);
	void Enter();
	void Update();
	void Exit();
};