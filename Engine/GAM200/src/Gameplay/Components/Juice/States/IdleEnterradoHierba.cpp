#include "IdleEnterradoHierba.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "ImGui/imgui.h"
#include "../../../../Gfx/Comp/SkeletonRenderable.h"

IdleEnterradoHierba::IdleEnterradoHierba(const char * name): State(name){}

void IdleEnterradoHierba::Enter()
{
	SkeletonRenderable * rend = m_actor->GetComponent<SkeletonRenderable>();
	if (!rend)
		return;
	rend->ChangeAnimation("IdleEnterradoHierba");
}
void IdleEnterradoHierba::Update()
{

}
void IdleEnterradoHierba::Exit()
{

}