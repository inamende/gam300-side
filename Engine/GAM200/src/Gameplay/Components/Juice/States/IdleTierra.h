#pragma once
#include "../../../../StateMachine/StateMachine.h"

class IdleTierra : public State
{
public:
	IdleTierra(const char * name);
	void Enter();
	void Update();
	void Exit();
};