#pragma once
#include "../../../Component/Component.h"
#include "../Strawss/States/StrawssParam.h"

class Transform;
//for the juice boxes and the gargoyle
class CollisionStartedEvent;
//class CollisionEndedEvent;

class JuiceBar : public Component
{
public:
	JuiceBar();
	~JuiceBar();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void RestoreJuice(float hp);
	void RestoreJuiceAndCheckpoint();
	bool AbilityUsageCheck();
	void StrawssDied();

	void OnCollisionStarted(const CollisionStartedEvent & e);
	//void OnCollisionEnded(const CollisionEndedEvent & e);

	Transform * GetPlayerTransform();
	Transform * GetCheckpointTransform();
	GameObject * GetJuiceBarUIObject();
	//helper functions for the UI component
	float get_max_juice();
	float * GetMaxJuice() // are you serious, A POINTER?
	{
		return & mMaxJuice;
	}
	float * GetCurrentJuice()
	{
		return &mCurrentJuice;
	}
	float * GetAbilityCost()
	{
		return & mAbilityCost;
	}
private:
	float mMaxJuice;
	float mCurrentJuice;
	float mAbilityCost;
	Transform * mTransformPointer;
	Transform * mCheckPointTransform;
	//these will be set by the imgui window
	std::string JuiceBoxName;
	std::string CheckpointName;
	std::string JuiceBarUIName;
	//the oject representing the juicebar UI object the player will see and that will be animated
	GameObject * mJuiceBarController;
	Strawss & Params = Strawss::GetParam();
};