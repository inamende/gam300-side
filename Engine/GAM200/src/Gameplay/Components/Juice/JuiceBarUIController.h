#pragma once
#include "../../../Component/Component.h"

class Transform;
class JuiceBar;
// IMPORTANT NOTES< PLEASE READ
//this class will control the position, animation and changes in the UI of the JuiceBar
//	there are at a minimun three components to the juicebar: the red bar, the top of the red bar animated always 
//	as a liquid, and the glass vial that is supposed to be trnsparent and "contain" the two other objects
//	this component must be placed on the Vial object

//NOTE 2
//Redbar max position and AnimPart position must have a difference of half the scale with respect to 
//the change in scale: For example, if position at 100 juice is 500 in the y, if we go from 100 juice 
//to 0 juice, scale will go from 100 to 0, but position will go from 500 to 450 in both the redbar and
//the animated part
class JuiceBarUIController : public Component
{
public:
	//common variables
	JuiceBarUIController();
	~JuiceBarUIController();
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	//temp variable of things that will be used constantly during the lerp and update
	float dt;	//set during the initialize
	float currentTimer;
	int mCurrentJuiceBarState;	//1 is no change, 2 is increasing, 3 is decreasing
	//Variables to be set in the change function and used in the lerp from previous to new
	/*************************************************************************************************/
	float PreviousJuiceRedBarPos;	
	float NewJuiceRedBarPos;	
	float PreviousJuiceRedBarScale;	
	float NewJuiceRedBarScale;	
	/*************************************************************************************************/
	float PreviousJuiceAnimPos;	
	float NewJuiceAnimPos;	
	/*************************************************************************************************/
	//functions to get the variables from Strawss (Located in the JuiceBar Component)
	JuiceBar * GetStrawssJuiceBarComponent();
	Transform * GetRedBarTransform();
	Transform * GetAnimTransform();
	//a function to be called from Strawss' JuiceBar component
	//	float newState will be either 1, 2, or 3
	void ChangeJuiceBarUI(int newState);
private:
	//a literal pointer to the Strawss JuiceBar component
	JuiceBar * mJuiceBarPointer;
	//variables to be set by the editor and the initialize
	/*************************************************************************************************/
	float MaxJuiceRedBarPos;	//the position the red bar will be at when stawss has maximun juice;
	float ZeroJuiceRedBarPos;	//the position the red bar will be at when stawss has 0 juice;      
	float MaxJuiceRedBarScale;	//the scale the red bar will be at when stawss has maximun juice;
	float ZeroJuiceRedBarScale;	//the scale the red bar will be at when stawss has 0 juice;
	/*************************************************************************************************/
	float MaxJuiceAnimPos;	//the position the red bar will be at when stawss has maximun juice;
	float ZeroJuiceAnimPos;	//the position the red bar will be at when stawss has 0 juice;      
	/*************************************************************************************************/
	std::string StrawssName;	//name of the player
	std::string RedBarName;		//name of the redbar that will move and scale
	std::string AnimName;		//name of the animated part at the very top of the red bar to simulate liquid
	Transform * RedBarTransform;//Transform of the red bar
	Transform * AnimTransform;	//Transform of the animated part
	bool HasSpawnedOtherComponents;
};