#include "JuiceBarUIController.h"
#include "../../../ObjectManager/ObjectManager.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Utils/Utilities.h"
#include "imGui/imgui.h"
#include "../../../Editor/ImGuiEditor.h"
#include "JuiceBar.h"
#include "../../../Level/Scene.h"
#include "../../../Audio/SoundEmitter.h"

JuiceBarUIController::JuiceBarUIController(): StrawssName("Strawss"), RedBarName("NULL"), AnimName("NULL"), RedBarTransform(NULL), AnimTransform(NULL), currentTimer(0.0f),
HasSpawnedOtherComponents(false)
{

}
JuiceBarUIController::~JuiceBarUIController()
{
	SoundEmitter * e;
	e = GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}

void JuiceBarUIController::Initialize()
{
	//set dt
	dt = ImGui::GetIO().DeltaTime;
	if (!HasSpawnedOtherComponents)
	{
		//	CreateOtherComponents();
	}
}
//1 is no change, 2 is increasing, 3 is decreasing
void JuiceBarUIController::ChangeJuiceBarUI(int newState)
{
	currentTimer = 0.0f;
	mCurrentJuiceBarState = newState;
	if (newState == 2.0)
	{
		SoundEmitter * e;
		e = GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			//e->PlaySound("StrawssDrinkJuice.mp3", 1);
			e->PlayOnce("StrawssDrinkJuice.mp3", false);
		}
	}
	//get the previous positions and scale
	if (GetRedBarTransform())
	{
		PreviousJuiceRedBarPos = RedBarTransform->position.y;
		PreviousJuiceRedBarScale = RedBarTransform->scale.y;
	}
	if (GetAnimTransform())
	{
		PreviousJuiceAnimPos = AnimTransform->position.y;
	}
	//compute the new position and scale with the respective ratios and current juice
	//sanity check to optimize
	if (*(GetStrawssJuiceBarComponent()->GetCurrentJuice()) == *(GetStrawssJuiceBarComponent()->GetMaxJuice()))
	{
		float tempCurrentJuice = *(GetStrawssJuiceBarComponent()->GetCurrentJuice());
		NewJuiceAnimPos		= MaxJuiceAnimPos;
		NewJuiceRedBarPos	= MaxJuiceRedBarPos;
		NewJuiceRedBarScale	= MaxJuiceRedBarScale;
	}//sanity check to optimize
	else if (*(GetStrawssJuiceBarComponent()->GetCurrentJuice()) == 0.0f)
	{
		NewJuiceAnimPos = ZeroJuiceAnimPos;
		NewJuiceRedBarPos = ZeroJuiceRedBarPos;
		NewJuiceRedBarScale = ZeroJuiceRedBarScale;
	}
	//otherwise we need to do the calculations
	else
	{
		//variable to store the current and max juice and certain diff magnitudes
		float tempCurrentJuice = *(GetStrawssJuiceBarComponent()->GetCurrentJuice());
		float tempMaxJuice = *(GetStrawssJuiceBarComponent()->GetMaxJuice());
		//get the new positions with the diff and the ratio current/max juice
		float juiceRatio = tempCurrentJuice / tempMaxJuice;
		float RedBarPosDiff = MaxJuiceRedBarPos - ZeroJuiceRedBarPos;
		float RedBarScaleDiff = MaxJuiceRedBarScale - ZeroJuiceRedBarScale;
		float AnimPosDiff = MaxJuiceAnimPos - ZeroJuiceAnimPos;
		//get the new locations based on whether or not we are increasing or decreasing
		NewJuiceAnimPos =	ZeroJuiceAnimPos + (juiceRatio * AnimPosDiff);
		NewJuiceRedBarPos = ZeroJuiceRedBarPos	+ (juiceRatio * RedBarPosDiff);
		NewJuiceRedBarScale = ZeroJuiceRedBarScale	+ (juiceRatio * RedBarScaleDiff);
	}
}
void JuiceBarUIController::Update()
{
	//increasing
	if (mCurrentJuiceBarState == 2)
	{

		currentTimer += dt;
		//*mCurrentXPosition = glm::mix(mPointB.x, mPointA.x, tn);
		GetRedBarTransform()->position.y = glm::mix(PreviousJuiceRedBarPos, NewJuiceRedBarPos, currentTimer);
		GetRedBarTransform()->scale.y = glm::mix(PreviousJuiceRedBarScale, NewJuiceRedBarScale, currentTimer);
		GetAnimTransform()->position.y = glm::mix(PreviousJuiceAnimPos, NewJuiceAnimPos, currentTimer);
		//if one of these is true we set everything to the new value and stop
		if (RedBarTransform->position.y >= NewJuiceRedBarPos)
		{
			RedBarTransform->position.y = NewJuiceRedBarPos;
			RedBarTransform->scale.y = NewJuiceRedBarScale;
			AnimTransform->position.y = NewJuiceAnimPos;
			AnimTransform;
			mCurrentJuiceBarState = 1;
			currentTimer = 0.0f;
		}
	}
	//decreasing
	else if (mCurrentJuiceBarState == 3)
	{
		currentTimer += dt;
		//*mCurrentXPosition = glm::mix(mPointB.x, mPointA.x, tn);
		GetRedBarTransform()->position.y = glm::mix(PreviousJuiceRedBarPos, NewJuiceRedBarPos, currentTimer);
		GetRedBarTransform()->scale.y = glm::mix(PreviousJuiceRedBarScale, NewJuiceRedBarScale, currentTimer);
		GetAnimTransform()->position.y = glm::mix(PreviousJuiceAnimPos, NewJuiceAnimPos, currentTimer);
		//if one of these is true we set everything to the new value and stop
		if (RedBarTransform->position.y <= NewJuiceRedBarPos)
		{
			RedBarTransform->position.y = NewJuiceRedBarPos;
			RedBarTransform->scale.y = NewJuiceRedBarScale;
			AnimTransform->position.y = NewJuiceAnimPos;
			AnimTransform;
			mCurrentJuiceBarState = 1;
			currentTimer = 0.0f;
		}
	}
	//no change
	else
	{

	}
}
void JuiceBarUIController::Terminate()
{

}
void JuiceBarUIController::load(Json::Value & val)
{
	active = val["JuiceBarUIController"].get("active", true).asBool();
	MaxJuiceRedBarPos		= val["JuiceBarUIController"].get("MaxJuiceRedBarPos", 100.0f).asFloat();
	ZeroJuiceRedBarPos		= val["JuiceBarUIController"].get("ZeroJuiceRedBarPos", 0.0f).asFloat();
	MaxJuiceRedBarScale		= val["JuiceBarUIController"].get("MaxJuiceRedBarScale", 100.0f).asFloat();
	ZeroJuiceRedBarScale	= val["JuiceBarUIController"].get("ZeroJuiceRedBarScale", 0.0f).asFloat();
	MaxJuiceAnimPos			= val["JuiceBarUIController"].get("MaxJuiceAnimPos", 100.0f).asFloat();
	ZeroJuiceAnimPos		= val["JuiceBarUIController"].get("ZeroJuiceAnimPos", 0.0f).asFloat();
	StrawssName				= val["JuiceBarUIController"].get("StrawssName", "NULL").asString();
	RedBarName				= val["JuiceBarUIController"].get("RedBarName", "NULL").asString();
	AnimName				= val["JuiceBarUIController"].get("AnimName", "NULL").asString();
	HasSpawnedOtherComponents = val["JuiceBarUIController"].get("HasSpawnedOtherComponents", false).asBool();
}
void JuiceBarUIController::save(Json::Value & val)
{
	val["JuiceBarUIController"]["active"] = active;
	val["JuiceBarUIController"]["MaxJuiceRedBarPos"] = MaxJuiceRedBarPos;
	val["JuiceBarUIController"]["ZeroJuiceRedBarPos"] = ZeroJuiceRedBarPos;
	val["JuiceBarUIController"]["MaxJuiceRedBarScale"] = MaxJuiceRedBarScale;
	val["JuiceBarUIController"]["ZeroJuiceRedBarScale"] = ZeroJuiceRedBarScale;
	val["JuiceBarUIController"]["MaxJuiceAnimPos"] = MaxJuiceAnimPos;
	val["JuiceBarUIController"]["ZeroJuiceAnimPos"] = ZeroJuiceAnimPos;
	val["JuiceBarUIController"]["StrawssName"] = StrawssName;
	val["JuiceBarUIController"]["RedBarName"] = RedBarName;
	val["JuiceBarUIController"]["AnimName"] = AnimName;
	val["JuiceBarUIController"]["HasSpawnedOtherComponents"] = HasSpawnedOtherComponents;
}
void JuiceBarUIController::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("JuiceBarUIController"))
	{
		//ImGui::InputFloat("mCurrentUIJuice##JuiceBarUIController", &mCurrentUIJuice);

		ImGuiEditor::InputText("Name of the Player##JuiceBar", StrawssName);
		ImGuiEditor::InputText("Name of the RedBarName##JuiceBar", RedBarName);
		ImGuiEditor::InputText("Name of the AnimName##JuiceBar", AnimName);
	
		ImGui::InputFloat("MaxJuiceRedBarPos##JuiceBarUIController", &MaxJuiceRedBarPos);
		ImGui::InputFloat("ZeroJuiceRedBarPos##JuiceBarUIController", &ZeroJuiceRedBarPos);
		ImGui::InputFloat("MaxJuiceRedBarScale##JuiceBarUIController", &MaxJuiceRedBarScale);
		ImGui::InputFloat("ZeroJuiceRedBarScale##JuiceBarUIController", &ZeroJuiceRedBarScale);
		ImGui::InputFloat("MaxJuiceAnimPos##JuiceBarUIController", &MaxJuiceAnimPos);
		ImGui::InputFloat("ZeroJuiceAnimPos##JuiceBarUIController", &ZeroJuiceAnimPos);

		ImGui::NewLine();
		if (ImGui::Button("Detach##JuiceBarUIController"))
			GetOwner()->detach(this);
	}
}
JuiceBar * JuiceBarUIController::GetStrawssJuiceBarComponent()
{
	mJuiceBarPointer = objectManager->FindObjectByName(StrawssName)->GetComponent<JuiceBar>();
	return mJuiceBarPointer;
}
Transform * JuiceBarUIController::GetRedBarTransform()
{
	if (RedBarTransform == NULL)
	{
		if (RedBarName == "NULL")
		{
			return NULL;
		}
		RedBarTransform = objectManager->FindObjectByName(RedBarName)->GetComponent<Transform>();
		return RedBarTransform;
	}
	return RedBarTransform;
}
Transform * JuiceBarUIController::GetAnimTransform()
{
	if (AnimTransform == NULL)
	{
		if (AnimName == "NULL")
		{
			return NULL;
		}
		AnimTransform = objectManager->FindObjectByName(AnimName)->GetComponent<Transform>();
		return AnimTransform;
	}
	return AnimTransform;
}
