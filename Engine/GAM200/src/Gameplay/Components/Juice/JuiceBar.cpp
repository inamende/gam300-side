#include "JuiceBar.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Physics/Physics.h"
#include "../../../Editor/ImGuiEditor.h"
#include "../../../ObjectManager/ObjectManager.h"
#include "JuiceBarUIController.h"

#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
#include "../../../Gameplay/Components/Attacks/SwordAttack.h"
#include "../../../Gameplay/Components/Enemies/Bowman/Arrow/ArrowDirection.h"

#include "../../../Level/Scene.h"
#include "../../../Audio/SoundEmitter.h"
#include "../../../Gameplay/Components/Strawss/PlayerController.h"
#include "../../../Physics/Comp/BoxCollider.h"
/**/
JuiceBar::JuiceBar(): mTransformPointer(NULL), mMaxJuice(100.0f), mCurrentJuice(100.0f), mAbilityCost(20.0f), JuiceBoxName("NULL"), CheckpointName("NULL"), mJuiceBarController(NULL)
{
	mCheckPointTransform = new Transform;
	//all values will be set in the first call to GetCheckpointTransform and/or when we actually get to a checkpoint
	mCheckPointTransform->position.x = 0.0f;
	mCheckPointTransform->position.y = 0.0f;
	//note this is just an initial value to be checked and inmediately changed if we want to use it
	mCheckPointTransform->position.z = -10000.0f;
}
JuiceBar::~JuiceBar()
{
	//SoundEmitter * e;
	//e = GetOwner()->GetComponent<SoundEmitter>();
	//if (e)
	//{
	//	//e->PlaySound("StrawssDrinkJuice.mp3", 1);
	//	e->StopVoice();
	//}
	delete mCheckPointTransform;
}
void JuiceBar::Initialize()
{
	//register the events for collecting juiceboxes and gargoyle fountains
	GetOwner()->register_handler(*this, &JuiceBar::OnCollisionStarted);
	mJuiceBarController = GetJuiceBarUIObject();
}
void JuiceBar::Update()
{

}
void JuiceBar::Terminate()
{
	GetOwner()->unregister_handler(*this, &JuiceBar::OnCollisionStarted);
}

void JuiceBar::load(Json::Value & val)
{
	active			= val["JuiceBar"].get("active", true).asBool();
	mMaxJuice = val["JuiceBar"].get("mMaxJuice", 100.0f).asFloat();
	mCurrentJuice = val["JuiceBar"].get("mCurrentJuice", 100.0f).asFloat();
	mAbilityCost	= val["JuiceBar"].get("mAbilityCost", 20.0f).asFloat();
	JuiceBoxName	= val["JuiceBar"].get("JuiceBoxName", "NULL").asString();
	CheckpointName	= val["JuiceBar"].get("CheckpointName", "NULL").asString();
	JuiceBarUIName = val["JuiceBar"].get("JuiceBarUIName", "NULL").asString();
	mCheckPointTransform->position.x = val["JuiceBar"]["position"].get("x", 0.0f).asFloat();
	mCheckPointTransform->position.y = val["JuiceBar"]["position"].get("y", 0.0f).asFloat();
	mCheckPointTransform->position.z = val["JuiceBar"]["position"].get("z", -10000.0f).asFloat();
}
void JuiceBar::save(Json::Value & val)
{
	val["JuiceBar"]["active"] = active;
	val["JuiceBar"]["mMaxJuice"] = mMaxJuice;
	val["JuiceBar"]["mCurrentHealth"] = mCurrentJuice;
	val["JuiceBar"]["mAbilityCost"] = mAbilityCost;
	val["JuiceBar"]["JuiceBoxName"] = JuiceBoxName;
	val["JuiceBar"]["CheckpointName"] = CheckpointName;
	val["JuiceBar"]["JuiceBarUIName"] = JuiceBarUIName;
	val["JuiceBar"]["position"]["x"] = mCheckPointTransform->position.x;
	val["JuiceBar"]["position"]["y"] = mCheckPointTransform->position.y;
	val["JuiceBar"]["position"]["z"] = mCheckPointTransform->position.z;
}
void JuiceBar::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("JuiceBar"))
	{
		ImGui::InputFloat("Max Juice##JuiceBar", &mMaxJuice);

		if (ImGui::InputFloat("Current Juice##JuiceBar", &mCurrentJuice))
		{
			//if (GetJuiceBarUIObject())
			//{
			//	GetJuiceBarUIObject()->GetComponent<JuiceBarUIController>()->ChangeJuiceBarUI(3);
			//}
		}

		ImGui::InputFloat("Ability Cost##JuiceBar", &mAbilityCost);
		ImGuiEditor::InputText("Name of the JuiceBox Objects##JuiceBar", JuiceBoxName);
		ImGuiEditor::InputText("Name of the Checkpoint Objects##JuiceBar", CheckpointName);
		ImGuiEditor::InputText("Name of the UI Object Objects##JuiceBar", JuiceBarUIName);

		ImGui::NewLine();

		if (ImGui::Button("DebugRestore##JuiceBar"))
		{
			RestoreJuice(10.0);
		}
		if (ImGui::Button("DebugCheckpoint##JuiceBar"))
		{
			RestoreJuiceAndCheckpoint();
			//std::cout << "New Checkpointis: " << GetCheckpointTransform()->position.x<<", "<< GetCheckpointTransform()->position.y<<", "<< GetCheckpointTransform()->position.z << std::endl;
		}
		if (ImGui::Button("DebugAbility##JuiceBar"))
		{
			AbilityUsageCheck();
		}
		if (ImGui::Button("Debug He Ded, Boi##JuiceBar"))
		{
			StrawssDied();
		}
		if (ImGui::Button("Detach##JuiceBar"))
			GetOwner()->detach(this);
	}
}
//NOTE, MAY NEED TO RENAME IF JUICE AND HP TURN OUT TO NOT BE THE SAME THING
void JuiceBar::RestoreJuice(float hp)
{
	//this will update the internal values of the JUICE
	float temp = mCurrentJuice + hp;
	if (temp >= mMaxJuice)
	{
		mCurrentJuice = mMaxJuice;
	}
	else
	{
		mCurrentJuice = temp;
	}
	//this will acces the UI object and modify its values
	//the the UI if it exists
	if (GetJuiceBarUIObject())
	{
		GetJuiceBarUIObject()->GetComponent<JuiceBarUIController>()->ChangeJuiceBarUI(2);
	}
	////call play juice from here?
	//SoundEmitter * e = GetJuiceBarUIObject()->GetComponent<SoundEmitter>();
	//if (e)
	//{
	//	e->PlaySound("StrawssDrinkJuice.mp3", 1);
	//}
}

void JuiceBar::RestoreJuiceAndCheckpoint()
{
	//std::cout << "Hit The checkpoint" << std::endl;
	//restore health
	mCurrentJuice = mMaxJuice;
	//set a new checkpoint
	*(mCheckPointTransform) = *(GetPlayerTransform());
	if (GetJuiceBarUIObject())
	{
		//if (mCurrentJuice != mMaxJuice)
		//{
			GetJuiceBarUIObject()->GetComponent<JuiceBarUIController>()->ChangeJuiceBarUI(2);
		//}
	}
	Params.SpawnAtCheckPoint = true;
	Params.CheckPointSpawnPos = GetOwner()->GetComponent<Transform>()->position;
	//call play juice from here?
	GameObject * j = GetJuiceBarUIObject();
	if (j)
	{
		SoundEmitter * e;
		e = j->GetComponent<SoundEmitter>();
		if (e)
		{
			//e->PlaySound("StrawssDrinkJuice.mp3", 1);
			e->PlayOnce("StrawssDrinkJuice.mp3", false);
		}
	}
	
}
//this will allow Strawss to use his abilities if he has enough juice
bool JuiceBar::AbilityUsageCheck()
{
	float temp;
	temp = mCurrentJuice - mAbilityCost;
	if (temp < 0.0)
	{
		return false;
	}
	else
	{
		mCurrentJuice -= mAbilityCost;
		//this will acces the UI object and modify its values
		//the the UI if it exists
		if (GetJuiceBarUIObject())
		{
			GetJuiceBarUIObject()->GetComponent<JuiceBarUIController>()->ChangeJuiceBarUI(3);
		}
		return true;
	}
}
//when the player has died he will be transported to the Last checkpoint, this is assuming the death animation has already played
void JuiceBar::StrawssDied()
{
	//GetOwner()->GetComponent<PlayerController>()->("Die");
	PlayerController *control = GetOwner()->GetComponent<PlayerController>();
	if (control) {
		if (control->mCurrentState->m_name != "Die" && control->mCurrentState->m_name != "FlyAway") {
			control->ChangeState("Die");
		}
	}
}
//this is used for the checkpoint
Transform * JuiceBar::GetPlayerTransform()
{
	if (mTransformPointer == NULL)
	{
		mTransformPointer = GetOwner()->mTransform;
		return mTransformPointer;
	}
	else
	{
		return mTransformPointer;
	}
}
Transform * JuiceBar::GetCheckpointTransform()
{
	//if this value of z is this we just re set it to whatever the player is
	if (mCheckPointTransform->position.z == -10000.0f)
	{
		*(mCheckPointTransform) = *(GetPlayerTransform());
	}
	else
	{
		return mCheckPointTransform;
	}
}
GameObject * JuiceBar::GetJuiceBarUIObject()
{
	if (mJuiceBarController == NULL)
	{
		mJuiceBarController = objectManager->FindObjectByName(JuiceBarUIName);
		return mJuiceBarController;
	}
	return mJuiceBarController;
}
void JuiceBar::OnCollisionStarted(const CollisionStartedEvent & e)
{
	PlayerController * p = NULL;
	p = this->GetOwner()->GetComponent<PlayerController>();
	//sanity check
	if (p->mCurrentState == NULL)
	{
		return;
	}
	// check the collision started with any collider the knight owns
	if (e.other_object->GetName() == JuiceBoxName)
	{
		//states when we do want to interact with checkpoint/juice:
		// Spectre, Dash, ChargeDash, DashRecovery, Die, FlyAway, GoBackToHumanForm,
		if ((p->mCurrentState->m_name != "Spectre") && (p->mCurrentState->m_name != "Dash")
			&& (p->mCurrentState->m_name != "ChargeDash") && (p->mCurrentState->m_name != "DashRecovery")
			&& (p->mCurrentState->m_name != "Die") && (p->mCurrentState->m_name != "FlyAway")
			&& (p->mCurrentState->m_name != "GoBackToHumanForm"))
		{
			//check if we are already at max health
			if (mCurrentJuice != mMaxJuice)
			{
				//if it is a juicebox we need to delete the box and gain juice
				// for now we use ability cost
				RestoreJuice(mAbilityCost);
				//destroy the juice box
				objectManager->RemoveGameObject(e.other_object);
			}
		}
		return;
	}
	if (e.other_object->GetName() == CheckpointName)
	{
		//sanity check in case the current state is not initialized
		//if this is note here it will crash on respawing strawss at the checkpoint
		if ((p->mCurrentState->m_name != "Spectre") && (p->mCurrentState->m_name != "Dash")
			&& (p->mCurrentState->m_name != "ChargeDash") && (p->mCurrentState->m_name != "DashRecovery")
			&& (p->mCurrentState->m_name != "Die") && (p->mCurrentState->m_name != "FlyAway")
			&& (p->mCurrentState->m_name != "GoBackToHumanForm"))
		{
			//now we need to see if strawss is is colliding with the correct collider of the checkpoint
			//this is Collider 0 of the checkpoint, NOT Collider 1
			BoxCollider * ActualCollider = e.other_object->GetComponent<BoxCollider>("BoxCollider_0");
			if (ActualCollider)
			{
				//get strawss's colliuder
				BoxCollider * s = GetOwner()->GetComponent<BoxCollider>();
				if (CollideAABBs(ActualCollider, s, NULL))
				{
					RestoreJuiceAndCheckpoint();
					return;
				}
			}
		}
		
	}
	//SwordAttack, if we get hit by a sword attack we either die if in normal form or
	// go back to normal if in specter
	SwordAttack * s = NULL;
	s = e.other_object->GetComponent<SwordAttack>();
	if (s)
	{
		if ((p->mCurrentState->m_name == "Spectre"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else if ((p->mCurrentState->m_name == "Dash"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else if ((p->mCurrentState->m_name == "ChargeDash"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else if ((p->mCurrentState->m_name == "DashRecovery"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else
		{
			
			this->StrawssDied();
			return;
		}
	}
	//BowAttack
	ArrowDirection * d = NULL;
	d = e.other_object->GetComponent<ArrowDirection>();
	if (d)
	{
		//
		if ((p->mCurrentState->m_name == "Spectre"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else if ((p->mCurrentState->m_name == "Dash"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else if ((p->mCurrentState->m_name == "ChargeDash"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else if ((p->mCurrentState->m_name == "DashRecovery"))
		{
			if (!(p->IsPossessingAnEnemy()))
			{
				p->ChangeState("GoBackToHumanForm");
				return;
			}
		}
		else
		{
			this->StrawssDied();
			return;
		}
	}
	//killbox logic
	if (e.other_object->GetName() == "KillBox")
	{
		this->StrawssDied();
		return;
	}
}

float JuiceBar::get_max_juice() { return mMaxJuice; }