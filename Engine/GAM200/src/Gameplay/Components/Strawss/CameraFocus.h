#ifndef CAMERAFOCUS_H
#define CAMERAFOCUS_H

#include "../../../Component/Component.h"

class Camera;
class Collider;
class CollisionPersistedEvent;
class CollisionEndedEvent;
class CollisionStartedEvent;
class CameraLogic;

class CameraFocus : public Component
{
public:
	CameraFocus();
	~CameraFocus();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	void OnCollisionStarted(const CollisionStartedEvent & object);
	void OnCollisionPersisted(const CollisionPersistedEvent & object);
	void OnCollisionEnded(const CollisionEndedEvent & object);

	Camera * cam;
	Collider * collider;
	GameObject * strawss;
	vec2 target;
	std::string stra_name;
	CameraLogic * comp;
	bool staticFocus;
};

#endif