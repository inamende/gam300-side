#include "CameraLogic.h"

#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Gfx.h"
#include "../../../Gfx/Reso/Camera.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Gfx/Comp/Renderable.h"
#include "../../../Level/Scene.h"
#include "../../../Utils/Utilities.h"
#include "../../../Input/Input.h"
#include "../../../Gameplay/Components/InteractableObjects/Interactable.h"
#include "imGui/imgui.h"
#include "PlayerController.h"
#include "../Enemies/Enemy.h"

CameraLogic::CameraLogic() : 
	back_camera(NULL),
	background_camera(NULL),
	camera(NULL),
	front_camera(NULL),
	tr(NULL),
	rb(NULL),
	Stra(NULL),
	innerBound(100.f),
	outerBound(200.f),
	black_bars{nullptr, nullptr},
	focus(false),
	move(false),
	offset(vec2())
{}

void CameraLogic::Initialize()
{
	rb = GetOwner()->mRigidBody;
	tr = GetOwner()->mTransform;
	camera = graphics->GetMainCamera();
	front_camera = graphics->GetCameraByRenderLayer("front");
	back_camera = graphics->GetCameraByRenderLayer("back");
	background_camera = graphics->GetCameraByRenderLayer("background");
	camera->scale = vec2(1, 1);
	cameraState = idle;
	cameraStatey = idley;

	camera->position.x = GetOwner()->mTransform->position.x + offset.x + innerBound;
	camera->position.y = GetOwner()->mTransform->position.y + offset.y + innerBoundy;
	camera->view.x = camera->position.x;
	camera->view.y = camera->position.y;

	Stra = GetOwner()->GetComponent<PlayerController>();

	black_bars[0] = CreateBar(700);
	black_bars[1] = CreateBar(-700);
}

void CameraLogic::Update()
{
	if (!move) {
		UpdateCamreBounds();
	}

	if (focus) {
		FocusBars(500);
	}
	else {
		UnFocusBars(700);
	}

	ParallaxMovement();
}

void CameraLogic::Terminate() {}

void CameraLogic::load(Json::Value & val)
{
	active = val["CameraLogic"].get("active", true).asBool();
	outerBound = val["CameraLogic"].get("outerBound", 200.f).asFloat();
	innerBound = val["CameraLogic"].get("innerBound", 100.f).asFloat();
	innerBoundy = val["CameraLogic"].get("innerBoundy", 100.f).asFloat();
	outerBoundy = val["CameraLogic"].get("outerBoundy", 200.f).asFloat();
	offset.x = val["CameraLogic"]["offset"].get("x", 300.f).asFloat();
	offset.y = val["CameraLogic"]["offset"].get("y", 100.f).asFloat();
}

void CameraLogic::save(Json::Value & val)
{
	val["CameraLogic"]["active"] = active;
	val["CameraLogic"]["outerBound"] = outerBound;
	val["CameraLogic"]["innerBound"] = innerBound;
	val["CameraLogic"]["outerBoundy"] = outerBoundy;
	val["CameraLogic"]["innerBoundy"] = innerBoundy;
	val["CameraLogic"]["offset"]["x"] = offset.x;
	val["CameraLogic"]["offset"]["y"] = offset.y;
}
void CameraLogic::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("CameraLogic"))
	{
		ImGui::Checkbox("active##CameraLogic", &active);
		ImGui::SliderFloat("Outer Bound##CameraLogic", &outerBound, 10.f, 1000.f);
		ImGui::SliderFloat("Inner Bound##CameraLogic", &innerBound, 10.f, 1000.f);

		ImGui::SliderFloat("Outer Boundy##CameraLogic", &outerBoundy, 10.f, 1000.f);
		ImGui::SliderFloat("Inner Boundy##CameraLogic", &innerBoundy, 10.f, 1000.f);

		ImGui::SliderFloat("offset x##CameraLogic", &offset.x, 0.f, 1000.f);
		ImGui::SliderFloat("offset y##CameraLogic", &offset.y, 0.f, 1000.f);

		ImGui::NewLine();
		if (ImGui::Button("Detach##CameraLogic"))
			GetOwner()->detach(this);
	}
}

void CameraLogic::UpdateCamreBounds() {
	if (!rb) rb = GetOwner()->mRigidBody;

	if (Stra->PossEnemy != nullptr)
		tr = Stra->PossEnemy->GetOwner()->mTransform;
	else
		tr = GetOwner()->mTransform;

	if (!rb || !tr)
		return;

	if (_isnan(tr->position.x) || _isnan(tr->position.y))
		return;

	//offset.x *= Strawss::GetParam().direction < 0 ? -1 : 1;
	vec2 camTarget = vec2(tr->position) + offset;
	vec2 cmPosition = vec2(camera->position);

	float dt = fabs(rb->velocity.x) * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime;
	switch (cameraState)
	{
	case idle:
		if (camTarget.x > cmPosition.x + outerBound)
			cameraState = translateBack;
		else if (camTarget.x < cmPosition.x - outerBound)
			cameraState = translateFront;
		break;

	case translateBack:
		cmPosition.x = Lerp(cmPosition.x, camTarget.x + innerBound, fabs(rb->velocity.x) * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime);

		if (glm::distance(camTarget.x, cmPosition.x - innerBound) < 10.5f)
			cameraState = idle;
		break;

	case translateFront:
		cmPosition.x = Lerp(cmPosition.x, camTarget.x - innerBound, fabs(rb->velocity.x) * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime);

		if (glm::distance(camTarget.x, cmPosition.x + innerBound) < 10.5f)
			cameraState = idle;
		break;
	}
	switch (cameraStatey)
	{
	case idley:
		if (camTarget.y > cmPosition.y + outerBoundy)
			cameraStatey = translateBacky;
		else if (camTarget.y < cmPosition.y - outerBoundy)
			cameraStatey = translateFronty;
		break;

	case translateBacky:
		cmPosition.y = Lerp(cmPosition.y, camTarget.y + innerBoundy, fabs(rb->velocity.y) * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime);

		if (glm::distance(camTarget.y, cmPosition.y - innerBoundy) < 10.5f)
			cameraStatey = idley;
		break;

	case translateFronty:
		cmPosition.y = Lerp(cmPosition.y, camTarget.y + innerBoundy, fabs(rb->velocity.y) * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime * ImGui::GetIO().DeltaTime * 0.001f + 2 * ImGui::GetIO().DeltaTime);

		if (glm::distance(camTarget.y, cmPosition.y + innerBoundy) < 10.5f)
			cameraStatey = idley;
		break;
	}

	camera->position.x = cmPosition.x;
	camera->position.y = cmPosition.y;
	camera->view.x = cmPosition.x;
	camera->view.y = cmPosition.y;

	static Scene * sc = scene;
	sc->DrawFancyCursor(glm::vec3(cmPosition.x, cmPosition.y, 99.0f), glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x + outerBound, cmPosition.y, 99.0f), glm::vec4(0.0f, 1.f, .0f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x - outerBound, cmPosition.y, 99.0f), glm::vec4(0.0f, 1.f, .0f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x + innerBound, cmPosition.y, 99.0f), glm::vec4(0.0f, 0.f, 1.f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x - innerBound, cmPosition.y, 99.0f), glm::vec4(0.0f, 0.f, 1.f, 1.0f));

	sc->DrawFancyCursor(glm::vec3(cmPosition.x, cmPosition.y, 99.0f), glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x, cmPosition.y + outerBoundy, 99.0f), glm::vec4(0.0f, 1.f, .0f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x, cmPosition.y - outerBoundy, 99.0f), glm::vec4(0.0f, 1.f, .0f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x, cmPosition.y + innerBoundy, 99.0f), glm::vec4(0.0f, 0.f, 1.f, 1.0f));
	sc->DrawFancyCursor(glm::vec3(cmPosition.x, cmPosition.y - innerBoundy, 99.0f), glm::vec4(0.0f, 0.f, 1.f, 1.0f));

	sc->DrawFancyCursor(glm::vec3(camTarget.x, camTarget.y, 99.0f), glm::vec4(1.0f, 0.f, 0.f, 1.0f));
}

GameObject * CameraLogic::CreateBar(float y) {
	GameObject *new_obj = scene->CreateGameObject();
	new_obj->SetName("");
	new_obj->save = false;
	if (!new_obj) {
		return nullptr;
	}
	Transform *trans = DBG_NEW Transform(vec3(0, y,400), 0.f, vec3(5000, 300, 1));
	new_obj->attach(trans);
	Renderable *rend = DBG_NEW Renderable;
	rend->shader = graphics->GetDefaultShader();
	rend->sprite = graphics->GetDefaultSprite();
	rend->texture = nullptr;
	rend->color = vec4(0.f,0.f,0.f,1.f);
	rend->layer = "UI";
	rend->visible = false;
	new_obj->attach(rend);
	new_obj->Initialize();
	return new_obj;
}

void CameraLogic::FocusBars(float y) {
	black_bars[0]->mRenderable->visible = black_bars[1]->mRenderable->visible = true;
	float b0_position_y = black_bars[0]->mTransform->position.y;
	float b1_position_y = black_bars[1]->mTransform->position.y;
	static float count = 0.f;
	if (b0_position_y >= y + 1.f) {
		count += 0.1f * ImGui::GetIO().DeltaTime;
		black_bars[0]->mTransform->position.y = Lerp<float>(black_bars[0]->mTransform->position.y, y, count);
		black_bars[1]->mTransform->position.y = Lerp<float>(black_bars[1]->mTransform->position.y, -y, count);
	}
	else {
		count = 0.f;
	}
}

void CameraLogic::UnFocusBars(float y) {
	float b0_position_y = black_bars[0]->mTransform->position.y;
	float b1_position_y = black_bars[1]->mTransform->position.y;
	static float count = 0.f;
	if (b0_position_y <= y - 1.f) {
		count += 0.1f * ImGui::GetIO().DeltaTime;
		black_bars[0]->mTransform->position.y = Lerp<float>(black_bars[0]->mTransform->position.y, y, count);
		black_bars[1]->mTransform->position.y = Lerp<float>(black_bars[1]->mTransform->position.y, -y, count);
	}
	else {
		count = 0.f;
		black_bars[0]->mRenderable->visible = black_bars[1]->mRenderable->visible = false;
	}
}

void CameraLogic::ParallaxMovement() {
	back_camera->Mimic(camera);
	back_camera->position.x = camera->position.x * 0.5f;
	back_camera->view.x = back_camera->position.x;

	front_camera->Mimic(camera);
	front_camera->position.x = camera->position.x * 2.4f;
	front_camera->view.x = front_camera->position.x;
}