#include "PlayerController.h"

#include "../../../Editor/ImGuiEditor.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Gfx/Gfx.h"
#include "../../../Gfx/Postprocessor.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../Input/Input.h"
#include "States/StrawssParam.h"
#include "States/Idle.h"
#include "States/Move.h"
#include "States/Jump.h"
#include "States/Fall.h"
#include "States/Dash.h"
#include "States/WakeUp.h"
#include "States/StrawssParam.h"
#include "States/GoBackToHumanForm.h"
#include "States/GoToSleep.h"
#include "States/Die.h"
#include "States/FlyAway.h"
#include "States/Start.h"
#include "States/MoveRight.h"
#include "States/Land.h"
#include "States/ChargeDash.h"
#include "States/DashRecovery.h"

#include "../../Components/Strawss/States/Spectre.h"
#include "States/StrawssParam.h"
#include "../GAM200/src/Level/Scene.h"

#include "../Enemies/Knight/Knight.h"
#include "../Enemies/Bowman/Bowman.h"
#include "../Enemies/Enemy.h"
#include "../../../Gfx/Comp/Animator.h"

PlayerController::PlayerController() : tr(NULL), PossEnemy(nullptr), EnemyTr(nullptr), them_toes(nullptr), them_body(nullptr) {}

PlayerController::~PlayerController() {}

void PlayerController::Initialize()
{
	auto & param = Strawss::GetParam();
	param.initialScale = fabs(GetOwner()->mTransform->scale.x);
	init_z = GetOwner()->mTransform->position.z;
	m_actor = GetOwner();
	AddState(DBG_NEW Idle("Idle"));
	AddState(DBG_NEW Move("Move"));
	AddState(DBG_NEW Jump("Jump"));
	AddState(DBG_NEW Fall("Fall"));
	AddState(DBG_NEW Spectre("Spectre"));
	AddState(DBG_NEW GoBackToHumanForm("GoBackToHumanForm"));
	AddState(DBG_NEW Dash("Dash"));
	AddState(DBG_NEW WakeUp("wakeup"));
	AddState(DBG_NEW GoToSleep("GoToSleep"));
	AddState(DBG_NEW Die("Die"));
	AddState(DBG_NEW FlyAway("FlyAway"));
	AddState(DBG_NEW Start("Start"));
	AddState(DBG_NEW MoveRight("MoveRight"));
	AddState(DBG_NEW Land("Land"));
	AddState(DBG_NEW ChargeDash("ChargeDash"));
	AddState(DBG_NEW DashRecovery("DashRecovery"));
	
	SetInitState("Start");
	mInitialState->InternalEnter();

	tr = m_actor->mTransform;

	GetOwner()->register_handler(*this, &PlayerController::OnCollisionStarted);

	if (strawss.body) {
		//objectManager->RemoveGameObject(strawss.body);
		strawss.body = nullptr;
	}
}

void PlayerController::Terminate()
{
	GetOwner()->unregister_handler(*this, &PlayerController::OnCollisionStarted);
}
void PlayerController::Update()
{
	if (_isnan(tr->position.x))
	{
		auto & param = Strawss::GetParam();
		tr->position = param.DashPosition;

		TABLE_FORMAT collision_table = CollissionTable::getInstance();

		collision_table[player][SpctreWalls] = 1;
		collision_table[SpctreWalls][player] = 1;
	}

	auto & param = Strawss::GetParam();
	tr->scale.x = (param.direction < 0) ? -param.initialScale : param.initialScale;

	StatusUpdate();
}


void PlayerController::load(Json::Value & val)
{
	active = val["PlayerController"].get("active", true).asBool();
}

void PlayerController::save(Json::Value & val)
{
	val["PlayerController"]["active"] = active;
}

void PlayerController::WriteToImguiWindow()
{
	auto & param = Strawss::GetParam();

	if (ImGui::CollapsingHeader("PlayerController"))
	{
		ImGui::InputFloat("velocity##PlayerController", &param.speed);

		ImGui::InputFloat("Jump velocity", &param.jumpVelocity);

		ImGui::InputFloat("Fall Speed", &param.fallMultplier);

		ImGui::InputFloat("MaxMovementSpeed", &param.maxSpeed);

		ImGui::InputFloat("Movement Decrease", &param.movementDrag);

		ImGui::InputFloat("Ghost Limit Radius", &param.GhostLimit);

		ImGui::InputFloat("Return Speed", &param.ghost_return_speed);

		//ImGui::InputFloat("mDashForceMultiplier", &mDashForceMultiplier);

		//ImGui::InputFloat("mMaxDashTime", &mMaxDashTime);
		ImGui::Checkbox("active", & active);
		ImGui::NewLine();
		if (ImGui::Button("Detach##PlayerController"))
			GetOwner()->detach(this);
	}

}

void PlayerController::OnCollisionStarted(const CollisionStartedEvent & spot)
{
	if (StrawssThings.IsDashing)
	{
		Enemy * enemy = spot.other_object->GetComponent<Enemy>();
		if (enemy != nullptr && !possesed_knight)
		{
			EnemyTr = enemy->GetOwner()->mTransform;
			enemy->possesed = true;
			GetOwner()->GetComponent<BoxCollider>()->ChangeTriggerState(true);
			GetOwner()->mAnimator->visible = false;
			GetOwner()->mTransform->position = Strawss::GetParam().body_position;
			ChangeState("DashRecovery");

			Knight * type = dynamic_cast<Knight *>(enemy);
			if (enemy != nullptr)
			{
				PossEnemy = enemy;
			}
			Bowman * bowman = dynamic_cast<Bowman *>(enemy);
			if (enemy != nullptr)
			{
				PossEnemy = enemy;
			}
		}
	}
}

void create_body_at_position(const glm::vec3 spawn_position)
{
	auto  & param = Strawss::GetParam();

	std::string name = "SleepinBeauty";
	float z = imGuiEditor->GetLastZ();

	param.body = scene->CreateGameObject();

	param.body->LoadJson("./Data/Jsons/" + name + ".json");

	Transform * transform = param.body->mTransform;
	BoxCollider * collider = param.body->GetComponent<BoxCollider>();

	if (collider == nullptr) {
		//std::cout << "NO COLLIDER" << std::endl;
		return;

	}
		
	collider->collision_group = slept_body;

	if (transform == nullptr) {
		//std::cout << "NO TRA" << std::endl;
		return;
	}
		

	transform->position.x = spawn_position.x;
	transform->position.y = spawn_position.y;
	transform->position.z = spawn_position.z;

	param.body_position = transform->position;

	param.body->Initialize();
}
