#include "CameraFocus.h"

#include "../../../Gfx/Gfx.h"
#include "../../../Gfx/Reso/Camera.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "CameraLogic.h"
#include <ImGui/imgui.h>

CameraFocus::CameraFocus() :
	cam(NULL),
	collider(NULL),
	strawss(NULL),
	target(vec2{}),
	stra_name("Strawss"), // who cares
	comp(NULL),
	staticFocus(true)
{}

CameraFocus::~CameraFocus() {}

void CameraFocus::Initialize() {
	cam = graphics->GetMainCamera();
	collider = GetOwner()->GetComponent<BoxCollider>();

	GetOwner()->register_handler(*this, &CameraFocus::OnCollisionStarted);
	GetOwner()->register_handler(*this, &CameraFocus::OnCollisionPersisted);
	GetOwner()->register_handler(*this, &CameraFocus::OnCollisionEnded);
}

void CameraFocus::Update() {
	if (!strawss) {
		strawss = objectManager->FindObjectByName(stra_name);
	}

	//target = vec2(GetOwner()->mTransform->position);
}

void CameraFocus::Terminate() {
	GetOwner()->unregister_handler(*this, &CameraFocus::OnCollisionStarted);
	GetOwner()->unregister_handler(*this, &CameraFocus::OnCollisionPersisted);
	GetOwner()->unregister_handler(*this, &CameraFocus::OnCollisionEnded);
}

void CameraFocus::load(Json::Value & val) {
	active = val["CameraFocus"].get("active", true).asBool();
	staticFocus = val["CameraFocus"].get("staticFocus", true).asBool();
}

void CameraFocus::save(Json::Value & val) {
	val["CameraFocus"]["active"] = active;
	val["CameraFocus"]["staticFocus"] = staticFocus;
}

void CameraFocus::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("CameraFocus")) {

		ImGui::Checkbox("static focus", &staticFocus);

		ImGui::NewLine();
		if (ImGui::Button("Detach##CameraFocus"))
			GetOwner()->detach(this);
	}
}

void CameraFocus::OnCollisionStarted(const CollisionStartedEvent & object) { 
	if (object.other_object->GetName() == stra_name) {
		target = vec2(GetOwner()->mTransform->position) - vec2(strawss->mTransform->position);
	}
}

void CameraFocus::OnCollisionPersisted(const CollisionPersistedEvent & object) {
	if (object.other_object->GetName() == stra_name) {
		if (staticFocus) {
			target = vec2(GetOwner()->mTransform->position) - vec2(strawss->mTransform->position);
		}
		if (!comp) {
			comp = strawss->GetComponent<CameraLogic>();
		}
		else {
			comp->offset = target;
		}
	}
}

void CameraFocus::OnCollisionEnded(const CollisionEndedEvent & object) {
	if (object.other_object->GetName() == stra_name) {
		if (comp) {
			comp->offset = vec2();
		}
	}
}
