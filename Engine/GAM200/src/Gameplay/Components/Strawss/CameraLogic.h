#ifndef CAMERALOGIC_H
#define CAMERALOGIC_H

#include "../../../Component/Component.h"

class Transform;
class Camera;
class RigidBody;
class PlayerController;

class CameraLogic : public Component
{
	friend class CameraFocus;
	enum CameraState {translateBack, translateFront, idle} cameraState;
	enum CameraStatey { translateBacky, translateFronty, idley } cameraStatey;
	
	void UpdateCamreBounds();

	void LeterBoxing();
	GameObject *CreateBar(float y);
	void FocusBars(float y);
	void UnFocusBars(float y);
 public:
	 CameraLogic();

	 virtual void Initialize();
	 virtual void Update();
	 virtual void Terminate();

	 virtual void load(Json::Value & val);
	 virtual void save(Json::Value & val);
	 virtual void WriteToImguiWindow();

	 bool focus;
	 bool move;

private:
	void ParallaxMovement();

	RigidBody * rb;
	Transform * tr;
	Camera * camera;
	Camera * front_camera, * back_camera, * background_camera;
	PlayerController * Stra;
	GameObject *black_bars[2];
	vec2 offset;

	float innerBound;
	float outerBound;
	float innerBoundy;
	float outerBoundy;
};

#endif