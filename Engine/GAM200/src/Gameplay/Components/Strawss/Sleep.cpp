#include "Sleep.h"
#include "../../../Editor/ImGuiEditor.h"
#include "../../../Gameplay/Components/Strawss/States/StrawssParam.h"
#include "../../../ObjectManager/ObjectManager.h"
#include "../../../Gfx/Comp/Transform.h"

void Sleep::Initialize()
{
	transform = GetOwner()->mTransform;
	animator = GetOwner()->mAnimator;

	if (Strawss::GetParam().direction == -1)
		transform->FlipVertical();

	animator->ChangeAnimation("strawss_fall_asleep");
	animator->Loop(false);
	animator->Reset();
	animator->Play();
	animator->currentFrame = 0;
	went_to_sleep = true;
}

void Sleep::Update()
{
	if (animator->finished && went_to_sleep)
	{
		animator->ChangeAnimation("strawss_sleep");
		animator->Loop(true);
		animator->Reset();
		animator->Play();

		went_to_sleep = false;
	}
	Strawss::GetParam().body_position = transform->position;
}

void Sleep::Terminate()
{
}

void Sleep::load(Json::Value & val)
{
	active = val["Sleep"].get("active", true).asBool();
}

void Sleep::save(Json::Value & val)
{
	val["Sleep"]["active"] = active;
}

void Sleep::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Sleep"))
	{
	}

	if (ImGui::Button("Detach##Sleep"))
		GetOwner()->detach(this);
}
