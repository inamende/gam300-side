#pragma once
#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Gfx/Comp/Animator.h"

class Sleep : public Component
{
public:
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

private:
	bool went_to_sleep = false;
	Transform * transform = nullptr;
	Animator * animator = nullptr;
};