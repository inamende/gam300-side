#ifndef PLAYER_CONTROLLER_H
#define PLAYER_CONTROLLER_H

#include "../../../StateMachine/StateMachine.h"
#include "States\StrawssParam.h"

class Transform;
class Enemy;
class CollisionStartedEvent;
class CollisionPersistedEvent;
class CollisionEndedEvent;

class PlayerController : public StateMachine
{
public:
	PlayerController();
	~PlayerController();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void OnCollisionStarted(const CollisionStartedEvent & spot);

	Transform * tr;
	//possession
	Strawss & StrawssThings = Strawss::GetParam();
	Enemy * PossEnemy;
	Transform * EnemyTr;
	BoxCollider *them_toes;
	BoxCollider *them_body;

	float initialScale;
	float init_z;
	//oscars Additions
	Enemy * GetPossEnemy()
	{
		return PossEnemy;
	}
	bool IsPossessingAnEnemy()
	{
		if (GetPossEnemy() == nullptr)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
};

void create_body_at_position(const glm::vec3 spawn_position);

#endif
