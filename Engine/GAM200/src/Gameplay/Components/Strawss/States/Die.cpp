#include "Die.h"
#include "StrawssParam.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Gfx/Comp/Transform.h"

Die::Die(const char * name) :
	State(name)
{}

void Die::Enter() {
	m_actor->mAnimator->ChangeAnimation("strawss_die");
	m_actor->mAnimator->Loop(false);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();
}

void Die::Update() {
	if (!anim) anim = m_actor->mAnimator;
	else {
		if (anim->finished) {
			m_owner->ChangeState("FlyAway");
		}
	}
}