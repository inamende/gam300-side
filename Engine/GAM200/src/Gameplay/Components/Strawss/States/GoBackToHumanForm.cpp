#include "GoBackToHumanForm.h"
#include "../GAM200/src/Editor/ImGuiEditor.h"
#include "../GAM200/src/ObjectManager/GameObject.h"
#include "StrawssParam.h"
#include "../../../../Utils/Utilities.h"
#include "../../../../Editor/ImGuiEditor.h"
#include "../../../../Gfx/Comp/Animator.h"

//debug
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Postprocessor.h"

//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../../../Physics/Comp/RigidBody.h"
#include "../../../../Gameplay/Components/Strawss/PlayerController.h"

bool GoBackToHumanForm::collision_ended = false;

GoBackToHumanForm::GoBackToHumanForm(const char * name) : State(name)
{
}

void GoBackToHumanForm::Enter()
{
	animation_finished = false;

	//std::cout << "Entering The Specter Return State" << std::endl;
	//oscars additions
	//NOTE, here the input is without the .mp3
	SoundEmitter * e = m_actor->GetComponent<SoundEmitter>();
	if (e)
	{
		//std::cout << "Playing the Specter Return sound once" << std::endl;
		e->PlayOnce("StrawssSpecterReturn.mp3", false);
	}
	

	m_actor->mAnimator->ChangeAnimation("spectre_death");
	m_actor->mAnimator->Loop(false);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();

	player_original_pos = m_actor->mTransform->position;
	player_pos = m_actor->mTransform;
	index = 0;
	m_actor->register_handler(*this, &GoBackToHumanForm::OnCollisionStarted);
}

void GoBackToHumanForm::Exit()
{
	m_actor->unregister_handler(*this, &GoBackToHumanForm::OnCollisionStarted);
	m_actor->GetComponent<BoxCollider>()->collision_group = player;
	
	//SoundEmitter * e = Strawss->GetComponent<SoundEmitter>();
	SoundEmitter * e = m_actor->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}

void GoBackToHumanForm::Update()
{
	if (m_actor->mAnimator->currentFrame == 5)
		animation_finished = true;

	if (animation_finished)
	{
		m_actor->mAnimator->SetAnimationFrame(0);
		m_actor->mAnimator->Pause();
			
		auto  & param = Strawss::GetParam();

		GoBack(player_original_pos, param.body_position, index);
		index += ImGui::GetIO().DeltaTime * param.ghost_return_speed;
	}
	else
		m_actor->mRigidBody->ForceFullStop();
}

void GoBackToHumanForm::GoBack(glm::vec3 origin, glm::vec3 dest, float t)
{
	if (t >= 1)
	{
		m_owner->ChangeState("wakeup");
		return;
	}


	graphics->InterpolateFilters(Strawss::GetParam().HUE, Strawss::GetParam().SAT, 0, glm::vec3(), 0, t*t);

	// este men...
	player_pos->position.x = Lerp<float>(origin.x, dest.x, t*t);
	player_pos->position.y = Lerp<float>(origin.y, dest.y, t*t);
	player_pos->position.z = m_actor->GetComponent<PlayerController>()->init_z;
}

void GoBackToHumanForm::OnCollisionStarted(const CollisionStartedEvent & ev)
{
	auto& param = Strawss::GetParam();

	if (ev.other_object == strawss.body)
	{
		if (collision_ended)
		{
			m_owner->ChangeState("wakeup");
		}

		collision_ended = false;
	}
}

