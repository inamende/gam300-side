#ifndef DIE_H
#define DIE_H

#include "../../../../StateMachine/StateMachine.h"

class Animator;

class Die : public State
{
public:
	Die(const char * name);

	void Enter();
	void Update();

	Animator *anim = nullptr;
};

#endif
