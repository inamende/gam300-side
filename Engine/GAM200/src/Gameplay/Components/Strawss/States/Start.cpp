#include "Start.h"

#include "StrawssParam.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Level/Scene.h"

Start::Start(const char * name) : 
	State(name),
	anim(nullptr)
{}

void Start::Enter() {
	if (Strawss::GetParam().he_dead) {
		anim = m_actor->mAnimator;
		anim->ChangeAnimation("strawss_wakeup");
		anim->Loop(false);
		anim->Reset();
		anim->Pause();
		if (Strawss::GetParam().SpawnAtCheckPoint)
		{
			m_actor->mTransform->position = Strawss::GetParam().CheckPointSpawnPos;
		}
	}
	if (scene->currLevel == "TutorialLevel.json" && first_play) {
		wake_up = false;
		anim = m_actor->mAnimator;
		anim->ChangeAnimation("strawss_sleep");
		anim->Loop(true);
		anim->Play();
	}
}

void Start::Update() {
	if (Strawss::GetParam().he_dead) {
		if (m_time_in_state >= 1.0 && m_time_in_state <= 1.1) {
			anim->Play();
		}
		if (anim->finished) {
			Strawss::GetParam().he_dead = false;
			m_owner->ChangeState("Idle");
		}
	}
	else if (scene->currLevel == "TutorialLevel.json" && first_play) {
		if (!wake_up && m_time_in_state >= 6.0 ) {
			anim->ChangeAnimation("strawss_wakeup");
			anim->Loop(false);
			anim->Reset();
			wake_up = true;
		}
		if (wake_up && anim->finished) {
			first_play = false;
			m_owner->ChangeState("Idle");
		}
	}
	else {
		m_owner->ChangeState("Idle");
	}
}