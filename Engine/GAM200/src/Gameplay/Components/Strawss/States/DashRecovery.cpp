#include "DashRecovery.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "StrawssParam.h"
#include "../../../../Gfx/Comp/Transform.h"

void DashRecovery::Enter()
{
	animator = m_actor->mAnimator;

	animator->ChangeAnimation("spectre_dash_recovery");
	animator->Loop(false);
	animator->Reset();
	animator->Play();

	Strawss::GetParam().did_dash = true;
}

void DashRecovery::Update()
{
	if (animator->finished)
	{
		auto transform = m_actor->mTransform;

		m_owner->ChangeState("Spectre");
	}
	else
		m_actor->mRigidBody->ForceFullStop();
}

void DashRecovery::Exit()
{

}
