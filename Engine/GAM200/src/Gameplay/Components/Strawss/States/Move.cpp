#include "Move.h"

#include "StrawssParam.h"

#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Input/Input.h"
#include "../PlayerController.h"
#include "StrawssParam.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "imGui/imgui.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
#include "../../../../Gameplay/Components/Juice/JuiceBar.h"
Move::Move(const char * name) : State(name) {}

void Move::Enter()
{
	m_actor->mAnimator->ChangeAnimation("strawss_run");
	m_actor->mAnimator->Loop(true);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();

	rigid = m_actor->mRigidBody;
	transform = m_actor->mTransform;

	//oscars additions
	//std::cout << "Entering The Move State" << std::endl;
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			//	std::cout << "Playing Move Sound looped" << std::endl;
			e->PlayLooped("StrawssDirtRun.mp3", false);
			//std::cout << "Volume of Moved is: "<<e->GetVoice()->GetVolume() << std::endl;
		}
	}
}
void Move::Exit()
{
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			if (e->IsInit)
				e->StopVoice();
		}
			
	}
}
void Move::Update()
{
	if (rigid->velocity.y < -20.f)
		m_owner->ChangeState("Fall");

	if (KeyDown(Keyboard::E) || ButtonDown(SDL_CONTROLLER_BUTTON_Y))
	{
		JuiceBar * j = m_actor->GetComponent<JuiceBar>();
		if (j)
		{
			if (!Strawss::GetParam().Denied)
				if (j->AbilityUsageCheck())
				{
						m_owner->ChangeState("Spectre");
				}
		}
		else
			if (!Strawss::GetParam().Denied)
				m_owner->ChangeState("Spectre");

	}

	if (KeyDown(Keyboard::SPACE) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
		m_owner->ChangeState("Jump");

	if (KeyPressed(Keyboard::D) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
		Strawss::GetParam().direction = 1;

	else if (KeyPressed(Keyboard::A) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
		Strawss::GetParam().direction = -1;

	else
		m_owner->ChangeState("Idle");
		
	MakeMove();
}

void Move::MakeMove()
{
	if ((rigid->velocity.x * Strawss::GetParam().direction) < 0)
		rigid->velocity.x += 50 * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) <= Strawss::GetParam().maxSpeed)
		rigid->velocity.x += Strawss::GetParam().speed * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) > Strawss::GetParam().maxSpeed)
		rigid->velocity.x = Strawss::GetParam().maxSpeed * Strawss::GetParam().direction;

	if (rigid->velocity.y < 0)
		rigid->velocity += vec2(0.f, 1.f)  * rigid->gravity.y * (Strawss::GetParam().fallMultplier - 1) * ImGui::GetIO().DeltaTime;
}
