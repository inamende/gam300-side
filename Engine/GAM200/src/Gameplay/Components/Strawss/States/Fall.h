#ifndef FALL_H
#define FALL_H

#include "../../../../StateMachine/StateMachine.h"

class RigidBody;
class BoxCollider;
class CollisionPersistedEvent;
class Animator;

class Fall : public State
{
public:
	Fall(const char * name);

	void Enter();
	void Exit();
	void Update();

	void MakeMove();
	void Decelerate();

	RigidBody * rigid = nullptr;
	Animator * animator = nullptr;
	BoxCollider *them_toes = nullptr;
	BoxCollider *them_body = nullptr;

	void OnCollisionPersisted(const CollisionPersistedEvent & obj);
};

#endif
