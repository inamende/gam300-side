#pragma once

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Physics/Physics.h"

class DashRecovery : public State
{
public:
	DashRecovery(const char * name) : State(name) {}

	void Enter();
	void Update();
	void Exit();

private:
	Animator * animator;

	vec2 direction;

};
