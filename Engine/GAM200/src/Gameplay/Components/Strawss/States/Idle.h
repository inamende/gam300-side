#ifndef IDLE_H
#define IDLE_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"

class RigidBody;
class Chaman;

class Idle : public State
{
public:
	Idle(const char * name);

	void Enter();
	void Update();

	void Decelerate();

	RigidBody * rigid = nullptr;
	Transform * transform = nullptr;
	float Counter = 0.0f;
};

#endif
