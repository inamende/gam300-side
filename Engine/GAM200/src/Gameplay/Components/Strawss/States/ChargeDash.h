#pragma once

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Physics/Physics.h"

class ChargeDash : public State
{
public:
	ChargeDash(const char * name) : State(name) {}

	void Enter();
	void Update();
	vec2 normalized_direction();
private:
	Animator * animator;

	vec2 direction;

};
