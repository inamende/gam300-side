#pragma once

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Physics/Physics.h"

class RigidBody;

class Land : public State
{
public:
	Land(const char * name) : State(name) {}

	void Enter();
	void Exit();
	void Update();

	Animator * animator = nullptr;
};