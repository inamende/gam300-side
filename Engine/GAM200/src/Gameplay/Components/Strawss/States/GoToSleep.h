#pragma once

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gameplay/Components/Strawss/PlayerController.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../Gfx/Comp/Animator.h"


class GoToSleep : public State
{
public:
	GoToSleep(const char* name) : State(name) {}

	void Enter();
	void Update();

private:
	Transform * transform = nullptr;
	Animator * animator = nullptr;
};