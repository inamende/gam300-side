#include "Jump.h"

#include "StrawssParam.h"

#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Input/Input.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "imGui/imgui.h"

//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
Jump::Jump(const char * name) : State(name) {}

void Jump::Enter()
{
	m_actor->mAnimator->ChangeAnimation("strawss_jump");
	m_actor->mAnimator->Loop(false);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();

	rigid = m_actor->mRigidBody;
	//std::cout << "Entering The Jump State" << std::endl;
	//oscars additions
	if (rigid)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			e->PlayOnce("StrawssDirtJump.mp3", false);
			//std::cout << "Playing the jump sound once" << std::endl;
		}
	}
	MakeJump();
}

void Jump::Exit() 
{
	SoundEmitter * e = rigid->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}

void Jump::Update()
{
	if (rigid->velocity.y <= 0.f)
		m_owner->ChangeState("Fall");

	bool jumping = mInput->using_controller ? ButtonPressed(SDL_CONTROLLER_BUTTON_A) : KeyPressed(Keyboard::SPACE);
	if (!jumping || m_time_in_state >= 0.3f)
	{
		rigid->velocity += vec2(0.f, 1.f) * rigid->gravity.y * (Strawss::GetParam().jumpMultplier - 1) * ImGui::GetIO().DeltaTime;
	}

	if (KeyPressed(Keyboard::D) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		Strawss::GetParam().direction = 1;
		MakeMove();
	}

	else if (KeyPressed(Keyboard::A) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		Strawss::GetParam().direction = -1;
		MakeMove();
	}
		
	else
		Decelerate();
}

void Jump::MakeJump()
{
	m_actor->mRigidBody->velocity.y = 0.f;
	m_actor->mRigidBody->acceleration.y = 0.f;

	m_actor->mRigidBody->velocity.y = Strawss::GetParam().jumpVelocity;
}

void Jump::MakeMove()
{
	if ((rigid->velocity.x * Strawss::GetParam().direction) < 0)
		rigid->velocity.x += 50 * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) <= Strawss::GetParam().maxSpeed)
		rigid->velocity.x += Strawss::GetParam().speed * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) > Strawss::GetParam().maxSpeed)
		rigid->velocity.x = Strawss::GetParam().maxSpeed * Strawss::GetParam().direction;
}

void Jump::Decelerate()
{
	if (abs(rigid->velocity.x) > 0)
		rigid->velocity.x -= (rigid->velocity.x) / Strawss::GetParam().movementDrag;
}
