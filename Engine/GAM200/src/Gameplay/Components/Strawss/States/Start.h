#ifndef START_H
#define START_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"

class Animator;

class Start : public State
{
public:
	Start(const char * name);

	void Enter();
	void Update();

	Animator *anim;
	bool wake_up;
};

#endif
