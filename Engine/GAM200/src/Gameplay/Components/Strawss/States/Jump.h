#ifndef JUMP_H
#define JUMP_H

#include "../../../../StateMachine/StateMachine.h"

class RigidBody;
class CollisionStartedEvent;
class BoxCollider;

class Jump : public State
{
public:
	Jump(const char * name);

	void Enter();
	void Exit() override;
	void Update();

	void MakeJump();
	void MakeMove();
	void Decelerate();

	RigidBody * rigid = NULL;

};

#endif
