#pragma once
#include "../../../../StateMachine/StateMachine.h"
#include "../GAM200/src/Level/Scene.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Physics/Physics.h"
#include "StrawssParam.h"


class Dash : public State
{
public:
	Dash(const char * name) : State(name) {}

	void Enter();
	void Exit() override;
	void Update();
	void do_the_dash();
	vec2 normalized_direction();

private:
	float time_dashing;
	Strawss & param = Strawss::GetParam();
	RigidBody * rigid_body;
	Transform * transform;

	vec2 direction;

	float mDashForceMultiplier;
	bool is_dashing;
};