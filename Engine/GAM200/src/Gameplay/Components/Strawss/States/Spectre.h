#pragma once

#include "../../../../StateMachine/StateMachine.h"
#include "../GAM200/src/Physics/Comp/RigidBody.h"
#include "../../../../Physics/Physics.h"
#include "StrawssParam.h"

class Spectre : public State
{
public:
	Spectre(const char * name);

	void Enter();
	virtual void Exit();
	void Update();

	bool CheckIfInRange();
	void ChangePos(unsigned returning = 1);
	glm::vec2 GetDirectionOfMovement();
	glm::vec2 ApplyDashForce(glm::vec2 & temp);

	glm::vec3 VectorToBody;
	glm::vec3 ItemPos;
	//bool IsDashing = false;
	float mMaxDashTime = 2;
	float mCurrentDashTimer;
	float Distance;
	float GhostLimit = 1000;
	glm::vec3 mDirectionOfMovement3;
	glm::vec2 mDirectionOfMovement2;
	float mDashForceMultiplier;
	RigidBody * mRigidBodyPointer = nullptr;


	Strawss & param = Strawss::GetParam();
	PlayerController * strawss_controller = nullptr;
	RigidBody * rigid_body = nullptr;
	Transform * transform = nullptr;

	bool is_touching_body;

	bool did_dash;

private:

	void OnCollisionEnded(const CollisionEndedEvent & ev);
	void OnCollisionPersisted(const CollisionPersistedEvent & ev);

};