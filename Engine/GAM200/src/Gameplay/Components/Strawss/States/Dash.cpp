#include "Dash.h"
#include "../../../../Editor/ImGuiEditor.h"
#include "../../../../Physics/CollisionTable.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../../../Gfx/Comp/Animator.h"

#include <glm/gtx/vector_angle.hpp>

void Dash::Enter()
{
	auto animator = m_actor->mAnimator;

	animator->ChangeAnimation("spectre_dash_blur");

	BoxCollider * collider = m_actor->GetComponent<BoxCollider>();
	collider->collision_group = player;

	rigid_body = m_actor->mRigidBody;
	transform = m_actor->mTransform;

	time_dashing = 0;
	mDashForceMultiplier = 30;

	param.IsDashing = true;
	direction = normalized_direction();

	TABLE_FORMAT & collision_table = CollissionTable::getInstance();

	collision_table[player][SpctreWalls] = 3;
	collision_table[SpctreWalls][player] = 3;
}

void Dash::Exit()
{
	Strawss::GetParam().IsDashing = false;
	TABLE_FORMAT & collision_table = CollissionTable::getInstance();
	collision_table[player][SpctreWalls] = 1;
	collision_table[SpctreWalls][player] = 1;
}

void Dash::Update()
{
	glm::vec2 horizontal(1.0, 0.0);

	auto param = Strawss::GetParam();

	//Transf->rotation = Transf->position.y > Direction.y ? glm::angle(Shoot_at, horizontal) : -glm::angle(Shoot_at, horizontal);
	transform->rotation = transform->position.y > param.DashDestination.y ? glm::angle(direction, horizontal) : -glm::angle(direction, horizontal);

	m_actor->mRigidBody->velocity.y = 0;
	if (transform->position == param.prev_pos)
	{
		m_owner->ChangeState("DashRecovery");
		return;
	}

	if (time_dashing < param.dash_time)
		do_the_dash();
	else
	{
		m_owner->ChangeState("DashRecovery");

		TABLE_FORMAT collision_table = CollissionTable::getInstance();

		collision_table[player][SpctreWalls] = 1;
		collision_table[SpctreWalls][player] = 1;

		rigid_body->ForceFullStop();
		param.IsDashing = false;
	}

	//param.prev_pos = transform->position;
}

void Dash::do_the_dash()
{
	//std::cout << "dashing" << std::endl;

	//rigid_body->AddForce(direction*mDashForceMultiplier);
	transform->position.x += direction.x*mDashForceMultiplier;
	transform->position.y += direction.y*mDashForceMultiplier;

	time_dashing += ImGui::GetIO().DeltaTime;
}

vec2 Dash::normalized_direction()
{
	auto param = Strawss::GetParam();

	auto pos_vec2 = vec2{ transform->position.x, transform->position.y };
	glm::vec2 direction = param.DashDestination - pos_vec2;

	return  glm::normalize(direction);//normalized direction
}
