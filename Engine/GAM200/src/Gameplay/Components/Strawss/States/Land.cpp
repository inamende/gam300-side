#include "Land.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Input/Input.h"

void Land::Enter()
{
	animator = m_actor->mAnimator;

	animator->ChangeAnimation("strawss_jump");
	animator->currentFrame = 10;
	animator->Loop(false);
}

void Land::Exit()
{
}

void Land::Update()
{
	if (animator->finished)
	{
		if (KeyPressed(Keyboard::D) || KeyPressed(Keyboard::A) || 
			ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT) ||
			JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
			m_owner->ChangeState("Move");
		else
			m_owner->ChangeState("Idle");
	}
}