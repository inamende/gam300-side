#include "Idle.h"

#include "StrawssParam.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gameplay/Components/Strawss/PlayerController.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Input/Input.h"
#include "imGui/imgui.h"
#include "../../Enemies/Chaman/Chaman.h"
#include "../../../../Gameplay/Components/Juice/JuiceBar.h"

Idle::Idle(const char * name) : State(name) {}

void Idle::Enter()
{
	m_actor->mAnimator->ChangeAnimation("strawss_idle");
	m_actor->mAnimator->Loop(true);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();

	rigid = m_actor->mRigidBody;
	transform = m_actor->mTransform;
	Strawss::GetParam().original_scale = transform->scale;
}

void Idle::Update()
{
	graphics->InterpolateFilters(Strawss::GetParam().HUE, Strawss::GetParam().SAT, 0, glm::vec3(), 0, m_time_in_state);
	Decelerate();

	if (KeyDown(Keyboard::E) || ButtonDown(SDL_CONTROLLER_BUTTON_Y))
	{
		JuiceBar * j = m_actor->GetComponent<JuiceBar>();
		if (j)
		{
			if (!Strawss::GetParam().Denied)
				if (j->AbilityUsageCheck())
				{
					m_owner->ChangeState("Spectre");
				}
		}
		else
			if (!Strawss::GetParam().Denied)
				m_owner->ChangeState("Spectre");
	}
	
	if (KeyPressed(Keyboard::R))
	{
		Counter += ImGui::GetIO().DeltaTime;
		if (Counter >= 1.5f)
		{
			m_owner->ChangeState("FlyAway");
		}
	}

	if (KeyUp(Keyboard::R))
	{
		Counter = 0.0f;
	}

	if (KeyDown(SDL_SCANCODE_SPACE) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
		m_owner->ChangeState("Jump");

	if (KeyDown(SDL_SCANCODE_D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		Strawss::GetParam().direction = 1;
		m_owner->ChangeState("Move");
	}
	else if (KeyDown(SDL_SCANCODE_A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		Strawss::GetParam().direction = -1;
		m_owner->ChangeState("Move");
	}

	if (rigid->velocity.y < 0)
		rigid->velocity += vec2(0.f, 1.f)  * rigid->gravity.y * (Strawss::GetParam().fallMultplier - 1) * ImGui::GetIO().DeltaTime;
}

void Idle::Decelerate()
{
	if (abs(rigid->velocity.x) > 0)
		rigid->velocity.x -= (rigid->velocity.x) / Strawss::GetParam().movementDrag;
}