#include "WakeUp.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "StrawssParam.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Postprocessor.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../GAM200/src/Editor/ImGuiEditor.h"

void WakeUp::Enter()
{
	RigidBody * rigid_body = m_actor->mRigidBody;
	objectManager->RemoveGameObject(strawss.body);
	strawss.body = nullptr;

	auto & param = Strawss::GetParam();
	animator = m_actor->mAnimator;
	m_actor->mTransform->scale = param.original_scale;
	param.initialScale = fabs(param.original_scale.x);
	m_actor->mTransform->rotation = 0;
	 
	animator->ChangeAnimation("strawss_wakeup");
	animator->Loop(false);
	animator->Reset();
	animator->Play();

	m_actor->mTransform->position = vec3(param.body_position.x, param.body_position.y, m_actor->mTransform->position.z);
	m_actor->GetComponent<BoxCollider>("BoxCollider_0")->collision_group = player;
	index = 0;
}

void WakeUp::Update()
{
	auto  & param = Strawss::GetParam();

	if (animator->finished && index >= 1)
		m_owner->ChangeState("Idle");

	auto delta_time = ImGui::GetIO().DeltaTime;
	index += delta_time * param.ghost_return_speed;
	graphics->InterpolateFilters(Strawss::GetParam().HUE, Strawss::GetParam().SAT, 0, glm::vec3(), 0, index * index);
}

void WakeUp::Exit()
{

}
