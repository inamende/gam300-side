#include "MoveRight.h"

#include "StrawssParam.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "imgui/imgui.h"

MoveRight::MoveRight(const char * name) :
	State::State(name)
{}

void MoveRight::Enter() {
	m_actor->mAnimator->ChangeAnimation("strawss_run");
	m_actor->mAnimator->Loop(true);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();

	rigid = m_actor->mRigidBody;
	transform = m_actor->mTransform;
}

void MoveRight::Update() {
	Strawss::GetParam().direction = 1;
	MakeMove();
}

void MoveRight::MakeMove() {
	if ((rigid->velocity.x * Strawss::GetParam().direction) < 0)
		rigid->velocity.x += 50 * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) <= Strawss::GetParam().maxSpeed)
		rigid->velocity.x += Strawss::GetParam().speed * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) > Strawss::GetParam().maxSpeed)
		rigid->velocity.x = Strawss::GetParam().maxSpeed * Strawss::GetParam().direction;

	if (rigid->velocity.y < 0)
		rigid->velocity += vec2(0.f, 1.f)  * rigid->gravity.y * (Strawss::GetParam().fallMultplier - 1) * ImGui::GetIO().DeltaTime;
}