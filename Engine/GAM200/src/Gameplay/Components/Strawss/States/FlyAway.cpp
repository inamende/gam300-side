#include "FlyAway.h"
#include "StrawssParam.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "ImGui/imgui.h"
#include "../../../../Utils/Utilities.h"
#include "../../../../Level/Scene.h"

FlyAway::FlyAway(const char * name) :
	State(name)
{}

void FlyAway::Enter() {
	Strawss::GetParam().he_dead = true;
	m_actor->mAnimator->ChangeAnimation("strawss_bat");
	m_actor->mAnimator->Loop(true);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();
}

void FlyAway::Update() {
	box = m_actor->GetComponent<BoxCollider>();
	if (!box) box = m_actor->GetComponent<BoxCollider>();
	else {
		box->ChangeTriggerState(true);
	}
	body = m_actor->GetComponent<RigidBody>();
	if (!body) body = m_actor->GetComponent<RigidBody>();
	else {
		body->gravity = vec2();
	}
	float angle = -1.5708;
	float dist = 1000;
	float amp = 10.f;
	float x_inc = glm::cos(angle + glm::sin(m_time_in_state * 1) * amp) * dist * ImGui::GetIO().DeltaTime;
	float y_inc = glm::sin(angle + glm::sin(m_time_in_state * 1) * amp) * dist * ImGui::GetIO().DeltaTime;
	m_actor->mTransform->position.x += x_inc;
	m_actor->mTransform->position.y += (m_time_in_state > 3.f) ? 3.f : y_inc;
	scene->restartSignal = graphics->FadeIn(-60, m_time_in_state * 0.7f);
}