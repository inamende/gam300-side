#include "../GAM200/src/EngineCommon.h"
#include "../../../../ObjectManager/GameObject.h"

#ifndef STRAWSS_PARAM_H
#define STRAWSS_PARAM_H

struct Strawss
{
	int direction{ 1 };
	int HP{ 3 };

	float jumpVelocity{ 800.f };
	float maxSpeed{ 300.f };
	float speed{ 500.f };
	float movementDrag{ 1.f };
	float fallMultplier{ 2000.f };
	float jumpMultplier {1500.f};
	float GhostLimit{ 500 };
	float ghost_return_speed{ 2 };
	float GhostDrag{ 100 };
	float Distance{};
	bool CheckToJump{};
	float lengthPercent{};
	float dash_time{ 0.2f };
	float mDashForceMultiplier{};
	vec3 prev_pos{};

	bool IsDashing{};
	float time_since_last_dash{}; // time since dash has been done
	const  float dash_cooldown{.0f};
	bool did_dash{};

	const float spectre_scale_x{ 2.1025f };
	const float spectre_scale_y{ 0.92f };
	glm::vec3 original_scale{};
	float initialScale{};

	GameObject * body{};

	glm::vec3 body_position{};
	glm::vec2 DashDestination{};
	glm::vec3 DashPosition{};
	glm::vec3 vector_to_body{};

	//CheckPoint
	glm::vec3 CheckPointSpawnPos{};
	bool SpawnAtCheckPoint{ false };

	float HUE {-30};
	float SAT {-29};
	float BRI {-3};
	float DEN {-3};

	float S_HUE{ -180 };
	float S_SAT{ -18 };
	float S_BRI{ -3 };

	bool he_dead{ false };

	//chaman
	bool Denied = false;

	static Strawss& GetParam();
};

extern Strawss strawss;

#endif