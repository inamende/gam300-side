#ifndef FLY_AWAY_H
#define FLY_AWAY_H

#include "../../../../StateMachine/StateMachine.h"

class BoxCollider;
class RigidBody;

class FlyAway : public State
{
public:
	FlyAway(const char * name);

	void Enter();
	void Update();
private:
	BoxCollider *box;
	RigidBody *body;
};

#endif
