#ifndef MOVE_H
#define MOVE_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"

class RigidBody;
class BoxCollider;

class Move : public State
{
public:
	Move(const char * name);

	void Enter();
	void Update();
	void Exit();
	void MakeMove();

	RigidBody * rigid = nullptr;
	Transform * transform = nullptr;
	BoxCollider *them_toes = nullptr;
	BoxCollider *them_body = nullptr;
};

#endif
