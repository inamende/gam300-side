#include "GoToSleep.h"
#include "../../../../ObjectManager/GameObject.h"


void GoToSleep::Enter()
{
	transform = m_actor->mTransform;
	animator = m_actor->mAnimator;

	animator->ChangeAnimation("strawss_fall_asleep");
	animator->Loop(false);
	animator->Reset();
	animator->Play();
}

void GoToSleep::Update()
{
	if (animator->finished)
	{
		m_owner->ChangeState("Spectre");
		create_body_at_position(transform->position);
	}
	else
	{
		m_actor->mRigidBody->ForceFullStop();
	}
}
