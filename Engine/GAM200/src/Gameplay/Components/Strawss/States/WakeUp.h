#pragma once

#include "../GAM200/src/StateMachine/StateMachine.h"
#include "../../../../ObjectManager/GameObject.h"


class WakeUp : public State
{
public:
	WakeUp(const char * name) : State(name) {}

	void Enter();
	void Update();
	void Exit() override;

private:
	float index = 0;
	Animator * animator;
};