#include "ChargeDash.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "StrawssParam.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "Dash.h"

#include <glm/gtx/vector_angle.hpp>

void ChargeDash::Enter()
{
	animator = m_actor->mAnimator;

	animator->ChangeAnimation("spectre_dash_anticip");
	animator->Loop(false);
	animator->Reset();
	animator->Play();

	auto  & param = Strawss::GetParam();
	Transform * transform = m_actor->mTransform;
	param.original_scale = transform->scale;
	//transform->scale.x = param.original_scale.x * param.spectre_scale_x; // why doesn't this work??
	//transform->scale.y = param.original_scale.y * param.spectre_scale_y;
	transform->scale.x = 169.570;
	transform->scale.y = 107.290;
	param.initialScale = transform->scale.x;
}

void ChargeDash::Update()
{
	if (animator->finished)
		m_owner->ChangeState("Dash");
	else
		m_actor->mRigidBody->ForceFullStop();
}

vec2 ChargeDash::normalized_direction()
{
	auto  & param = Strawss::GetParam();
	Transform * transform = m_actor->mTransform;

	auto pos_vec2 = vec2{ transform->position.x, transform->position.y };
	glm::vec2 direction = param.DashDestination - pos_vec2;

	return  glm::normalize(direction);//normalized direction
}
