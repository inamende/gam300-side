#pragma once

#include "../GAM200/src/StateMachine/StateMachine.h"
#include "../GAM200/src/Level/Scene.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Physics/Physics.h"


class GoBackToHumanForm : public State
{
public:
	GoBackToHumanForm(const char * name);

	void Enter();
	void Exit();
	void Update();
	void GoBack(vec3 origin, vec3 dest, float index);

	static bool collision_ended;

private:
	void OnCollisionStarted(const CollisionStartedEvent & ev);

	bool animation_finished = false;
	float index;
	Transform * player_pos;
	vec3 player_original_pos;
};