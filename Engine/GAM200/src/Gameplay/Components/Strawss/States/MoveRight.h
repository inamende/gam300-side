#ifndef MOVERIGHT_H
#define MOVERIGHT_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"

class RigidBody;

class MoveRight : public State
{
public:
	MoveRight(const char * name);

	void Enter();
	void Update();
	void MakeMove();

	RigidBody * rigid = nullptr;
	Transform * transform = nullptr;
};

#endif
