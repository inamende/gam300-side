#include "Spectre.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Input/Input.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../Gfx/Postprocessor.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Reso/Camera.h"
#include "../../Strawss/States/StrawssParam.h"
#include "imGui/imgui.h"
#include "GoBackToHumanForm.h"
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
#include "../PlayerController.h"
#include "../../Enemies/Enemy.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../Enemies/Knight/Knight.h"
#include "../../../../Menu/Cursor.h"


Spectre::Spectre(const char * name) : State(name) {}

void Spectre::Enter()
{
	is_touching_body = false;

	BoxCollider * collider = m_actor->GetComponent<BoxCollider>();
	collider->collision_group = player;

	transform = m_actor->mTransform;
	
	transform->scale = param.original_scale;
	//param.initialScale = param.direction = -1 ? param.original_scale.x : -param.original_scale.x;
	param.initialScale = fabs(param.original_scale.x);

	if (strawss.body == nullptr) {
		create_body_at_position(transform->position);
	}
		
	m_actor->GetComponent<BoxCollider>()->collision_group = Specter;
	m_actor->mAnimator->ChangeAnimation("spectre_idle");
	m_actor->mAnimator->Loop(true);
	m_actor->mAnimator->Reset();
	m_actor->mAnimator->Play();

	rigid_body = m_actor->mRigidBody;

	auto  & param = Strawss::GetParam();
	mRigidBodyPointer = m_actor->GetComponent<RigidBody>();
	//oscars additions
	if (mRigidBodyPointer)
	{
		//NOTE, here the input is without the .mp3
		SoundEmitter * e = mRigidBodyPointer->GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			//void SoundEmitter::PlayOnce(std::string SoundName, bool init_pause)
			//std::cout << "Playing the Whoosh sound once" << std::endl;
			e->PlayOnce("Whoosh.mp3", false);
		}
	}

	m_actor->register_handler(*this, &Spectre::OnCollisionEnded);
	m_actor->register_handler(*this, &Spectre::OnCollisionPersisted);

	//param.body_position = m_actor->mTransform->position;
	mMaxDashTime = 2;
	mCurrentDashTimer = 0;
	
	//possession
	param.IsDashing = false;
	param.prev_pos = mRigidBodyPointer->transform->position;
	strawss_controller = m_actor->GetComponent<PlayerController>();

	m_actor->mTransform->rotation = 0;

	if(param.did_dash)
		param.time_since_last_dash = 0;
	else 
		param.time_since_last_dash = param.dash_cooldown;
}

void Spectre::Update()
{
	if (param.time_since_last_dash >= param.dash_cooldown)
		did_dash = false;

	if(param.did_dash)
		param.time_since_last_dash += ImGui::GetIO().DeltaTime;

	graphics->InterpolateFilters(Strawss::GetParam().S_HUE, Strawss::GetParam().S_SAT, 0, glm::vec3(), 0, m_time_in_state);
	if (rigid_body)
	{
		rigid_body->velocity.x = (rigid_body->velocity.y = 0);
		rigid_body->acceleration.x = (rigid_body->acceleration.y = 0);
	}

	if (!strawss_controller->PossEnemy)
	{
		if (KeyPressed(Keyboard::W) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_UP) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTY) < 0.f)
		{
			m_actor->mTransform->position.y += 10;
		}
		if (KeyPressed(Keyboard::A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
		{
			m_actor->mTransform->position.x -= 10;
			Strawss::GetParam().direction = -1;
		}
		if (KeyPressed(Keyboard::S) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_DOWN) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTY) > 0.f)
		{
			m_actor->mTransform->position.y -= 10;
		}
		if (KeyPressed(Keyboard::D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
		{
			m_actor->mTransform->position.x += 10;
			Strawss::GetParam().direction = 1;
		}
	}
	if (KeyDown(Keyboard::E) || ButtonDown(SDL_CONTROLLER_BUTTON_Y))
	{
		if (strawss_controller->PossEnemy != nullptr)
		{
			m_actor->GetComponent<BoxCollider>()->ChangeTriggerState(false);
			m_actor->mAnimator->visible = true;

			strawss_controller->PossEnemy->possesed = false;
			m_actor->mTransform->position.x = strawss_controller->PossEnemy->GetOwner()->mTransform->position.x;
			m_actor->mTransform->position.y = strawss_controller->PossEnemy->GetOwner()->mTransform->position.y;
			m_actor->mTransform->position.y += strawss_controller->PossEnemy->GetOwner()->mTransform->position.y / 1.5;

			Knight * type = dynamic_cast<Knight *>(strawss_controller->PossEnemy);

			if (type != nullptr && !type->const_retreat_point)
			{
				type->retreatPoint = strawss_controller->PossEnemy->GetOwner()->mTransform->position.x;
			}
			strawss_controller->PossEnemy = nullptr;
		}

		if (is_touching_body)
			m_owner->ChangeState("wakeup");
		else
			m_owner->ChangeState("GoBackToHumanForm");
	}

	if (CheckIfInRange() && !(param.IsDashing))
	{
		ChangePos();
	}

	if (MouseDown(SDL_BUTTON_RIGHT) || ButtonDown(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER) || (Strawss::GetParam().Denied && strawss_controller->PossEnemy != nullptr))
	{
		if (param.time_since_last_dash < param.dash_cooldown)
			return;

		param.did_dash = true;

		PlayerController * Stra = m_actor->GetComponent<PlayerController>();
		if (Stra->PossEnemy != nullptr)
		{
			m_actor->GetComponent<BoxCollider>()->ChangeTriggerState(false);
			m_actor->mAnimator->visible = true;

			Stra->PossEnemy->possesed = false;
			m_actor->mTransform->position.x = Stra->PossEnemy->GetOwner()->mTransform->position.x;
			m_actor->mTransform->position.y = Stra->PossEnemy->GetOwner()->mTransform->position.y;

			Knight * type = dynamic_cast<Knight *>(Stra->PossEnemy);

			if (type != nullptr && !type->const_retreat_point)
			{
				type->retreatPoint = Stra->PossEnemy->GetOwner()->mTransform->position.x;
			}

			Stra->PossEnemy = nullptr;
			return;
		}

		m_owner->ChangeState("ChargeDash");
		auto & param = Strawss::GetParam();
		param.DashDestination = mInput->using_controller ? scene->cursor->GetComponent<Cursor>()->get_camera_coord("default") : graphics->GetCameraByRenderLayer("default")->ViewportToWorld(mousePosition);
		param.DashPosition = m_actor->mTransform->position;
		return;
	}

	auto  & param = Strawss::GetParam();
	param.prev_pos = mRigidBodyPointer->transform->position;
}

void Spectre::Exit()
{
	m_actor->unregister_handler(*this, &Spectre::OnCollisionEnded);
	m_actor->unregister_handler(*this, &Spectre::OnCollisionPersisted);
	SoundEmitter * e = mRigidBodyPointer->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}

bool Spectre::CheckIfInRange()
{
	Distance = glm::distance(vec2(strawss.body_position), vec2(m_actor->mTransform->position));
	
	if (Distance > GhostLimit)
		return true;

	return false;
}

void Spectre::ChangePos(unsigned returning)
{
	auto  & param = Strawss::GetParam();

	//the further you are the stronger it will pull you
	float lengthPercent = (Distance * returning / GhostLimit);

	if (lengthPercent <= 1.0f)
	{
		lengthPercent = 1.0f;
	}
	else if (lengthPercent >= 5.0f)
	{
		lengthPercent = 5.0f;
	}

	param.vector_to_body = m_actor->mTransform->position - param.body_position;

	m_actor->mTransform->position.x -= param.vector_to_body.x / (param.GhostDrag / lengthPercent);
	m_actor->mTransform->position.y -= param.vector_to_body.y / (param.GhostDrag / lengthPercent);
}

glm::vec2 Spectre::GetDirectionOfMovement()
{
	//std::cout << "Getting Direction" << std::endl;
	glm::vec3 temp = mRigidBodyPointer->GetTransform()->position - mRigidBodyPointer->prevPosition;
	mDirectionOfMovement3 = temp;
	mDirectionOfMovement2.x = temp.x;
	mDirectionOfMovement2.y = temp.y;
	return mDirectionOfMovement2;
}
glm::vec2 Spectre::ApplyDashForce(glm::vec2 & temp)
{
	temp.x = temp.x * mDashForceMultiplier;
	temp.y = temp.y * mDashForceMultiplier;
	return temp;
}

void Spectre::OnCollisionEnded(const CollisionEndedEvent & ev)
{
	if (ev.other_object == strawss.body)
	{
		GoBackToHumanForm::collision_ended = true;
		is_touching_body = false;
	}
}

void Spectre::OnCollisionPersisted(const CollisionPersistedEvent & ev)
{
	if (ev.other_object == strawss.body)
	{
		is_touching_body = true;
	}
}
