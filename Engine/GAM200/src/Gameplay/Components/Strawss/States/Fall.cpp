#include "Fall.h"

#include "StrawssParam.h"

#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Gfx/Comp/Animator.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../Input/Input.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "imGui/imgui.h"

Fall::Fall(const char * name) : State(name) {}

void Fall::Enter()
{
	animator = m_actor->mAnimator;

	animator->ChangeAnimation("strawss_fall");
	animator->Loop(true);
	animator->Reset();
	animator->Play();

	rigid = m_actor->mRigidBody;
	m_actor->register_handler(*this, &Fall::OnCollisionPersisted);
}
void Fall::Exit()
{
	m_actor->unregister_handler(*this, &Fall::OnCollisionPersisted);
}

void Fall::Update()
{
	if (rigid->velocity.y == 0.f) {
		m_owner->ChangeState("Land");
	}
	
	if (KeyDown(Keyboard::SPACE) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
	{
		m_actor->mTransform->position.y += 1;
	}

	if (KeyPressed(Keyboard::D) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) > 0.f)
	{
		Strawss::GetParam().direction = 1;
	}

	else if (KeyPressed(Keyboard::A) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || JoyStickDirection(SDL_CONTROLLER_AXIS_LEFTX) < 0.f)
	{
		Strawss::GetParam().direction = -1;
		MakeMove();
	}

	else
		Decelerate();

	if (rigid->velocity.y < 0)
		rigid->velocity += vec2(0.f, 1.f)  * rigid->gravity.y * (Strawss::GetParam().fallMultplier - 1) * ImGui::GetIO().DeltaTime;
}

void Fall::MakeMove()
{
	if ((rigid->velocity.x * Strawss::GetParam().direction) < 0)
		rigid->velocity.x += 50 * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) <= Strawss::GetParam().maxSpeed)
		rigid->velocity.x += Strawss::GetParam().speed * Strawss::GetParam().direction;

	if (abs(rigid->velocity.x) > Strawss::GetParam().maxSpeed)
		rigid->velocity.x = Strawss::GetParam().maxSpeed * Strawss::GetParam().direction;
}

void Fall::Decelerate()
{
	if (abs(rigid->velocity.x) > 0)
		rigid->velocity.x -= (rigid->velocity.x) / Strawss::GetParam().movementDrag;
}

void Fall::OnCollisionPersisted(const CollisionPersistedEvent & obj) {
	if (!them_toes) them_toes = m_actor->GetComponent<BoxCollider>("BoxCollider_1");
	if (!them_body) them_body = m_actor->GetComponent<BoxCollider>("BoxCollider_0");
	if (!them_toes || !them_body) {
		return;
	}
	std::vector<Collider*> other_colliders = obj.other_object->GetComponents<Collider>();
	for (auto col : other_colliders) {
		if (col == them_body) {
			continue;
		}
		if (col->IsTrigger()) {
			continue;
		}
		if (!CollideAABBs(col, them_toes, NULL)) {
			continue;
		}
		m_owner->ChangeState("Land");
		return;
	}
}
