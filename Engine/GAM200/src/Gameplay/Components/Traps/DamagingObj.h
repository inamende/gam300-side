#ifndef DMGOBJ_H
#define DMGOBJ_H

#include "../../../ObjectManager/GameObject.h"

class CollisionStartedEvent;

class DamagingObj : public Component
{
public:
	DamagingObj();
	~DamagingObj();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void OnCollisionStarted(const CollisionStartedEvent & object);

	float PreviousYVelocity;

};



#endif
