#include "DamagingObj.h"

#include "../../../Editor/ImGuiEditor.h"
#include "../../../Physics/Comp/RigidBody.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "../../Components/Enemies/Bowman/Arrow/ArrowDirection.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Physics/Physics.h"
//oscars additions
#include "../../../Audio/SoundEmitter.h"
#include "../../../Gameplay/Components/Enemies/Knight/Box/BoxLogic.h"
DamagingObj::DamagingObj() : PreviousYVelocity(0.0f)
{
}

DamagingObj::~DamagingObj()
{
}

void DamagingObj::Initialize()
{
	GetOwner()->register_handler(*this, &DamagingObj::OnCollisionStarted);
}

void DamagingObj::Update()
{
	RigidBody * Vel = GetOwner()->GetComponent<RigidBody>();
	PreviousYVelocity = abs(Vel->velocity.y);

	//if (GetOwner()->mTransform->position.z > 100)
	//	std::cout << "helolo!@ bugyv";
}

void DamagingObj::Terminate()
{
	GetOwner()->unregister_handler(*this, &DamagingObj::OnCollisionStarted);
}

void DamagingObj::load(Json::Value & val)
{
	active = val["DamagingObj"].get("active", true).asBool();
}

void DamagingObj::save(Json::Value & val)
{
	val["DamagingObj"]["active"] = active;
}

void DamagingObj::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("DamagingObj"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Detach##DamagingObj"))
			GetOwner()->detach(this);
	}
}

void DamagingObj::OnCollisionStarted(const CollisionStartedEvent & object)
{
	BoxCollider * Box = GetOwner()->GetComponent<BoxCollider>("BoxCollider_1");
	ArrowDirection * Arrow = object.other_object->GetComponent<ArrowDirection>();
	////oscars additiuons
	////sounds
	//if ((object.other_object->GetName() == "Bowman") || (object.other_object->GetName() == "Knight")
	//	|| (object.other_object->GetName() == "Chaman"))
	//{
	//	SoundEmitter * e = NULL;
	//	//mJuiceBarPointer = objectManager->FindObjectByName(StrawssName)->GetComponent<JuiceBar>();
	//	GameObject * g = objectManager->FindObjectByName("GeneralPurposeSoundEmitter");
	//	e = g->GetComponent<SoundEmitter>();
	//	if (e)
	//	{
	//		BoxLogic * b = GetOwner()->GetComponent<BoxLogic>();
	//		if (b != nullptr)
	//		{
	//			//e->PlaySound("WoodenDoorBreak.mp3", 1);
	//			e->PlayOnce("WoodenDoorBreak.mp3", false);
	//		}
	//		else
	//		{
	//			//e->PlaySound("IronClang.mp3", 1);
	//			e->PlayOnce("IronClang.mp3", false);
	//		}
	//	}
	//}
	
	
	if (Arrow)
	{
		if (Box)
		{
			BoxCollider * Box2 = object.other_object->GetComponent<BoxCollider>();
			if(Box2)
			if (CollideAABBs(Box, Box2, NULL))
			{
				RigidBody * Body = GetOwner()->GetComponent<RigidBody>();
				GetOwner()->mRigidBody->invMass = 0.000001f;
				GetOwner()->mRigidBody->gravity.y = -0.001f;
				physics->SwapContainer(Box, false);
				//GetOwner()->detach(Box);
				Box->GetTransform()->scale.x = 0;
				Box->GetTransform()->scale.y = 0;
			}
		}
	}
	
}
