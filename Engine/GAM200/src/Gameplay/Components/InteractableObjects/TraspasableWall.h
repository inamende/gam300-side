#ifndef WALL_H
#define WALL_H

//#include "../../Component/Component.h"

#include "../../Engine/GAM200/src/Component/Component.h"

class Wall : public Component
{
public:
	Wall();
	~Wall();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

#endif
