#pragma once
#include <string>
#include "../../Engine/GAM200/src/Component/Component.h"

struct InteractableEvent;
class GameObject;

class DoorReactor : public Component
{
public:
	DoorReactor();
	~DoorReactor();
	
	virtual void Initialize();
	virtual void Update();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void OpenDoor(const InteractableEvent &);
	void ChangeOpenStatus();

	// the bool recieved it's just to tell the function which point you want to set
	// true for Start Point, false for End Point
	void SetNewPoint(bool State);

private:
	//When The door is closed it will be at position  A
	bool opening;
	glm::vec2 StartPoint;
	glm::vec2 EndPoint;

	float mCurrTime = 0.0f;
	float mDuration = 1.0f;

	float * CurrentPositionX;
	float * CurrentPositionY;

	bool MultipleSwitches;

	bool IsBridge;

	enum AxesState
	{
		Y, X, XY
	};

	std::vector<std::string> Axes;

	//true if close false if open
	bool DefaultState;

	AxesState AxesOfMovement;

	GameObject *strawss;

	bool change_to_open;
};