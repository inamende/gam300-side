#include "DoorReactor.h"
#include "Interactable.h"
#include "../../../Editor/ImGuiEditor.h"
#include "../GAM200/src/ObjectManager/ObjectManager.h"
#include "../GAM200/src/Gfx/Comp/Transform.h"
#include "../GAM200/src/ObjectManager/GameObject.h"
#include "../GAM200/src/Physics/Comp/BoxCollider.h"
#include "../GAM200/src/Gfx/Comp/Animator.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

#include "../../../Gfx/Gfx.h"
#include "../../../Gfx/Reso/Camera.h"
DoorReactor::DoorReactor() : StartPoint(0, 0), EndPoint(0, 0), opening(false), MultipleSwitches(false), AxesOfMovement(Y), DefaultState(false), IsBridge(false), strawss(nullptr), change_to_open(false)
{
	Axes.push_back("Y");
	Axes.push_back("X");
	Axes.push_back("XY");
	opening = false;
}

DoorReactor::~DoorReactor() 
{
	GetOwner()->unregister_handler((*this), &DoorReactor::OpenDoor);
	SoundEmitter * e = GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}


void DoorReactor::Initialize()
{
	GetOwner()->register_handler((*this), &DoorReactor::OpenDoor);
	CurrentPositionX = &(GetOwner()->GetComponent<Transform>()->position.x);
	CurrentPositionY = &(GetOwner()->GetComponent<Transform>()->position.y);


	if (DefaultState)
	{
		opening = false;
		*CurrentPositionX = StartPoint.x;
		*CurrentPositionY = StartPoint.y;
	}
	else
	{
		opening = true;
		*CurrentPositionX = EndPoint.x;
		*CurrentPositionY = EndPoint.y;
	}
}

void DoorReactor::Update()
{
	if (!strawss) {
		strawss = objectManager->FindObjectByName("Strawss");
	}

	mCurrTime += ImGui::GetIO().DeltaTime;
	mCurrTime = glm::clamp(mCurrTime, 0.0f, mDuration);
	float tn = mCurrTime / mDuration;
	//if we are openening we go from Start/Current to End
	if (opening)
	{
		switch (AxesOfMovement)
		{
		case X:
			*CurrentPositionX = glm::mix(*CurrentPositionX, EndPoint.x, tn);
			break;
		case Y:
			*CurrentPositionY = glm::mix(*CurrentPositionY, EndPoint.y, tn);
			break;
		case XY:
			*CurrentPositionX = glm::mix(*CurrentPositionX, EndPoint.x, tn);
			*CurrentPositionY = glm::mix(*CurrentPositionY, EndPoint.y, tn);
			break;
		}
	}
	//else we go from End/Current to Start
	else
	{
		switch (AxesOfMovement)
		{
		case X:
			*CurrentPositionX = glm::mix(*CurrentPositionX, StartPoint.x, tn);
			break;
		case Y:
			*CurrentPositionY = glm::mix(*CurrentPositionY, StartPoint.y, tn);
			break;
		case XY:
			*CurrentPositionX = glm::mix(*CurrentPositionX, StartPoint.x, tn);
			*CurrentPositionY = glm::mix(*CurrentPositionY, StartPoint.y, tn);
			break;
		}
	}
	static Animator *anim = GetOwner()->GetComponent<Animator>();
	if (!anim) {
		anim = GetOwner()->GetComponent<Animator>();
		if (anim) anim->loop = false;
	}
	else if (anim->finished){
		strawss->local_handler.handle(CameraEvent(0));
		strawss->local_handler.handle(CameraEvent(3));
	}
	static Camera *cam = graphics->GetMainCamera();
	if (change_to_open) {
		vec3 dir = GetOwner()->mTransform->position - cam->position;
		if (dir != vec3()) dir = normalize(dir);
		cam->position.x += 5 * dir.x;
		cam->position.y += 5 * dir.y;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;

		if (cam->position.x == GetOwner()->mTransform->position.x) {
			anim->Play();
			ChangeOpenStatus();
			change_to_open = false;
		}
	}
}
	
void DoorReactor::load(Json::Value & val)
{
	StartPoint.x = val["DoorReactor"]["StartPoint"].get("x", 0.0).asFloat();
	StartPoint.y = val["DoorReactor"]["StartPoint"].get("y", 0.0).asFloat();
	EndPoint.x = val["DoorReactor"]["EndPoint"].get("x", 0.0).asFloat();
	EndPoint.y = val["DoorReactor"]["EndPoint"].get("y", 0.0).asFloat();
	mDuration = val["DoorReactor"].get("mDuration", 1.0f).asFloat();
	AxesOfMovement = static_cast<AxesState>(val["DoorReactor"].get("mAxisState", 1).asInt());
	DefaultState = val["DoorReactor"].get("DefaultState", true).asBool();
	IsBridge = val["DoorReactor"].get("IsBridge", false).asBool();
}
void DoorReactor::save(Json::Value & val)
{
	val["DoorReactor"]["StartPoint"]["x"] = StartPoint.x;
	val["DoorReactor"]["StartPoint"]["y"] = StartPoint.y;
	val["DoorReactor"]["EndPoint"]["x"] = EndPoint.x;
	val["DoorReactor"]["EndPoint"]["y"] = EndPoint.y;
	val["DoorReactor"]["mDuration"] = mDuration;
	val["DoorReactor"]["mAxisState"] = static_cast<unsigned int>(AxesOfMovement);
	val["DoorReactor"]["DefaultState"] = DefaultState;
	val["DoorReactor"]["IsBridge"] = IsBridge;
}
void DoorReactor::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("DoorReactor"))
	{
		ImGui::Checkbox("active##DoorCollider", &active);
		ImGuiEditor::InputFloat2("StartPoint##DoorReactor", StartPoint);
		ImGuiEditor::InputFloat2("EndPoint##DoorReactor", EndPoint);
		ImGui::SliderFloat("mDuration##DoorReactor", &mDuration, 1.0f, 500.0f);
		if (ImGui::Button("Axes Of Movement"))
			ImGui::OpenPopup("Axes Of Movement");

		if (ImGui::BeginPopup("Axes Of Movement"))
		{
			ImGui::Text("Axes Of Movement");
			ImGui::Separator();

			for (unsigned i = 0; i < 3; i++)
			{
				if (ImGui::Selectable(Axes[i].c_str()))
				{
					AxesOfMovement = static_cast<AxesState>(i);
				}
			}
			ImGui::EndPopup();
		}

		if (ImGui::Button("DebugOpen##DoorReactor"))
		{
			ChangeOpenStatus();
		}

		if (ImGui::Button("Set as Start Point"))
		{
			SetNewPoint(true);
		}

		if (ImGui::Button("Set as End Point"))
		{
			SetNewPoint(false);
		}

		if (DefaultState)
		{
			ImGui::Text("Default State is Closed");
		}
		else
		{
			ImGui::Text("Default State is Open");
		}

		if (ImGui::Button("Change Default State"))
		{
			DefaultState = !DefaultState;
		}

		if (ImGui::Checkbox("Is A bridge?", &IsBridge))
		{
		}

			ImGui::NewLine();
		if (ImGui::Button("Detach##DoorReactor"))
			GetOwner()->detach(this);

	}
}

void DoorReactor::OpenDoor(const InteractableEvent &)
{
	//oscars additions
	//NOTE, here the input is without the .mp3
	SoundEmitter * e = GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (!IsBridge)
		{
			e->PlayOnce("MetalGrateRustyOpen.mp3", false);
		}
		else
		{
			e->PlayOnce("DrawBridge.mp3", false);

		}
	}
	strawss->local_handler.handle(CameraEvent(2));
	change_to_open = true;
}

void DoorReactor::ChangeOpenStatus()
{
	if (!IsBridge)
	{
		if (opening)
		{
			opening = false;
		}
		else
		{
			opening = true;
		}
	}
	else
	{
		//llega aqui si es un puente, aqui tenemos que hacer que el collider 1 deje de ser ghost
		//y el collider 0 lo sea.
		if (GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"))
		{
			GetOwner()->GetComponent<BoxCollider>("BoxCollider_0")->ChangeTriggerState(true);
		}
		if (GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"))
		{
			GetOwner()->GetComponent<BoxCollider>("BoxCollider_1")->ChangeTriggerState(false);
		}

		GetOwner()->mTransform->position.z = 4.5;
	}
	mCurrTime = 0.0f;
}

void DoorReactor::SetNewPoint(bool State)
{
	if (State)
	{
		StartPoint.x = *CurrentPositionX;
		StartPoint.y = *CurrentPositionY;
	}
	else
	{
		EndPoint.x = *CurrentPositionX;
		EndPoint.y = *CurrentPositionY;
	}
}

