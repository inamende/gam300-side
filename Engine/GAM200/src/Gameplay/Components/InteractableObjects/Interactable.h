#ifndef INTERACTABLE_H
#define INTERACTABLE_H

#include "../../Engine/GAM200/src/Events/event.h"
#include "../../Engine/GAM200/src/Component/Component.h"

class GameObject;
class CollisionStartedEvent;
class CollisionEndedEvent;
class CollisionStartedEvent;
using std::string;

struct InteractableEvent : public Event
{
	InteractableEvent() {}
};

class CameraEvent : public Event
{
public:
	CameraEvent(int f) : foc(f) {}
	int foc;
};

//A component that will make it so the player can send events or do something and
//	this will coll the specific handle function
struct Interactable : public Component
{
public:
	Interactable();
	~Interactable();
	
	void GetAvailableItems();
	std::vector<GameObject*> Targets;
	std::vector<std::string> ToLoad;
	std::vector<GameObject*> AvailabeItems;
	GameObject *strawss;
};

#endif