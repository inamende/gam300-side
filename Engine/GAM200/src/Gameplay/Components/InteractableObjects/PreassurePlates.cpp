#include "PreassurePlates.h"
#include "../../../../src/Editor/ImGuiEditor.h"
#include "../../../../src/ObjectManager/ObjectManager.h"
#include "../../../../src/Physics/Comp/BoxCollider.h"
#include "../../../../src/ObjectManager/GameObject.h"
#include "../../../../src/Gfx/Comp/Renderable.h"
#include "../../../../src/ResourceManager/ResoManager.h"
#include "../../../../src/Gfx/Reso/Texture.h"
#include "../../../../src/Audio/SoundEmitter.h"
#include "../../../../src/Audio/Audio.h"

#include "../../../../src/Gameplay/Components/Enemies/Bowman/Bowman.h"
#include "../../../Gameplay/Components/Enemies/Knight/Box/BoxLogic.h"
#include "../../../../src/Gameplay/Components/Enemies/Knight/Knight.h"

PreassurePlates::PreassurePlates() : ThingsOnTop(0)
{
}

PreassurePlates::~PreassurePlates()
{
}

void PreassurePlates::Initialize()
{
	ObjectManager * Om = objectManager;

	for (unsigned i = 0; i < ToLoad.size(); i++)
	{
		Targets.push_back(Om->FindObjectByName(ToLoad[i]));
	}

	GetOwner()->register_handler(*this, &PreassurePlates::SendEventToTarget);
	GetOwner()->register_handler(*this, &PreassurePlates::BoxOrKnightOnTop);
	GetOwner()->register_handler(*this, &PreassurePlates::BoxOrKnightNotOnTop);

	Collider * temp = GetOwner()->GetComponent<BoxCollider>();
	if (temp)
	{
		if (!temp->IsTrigger())
		{
			temp->ChangeTriggerState(true);
		}
	}
	GetAvailableItems();
}

void PreassurePlates::Update()
{
	if (ThingsOnTopLastF != ItemsOnTop.size())
	{
		GetOwner()->GetComponent<Renderable>()->texture = (ItemsOnTop.size() == 0) ? (resoManager->getReso<Texture>("pressure_up.png")) : (resoManager->getReso<Texture>("pressure_down.png"));
	}
	ThingsOnTopLastF = ItemsOnTop.size();
	TriggerOnce = true;
}

void PreassurePlates::Terminate()
{
	this->GetOwner()->unregister_handler((*this), &PreassurePlates::SendEventToTarget);
}

void PreassurePlates::load(Json::Value & val)
{
	Targets.clear();
	ToLoad.clear();
	ObjectManager * Om = objectManager;
	active = val["PreassurePlates"].get("active", true).asBool();
	for (unsigned i = 0; i < val["PreassurePlates"]["Targets"]["name"].size(); i++)
	{
		ToLoad.push_back(val["PreassurePlates"]["Targets"]["name"][i].asString());
	}
}

void PreassurePlates::save(Json::Value & val)
{
	val["PreassurePlates"]["active"] = active;
	for (unsigned i = 0; i < Targets.size(); i++)
	{
		if (Targets[i])
		{
			val["PreassurePlates"]["Targets"]["name"].append(Targets[i]->GetName());
		}
	}
}

void PreassurePlates::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Preassure Plate"))
	{
		ImGui::Checkbox("active##Preassure Plate", &active);

		ImGui::Text("MAKE SURE TO NAME EVERYTHING PROPERLY");

		ImGui::Text("Press this button before adding a target");

		if (ImGui::Button("Get Availabale targets on screen"))
		{
			GetAvailableItems();
		}

		if (ImGui::Button("Add target to the lever"))
			ImGui::OpenPopup("AvailabaleItems");

		ImGui::Text("Avoid adding a target twice");

		if (ImGui::BeginPopup("AvailabaleItems"))
		{
			for (unsigned i = 0; i < AvailabeItems.size(); i++)
				if (ImGui::Selectable(AvailabeItems[i]->GetName().c_str()))
				{
					Targets.push_back(AvailabeItems[i]);
				}
			ImGui::EndPopup();
		}

		if (ImGui::Button("Levers's current targets"))
			ImGui::OpenPopup("Targets");

		if (ImGui::BeginPopup("Targets"))
		{
			for (unsigned i = 0; i < Targets.size(); i++)
			{
				if (ImGui::Selectable(Targets[i]->GetName().c_str()))
				{
				}
			}
			ImGui::EndPopup();
		}

		if (ImGui::Button("Clear Preassure Plate's Targets"))
		{
			Targets.clear();
		}

		ImGui::NewLine();
		if (ImGui::Button("DebugSendLocalEvent##Interactable"))
		{
			//std::cout << "SENDING DEBUG BUTTON" << std::endl;
			SendEventToTarget(InteractableEvent());
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##Preassure Plate"))
			GetOwner()->detach(this);
	}
}

void PreassurePlates::SendEventToTarget(const InteractableEvent &)
{
	//oscars additions for the sound
	SoundEmitter * e = nullptr;
	e = GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->PlayOnce("PressurePlate.mp3", false);
	}
	for (unsigned i = 0; i < Targets.size(); i++)
	{
		if (Targets[i])
		{
			Targets[i]->local_handler.handle(InteractableEvent());
		}
	}
}

void PreassurePlates::BoxOrKnightOnTop(const CollisionStartedEvent & CSE)
{
	if (CSE.other_object->GetComponent<Knight>() || CSE.other_object->GetComponent<BoxLogic>() && TriggerOnce)
	{
		//if it's the knight the one on top we check if it's colliding with the collider 0
		if (CSE.other_object->GetComponent<Knight>())
		{
			//if it isn't we skip
			if (!CollideAABBs(CSE.other_object->GetComponent<BoxCollider>(), GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"), NULL))
			{
				return;
			}
		}
		//we check if the object was already in contact (Knights have 3 colliders)
		for (auto it : ItemsOnTop)
		{
			if (it == CSE.other_object)
			{
				return;
			}
		}
		//if it wasnt in contact we add it into the vector if dhit on top
		ItemsOnTop.push_back(CSE.other_object);
		//std::cout << ItemsOnTop.size() << std::endl;
		if ((ThingsOnTopLastF != ItemsOnTop.size()) && ThingsOnTopLastF == 0 && ItemsOnTop.size() < 2)
		{
			TriggerOnce = false;
			SendEventToTarget(InteractableEvent());
		}
	}
}

void PreassurePlates::BoxOrKnightNotOnTop(const CollisionEndedEvent & CEE)
{
	if (CEE.other_object->GetComponent<Knight>() || CEE.other_object->GetComponent<BoxLogic>())
	{
		//if the knight is still colliding we skip it
		if (CEE.other_object->GetComponent<Knight>())
		{
			if (CollideAABBs(CEE.other_object->GetComponent<BoxCollider>(), GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"), NULL))
			{
				return;
			}
		}
		//remove the item from the vector
		for (VectorIterator = ItemsOnTop.begin(); VectorIterator != ItemsOnTop.end(); VectorIterator++)
		{
			if (*VectorIterator == CEE.other_object)
			{
				ItemsOnTop.erase(VectorIterator);
				break;
			}
		}
		
		//std::cout << ItemsOnTop.size() << std::endl;
		if (ItemsOnTop.size() == 0 && TriggerOnce && (ItemsOnTop.size() != ThingsOnTopLastF))
		{
			SendEventToTarget(InteractableEvent());
			TriggerOnce = false;
		}
	}
}
