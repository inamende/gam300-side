#ifndef BULLSEYE_H
#define BULLSEYE_H

#include "Interactable.h"

class Bullseye : public Interactable
{
public:
	Bullseye();

	~Bullseye();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void HitByArrow(const CollisionStartedEvent & CSE);
	void SendEventToTarget(const InteractableEvent &);
};

#endif