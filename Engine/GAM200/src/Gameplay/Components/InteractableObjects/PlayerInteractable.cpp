#include "PlayerInteractable.h"

#include "../../../../src/ObjectManager/ObjectManager.h"
#include "../../../../src/ObjectManager/GameObject.h"
#include "../../../../src/Input/Input.h"
#include "Lever.h"
#include "../../../../src/Editor/ImGuiEditor.h"
#include "../../../../src/Physics/Physics.h"
#include "../../../../src/Physics/Comp/Collider.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "../../../../src/Physics/Physics.h"
#include "../../../../src/Gameplay/Components/Strawss/PlayerController.h"
#include "../../../../src/Gameplay/Components/Enemies/Bowman/Bowman.h"
#include "../Enemies/Knight/States/KnightParam.h"

PlayerInteractable::PlayerInteractable(): mTargetObject(NULL) {}

PlayerInteractable::PlayerInteractable(string name, string target, string event) : 
	mName(name),
	mTargetObject(NULL),
	can_activate(false)
{}

PlayerInteractable::~PlayerInteractable(){}


void PlayerInteractable::Initialize()
{
	this->GetOwner()->register_handler((*this), &PlayerInteractable::PlayerIsCurrentlyColliding);
	auto owner = GetOwner();
	owner->register_handler((*this), &PlayerInteractable::PlayerNoLongerColliding);
}
void PlayerInteractable::Update()
{
	
}
void PlayerInteractable::Terminate()
{
	this->GetOwner()->unregister_handler((*this), &PlayerInteractable::PlayerIsCurrentlyColliding);
	this->GetOwner()->unregister_handler((*this), &PlayerInteractable::PlayerNoLongerColliding);
}
void PlayerInteractable::load(Json::Value & val)
{
	mName = val["PlayerInteractable"].get("mName", "Player").asString();
}
void PlayerInteractable::save(Json::Value & val)
{
	val["PlayerInteractable"]["mName"] = mName;
}
void PlayerInteractable::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("PlayerInteractable"))
	{
		ImGui::NewLine();
		//debug send an InteractableEvent
		if (ImGui::Button("DebugSendEvent##Interactable"))
		{
			//std::cout << "SENDING DEBUG BUTTON" << std::endl;
			mTargetObject->GetComponent<Lever>()->SendEventToTarget(InteractableEvent());
		}
		if (ImGui::Button("Detach##Interactable"))
			GetOwner()->detach(this);
	}
}

void PlayerInteractable::PlayerIsCurrentlyColliding(const CollisionPersistedEvent & e)
{
	if (KnightP::GetParam().pulling_lever) {
		return;
	}
	if (e.other_object->GetComponent<Lever>())
	{
		if ((GetOwner()->GetComponent<Enemy>()->possesed == true))
		{
			if ((GetOwner()->GetComponent<Enemy>()->possesed == true))
			{
				if (CollideAABBs(e.other_object->GetComponent<BoxCollider>(), GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"), NULL))
				{
					if (KeyDown(Keyboard::Q) || ButtonDown(SDL_CONTROLLER_BUTTON_B)) {
						can_activate = true;
						e.other_object->GetComponent<Lever>()->SendEventToTarget(InteractableEvent());
					}
				}
			}
		}
	}
}

void PlayerInteractable::PlayerNoLongerColliding(const CollisionEndedEvent & e)
{
	if (e.other_object->GetComponent<Interactable>())
	{
		can_activate = false;
		mTargetObject = NULL;
	}
}
