#ifndef OPEN_D_H
#define OPEN_D_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../Interactable.h"

class OpenDoor : public State
{
public:
	OpenDoor(const char * name) : State(name) {}

	void Enter();
	void Update();
	void Exit();
	void OpenDoor_(const InteractableEvent &);
};

#endif
