#include "Door.h"
#include "../../GAM200/src/Editor/ImGuiEditor.h"
#include "../../GAM200/src/ObjectManager/GameObject.h"
#include "../../GAM200/src/ObjectManager/ObjectManager.h"
#include "Close.h"
#include "Moving.h"
#include "Open.h"
#include "../Bridge/BFocusing.h"

Door::Door() {}

void Door::Initialize()
{

	CurrentPositionX = &(GetOwner()->GetComponent<Transform>()->position.x);
	CurrentPositionY = &(GetOwner()->GetComponent<Transform>()->position.y);

	m_actor = GetOwner();

	AddState(DBG_NEW OpenDoor("Open"));
	AddState(DBG_NEW CloseDoor("Close"));
	AddState(DBG_NEW MovingDoor("Moving"));
	AddState(DBG_NEW BFocusing("Focus", true, "Moving"));
	AddState(DBG_NEW BFocusing("UnFocus", false, "Moving"));

	Axes.clear();

	if (DefaultState)
	{
		SetInitState("Close");
		Opening = false;
	}
	else
	{
		SetInitState("Open");
		Opening = true;
	}
	
	mInitialState->InternalEnter();

	GetOwner()->register_handler((*this), &Door::OpenDoor_);

	Axes.push_back("Y");
	Axes.push_back("X");
	Axes.push_back("XY");

}

void Door::Update()
{
	StatusUpdate();
}

void Door::Terminate()
{
	GetOwner()->unregister_handler((*this), &Door::OpenDoor_);
}

void Door::load(Json::Value & val)
{
	active = val["Door"].get("active", true).asBool();
	StartPoint.x = val["Door"]["StartPoint"].get("x", 0.0).asFloat();
	StartPoint.y = val["Door"]["StartPoint"].get("y", 0.0).asFloat();
	EndPoint.x = val["Door"]["EndPoint"].get("x", 0.0).asFloat();
	EndPoint.y = val["Door"]["EndPoint"].get("y", 0.0).asFloat();
	mDuration = val["Door"].get("mDuration", 1.0f).asFloat();
	AxesOfMovement = static_cast<AxesState>(val["Door"].get("mAxisState", 1).asInt());
}

void Door::save(Json::Value & val)
{
	val["Door"]["active"] = active;
	val["Door"]["StartPoint"]["x"] = StartPoint.x;
	val["Door"]["StartPoint"]["y"] = StartPoint.y;
	val["Door"]["EndPoint"]["x"] = EndPoint.x;
	val["Door"]["EndPoint"]["y"] = EndPoint.y;
	val["Door"]["mDuration"] = mDuration;
	val["Door"]["mAxisState"] = static_cast<unsigned int>(AxesOfMovement);
}

void Door::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("DoorReactor"))
	{
		ImGui::Checkbox("active##DoorCollider", &active);
		ImGuiEditor::InputFloat2("StartPoint##DoorReactor", StartPoint);
		ImGuiEditor::InputFloat2("EndPoint##DoorReactor", EndPoint);
		ImGui::SliderFloat("mDuration##DoorReactor", &mDuration, 1.0f, 500.0f);
		if (ImGui::Button("Axes Of Movement"))
			ImGui::OpenPopup("Axes Of Movement");

		if (ImGui::BeginPopup("Axes Of Movement"))
		{
			ImGui::Text("Axes Of Movement");
			ImGui::Separator();

			for (unsigned i = 0; i < 3; i++)
			{
				if (ImGui::Selectable(Axes[i].c_str()))
				{
					AxesOfMovement = static_cast<AxesState>(i);
				}
			}
			ImGui::EndPopup();
		}

		if (ImGui::Button("DebugOpen##Door"))
		{
			ChangeOpenStatus();
		}

		if (ImGui::Button("Set as Start Point"))
		{
			SetNewPoint(true);
		}

		if (ImGui::Button("Set as End Point"))
		{
			SetNewPoint(false);
		}

		if (mInitialState->m_name == "Open")
		{
			ImGui::Text("Default State is Open");
		}
		else if (mInitialState->m_name == "Close")
		{
			ImGui::Text("Default State is Closed");
		}

		if (ImGui::Button("Change Default State"))
		{
			if (mInitialState->m_name == "Open")
			{
				SetInitState("Close");
				Opening = !Opening;
				DefaultState = true;
			}
			else if (mInitialState->m_name == "Close")
			{
				SetInitState("Open");
				Opening = !Opening;
				DefaultState = false;
			}
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##DoorReactor"))
			GetOwner()->detach(this);

	}
}

void Door::OpenDoor_(const InteractableEvent &)
{
	ChangeOpenStatus();
}

void Door::ChangeOpenStatus()
{
	ChangeState("Moving");
	Opening = !Opening;
	mCurrTime = 0.0f;
}

void Door::SetNewPoint(bool State)
{
	if (State)
	{
		StartPoint.x = (*CurrentPositionX);
		StartPoint.y = (*CurrentPositionY);
	}
	else
	{
		EndPoint.x = (*CurrentPositionX);
		EndPoint.y = (*CurrentPositionY);
	}
}