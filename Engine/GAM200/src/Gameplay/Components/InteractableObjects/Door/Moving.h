#ifndef MOVING_D_H
#define MOVING_D_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../Interactable.h"
#include "Door.h"
class RigidBody;

class MovingDoor : public State
{
public:
	MovingDoor(const char * name) : State(name) {}

	void Enter();
	void Update();
	void Exit();
	Door* Owner;
};

#endif
