#include "Close.h"
#include "DoorParam.h"
#include "../../GAM200/src/ObjectManager/GameObject.h"
#include "../../GAM200/src/ObjectManager/ObjectManager.h"
#include "Door.h"
#include "../../../../Audio/SoundEmitter.h"


void CloseDoor::Enter()
{
	(*m_owner->GetOwner()->GetComponent<Door>()->CurrentPositionX) = m_owner->GetOwner()->GetComponent<Door>()->StartPoint.x;
	(*m_owner->GetOwner()->GetComponent<Door>()->CurrentPositionY) = m_owner->GetOwner()->GetComponent<Door>()->StartPoint.y;
}

void CloseDoor::Update()
{
	
}
void CloseDoor::Exit()
{
}
void CloseDoor::OpenDoor(const InteractableEvent &)
{
}