#ifndef CLOSE_D_H
#define CLOSE_D_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../Interactable.h"

class CloseDoor : public State
{
public:
	CloseDoor(const char * name) : State(name) {}

	void Enter();
	void Update();
	void Exit();
	void OpenDoor(const InteractableEvent &);
};

#endif
