#include "Moving.h"
#include "../../../../Editor/ImGuiEditor.h"
#include "../../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
#include "../../GAM200/src/ObjectManager/ObjectManager.h"
#include "../../GAM200/src/ObjectManager/GameObject.h"
#include "../../GAM200/src/Component/Component.h"
#include "../../GAM200/src/ObjectManager/GameObject.h"
#include "../../GAM200/src/ObjectManager/ObjectManager.h"
#include "Door.h"
void MovingDoor::Enter()
{
	SoundEmitter * e = m_owner->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
			e->PlayOnce("MetalGrateRustyOpen.mp3", false);
	}
	Owner = m_owner->GetOwner()->GetComponent<Door>();
}

void MovingDoor::Update()
{
	Owner->mCurrTime += ImGui::GetIO().DeltaTime;
	Owner->mCurrTime = glm::clamp(Owner->mCurrTime, 0.0f, Owner->mDuration);
	float tn = Owner->mCurrTime / Owner->mDuration;
	if (Owner->Opening)
	{
		switch (Owner->AxesOfMovement)
		{
		case Door::X:
			(*Owner->CurrentPositionX) = glm::mix((*Owner->CurrentPositionX), Owner->EndPoint.x, tn);
			break;
		case Door::Y:
			(*Owner->CurrentPositionY) = glm::mix((*Owner->CurrentPositionY), Owner->EndPoint.y, tn);
			break;
		case Door::XY:
			(*Owner->CurrentPositionX) = glm::mix((*Owner->CurrentPositionX), Owner->EndPoint.x, tn);
			(*Owner->CurrentPositionY) = glm::mix((*Owner->CurrentPositionY), Owner->EndPoint.y, tn);
			break;
		}
		if ((*Owner->CurrentPositionY) == Owner->EndPoint.y)
		{
			m_owner->ChangeState("Open");
		}
	}
	else
	{
		switch (Owner->AxesOfMovement)
		{
		case Door::X:
			(*Owner->CurrentPositionX) = glm::mix((*Owner->CurrentPositionX), Owner->StartPoint.x, tn);
			break;
		case Door::Y:
			(*Owner->CurrentPositionY) = glm::mix((*Owner->CurrentPositionY), Owner->StartPoint.y, tn);
			break;
		case Door::XY:
			(*Owner->CurrentPositionX) = glm::mix((*Owner->CurrentPositionX), Owner->StartPoint.x, tn);
			(*Owner->CurrentPositionY) = glm::mix((*Owner->CurrentPositionY), Owner->StartPoint.y, tn);
			break;
		}
		if ((*Owner->CurrentPositionY) == Owner->StartPoint.y)
		{
			m_owner->ChangeState("Closed");
		}
	}
}
void MovingDoor::Exit()
{
	SoundEmitter * e = m_actor->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}