#include "Open.h"
#include "DoorParam.h"
#include "../../GAM200/src/ObjectManager/GameObject.h"
#include "../../GAM200/src/ObjectManager/ObjectManager.h"
#include "Door.h"
#include "../../../../Audio/SoundEmitter.h"
void OpenDoor::Enter()
{
	(*m_owner->GetOwner()->GetComponent<Door>()->CurrentPositionX) = m_owner->GetOwner()->GetComponent<Door>()->EndPoint.x;
	(*m_owner->GetOwner()->GetComponent<Door>()->CurrentPositionY) = m_owner->GetOwner()->GetComponent<Door>()->EndPoint.y;
}

void OpenDoor::Update()
{
	//m_owner->ChangeState("UnFocus");
}
void OpenDoor::Exit()
{
}
void OpenDoor::OpenDoor_(const InteractableEvent &) {}