#ifndef DOOR_H
#define DOOR_H

#include "../../../../StateMachine/StateMachine.h"
#include "../Interactable.h"
#include "DoorParam.h"

class Door : public StateMachine
{
public:
	Door();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void OpenDoor_(const InteractableEvent &);
	void ChangeOpenStatus();

	void SetNewPoint(bool State);


	glm::vec2 StartPoint;
	glm::vec2 EndPoint;

	float mCurrTime = 0.0f;
	float mDuration;

	float * CurrentPositionX;
	float * CurrentPositionY;

	enum AxesState
	{
		Y, X, XY
	};

	std::vector<std::string> Axes;

	AxesState AxesOfMovement;

	bool Opening;

	bool DefaultState = true;
};

#endif