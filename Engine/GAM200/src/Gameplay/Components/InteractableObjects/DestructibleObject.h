#ifndef DESTRUCTIBLE_OBJECT_H
#define DESTRUCTIBLE_OBJECT_H

#include "../../../Component/Component.h"

class GameObject;
class CollisionStartedEvent;

class DestructibleObject : public Component
{
public:
	DestructibleObject();
	~DestructibleObject();
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	void OnCollisionStarted(const CollisionStartedEvent & eve);
};

#endif