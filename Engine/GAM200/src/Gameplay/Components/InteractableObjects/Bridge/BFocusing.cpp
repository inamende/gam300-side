#include "BFocusing.h"

#include "../../../../Gfx/Gfx.h"
#include "../../../../Gfx/Reso/Camera.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../Level/Scene.h"
#include "../../../../Gameplay/Components/Strawss/CameraLogic.h"

BFocusing::BFocusing(const char * name, bool focus, std::string st) :
	State::State(name),
	foc(focus),
	next_state(st)
{}

void BFocusing::Enter() {
	cam = graphics->GetMainCamera();
}

void BFocusing::Update() {
	if (!strawss) strawss = scene->FindObject("Strawss");
	if (strawss) {
		log = strawss->GetComponent<CameraLogic>();
	}
	if (!cam || !log) return;
	if (foc) {
		log->move = foc;
		log->focus = foc;
		if (cam->MoveOver(vec2(m_actor->mTransform->position), m_time_in_state * 0.05f)) {
			m_owner->ChangeState(next_state.c_str());
		}
	}
	else {
		if (m_time_in_state >= 1.f) {
			log->move = foc;
			log->focus = foc;
		}
	}
}
