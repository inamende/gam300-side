#ifndef B_FOCUSING_H
#define B_FOCUSING_H

#include "../../../../StateMachine/StateMachine.h"

class Camera;
class GameObject;
class CameraLogic;

class BFocusing : public State
{
public:
	BFocusing(const char * name, bool focus, std::string st);

	void Enter();
	void Update();
private:
	bool foc;
	Camera *cam;
	GameObject *strawss;
	CameraLogic *log;
	std::string next_state;
};

#endif
