#include "CloseB.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../Physics/Comp/BoxCollider.h"

void CloseBridge::Enter()
{
	if (m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"))
	{
		m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_0")->ChangeTriggerState(false);
	}
	if (m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"))
	{
		m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1")->ChangeTriggerState(true);
	}
}

void CloseBridge::Update()
{
}
