#ifndef BRIDGE_H
#define BRIDGE_H

#include "../../../../StateMachine/StateMachine.h"
#include "../Interactable.h"

class Bridge : public StateMachine
{
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow(); 

	void ChangeToOpen();
	void Open(const InteractableEvent &);
};

#endif