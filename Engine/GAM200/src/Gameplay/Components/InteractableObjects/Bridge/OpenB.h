#ifndef OPEN_B_H
#define OPEN_B_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"

class Camera;
class GameObject;
class CameraLogic;

class OpenBridge : public State
{
public:
	OpenBridge(const char * name) : State(name) {}

	void Enter();
	void Update();
private:
	Camera *cam;
	GameObject *strawss;
	CameraLogic *log;
};

#endif
