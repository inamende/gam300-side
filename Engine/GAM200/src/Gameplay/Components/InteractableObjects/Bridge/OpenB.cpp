#include "OpenB.h"
#include "../../../../Gfx/Gfx.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../../../Physics/Comp/BoxCollider.h"
#include "../../../../Level/Scene.h"
#include "../../../../Gameplay/Components/Strawss/CameraLogic.h"

void OpenBridge::Enter()
{
	if (m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_0"))
	{
		m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_0")->ChangeTriggerState(true);
	}
	if (m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1"))
	{
		m_owner->GetOwner()->GetComponent<BoxCollider>("BoxCollider_1")->ChangeTriggerState(false);
	}
}

void OpenBridge::Update()
{
	m_owner->ChangeState("UnFocus");
}
