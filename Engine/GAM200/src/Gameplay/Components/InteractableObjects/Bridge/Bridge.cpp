#include "Bridge.h"
#include "CloseB.h"
#include "OpenB.h"
#include "OpeningB.h"
#include "BFocusing.h"
#include "../../../../Editor/ImGuiEditor.h"
#include "../../../../ObjectManager/GameObject.h"

void Bridge::Initialize()
{
	m_actor = GetOwner();
	AddState(DBG_NEW OpenBridge("Open"));
	AddState(DBG_NEW CloseBridge("Close"));
	AddState(DBG_NEW OpeningBridge("Opening"));
	AddState(DBG_NEW BFocusing("Focus", true, "Opening"));
	AddState(DBG_NEW BFocusing("UnFocus", false, "Opening"));
	SetInitState("Close");
	mInitialState->InternalEnter();
	GetOwner()->register_handler((*this), &Bridge::Open);
}

void Bridge::Update()
{
	StatusUpdate();
}

void Bridge::Terminate()
{
	GetOwner()->unregister_handler((*this), &Bridge::Open);
}

void Bridge::load(Json::Value & val)
{
	active = val["Bridge"].get("active", true).asBool();
}

void Bridge::save(Json::Value & val)
{
	val["Bridge"]["active"] = active;
}

void Bridge::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Bridge"))
	{
		ImGui::Checkbox("active##DoorCollider", &active);

		if (ImGui::Button("DebugOpen##Bridge"))
		{
			ChangeToOpen();
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##Bridge"))
			GetOwner()->detach(this);
	}
}

void Bridge::ChangeToOpen()
{
	ChangeState("Focus");
	//ChangeState("Opening");
}

void Bridge::Open(const InteractableEvent &)
{
	ChangeToOpen();
}
