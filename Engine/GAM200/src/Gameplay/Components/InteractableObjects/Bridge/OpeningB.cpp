#include "OpeningB.h"
#include "../../../../ObjectManager/GameObject.h"
#include "../../../../ObjectManager/ObjectManager.h"
#include "../../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"
#include "../../GAM200/src/Component/Component.h"
void OpeningBridge::Enter()
{
	if (m_owner->GetOwner()->GetComponent<Animator>())
	{
		Anim = m_owner->GetOwner()->GetComponent<Animator>();
	}
	SoundEmitter * e = m_owner->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		//if (e->IsInit)
		//	e->PlaySound("DrawBridge.mp3", 1);
		if (e->IsInit)
			e->PlayOnce("DrawBridge.mp3", false);
	}
}

void OpeningBridge::Update()
{
	if (Anim)
	{
		Anim->Play();
		if (Anim->finished)
		{
			m_owner->ChangeState("Open");
		}
	}
}
void OpeningBridge::Exit()
{
	SoundEmitter * e = m_owner->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}
