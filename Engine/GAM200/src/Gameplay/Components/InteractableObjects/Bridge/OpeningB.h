#ifndef OPENING_B_H
#define OPENING_B_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"
#include "../GAM200/src/Gfx/Comp/Animator.h"

class Camera;
class GameObject;
class CameraLogic;

class OpeningBridge : public State
{
public:
	OpeningBridge(const char * name) : State(name) {}

	void Enter();
	void Update();
	void Exit();
private:
	Animator* Anim;
};

#endif
