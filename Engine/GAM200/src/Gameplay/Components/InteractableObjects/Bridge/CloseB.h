#ifndef CLOSE_B_H
#define CLOSE_B_H

#include "../../../../StateMachine/StateMachine.h"
#include "../../../../Gfx/Comp/Transform.h"

class RigidBody;

class CloseBridge : public State
{
public:
	CloseBridge(const char * name) : State(name) {}

	void Enter();
	void Update();
};

#endif
