#include "Interactable.h"
#include "../../Engine/GAM200/src/Editor/ImGuiEditor.h"
#include "../../Engine/GAM200/src/ObjectManager/ObjectManager.h"
#include "../../Engine/GAM200/src/Physics/Comp/BoxCollider.h"
#include "../../Engine/GAM200/src/ObjectManager/GameObject.h"
#include "../GAM200/src/Gfx/Comp/Renderable.h"
//#include "../Strawss/CameraLogic.h"
#include "Door/Door.h"
#include "Bridge/Bridge.h"
//oscars additions
#include "../GAM200/src/Audio/SoundEmitter.h"
#include "../../GAM200/src/Audio/Audio.h"

Interactable::Interactable()
{

}
 
Interactable::~Interactable()
{

}

//void Interactable::Initialize()
//{
//	ObjectManager * Om = objectManager;
//
//	for (unsigned i = 0; i < ToLoad.size(); i++)
//	{
//		Targets.push_back(Om->FindObjectByName(ToLoad[i]));
//	}
//
//	GetOwner()->register_handler(*this, &Interactable::SendEventToTarget);
//	GetOwner()->register_handler(*this, &Interactable::HitByArrow);
//
//	Collider * temp = GetOwner()->GetComponent<BoxCollider>();
//	if (temp)
//	{
//		if (!temp->IsTrigger())
//		{
//			temp->ChangeTriggerState(true);
//		}
//	}
//	GetAvailableItems();
//
//}
//void Interactable::Update()
//{
//	if (!strawss) {
//		strawss = objectManager->FindObjectByName("Strawss");
//	}
//}
//
//void Interactable::Terminate()
//{
//	this->GetOwner()->unregister_handler((*this), &Interactable::SendEventToTarget);
//	GetOwner()->unregister_handler(*this, &Interactable::HitByArrow);
//}
//
//void Interactable::SendEventToTarget(const InteractableEvent &)
//{
//	GetOwner()->mTransform->position.x *= -1;
//	for (unsigned i = 0; i < Targets.size(); i++)
//	{
//		Targets[i]->local_handler.handle(InteractableEvent());
//	}
//
//	SoundEmitter * e = GetOwner()->GetComponent<SoundEmitter>();
//	if (e)
//	{
//		if (e->IsInit)
//			e->PlaySound("LeverActivate.mp3", 2);
//
//	}
//
//	strawss->local_handler.handle(CameraEvent(1));
//}
//

void Interactable::GetAvailableItems()
{
	AvailabeItems.clear();
	auto Iterator = objectManager->aliveObjects.begin();
	for (Iterator; Iterator != objectManager->aliveObjects.end(); Iterator++)
	{
		if ((*Iterator)->GetComponent<Door>() || (*Iterator)->GetComponent<Bridge>())
		{
			AvailabeItems.push_back(*Iterator);
		}
	}
}

//void Interactable::HitByArrow(const CollisionStartedEvent & CSE)
//{
//	if (Bullseye)
//	{
//		if (CSE.other_object->GetName() == "Arrow")
//		{
//			std::cout << "aylmaoo" << std::endl;
//			SendEventToTarget(InteractableEvent());
//			objectManager->RemoveGameObject(GetOwner());
//		}
//	}
//}
//
//void Interactable::load(Json::Value & val)
//{
//	Targets.clear();
//	ToLoad.clear();
//	ObjectManager * Om = objectManager;
//
//	for (unsigned i = 0; i < val["Interactable"]["Targets"]["name"].size(); i++)
//	{
//		ToLoad.push_back(val["Interactable"]["Targets"]["name"][i].asString());
//	}
//	Bullseye = val["Interactable"].get("Bullseye", false).asBool();
//}
//void Interactable::save(Json::Value & val)
//{
//	for (unsigned i = 0; i < Targets.size(); i++)
//	{
//		if (Targets[i])
//		{
//			val["Interactable"]["Targets"]["name"].append(Targets[i]->GetName());
//		}
//	}
//	val["Interactable"]["Bullseye"] = Bullseye;
//}
//void Interactable::WriteToImguiWindow()
//{
//	if (ImGui::CollapsingHeader("Interactable"))
//	{
//		ImGui::Checkbox("active##Interactable", &active);
//		
//		ImGui::Text("Before giving the switch a target make sure to press Get targets first");
//
//		if (ImGui::Button("Get Availabale targets on screen"))
//		{
//			GetAvailableItems();
//		}
//
//		if (ImGui::Button("Get Switch targets"))
//			ImGui::OpenPopup("AvailabaleItems");
//
//		ImGui::Text("Avoid adding a target twice");
//
//		if (ImGui::BeginPopup("AvailabaleItems"))
//		{
//			for (unsigned i = 0; i < AvailabeItems.size(); i++)
//				if (ImGui::Selectable(AvailabeItems[i]->GetName().c_str()))
//				{
//					Targets.push_back(AvailabeItems[i]);
//				}
//			ImGui::EndPopup();
//		}
//
//		if (ImGui::Button("Switch's targets"))
//			ImGui::OpenPopup("Targets");
//
//		if (ImGui::BeginPopup("Targets"))
//		{
//			for (unsigned i = 0; i < Targets.size(); i++)
//			{
//				if (ImGui::Selectable(Targets[i]->GetName().c_str()))
//				{
//				}
//			}
//			ImGui::EndPopup();
//		}
//
//		if (ImGui::Checkbox("Is A Bullseye?", &Bullseye))
//		
//		ImGui::NewLine();
//		if (ImGui::Button("DebugSendLocalEvent##Interactable"))
//		{
//			std::cout << "SENDING DEBUG BUTTON" << std::endl;
//			SendEventToTarget(InteractableEvent());
//		}
//
//		ImGui::NewLine();
//		if (ImGui::Button("Detach##Interactable"))
//			GetOwner()->detach(this);
//	}
//}

