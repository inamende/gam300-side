#include "Lever.h"
#include "../../../../src/Editor/ImGuiEditor.h"
#include "../../../../src/ObjectManager/ObjectManager.h"
#include "../../../../src/Physics/Comp/BoxCollider.h"
#include "../../../../src/ObjectManager/GameObject.h"
#include "../../../../src/Gfx/Comp/Renderable.h"
#include "../../../../src/ResourceManager/ResoManager.h"
#include "../../../../src/Gfx/Reso/Texture.h"
#include "../../../../src/Audio/SoundEmitter.h"
#include "../../../../src/Audio/Audio.h"

Lever::Lever() {}

Lever::~Lever() {}

void Lever::Initialize()
{
	ObjectManager * Om = objectManager;

	for (unsigned i = 0; i < ToLoad.size(); i++)
	{
		Targets.push_back(Om->FindObjectByName(ToLoad[i]));
	}

	GetOwner()->register_handler(*this, &Lever::SendEventToTarget);

	Collider * temp = GetOwner()->GetComponent<BoxCollider>();
	if (temp)
	{
		if (!temp->IsTrigger())
		{
			temp->ChangeTriggerState(true);
		}
	}
	GetAvailableItems();
	GetOwner()->mRenderable->texture = resoManager->getReso<Texture>("switch01.png");
	Lever01 = true;
}

void Lever::Update()
{
	ActivatedThisFrame = false;
}

void Lever::Terminate()
{
	this->GetOwner()->unregister_handler((*this), &Lever::SendEventToTarget);
}

void Lever::load(Json::Value & val)
{
	Targets.clear();
	ToLoad.clear();
	ObjectManager * Om = objectManager;
	active = val["Lever"].get("active", true).asBool();
	for (unsigned i = 0; i < val["Lever"]["Targets"]["name"].size(); i++)
	{
		ToLoad.push_back(val["Lever"]["Targets"]["name"][i].asString());
	}
}

void Lever::save(Json::Value & val)
{
	val["Lever"]["active"] = active;
	for (unsigned i = 0; i < Targets.size(); i++)
	{
		if (Targets[i])
		{
			val["Lever"]["Targets"]["name"].append(Targets[i]->GetName());
		}
	}
}

void Lever::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Lever"))
	{
		ImGui::Checkbox("active##Lever", &active);
		
		ImGui::Text("MAKE SURE TO NAME EVERYTHING PROPERLY");

		ImGui::Text("Press this button before adding a target");
	
		if (ImGui::Button("Get Availabale targets on screen"))
		{
			GetAvailableItems();
		}
	
		if (ImGui::Button("Add target to the lever"))
			ImGui::OpenPopup("AvailabaleItems");
	
		ImGui::Text("Avoid adding a target twice");
	
		if (ImGui::BeginPopup("AvailabaleItems"))
		{
			for (unsigned i = 0; i < AvailabeItems.size(); i++)
				if (ImGui::Selectable(AvailabeItems[i]->GetName().c_str()))
				{
					Targets.push_back(AvailabeItems[i]);
				}
			ImGui::EndPopup();
		}
	
		if (ImGui::Button("Levers's current targets"))
			ImGui::OpenPopup("Targets");
	
		if (ImGui::BeginPopup("Targets"))
		{
			for (unsigned i = 0; i < Targets.size(); i++)
			{
				if (ImGui::Selectable(Targets[i]->GetName().c_str()))
				{
				}
			}
			ImGui::EndPopup();
		}
		
		if (ImGui::Button("Clear Lever's Targets"))
		{
			Targets.clear();
		}

		ImGui::NewLine();
		if (ImGui::Button("DebugSendLocalEvent##Interactable"))
		{
			//std::cout << "SENDING DEBUG BUTTON" << std::endl;
			SendEventToTarget(InteractableEvent());
		}
	
		ImGui::NewLine();
		if (ImGui::Button("Detach##Lever"))
			GetOwner()->detach(this);
	}
}

void Lever::SendEventToTarget(const InteractableEvent &)
{
	if (!ActivatedThisFrame)
	{
		if (!Lever01)
		{
			GetOwner()->mRenderable->texture = resoManager->getReso<Texture>("switch01.png");
			Lever01 = true;
		}
		else
		{
			GetOwner()->mRenderable->texture = resoManager->getReso<Texture>("switch02.png");
			Lever01 = false;
		}

		for (unsigned i = 0; i < Targets.size(); i++)
		{
			if (Targets[i])
			{
				Targets[i]->local_handler.handle(InteractableEvent());
			}
		}
		SoundEmitter * e = GetOwner()->GetComponent<SoundEmitter>();
		if (e)
		{
			if (e->IsInit)
				e->PlayOnce("LeverActivate.mp3", false);
		}

		ActivatedThisFrame = true;
	}
}
