#ifndef AMBIENT_CHANGE_H
#define AMBIENT_CHANGE_H

#include "../../../Component/Component.h"

class CollisionStartedEvent;
class CollisionPersistedEvent;
class BoxCollider;

class AmbientChange : public Component {
public:
	AmbientChange();
	~AmbientChange();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	void OnCollisionStarted(const CollisionStartedEvent & object);
	void OnCollisionPersisted(const CollisionPersistedEvent & object);

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	bool DecreaseAmbientLight(vec3 amb, float dt);
	void ApplyAmbientLight(GameObject *obj);
	vec3 ambient;
	vec3 start;
	BoxCollider *coll;
};

#endif
