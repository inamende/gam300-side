#ifndef LEVER_H
#define LEVER_H

#include "Interactable.h"

class Lever : public Interactable
{
public:
	Lever();

	~Lever();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void SendEventToTarget(const InteractableEvent &);
	bool ActivatedThisFrame;
	bool Lever01;
};

#endif