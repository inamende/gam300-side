#include "Bullseye.h"
#include "../../Engine/GAM200/src/Editor/ImGuiEditor.h"
#include "../../Engine/GAM200/src/ObjectManager/ObjectManager.h"
#include "../../Engine/GAM200/src/Physics/Comp/BoxCollider.h"
#include "../../Engine/GAM200/src/ObjectManager/GameObject.h"
#include "../GAM200/src/Gfx/Comp/Renderable.h"

Bullseye::Bullseye()
{
}

Bullseye::~Bullseye()
{
}

void Bullseye::Initialize()
{
	ObjectManager * Om = objectManager;

	for (unsigned i = 0; i < ToLoad.size(); i++)
	{
		Targets.push_back(Om->FindObjectByName(ToLoad[i]));
	}

	GetOwner()->register_handler(*this, &Bullseye::SendEventToTarget);
	GetOwner()->register_handler(*this, &Bullseye::HitByArrow);

	Collider * temp = GetOwner()->GetComponent<BoxCollider>();
	if (temp)
	{
		if (!temp->IsTrigger())
		{
			temp->ChangeTriggerState(true);
		}
	}
	GetAvailableItems();
}

void Bullseye::Update()
{
	
}

void Bullseye::Terminate()
{
	GetOwner()->unregister_handler(*this, &Bullseye::SendEventToTarget);
	GetOwner()->unregister_handler(*this, &Bullseye::HitByArrow);
}

void Bullseye::load(Json::Value & val)
{
	Targets.clear();
	ToLoad.clear();
	ObjectManager * Om = objectManager;
	active = val["Bullseye"].get("active", true).asBool();
	for (unsigned i = 0; i < val["Bullseye"]["Targets"]["name"].size(); i++)
	{
		ToLoad.push_back(val["Bullseye"]["Targets"]["name"][i].asString());
	}
}

void Bullseye::save(Json::Value & val)
{
	val["Bullseye"]["active"] = active;
	for (unsigned i = 0; i < Targets.size(); i++)
	{
		if (Targets[i])
		{
			val["Bullseye"]["Targets"]["name"].append(Targets[i]->GetName());
		}
	}
}

void Bullseye::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Bullseye"))
	{
		ImGui::Checkbox("active##Bullseye", &active);

		ImGui::Text("MAKE SURE TO NAME EVERYTHING PROPERLY");

		ImGui::Text("Press this button before adding a target");

		if (ImGui::Button("Get Availabale targets on screen"))
		{
			GetAvailableItems();
		}

		if (ImGui::Button("Add target to the Bullseye"))
			ImGui::OpenPopup("AvailabaleItems");

		ImGui::Text("Avoid adding a target twice");

		if (ImGui::BeginPopup("AvailabaleItems"))
		{
			for (unsigned i = 0; i < AvailabeItems.size(); i++)
				if (ImGui::Selectable(AvailabeItems[i]->GetName().c_str()))
				{
					Targets.push_back(AvailabeItems[i]);
				}
			ImGui::EndPopup();
		}

		if (ImGui::Button("Levers's current targets"))
			ImGui::OpenPopup("Targets");

		if (ImGui::BeginPopup("Targets"))
		{
			for (unsigned i = 0; i < Targets.size(); i++)
			{
				if (ImGui::Selectable(Targets[i]->GetName().c_str()))
				{
				}
			}
			ImGui::EndPopup();
		}

		if (ImGui::Button("Clear Bullseye's Targets"))
		{
			Targets.clear();
		}

		ImGui::NewLine();
		if (ImGui::Button("DebugSendLocalEvent##Interactable"))
		{
			//std::cout << "SENDING DEBUG BUTTON" << std::endl;
			SendEventToTarget(InteractableEvent());
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##Lever"))
			GetOwner()->detach(this);
	}
}

void Bullseye::HitByArrow(const CollisionStartedEvent & CSE)
{
	if (CSE.other_object->GetName() == "Arrow")
	{
		SendEventToTarget(InteractableEvent());
		objectManager->RemoveGameObject(GetOwner());
	}
}

void Bullseye::SendEventToTarget(const InteractableEvent &)
{
	GetOwner()->mTransform->position.x *= -1;
	for (unsigned i = 0; i < Targets.size(); i++)
	{
		Targets[i]->local_handler.handle(InteractableEvent());
	}
	//objectManager->RemoveGameObject(GetOwner());
}