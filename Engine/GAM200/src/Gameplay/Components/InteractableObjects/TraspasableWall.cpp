#include "TraspasableWall.h"

#include "../../../Editor/ImGuiEditor.h"

#include "../../../ObjectManager/GameObject.h"

Wall::Wall() {}

Wall::~Wall() {}

void Wall::Initialize() {}

void Wall::Update() {}

void Wall::Terminate() {}

void Wall::load(Json::Value & val)
{
	active = val["Wall"].get("active", true).asBool();
}

void Wall::save(Json::Value & val)
{
	val["Wall"]["active"] = active;
}

void Wall::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Wall"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Detach##Wall"))
			GetOwner()->detach(this);
	}
}

