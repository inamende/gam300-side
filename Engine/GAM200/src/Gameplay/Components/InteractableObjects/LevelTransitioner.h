#ifndef LEVEL_TRANSITIONER_H
#define LEVEL_TRANSITIONER_H

#include "../../../Component/Component.h"
#include "../Strawss/States/StrawssParam.h"
class CollisionPersistedEvent;
class Scene;
class PlayerController;

class LevelTransitioner : public Component {
public:
	LevelTransitioner();
	~LevelTransitioner();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	void OnCollisionPersisted(const CollisionPersistedEvent & object);

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	std::string next_level;
	Scene *sc;
	float dt;
	PlayerController *pl;
	Strawss &Params = Strawss::GetParam();
};

#endif
