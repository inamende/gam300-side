#include "AmbientChange.h"

#include "../../../ObjectManager/GameObject.h"
#include "../../../Gfx/Gfx.h"
#include "../../../Physics/Physics.h"
#include "../../../Physics/Comp/BoxCollider.h"
#include "../../../Gfx/Comp/Transform.h"
#include "../../../Utils/Utilities.h"
#include "../../../Editor/ImGuiEditor.h"
#include "../../../Level/Scene.h"

AmbientChange::AmbientChange() {}

AmbientChange::~AmbientChange() {}

void AmbientChange::Initialize() {
	GetOwner()->register_handler(*this, &AmbientChange::OnCollisionStarted);
	GetOwner()->register_handler(*this, &AmbientChange::OnCollisionPersisted);
}

void AmbientChange::Update() {}

void AmbientChange::Terminate() {
	GetOwner()->unregister_handler(*this, &AmbientChange::OnCollisionStarted);
	GetOwner()->unregister_handler(*this, &AmbientChange::OnCollisionPersisted);
}

void AmbientChange::OnCollisionStarted(const CollisionStartedEvent & object) {
	start = graphics->global_ambient_lighting;
	ApplyAmbientLight(object.other_object);
}

void AmbientChange::OnCollisionPersisted(const CollisionPersistedEvent & object) {
	ApplyAmbientLight(object.other_object);
}

void AmbientChange::load(Json::Value & val) {
	active = val["AmbientChange"].get("active", true).asBool();

	ambient.x = val["AmbientChange"]["ambient"].get("x", 0.f).asFloat();
	ambient.y = val["AmbientChange"]["ambient"].get("y", 0.f).asFloat();
	ambient.z = val["AmbientChange"]["ambient"].get("z", 0.f).asFloat();
}

void AmbientChange::save(Json::Value & val) {
	val["AmbientChange"]["active"] = active;

	val["AmbientChange"]["ambient"]["x"] = ambient.x;
	val["AmbientChange"]["ambient"]["y"] = ambient.y;
	val["AmbientChange"]["ambient"]["z"] = ambient.z;
}

void AmbientChange::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("AmbientChange")) {
		ImGui::NewLine();
		ImGuiEditor::InputFloat3("final ambient", ambient);
		if (ImGui::Button("Detach##AmbientChange"))
			GetOwner()->detach(this);
	}
}

bool AmbientChange::DecreaseAmbientLight(vec3 amb, float dt) {
	vec4 &in = graphics->global_ambient_lighting;
	graphics->global_ambient_lighting;
	in = vec4(Lerp<vec3>(start ,amb ,dt), 1.0);
	return false;
}

void AmbientChange::ApplyAmbientLight(GameObject *obj) {
	float width = 0.f;
	if (!coll) coll = GetOwner()->GetComponent<BoxCollider>();
	if (coll) {
		width = GetOwner()->GetComponent<BoxCollider>()->GetTransform()->scale.x * 0.5f;
	}
	float dist = distance(obj->mTransform->position.x, GetOwner()->mTransform->position.x);
	if (dist > width) {
		dist = width;
	}
	float dt = fabs(width - dist) / width;
	DecreaseAmbientLight(vec3(ambient), dt);
}