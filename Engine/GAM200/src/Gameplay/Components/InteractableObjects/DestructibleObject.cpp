#include "DestructibleObject.h"
#include "../../../ObjectManager/GameObject.h"
#include "../../../ObjectManager/ObjectManager.h"
#include "../../../Physics/Physics.h"
#include "imgui/imgui.h"
//oscars additions
#include "../../../Audio/SoundEmitter.h"
DestructibleObject::DestructibleObject() {}
DestructibleObject::~DestructibleObject()
{
}
void DestructibleObject::Initialize() {
	GetOwner()->register_handler(*this, &DestructibleObject::OnCollisionStarted);
}

void DestructibleObject::Update() {}

void DestructibleObject::Terminate() {
	GetOwner()->unregister_handler(*this, &DestructibleObject::OnCollisionStarted);
}

void DestructibleObject::load(Json::Value & val) {
	active = val["DestructibleObject"].get("active", true).asBool();
}

void DestructibleObject::save(Json::Value & val) {
	val["DestructibleObject"]["active"] = active;
}

void DestructibleObject::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("DestructibleObject")) {
		ImGui::NewLine();
		if (ImGui::Button("Detach##DestructibleObject"))
			GetOwner()->detach(this);
	}
}

void DestructibleObject::OnCollisionStarted(const CollisionStartedEvent & eve) 
{
	//play the sound if it has a sound emitter
	if (eve.other_object->GetName() == "sword_attack")
	{
		//DoorSoundEmiter
		SoundEmitter * e = NULL;
		//mJuiceBarPointer = objectManager->FindObjectByName(StrawssName)->GetComponent<JuiceBar>();
		GameObject * g = objectManager->FindObjectByName("GeneralPurposeSoundEmitter");
		e = g->GetComponent<SoundEmitter>();
		if (e)
		{
			//e->PlaySound("BowmanWalk.mp3", 3);
			//e->PlaySound("WoodenDoorBreak.mp3", 1);
			e->PlayOnce("WoodenDoorBreak.mp3", false);
		}
		objectManager->RemoveGameObject(GetOwner());
	}
}