#ifndef PLAYER_INTERACTABLE_H
#define PLAYER_INTERACTABLE_H

#include "../../GAM200/src/Component/Component.h"
#include "../../GAM200/src/Events/event.h"
//forward declarations
using std::string;
struct InteractableEvent;
class GameObject;
class CollisionPersistedEvent;
class CollisionEndedEvent;

class PlayerInteractable : public Component
{
public:
	PlayerInteractable();
	PlayerInteractable(string name, string target, string event);
	~PlayerInteractable();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	//this is for finding the levers when the player collides with them
	void PlayerIsCurrentlyColliding(const CollisionPersistedEvent &);
	void PlayerNoLongerColliding(const CollisionEndedEvent &);

	bool can_activate;
private:
	string mName;
	GameObject * mTargetObject;
};

#endif
