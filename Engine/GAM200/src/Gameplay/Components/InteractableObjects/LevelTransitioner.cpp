#include "LevelTransitioner.h"

#include "../../../ObjectManager/GameObject.h"
#include "../../../Gameplay/Components/Strawss/PlayerController.h"
#include "../../../Physics/Physics.h"
#include "../../../Level/Scene.h"
#include "../../../Gfx/Gfx.h"
#include "imgui/imgui.h"

LevelTransitioner::LevelTransitioner() :
	next_level(""),
	sc(nullptr),
	dt(0.0),
	pl(nullptr)
{}

LevelTransitioner::~LevelTransitioner() {}

void LevelTransitioner::Initialize() {
	sc = scene;
	GetOwner()->register_handler(*this, &LevelTransitioner::OnCollisionPersisted);
}

void LevelTransitioner::Update() {}

void LevelTransitioner::Terminate() {
	GetOwner()->unregister_handler(*this, &LevelTransitioner::OnCollisionPersisted);
}

void LevelTransitioner::load(Json::Value & val) {
	active = val["LevelTransitioner"].get("active", true).asBool();

	next_level = val["LevelTransitioner"].get("next_level", "").asString();
}

void LevelTransitioner::save(Json::Value & val) {
	val["LevelTransitioner"]["active"] = active;
	val["LevelTransitioner"]["next_level"] = next_level;
}

void LevelTransitioner::WriteToImguiWindow() {
	static int selected_level = 0;
	if (ImGui::CollapsingHeader("LevelTransitioner"))
	{
		if (ImGui::Button("next level##LevelTransitioner")) {
			ImGui::OpenPopup("next_level##LevelTransitioner");
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##LevelTransitioner")) {
			GetOwner()->detach(this);
		}
	}
	if (ImGui::BeginPopup("next_level##LevelTransitioner")) {
		ImGui::Text("Levels");
		ImGui::Separator();
		for (unsigned i = 0; i < sc->levelNames.size(); i++) {
			if (ImGui::Selectable(sc->levelNames[i].c_str())) {
				selected_level = i;
				next_level = sc->levelNames[selected_level];
			}
		}
		ImGui::EndPopup();
	}
}

void LevelTransitioner::OnCollisionPersisted(const CollisionPersistedEvent & object) {
	dt += ImGui::GetIO().DeltaTime;
	bool change_level = graphics->FadeIn(-100, dt * 0.5f);

	if (!pl) {
		pl = object.other_object->GetComponent<PlayerController>();
	}
	if (pl) {
		pl->ChangeState("MoveRight");
	}

	if (change_level) {
		if (next_level != "") {
			Params.SpawnAtCheckPoint = false;
			sc->currLevel = next_level;
		}
	}
}
