#ifndef PREASSURE_PLATES_H
#define PREASSURE_PLATES_H

#include "Interactable.h"

class PreassurePlates : public Interactable
{
public:
	PreassurePlates();

	~PreassurePlates();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void SendEventToTarget(const InteractableEvent &);

	void BoxOrKnightOnTop(const CollisionStartedEvent & CSE);
	void BoxOrKnightNotOnTop(const CollisionEndedEvent & CEE);

	unsigned ThingsOnTop;
	unsigned ThingsOnTopLastF;

	std::vector<GameObject*> ItemsOnTop;
	std::vector<GameObject*>::iterator VectorIterator;
	bool TriggerOnce;
	bool Removed;
};

#endif