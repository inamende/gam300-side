#include "Collectible.h"
#include "../../../Input/Input.h"
#include "../../../Editor/ImGuiEditor.h"
#include "../../../Gfx/Comp/Renderable.h"
#include "../../../Gfx/Comp/FontRenderable.h"
#include "../../../Level/Scene.h"

GameObject object_press_a_to_pick;

Collectible::Collectible()
{
}

Collectible::~Collectible()
{
}

void Collectible::Initialize()
{
	reading = false;
	created = false;
	has_sttoped_reading_this_frame = false;
	texture_name = "messageBox.png";

	GetOwner()->local_handler.register_handler(*this, &Collectible::OnCollisionPersisted);
	GetOwner()->local_handler.register_handler(*this, &Collectible::OnCollisionEnded);
}

void Collectible::Update()
{
	if ((KeyDown(Keyboard::Q) || ButtonDown(SDL_CONTROLLER_BUTTON_A)) && reading)
	{
		objectManager->RemoveGameObject(text_object);
		reading = false;
		has_sttoped_reading_this_frame = true;
	}
	else
		has_sttoped_reading_this_frame = false;
}

void Collectible::Terminate()
{
	if (created)
	{
		//objectManager->RemoveGameObject(press_a_to_pick);
		created = false;
	}

	GetOwner()->local_handler.unregister_handler(*this, &Collectible::OnCollisionPersisted);
	GetOwner()->local_handler.unregister_handler(*this, &Collectible::OnCollisionEnded);
}

void Collectible::load(Json::Value & val)
{
	// load message created
	active = val["Collectible"].get("active", true).asBool();
	message = val["Collectible"].get("message", true).asCString();
	texture_name = val["Collectible"].get("textureName", true).asCString();
}

void Collectible::save(Json::Value & val)
{
	// save message created
	val["Collectible"]["active"] = active;
 	val["Collectible"]["message"] = message.data();
	val["Collectible"]["textureName"] = texture_name.data();
}

void Collectible::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Collectible"))
	{
		char * temp;
		temp = new char[255];
		std::memset(temp, 0, 255);

		ImGui::InputText("Message", temp, 255);

		message = temp;

		ImGui::NewLine();

		ImGui::InputText("Texture Name", temp, 255);

		texture_name = temp;

		delete[] temp;

		if (ImGui::Button("Detach##Collectible"))
			GetOwner()->detach(this);
	}
}


void Collectible::OnCollisionPersisted(const CollisionPersistedEvent & ev)
{
	if (ev.other_object->GetName() != "Strawss" || reading)
		return;

	if (!created)
		create_press_a_to_pick();

	// make "press A to pick" object visible
 	press_a_to_pick_render->visible = true;

	//if player presses A, show_collectible()
	if ((KeyDown(Keyboard::Q) || ButtonDown(SDL_CONTROLLER_BUTTON_A)) && !has_sttoped_reading_this_frame)
		show_collectible();
}

void Collectible::OnCollisionEnded(const CollisionEndedEvent & ev)
{
	press_a_to_pick_render->visible = false;
}

void Collectible::show_collectible()
{
	// spawn collectible archetype
	create_text_object();

	// put corresponding message on the collectible
	auto font_renderable = text_object->GetComponent<FontRenderable>();
	font_renderable->displayText = message.data();

	// make "press A to pick" object invisible
	press_a_to_pick_render->visible = false;

	reading = true;
}

void Collectible::create_press_a_to_pick()
{
	press_a_to_pick = scene->CreateGameObject();

	std::string name = "PressAToPick";
	press_a_to_pick->LoadJson("./Data/Jsons/" + name + ".json");
	press_a_to_pick_render = press_a_to_pick->GetComponent<Renderable>();

	press_a_to_pick->Initialize();

	created = true;
}

void Collectible::create_text_object()
{
	text_object = scene->CreateGameObject();

	std::string archetype_name = "TextObject";
	text_object->LoadJson("./Data/Jsons/" + archetype_name + ".json");

	auto render = text_object->GetComponent<Renderable>();
	render->ChangeTextureUvs(texture_name);

	text_object->Initialize();
}