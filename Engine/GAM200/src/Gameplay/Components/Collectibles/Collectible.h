#pragma once

#include "../../../Component/Component.h"
#include "../../../Events/event.h"
#include "../../../Physics/Physics.h"
#include <memory>

class CollectibleEvent : public Event
{

};

class Collectible : public Component
{
public:
	Collectible(); 
	~Collectible();

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Terminate() override;

	virtual void load(Json::Value & val) override;
	virtual void save(Json::Value & val) override; 
	virtual void WriteToImguiWindow() override;

	void show_collectible();
	void OnCollisionPersisted(const CollisionPersistedEvent & ev);
	void OnCollisionEnded(const CollisionEndedEvent & ev);

private:
	void create_press_a_to_pick();
	void create_text_object();

	std::string  message;
	std::string texture_name;
	bool reading;

	GameObject * text_object;
	GameObject * press_a_to_pick{nullptr};
	bool created{false};
	bool has_sttoped_reading_this_frame{ false };
	Renderable * press_a_to_pick_render;
};