#include "Tester.h"

#include "../../Editor/ImGuiEditor.h"

#include "../../ObjectManager/GameObject.h"
#include "../../Gfx/Comp/Transform.h"
#include "../../Gfx/Gfx.h"

#include "../../Gfx/Reso/Shader.h"
#include "../../Gfx/Reso/Texture.h"

Tester::Tester() {}

Tester::~Tester() {}

void Tester::Initialize() {}

void Tester::Update() {}

void Tester::Terminate() {}

void Tester::load(Json::Value & val)
{
	active = val["Tester"].get("active", true).asBool();
}

void Tester::save(Json::Value & val)
{
	val["Tester"]["active"] = active;
}

void Tester::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Tester"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Detach##KnightBreakerMachine"))
			GetOwner()->detach(this);
	}
}

