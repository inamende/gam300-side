//FIND MEMORY LEAKS
/**************************/
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  
/**************************/

#include "Gfx/Gfx.h"
#include "Gfx/Window.h"

#include "Level/Scene.h"

#include "ResourceManager/ResoManager.h"
#include "Factory/Factory.h"
#include "Input/Input.h"

#include "Audio/Audio.h"

#include "Physics/Physics.h"
#include "Physics/Comp/BoxCollider.h"
#include "Physics/Comp/BoxColliderOriented.h"
#include "Physics/Comp/CircleCollider.h"

#include "Gameplay/Components/Tester.h"
#include "Gameplay/Components/Strawss/PlayerController.h"
#include "Gameplay/Components/Strawss/CameraLogic.h"
#include "Gameplay/Components/Strawss/CameraFocus.h"
#include "Gameplay/Components/Enemies/Knight/Knight.h"
#include "Gameplay/Components/Enemies/Knight/Box/BoxLogic.h"
#include "Gameplay/Components/Enemies/Bowman/Bowman.h"
#include "Gameplay/Components/Enemies/Bowman/Arrow/ArrowDirection.h"
#include "Gameplay/Components/Enemies/Chaman/Chaman.h"
#include "Gameplay/Components/Enemies/Boss/Boss.h"
#include "Gameplay/Components/Enemies/Boss/Orb.h"
#include "Gameplay/Components/Attacks/SwordAttack.h"
#include "Gameplay/Components/Traps/DamagingObj.h"
#include "Gameplay/Components/InteractableObjects/TraspasableWall.h"
#include "Gameplay/Components/InteractableObjects/LevelTransitioner.h"
#include "Gameplay/Components/InteractableObjects/AmbientChange.h"
#include "Menu/Menu.h"
#include "Menu/RollTheCredits.h"
#include "Gameplay/Components/InteractableObjects/Interactable.h"
#include "Gameplay/Components/InteractableObjects/PlayerInteractable.h"
#include "Gameplay/Components/InteractableObjects/Bridge/Bridge.h"
#include "Gameplay/Components/InteractableObjects/Door/Door.h"
#include "Gameplay/Components/InteractableObjects/Bullseye.h"
#include "Gameplay/Components/InteractableObjects/Lever.h"
#include "Gameplay/Components/InteractableObjects/DestructibleObject.h"
#include "Gameplay/Components/InteractableObjects/PreassurePlates.h"
#include "Gameplay/Components/Collectibles/Collectible.h"
#include "Menu/TheEnd.h"

#include "imGui/imgui.h"

#include "Actions/Actions.h"

#include "Gameplay/Components/Juice/JuiceBar.h"
#include "Gameplay/Components/Juice/JuiceBarUIController.h"
#include "Gameplay\Components\Juice\CheckpointAnimatorController.h"

#include "Gameplay/Components/Effects/FogMove.h"
#include "Gameplay/Components/Effects/FlickerEffecc.h"

#include "Audio/SoundEmitter.h"
#include "Audio\MusicEmitter.h"
#include "Gameplay\Components\InteractableObjects\DoorReactor.h"
#include "Gameplay\Components\InteractableObjects/Interactable.h"
#include "Gameplay/Components/InteractableObjects/PlayerInteractable.h"
#include "Gameplay\Components\Strawss\Sleep.h"

#include "Menu/FadeIntoMenu/FadeIntoMenu.h"
#include "Menu/Cursor.h"

//FIND MEMORY LEAKS
/*****************************************************************/
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#else
#define DBG_NEW new
#endif
/*****************************************************************/

#include <ft2build.h>
#include FT_FREETYPE_H

#define FPS 60

Uint32 curTime = 0; 
Uint32 prevTime = 0;

Uint32 sdlCur = 0;
Uint32 sdlPrev = 0;

unsigned now = 0;
unsigned last = 0;

int main(int argc, char* args[])
{
	// Initialize the audio manager
	if (gAudioMgr.Initialize() == false)
		return 0;

	//REGISTER COMPONENTS
	mFactory->Register<GameObject>();
	mFactory->Register<BoxCollider>();
	mFactory->Register<BoxColliderOriented>();
	mFactory->Register<CircleCollider>();
	mFactory->Register<Transform>();


	//
	mFactory->Register<Tester>();
	mFactory->Register<PlayerController>();
	mFactory->Register<CameraLogic>();
	mFactory->Register<CameraFocus>();
	mFactory->Register<Boss>();
	mFactory->Register<Orb>();
	mFactory->Register<Knight>();
	mFactory->Register<BoxLogic>();
	mFactory->Register<SwordAttack>();
	mFactory->Register<Bowman>();
	mFactory->Register<ArrowDirection>();
	mFactory->Register<DamagingObj>();
	mFactory->Register<Sleep>();
	mFactory->Register<Chaman>();

	mFactory->Register<JuiceBar>();
	mFactory->Register<JuiceBarUIController>();
	mFactory->Register<CheckpointAnimatorController>();

	mFactory->Register<Menu>();
	mFactory->Register<StartGame>();
	mFactory->Register<Options>();
	mFactory->Register<HowToPlay>();
	mFactory->Register<GraphiccOptions>();
	mFactory->Register<SoundOptions>();
	mFactory->Register<Credits>();
	mFactory->Register<Quit>();
	mFactory->Register<FullScreenButton>();
	mFactory->Register<VSyncButton>();
	mFactory->Register<BackToMain>();
	mFactory->Register<NoToPause>();
	mFactory->Register<NoToPauseFromRestart>();
	mFactory->Register<YesToMain>();
	mFactory->Register<YesToRestart>();
	mFactory->Register<MasterVolumeButton>();
	mFactory->Register<SFXVolumeButton>();
	mFactory->Register<MusicVolumeButton>();
	mFactory->Register<BackToPause>();
	mFactory->Register<BackToGraphicc>();
	mFactory->Register<BackToSound>();
	mFactory->Register<Resume>();
	mFactory->Register<Restart>();
	mFactory->Register<RollCredits>();
	mFactory->Register<Controls>();
	mFactory->Register<Resolutions>();
	mFactory->Register<Apply>();

	mFactory->Register<SoundEmitter>();
	mFactory->Register<MusicEmitter>();
	mFactory->Register<PlayerInteractable>();
	mFactory->Register<Wall>();
	mFactory->Register<Door>();
	mFactory->Register<Lever>();
	mFactory->Register<Bullseye>();
	mFactory->Register<Bridge>();
	mFactory->Register<LevelTransitioner>();
	mFactory->Register<AmbientChange>();
	mFactory->Register<DestructibleObject>();
	mFactory->Register<PreassurePlates>();
	mFactory->Register<Collectible>();

	mFactory->Register<FogMove>();
	mFactory->Register<FlickerEfecc>();

	mFactory->Register<FadeIntoMenu>();
	mFactory->Register<Cursor>();
	mFactory->Register<TheEnd>();

	//INITIALIZE GFX
	mWindow->Initialize();
	graphics->Initialize();

	//INITIALIZE PHYSICS
	physics->Initialize();

	//INITIALIZE SCENE
	scene->Initialize();
	graphics->LoadLayers();

	//INITIALIZE INPUT
	mInput->Initialize();

	#ifndef EDITOR
		SDL_ShowCursor(false);
	#endif

	//UPDATE
	while (!mWindow->quit)
	{
		mInput->PollInputEvents();
		mInput->controller_update();

		now = SDL_GetTicks();
		unsigned delta = now - last;

		mWindow->Update();
		
		if (delta < 60) { // uff
			scene->Update();	// update the scene
			physics->Update();	// then, check the events
		}
		physics->Render();
		gAudioMgr.Update();
		//Cap frame rate
		prevTime = curTime;
		curTime = (Uint32)ImGui::GetIO().Framerate;

		if (curTime - prevTime < 1000 / FPS)
			SDL_Delay(1000 / FPS - (Uint32)ImGui::GetIO().Framerate + prevTime);

#ifdef EDITOR
		if (Input::getInstance()->KeyIsDown(SDL_SCANCODE_ESCAPE, false))
			mWindow->quit = true;
#endif

		mInput->Update();

		last = now;
	}

	// Cleanup
	mActions->Terminate();
	mInput->Terminate();
	mWindow->Terminate();
	imGuiEditor->Terminate();
	scene->Terminate();
	resoManager->Terminate();
	graphics->Terminate();
	physics->Terminate();
	mFactory->Terminate();
	
	// shutdown the audio manager
	gAudioMgr.Shutdown();

	_CrtDumpMemoryLeaks();

	return 0;
}