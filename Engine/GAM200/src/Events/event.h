/*!**************************************************************************
\file    event.hh

\author  Ignacio Mendezona 

\par     DP email:  ignacio.mendezona\@digipen.edu 

\par     Course:    Cs225 

\par     Assignmet 2 Events 

\brief
Event, HandlerFunction, MemberFunvtionHandler and EventHandler classes

***************************************************************************/
#pragma once
#include "type_info.h"
#include <vector>
#include <string>
#include <map>
#include <iostream>

class Event
{
    public:
    virtual ~Event(){}
};

class HandlerFunction
{
    public:

    // Handle the given event
    void handle(const Event&);
	
    virtual ~HandlerFunction(){}
    
    protected:
    virtual void call(const Event & ) = 0;
};

template <typename object, typename E>
class MemberFunctionHandler : public HandlerFunction
{
    public:
    typedef void(object::*MemberFunction)(const E&);
    MemberFunctionHandler(object* other, MemberFunction fp):object_(other), function_(fp) {}
    void call(const Event & );
    
    
    object* object_;
    MemberFunction function_;
};

template <typename object, typename E>
void MemberFunctionHandler<object, E>::call(const Event & ev)
{
    (object_->*function_)(static_cast<const E&>(ev));
}
    
class EventHandler
{
    public:
    
    ~EventHandler();
    template<typename object, typename E>
    void register_handler(object & obj ,void(object::*fp)(const E&))
    {
        //create a MemberFunctionHandler object and save it
        HandlerFunction* handler = new MemberFunctionHandler<object, E>(&obj, fp);
        memberFunctions[type_of<E>().get_name()].push_back(handler);
		//std::cout <<"register"<< memberFunctions.size() << "\n";
    }

	template<typename O, typename E>
	short int unregister_handler(O & obj ,void(O::*fp)(const E&))
	{
		auto name = type_of<E>().get_name();

		std::map<std::string, std::vector<HandlerFunction*>>::iterator handler_container = memberFunctions.find(type_of<E>().get_name()); 

		if (handler_container != memberFunctions.end())
		{
			for (std::vector<HandlerFunction*>::iterator itr = handler_container->second.begin(); itr != handler_container->second.end(); itr++)
			{
				MemberFunctionHandler<O, E> * itr_handler = dynamic_cast<MemberFunctionHandler<O, E> *>(*itr);

				if (itr_handler != nullptr)
				{
					MemberFunctionHandler<O, E> other(&obj, fp);

					if (other.function_ == itr_handler->function_ && other.object_ == itr_handler->object_)
					{
						//wedont delete here
						handler_container->second.erase(itr);
						//std::cout << "Deleted " << name << std::endl;

						if (handler_container->second.empty())
							return -1;

						break;
					}
				}
			}
		}

		return 0;
	}
    
    template<typename E>
    void handle(const E& ev)
    {
		if (memberFunctions.empty()) {
			return;
		}
        auto it = memberFunctions.find(type_of<E>().get_name());    
        if(it == memberFunctions.end())
            return;
        
		//iterate through the second element of each pair (vector of HandlerFunction*)
		for (auto & handler_function : it->second)
			handler_function->handle(ev);
    }

	const std::map<std::string, std::vector<HandlerFunction*>>& get_map();

	void Terminate();

    private:
    //map with a member function as a key, and HandlerFunction* as type
    std::map<std::string, std::vector<HandlerFunction*>> memberFunctions;
};

extern EventHandler global_handler;