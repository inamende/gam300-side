/*!**************************************************************************
\file    type_info.cc

\author  Ignacio Mendezona 

\par     DP email:  ignacio.mendezona\@digipen.edu 

\par     Course:    Cs225 

\par     Assignmet 2 Events 

\brief
Typeinfo class
***************************************************************************/
#pragma once
#include <typeinfo>

class TypeInfo
{ 
    public:
    template <typename T>
    TypeInfo(const T& other): _type(typeid(other)) {}
    TypeInfo(const std::type_info& other) : _type(other){}
    bool operator==(const TypeInfo& ) const;
    bool operator!=(const TypeInfo& ) const;
    bool operator<(const TypeInfo& ) const;
    const char* get_name() const;
    
    private:
    const std::type_info& _type;
};

template <typename A>
TypeInfo type_of(const A& obj)
{
    return TypeInfo(typeid(obj));
}

template <typename A>
TypeInfo type_of()
{
    return TypeInfo(typeid(A));
}

