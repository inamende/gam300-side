/*!**************************************************************************
\file    event.cc

\author  Ignacio Mendezona 

\brief
Some member functions from Event, HandlerFunction, MemberFunvtionHandler and EventHandler classes

The functions included are:
- void HandlerFunction::handle(const Event& obj);
- EventHandler::~EventHandler();

***************************************************************************/
#include "event.h"

/**************************************************************************
*!
\fn     HandlerFunction::handle

\brief 
Handle the given event

\param  const Event& obj


*
**************************************************************************/
void HandlerFunction::handle(const Event& obj)
{
    this->call(obj);
}

/**************************************************************************
*!
\fn     EventHandler::~EventHandler

\brief 
Destructor for the EventHandler. Delete all the HandlerFunctions of our
memberFunctions

*
**************************************************************************/
EventHandler::~EventHandler()
{    
	for (auto & member_function : memberFunctions)
		for (auto & handler_function : member_function.second)
			delete handler_function;

	memberFunctions.clear();
}

const std::map<std::string, std::vector<HandlerFunction*>>& EventHandler::get_map()
{
	return memberFunctions;
}

void EventHandler::Terminate()
{
	memberFunctions.clear();
}

EventHandler global_handler;
