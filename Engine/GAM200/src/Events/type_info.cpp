/*!**************************************************************************
\file    type_info.cc

\author  Ignacio Mendezona 

\par     DP email:  ignacio.mendezona\@digipen.edu 

\par     Course:    Cs225 

\par     Assignmet 2 Events 

\brief
Some of Typeinfo's member functions

The functions included are:
- const char* TypeInfo::get_name() const;
- bool TypeInfo::operator==(const TypeInfo& other) const;
- bool TypeInfo::operator!=(const TypeInfo& other) const;
- bool TypeInfo::operator<(const TypeInfo& other) const;

***************************************************************************/

#include "type_info.h"

/**************************************************************************
*!
\fn     TypeInfo::get_name

\brief 
Rwturns the type of an object.

\return const char*

*
**************************************************************************/
const char* TypeInfo::get_name() const
{
    return _type.name();
}

/**************************************************************************
*!
\fn     TypeInfo::operator==

\brief 
Compare if 2 names are equal

\param  const TypeInfo& other

\return bool

*
**************************************************************************/
bool TypeInfo::operator==(const TypeInfo& other) const
{
    return _type == other._type;
}

/**************************************************************************
*!
\fn     TypeInfo::operator!=

\brief 
Compare if 2 names are not equal. 1 if true, 0 if false

\param  const TypeInfo& other

\return bool

*
**************************************************************************/
bool TypeInfo::operator!=(const TypeInfo& other) const
{    
    return _type != other._type;
}

/**************************************************************************
*!
\fn     TypeInfo::operator<

\brief 

\param  const TypeInfo& other

\return bool

*
**************************************************************************/
bool TypeInfo::operator<(const TypeInfo& other) const
{
    return _type.before(other._type);
}