#include "Resomanager.h"

ResoManager * ResoManager::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW ResoManager(); //dealloc ok

	return instance;
}

Resource::Resource() : handle(0), filePath("") {}

Resource::~Resource() {}

ResoManager::ResoManager() {}

ResoManager::~ResoManager() {}

void ResoManager::AddReso(Resource * resource, std::string name, const char * path)
{
	resource->filePath = path;
	resource->handle = resources.size();
	resource->LoadResource();
	
	resources.insert(std::pair<std::string, Resource*>(name, resource));
}

const std::string ResoManager::getResoName(Resource * resource)
{
	if (resource != NULL)
	{
		for (std::map<std::string, Resource*>::iterator it = resources.begin(); it != resources.end(); it++)
		{
			if (it->second == resource)
			{
				return it->first;
			}
		}

		return "";
	}
	return "";
}

void ResoManager::Terminate()
{
	for (std::map<std::string, Resource*>::iterator it = resources.begin(); it != resources.end(); it++)
	{
		delete it->second;
	}
	resources.clear();

	delete instance;
}

ResoManager * ResoManager::instance = 0;