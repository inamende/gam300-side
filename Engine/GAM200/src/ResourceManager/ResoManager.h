#ifndef RESOMANAGER_H
#define RESOMANAGER_H

#include "../Component/Entity.h"
#include <GL/glew.h>

class Resource : public Entity
{
public:
	GLuint handle;
	std::string filePath;

	Resource();
	virtual ~Resource();
	virtual void LoadResource() = 0;
};

class ResoManager
{
private:

	static ResoManager * instance;
	ResoManager();

public:

	static ResoManager * getInstance();

	std::map<std::string, Resource*> resources;

	virtual ~ResoManager();

	virtual void Terminate();
	void AddReso(Resource * resource, std::string name, const char * path = "ok");

	const std::string getResoName(Resource * resource);

	template<typename T>
	T * getReso(std::string name)
	{
		
		std::map<std::string, Resource*>::iterator it = resources.find(name);

		T * reso = NULL;

		if (it != resources.end())
			reso = dynamic_cast<T*>(resources.find(name)->second);

		if (reso)
			return reso;
		else
			return NULL;
	}
};

#define resoManager ResoManager::getInstance() 

#endif