#ifndef POSTPROCESSOR_H
#define POSTPROCESSOR_H 

#include "../EngineCommon.h"
#include <GL/glew.h>

class Texture;
class Shader;
class Sprite;

class Postprocessor 
{

public:

	~Postprocessor();
	Postprocessor(Shader * sh, Sprite * sp, float w, float h);

	void BeginRender();
	void EndRender();
	void Render();

	void CreateProcess(Shader * sh, Sprite * sp, float w, float h);
	void Clear();
	void Reset(unsigned w, unsigned h);

	void Interpolate(float h, float sat, float br, glm::vec3 col, float dens, float dt);


	Texture * texture;
	Shader * shader;
	Sprite * sprite;

	float width, height;

	struct Postprocess
	{
		float hue = 0.0f;
		float saturation = 0.0f;
		float brightness = 0.0f;
		
		glm::vec3 color = glm::vec3(1.0f, 0.0f, 1.0f);
		float density = 0.0f;

	} postVar;

//private:
	GLuint FBO, RBO, THandle;
};

#endif