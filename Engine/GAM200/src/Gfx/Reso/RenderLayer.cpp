#include "RenderLayer.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include "Camera.h"
#include "Shader.h"
#include "Sprite.h"
#include "../../Gfx/Comp/Renderable.h"
#include "../../Gfx/Comp/Transform.h"
#include "../../Gfx/Window.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Physics/Collisions.h"

RenderLayer::RenderLayer() :
	postprocess(Postprocessor(resoManager->getReso<Shader>("postprocess"), resoManager->getReso<Sprite>("post_plane.obj"), mWindow->width, mWindow->height))
{}

RenderLayer::RenderLayer(std::string & layerName) : RenderLayer::RenderLayer() { SetName(layerName);  }

RenderLayer::~RenderLayer()
{
	cameras.clear();
	renderables.clear();
}

void RenderLayer::Render()
{
	for (std::list<Camera*>::iterator cams = cameras.begin(); cams != cameras.end(); cams++)
	{
		glm::mat4 view = (*cams)->getViewMatrix();
		glm::mat4 proj = (*cams)->getProjectionMatrix();

		glViewport(-(GLint)((*cams)->width / 2), (GLint)((*cams)->width / 2), -(GLint)((*cams)->height / 2), (GLint)((*cams)->height / 2));

		for (std::vector<Renderable*>::iterator rends = renderables.begin(); rends != renderables.end(); rends++)
		{
			if (!IsInsideFrustum(*cams, *rends)) continue;
			if ((*rends)->shader != NULL)
			{
				(*rends)->shader->use();
				(*rends)->shader->setUniform("V", view);
				(*rends)->shader->setUniform("P", proj);
			}
			(*rends)->Render();
		}
	}
}

void RenderLayer::AddRenderable(Renderable * rend)
{
	if (rend != NULL)
	{
		for (std::vector<Renderable*>::iterator it = renderables.begin(); it != renderables.end(); it++)
		{
			if ((*it) == rend)
				return;
		}
		renderables.push_back(rend);
	}
	else
		return;
}

void RenderLayer::RemoveRenderable(Renderable * rend)
{
	if (rend != NULL)
		renderables.erase(std::remove(renderables.begin(), renderables.end(), rend), renderables.end());
	else
		return;
}

void RenderLayer::SortRenderables()
{
	std::sort(renderables.begin(), renderables.end(),
		[](Renderable * first, Renderable * second) {
		return (first->renderableTransform->position.z) < (second->renderableTransform->position.z);
	}
	);
}

void RenderLayer::AddCamera(Camera * cam)
{
	RemoveCamera(cam);
	cameras.push_back(cam);
}

void RenderLayer::RemoveCamera(Camera * cam)
{
	cameras.remove(cam);
}

bool RenderLayer::IsInsideFrustum(Camera *cam, Renderable *rend) {
	return  StaticRectToStaticRect(
		vec2(cam->position), fabsf(cam->width), fabsf(cam->height),
		vec2(rend->GetOwner()->mTransform->position), fabsf(rend->GetOwner()->mTransform->scale.x), fabsf(rend->GetOwner()->mTransform->scale.y));
}
