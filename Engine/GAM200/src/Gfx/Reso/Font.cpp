#include "Font.h"

#include "../../Gfx/Reso/Character.h"
#include "../../Gfx/Reso/Shader.h"

Font::Font() : Resource::Resource() {}

Font::~Font() {}

void Font::LoadResource()
{
	InitFont(Font::filePath.c_str());
}

void Font::InitFont(const char * name)
{
	if (FT_Init_FreeType(&ft)) //0 is succes
		std::cout << "COULD NOT INITIALIZE ftlibrary" << std::endl;

	if (FT_New_Face(ft, name, 0, &face))
		std::cout << "COULD NOT LOAD FONT" << std::endl;

	FT_Set_Pixel_Sizes(face, 0, 48);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (GLubyte i = 0; i < 128; i++)
	{
		if (FT_Load_Char(face, i, FT_LOAD_RENDER))
		{
			//std::cout << "COULD NOT LOAD GLYPH" << std::endl;
			continue; //exit loop
		}

		glGenTextures(1, &textureHandle);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureHandle);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		Character character(textureHandle, glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows), glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top), face->glyph->advance.x);
		characters.insert(std::pair<char, Character>(i, character));
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	GLuint vao;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &posBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, posBufferObj);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

	// Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(vao);

	vaoHandle = vao;
}

void Font::Draw(Shader * shader)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureHandle);
	shader->setUniform("textureData", 0);

	glBindVertexArray(vaoHandle);
	int counter = 0;
	GLfloat lame_newline = 0;
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		counter++;
		Character ch = characters[*c];
		GLfloat xpos = textPosition.x + ch.bearing.x * textScale;
		GLfloat ypos = textPosition.y - (ch.size.y - ch.bearing.y) * textScale;

		GLfloat w = ch.size.x * textScale;
		GLfloat h = ch.size.y * textScale;

		if (*c == '{') {
			lame_newline -= (ch.advance >> 6) * textScale * 2.5;
			textPosition.x = initPosition;
			continue;
		}

		GLfloat vertices[6][4]
		{
			{ xpos, ypos + h + lame_newline, 0.0f , 0.0f },
			{ xpos, ypos + lame_newline,      0.0f , 1.0f },
			{ xpos + w, ypos + lame_newline,      1.0f , 1.0f },
			{ xpos, ypos + h + lame_newline, 0.0f , 0.0f },
			{ xpos + w, ypos + lame_newline,      1.0f , 1.0f },
			{ xpos + w, ypos + h + lame_newline, 1.0f , 0.0f }
		};

		glBindTexture(GL_TEXTURE_2D, ch.textureHandle);

		glBindBuffer(GL_ARRAY_BUFFER, posBufferObj);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		textPosition.x += (ch.advance >> 6) * textScale; // the space between characters
	}

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Font::DrawSomeDebugText(std::string tex, glm::vec2 position, GLfloat scale)
{
	text = tex;
	textPosition = position;
	textScale = scale;
}