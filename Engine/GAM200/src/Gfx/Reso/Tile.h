#ifndef TILE_H
#define TILE_H

#include "../../Gfx/Reso/Sprite.h"

class Texture;

class Tile : public Sprite
{
public:
	Tile();
	virtual ~Tile();
	Tile(Sprite * sp, Texture * tex);

	GLuint GetVaoHandle();
	GLuint GetTextureHandle();

	void SetTexture(Texture * tex);

private:
	Sprite    * sprite;
	Texture   * texture;
};


// ---------------------------------------------------------------------------
#endif