#ifndef CAMERA_H
#define CAMERA_H

#include "../../EngineCommon.h"
#include "../../Component/Entity.h"

#define ZOOM_VEL 0.1f
#define ROT_VEL 0.007f
#define MOVE_VEL 1.f

class GameObject;

typedef bool(*ObjectPointContainFn) (const glm::vec2 &, GameObject *);

class Camera : public Entity
{
public:

	std::string renderLayerName;

	glm::vec3 position;
	glm::vec3 view;
	glm::vec3 up;

	glm::vec2 scale;

	float nearPlane;
	float farPlane;

	float height;
	float width;

	float rotation;

	Camera();
	Camera(glm::vec3 & pos, glm::vec3 & view, glm::vec3 & up);
	~Camera();

	virtual void Initialize();
	virtual void Update();

	virtual void Load(Json::Value &);
	virtual void Save(Json::Value &);

	glm::mat4 CamToWorld();
	glm::mat4 WorldToCam();

	glm::vec2 ViewportToWorld(glm::vec2 point);
	glm::vec2 WorldToViewport(glm::vec2 point);

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	bool PickGameObject(const glm::vec3 & pointWin, GameObject * obj, ObjectPointContainFn selector);
	GameObject * PickGameObject(const glm::vec3 & pointWin, const std::list<GameObject*> & objs, ObjectPointContainFn selector);
	unsigned PickGameObjects(const glm::vec3 & pointWin, const std::list<GameObject*> & inObjs, std::list<GameObject*> & outObjs, ObjectPointContainFn selector);

	bool MoveOver(vec2 newPosition, float dt);
	void Mimic(Camera *);
};

#endif