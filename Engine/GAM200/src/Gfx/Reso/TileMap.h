#ifndef TILEMAP_H
#define TILEMAP_H

// this is so sad... Alexa, play despacito 2

#include "../../Gfx/Comp/Renderable.h"
#include <glm/glm.hpp>

class Tile;
class Shader;
class Texture;
class Sprite;
class AnimationData;
class BoxCollider;

class TileMap : public Renderable
{
public:

	TileMap();
	virtual ~TileMap();

	virtual void Initialize();
	virtual void Update();
	void Render();

	bool LoadTiles();
	void CleanTiles();
	void ChangeTexture();
	void Reload();
	void ReMake();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void Edit();
	void EditWindow();
	void AddCollider(unsigned idx);
	void RemoveCollider(unsigned idx);

	bool GetEdit();
	void SetWidth(unsigned w);
	void SetHeight(unsigned h);
	bool GetMoving();

	bool isMoving;

private:
	float offset;

	std::vector<glm::vec3> tilePositions;
	std::vector<Tile*> tiles;
	std::vector<int> data;
	glm::mat4 * tileModels;
	glm::mat4 * tileT;
	glm::mat4 * tileS;

	std::vector<BoxCollider*> colliders;
	std::vector<int> collisionData;

	AnimationData * metaData;

	unsigned insBufferObj;
	unsigned mBufferObj;
	unsigned sBufferObj;

	bool edit;
	int brush;

	unsigned width;
	unsigned height;
};

#endif