#ifndef FONT_H
#define FONT_H

#include "../../ResourceManager/ResoManager.h"

#include <ft2build.h>
#include FT_FREETYPE_H

#include <glm/glm.hpp>

class Character;
class Shader;

class Font : public Resource
{
public:
	GLuint vaoHandle;
	GLuint posBufferObj;
	GLuint textureHandle;

	FT_Library ft;
	FT_Face face;

	std::string text;
	float initPosition;
	glm::vec2 textPosition;
	GLfloat textScale;

	std::map<char, Character>characters;

	Font();
	virtual ~Font();

	virtual void LoadResource();

	virtual void InitFont(const char * name);
	virtual void Draw(Shader * shader);

	void DrawSomeDebugText(std::string text, glm::vec2 position, GLfloat scale);
};


#endif