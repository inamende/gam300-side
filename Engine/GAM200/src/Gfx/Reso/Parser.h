#ifndef PARSER_H
#define PARSER_H

#include "../../EngineCommon.h"
#include <glm/glm.hpp>

void LoadDataFromObj(const char * file, std::vector<glm::vec3> & vertices, std::vector<glm::vec2> & textures, std::vector<glm::vec3> & normals);

#endif