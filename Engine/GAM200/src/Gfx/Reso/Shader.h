#ifndef SHADER_H
#define SHADER_H

#include "../../ResourceManager/ResoManager.h"

enum ShaderType { VERTEX , FRAGMENT};

class Shader : public Resource
{
public:

	Shader();
	virtual ~Shader();

	void LoadResource();

	bool compileFromFile(std::string & file, ShaderType type);
	bool compileFromString(std::string & string, ShaderType type);
	bool link();
	void use();

	GLuint getHandle();

	void setUniform(const char *name, float x, float y, float z) const;
	void setUniform(const char *name, const glm::vec3 & v) const;
	void setUniform(const char *name, const glm::vec4 & v) const;
	void setUniform(const char *name, const glm::mat4 & m) const;
	void setUniform(const char *name, const glm::mat3 & m) const;
	void setUniform(const char *name, float val) const;
	void setUniform(const char *name, int val) const;
	void setUniform(const char *name, bool val) const;

private:
	GLuint  shaderHandle;
	std::string log_string_;
};

#endif
