#include "AnimationData.h"

#include "Texture.h"
#include "../Gfx.h"

AnimationData::AnimationData() : MetaData::MetaData()
{
	graphics->AddAnimation(this);
}

AnimationData::AnimationData(std::string name, unsigned r, unsigned c) : speed(10.f), ping_pong(false)
{
	row = r; col = c; SetName(name); 

	texture = graphics->GetDefaultTexture();

	for (unsigned i = 0; i < r * c; i++)
	{
		grid.push_back(i);
		delays.push_back(0.025f);
	}

	graphics->AddAnimation(this);
}

void AnimationData::CoreLoad(Json::Value & val)
{
	SetName(val.get("name", "Set_Name_Please").asString());
	row = val.get("row", 0).asInt();
	col = val.get("col", 0).asInt();
	texture = resoManager->getReso<Texture>(val.get("texture", "Set_Texture_Please").asString());
	for (unsigned i = 0; i < val["grid"].size(); i++)
		grid.push_back(val["grid"][i].asInt());
	for (unsigned i = 0; i < val["delay"].size(); i++)
		delays.push_back(val["delay"][i].asFloat());
	speed = val.get("speed", 1.0f).asFloat();
	ping_pong = val.get("ping_pong", false).asFloat();
}

void AnimationData::CoreSave(Json::Value & val)
{
	val["name"] = GetName();
	val["row"] = row;
	val["col"] = col;
	val["texture"] = resoManager->getResoName(texture);
	for (unsigned i = 0; i < grid.size(); i++)
		val["grid"][i] = grid[i];
	for (unsigned i = 0; i < delays.size(); i++)
		val["delay"][i] = delays[i];
	val["speed"] = speed;
	val["ping_pong"] = ping_pong;
}

unsigned AnimationData::GetRow() { return row; }

unsigned AnimationData::GetCol() { return col; }


