#ifndef ATLAS_DATA_H
#define ATLAS_DATA_H

#include "MetaData.h"

#include "../Comp/Transform.h"

class AtlasData : public MetaData
{
public:
	AtlasData();
	~AtlasData();

	virtual void CoreLoad(Json::Value & val);
	virtual void CoreSave(Json::Value & val);

	std::map<std::string, Transform> atlasObj;
	unsigned size;
	unsigned width;
	unsigned height;

private:
	Json::Value val;
};

#endif