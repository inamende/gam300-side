#ifndef TEXTURE_PACKER_H
#define TEXTURE_PACKER_H

#include "../../Component/Entity.h"

class SDL_Surface;

struct Box
{
	float top, bottom, right, left;
	float width, height;

	Box();
	Box(float t, float b, float r, float l);
};

struct Shelf
{
	Shelf * child[2];
	Box shelf;
	bool empty;

	Shelf();

	int Fits(Box & box0, Box & box1);
	int Fits(Box & shef, SDL_Surface * texture);
};

Shelf * Insert(Box & theBox, Shelf * prevNode);
Shelf * Insert(SDL_Surface * image, Shelf * prevNode);

#endif