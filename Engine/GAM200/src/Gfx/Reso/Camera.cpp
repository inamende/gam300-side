#include "Camera.h"

#include "../Gfx.h"
#include "../../Gfx/Window.h"
#include "../../Level/Scene.h"
#include "../../Input/Input.h"
#include "../../Editor/ImGuiEditor.h"
#include "../../Utils/Utilities.h"
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera()
{
	position = glm::vec3(0.f, 0.f, 1.f);
	view = glm::vec3(0.f, 0.f, 0.f);
	up = glm::vec3(0.f, 1.f, 0.f);
	nearPlane = -10000.0f;
	farPlane = 10000.0f;
	height = (float)mWindow->height;
	width = (float)mWindow->width;
	scale = glm::vec2(1.0f, 1.0f);
	rotation = 0.0f;
	renderLayerName = "defaultRenderLayer";
}

Camera::Camera(glm::vec3 & p, glm::vec3 & v, glm::vec3 & u)
{
	position = p;
	view = v;
	up = u;
	nearPlane = -10000.0f;
	farPlane = 10000.0f;
	height = (float)mWindow->height;
	width = (float)mWindow->width;
	scale = glm::vec2(1.0f, 1.0f);
	rotation = 0.0f;
}

Camera::~Camera() {}

void Camera::Initialize() {}

void Camera::Update()
{
	/*float add_speed = 10.f;

	if (KeyPressed(SDL_SCANCODE_UP))
		position.y += MOVE_VEL * add_speed;
	
	if (KeyPressed(SDL_SCANCODE_DOWN))
		position.y -= MOVE_VEL * add_speed;
	
	if (KeyPressed(SDL_SCANCODE_RIGHT))
		position.x += MOVE_VEL * add_speed;
	
	if (KeyPressed(SDL_SCANCODE_LEFT))
		position.x -= MOVE_VEL * add_speed;

	if (MousePressed(SDL_BUTTON_MIDDLE))
	{
		glm::vec2 pos = glm::vec2(position);
		glm::vec2 mouse = graphics->GetMainCamera()->ViewportToWorld(mousePosition);
		glm::vec2 mousePrev = graphics->GetMainCamera()->ViewportToWorld(mousePrevPosition);
		glm::vec2 mov = glm::vec2(mousePrev.x - mouse.x, mousePrev.y - mouse.y);
		pos += mov * MOVE_VEL;
		position.x = pos.x;
		position.y = pos.y;
	}
	
	if (mInput->KeyIsPressed(SDL_SCANCODE_LCTRL, false) && KeyPressed(SDL_SCANCODE_Q))
		rotation -= ROT_VEL;

	if (mInput->KeyIsPressed(SDL_SCANCODE_LCTRL, false) && KeyPressed(SDL_SCANCODE_E))
		rotation += ROT_VEL;

	 static int dir0 = 0;
	 if (!Input::getInstance()->KeyIsPressed(SDL_SCANCODE_LCTRL, false) && MouseWheel(dir0) && !imGuiEditor->HoveringImGui())
	 	position.y += dir0 * MOVE_VEL * add_speed * 2.f;

	view.x = position.x;
	view.y = position.y;
	view.z = 0.0f;

	static int dir1 = 0;
	if (mInput->KeyIsPressed(SDL_SCANCODE_LCTRL, false) && MouseWheel(dir1))
		scale.x -= dir1 * ZOOM_VEL;

	scale.y = scale.x;*/

	DrawColorBox(glm::vec3(position.x, position.y, 99.0f), width * 0.5f, height * 0.5f, glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
	scene->DrawFancyCursor(glm::vec3(position.x, position.y, 99.0f), glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
}

void Camera::Load(Json::Value & val)
{
	position.x = val["position"].get("x", 0.0f).asFloat();
	position.y = val["position"].get("y", 0.0f).asFloat();
	position.z = val["position"].get("z", 1.0f).asFloat();

	view.x = val["view"].get("x", 0.0f).asFloat();
	view.y = val["view"].get("y", 0.0f).asFloat();
	view.z = val["view"].get("z", 0.0f).asFloat();

	up.x = val["up"].get("x", 0.0f).asFloat();
	up.y = val["up"].get("y", 1.0f).asFloat();
	up.z = val["up"].get("z", 0.0f).asFloat();

	scale.x = val["scale"].get("x", 1.0f).asFloat();
	scale.y = val["scale"].get("y", 1.0f).asFloat();

	nearPlane = val.get("nearPlane", -10000.f).asFloat();
	farPlane = val.get("farPlane", 10000.f).asFloat();

	width = val.get("width", (float)mWindow->width).asFloat();
	height = val.get("height", (float)mWindow->height).asFloat();
	
	rotation = val.get("rotation", 0.0f).asFloat();
}

void Camera::Save(Json::Value & val)
{
	val["position"]["x"] = position.x;
	val["position"]["y"] = position.y;
	val["position"]["z"] = position.z;

	val["view"]["x"] = view.x;
	val["view"]["y"] = view.y;
	val["view"]["z"] = view.z;

	val["up"]["x"] = up.x;
	val["up"]["y"] = up.y;
	val["up"]["z"] = up.z;

	val["scale"]["x"] = scale.x;
	val["scale"]["y"] = scale.y;

	val["nearPlane"] = nearPlane;
	val["farPlane"] = farPlane;

	val["width"] = width;
	val["height"] = height;
	
	val["rotation"] = rotation;
}

glm::mat4 Camera::CamToWorld()
{
	glm::mat4 T = glm::translate(glm::mat4(1.0f), glm::vec3(position));
	glm::mat4 TR = glm::rotate(T, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	glm::mat4 TRS = glm::scale(TR, glm::vec3(scale.x, scale.y, 1.0f));

	return TRS;
}

glm::mat4 Camera::WorldToCam()
{
	glm::mat4 invS = glm::inverse(glm::scale(glm::mat4(1.0f), glm::vec3(scale.x, scale.y, 1.0f)));
	glm::mat4 invSR = glm::inverse(glm::rotate(invS, rotation, glm::vec3(0.0f, 0.0f, 1.0f)));
	glm::mat4 invSRT = glm::inverse(glm::translate(invSR, glm::vec3(position)));

	return invSRT;
}

glm::vec2 Camera::ViewportToWorld(glm::vec2 point)
{
	glm::vec4 centeredPoint = glm::vec4(point.x - mWindow->width / 2, -point.y + mWindow->height / 2, 0.0f, 1.0f);

	glm::mat4 TRSS = glm::scale(CamToWorld(), glm::vec3(width, height, 1.0f));
	glm::mat4 TRSSS = glm::scale(TRSS, glm::vec3(1.0f / mWindow->width, 1.0f / mWindow->height, 1.0f));

	return TRSSS * centeredPoint;
}

glm::vec2 Camera::WorldToViewport(glm::vec2 point)
{
	glm::mat4 TRSS = glm::scale(WorldToCam(), glm::vec3(1.0f / width, 1.0f / height, 1.0f));
	glm::mat4 TRSSS = glm::scale(TRSS, glm::vec3(mWindow->width, mWindow->height, 1.0f));

	vec2 p = TRSSS * glm::vec4(point, 0.0f, 1.0f);
	p.x += mWindow->width / 2;
	p.y = -p.y;
	p.y += mWindow->height / 2;
	return { p.x, p.y };
}

glm::mat4 Camera::getViewMatrix()
{
	glm::mat4 Rot = glm::mat3(glm::rotate(glm::mat4(1.f), rotation, glm::vec3(0.f, 0.f, 1.f)));
	glm::vec4 newUp = Rot * glm::vec4(up, 0.0f);
	return glm::lookAt(position, view, glm::vec3(newUp));
}

glm::mat4 Camera::getProjectionMatrix()
{
	return glm::ortho(-(float)(width / 2) * scale.x, (float)(width / 2) * scale.x, -(float)(height / 2) * scale.y, (float)(height / 2) * scale.y, nearPlane, farPlane);
}

bool Camera::PickGameObject(const glm::vec3 & pointWin, GameObject * obj, ObjectPointContainFn selector)
{
	if (obj == NULL)
		return false;

	return selector(ViewportToWorld(pointWin), obj);
}

GameObject * Camera::PickGameObject(const glm::vec3 & pointWin, const std::list<GameObject*> & objs, ObjectPointContainFn selector)
{
	for (std::list<GameObject*>::const_iterator it = objs.begin(); it != objs.end(); it++)
		if (PickGameObject(pointWin, (*it), selector))
			return (*it);

	return NULL;
}

unsigned Camera::PickGameObjects(const glm::vec3 & pointWin, const std::list<GameObject*> & inObjs, std::list<GameObject*> & outObjs, ObjectPointContainFn selector)
{
	for (std::list<GameObject*>::const_iterator it = inObjs.begin(); it != inObjs.end(); it++)
	{
		if ((*it) != NULL)
		{
			if (selector(ViewportToWorld(pointWin), (*it)))
			{
				std::list<GameObject*>::iterator finder = std::find_if(outObjs.begin(), outObjs.end(), [&](const GameObject * first) { return first == (*it); });
				if(finder == outObjs.end()) outObjs.push_back((*it)); //no duplicates please
			}
		}
	}
	return outObjs.size();
}

bool Camera::MoveOver(vec2 newPosition, float dt) {
	if (!CompareFloat(position.x, newPosition.x, 1.f) || !CompareFloat(position.y, newPosition.y, 1.f)) {
		position.x = Lerp(position.x, newPosition.x, dt);
		position.y = Lerp(position.y, newPosition.y, dt);
		view.x = position.x;
		view.y = position.y;
		return false;
	}
	return true;
}

void Camera::Mimic(Camera * cam) {
	position.x = cam->position.x;
	position.y = cam->position.y;
	view.x = cam->view.x;
	view.y = cam->view.y;
	scale = cam->scale;
	width = cam->width;
	height = cam->height;
}






