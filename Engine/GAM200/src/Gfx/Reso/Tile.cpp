#include "Tile.h"

#include "../../Gfx/Reso/Texture.h"

Tile::Tile() : sprite(NULL), texture(NULL) {}

Tile::~Tile() {}

Tile::Tile(Sprite * sp, Texture * tex) : sprite(sp), texture(tex) {}

GLuint Tile::GetVaoHandle() { return sprite->vaoHandle; }

GLuint Tile::GetTextureHandle() { return texture->texHandle; }

void Tile::SetTexture(Texture * tex) { texture = tex; }