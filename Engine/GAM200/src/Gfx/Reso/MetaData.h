#ifndef META_DATA_H
#define META_DATA_H

#include "../../ResourceManager/ResoManager.h"

class MetaData : public Resource
{
public:
	MetaData();
	void LoadResource();
	void Loader(std::string filePath);
	void Writer();
	void Writer(std::string filePath);

private:
	virtual void CoreLoad(Json::Value & val) = 0;
	virtual void CoreSave(Json::Value & val) = 0;
	Json::Value metaData;
	std::string fileName;
};

#endif