#include "TexturePacker.h"

#include <SDL2/SDL_image.h>

Box::Box() : top(0), bottom(0), right(0), left(0), width(0), height(0) {}

Box::Box(float t, float b, float r, float l)
{
	top = t;
	bottom = b;
	right = r;
	left = l;
	width = abs(right - left);
	height = abs(bottom - top);
}

Shelf::Shelf() : empty(true) { child[0] = child[1] = NULL; }

int Shelf::Fits(Box & shelf, Box & box)
{
	// shelf bigger than texture
	if (shelf.width > box.width || shelf.height > box.height)
		return 2;

	// shelf equal to texture
	else if (shelf.width == box.width && shelf.height == box.height)
		return 1;

	// shelf smaller than texture
	else
		return 0;
}

int Shelf::Fits(Box & shef, SDL_Surface * texture)
{
	// shelf bigger than texture
	if (shelf.width > texture->w || shelf.height > texture->h)
		return 2;

	// shelf equal to texture
	else if (shelf.width == texture->w && shelf.height == texture->h)
		return 1;

	// shelf smaller than texture
	else
		return 0;
}

Shelf * Insert(Box & theBox, Shelf * prevNode)
{
	// if we are not at a leaf
	if (prevNode->child[0] != NULL || prevNode->child[1] != NULL)
	{
		Shelf * newShelf = Insert(theBox, prevNode->child[0]);
		if (newShelf != NULL) return newShelf;

		return Insert(theBox, prevNode->child[1]);
	}
	else
	{
		// there is already a texture here
		if (!prevNode->empty) return NULL;

		// texture it's too big
		if (prevNode->Fits(prevNode->shelf, theBox) == 0) return NULL;

		// perfect fit
		if (prevNode->Fits(prevNode->shelf, theBox) == 1) return prevNode;

		prevNode->child[0] = DBG_NEW Shelf;
		prevNode->child[1] = DBG_NEW Shelf;

		float w = prevNode->shelf.width - theBox.width;
		float h = prevNode->shelf.height - theBox.height;

		if (w > h)
		{
			prevNode->child[0]->shelf = Box(prevNode->shelf.top, prevNode->shelf.bottom, prevNode->shelf.left + theBox.width, prevNode->shelf.left);
			prevNode->child[1]->shelf = Box(prevNode->shelf.top, prevNode->shelf.bottom, prevNode->shelf.right, prevNode->shelf.left + theBox.width);
		}
		else
		{
			prevNode->child[0]->shelf = Box(prevNode->shelf.top, prevNode->shelf.top + theBox.height, prevNode->shelf.right, prevNode->shelf.left);
			prevNode->child[1]->shelf = Box(prevNode->shelf.top + theBox.height, prevNode->shelf.bottom, prevNode->shelf.right, prevNode->shelf.left);
		}

		return Insert(theBox, prevNode->child[0]);
	}
	
}

Shelf * Insert(SDL_Surface * image, Shelf * prevNode)
{
	// if we are not at a leaf
	if (prevNode->child[0] != NULL || prevNode->child[1] != NULL)
	{
		Shelf * newShelf = Insert(image, prevNode->child[0]);
		if (newShelf != NULL) return newShelf;

		return Insert(image, prevNode->child[1]);
	}
	else
	{
		// there is already a texture here
		if (!prevNode->empty) return NULL;

		// texture it's too big
		if (prevNode->Fits(prevNode->shelf, image) == 0) return NULL;

		// perfect fit
		if (prevNode->Fits(prevNode->shelf, image) == 1) return prevNode;

		prevNode->child[0] = DBG_NEW Shelf;
		prevNode->child[1] = DBG_NEW Shelf;

		float w = prevNode->shelf.width - image->w;
		float h = prevNode->shelf.height - image->h;

		if (w > h)
		{
			prevNode->child[0]->shelf = Box(prevNode->shelf.top, prevNode->shelf.bottom, prevNode->shelf.left + image->w, prevNode->shelf.left);
			prevNode->child[1]->shelf = Box(prevNode->shelf.top, prevNode->shelf.bottom, prevNode->shelf.right, prevNode->shelf.left + image->w);
		}
		else
		{
			prevNode->child[0]->shelf = Box(prevNode->shelf.top, prevNode->shelf.top + image->h, prevNode->shelf.right, prevNode->shelf.left);
			prevNode->child[1]->shelf = Box(prevNode->shelf.top + image->h, prevNode->shelf.bottom, prevNode->shelf.right, prevNode->shelf.left);
		}

		return Insert(image, prevNode->child[0]);
	}
}

