#ifndef CHARACTER_H
#define CHARACTER_H

#include "../../Component/Entity.h"
#include <GL/glew.h>

class Character : Entity
{
public:
	GLuint textureHandle;
	//these are related to glyph metrics
	glm::ivec2 size;
	glm::ivec2 bearing;
	GLuint advance;

	Character();
	Character(GLuint tex, glm::ivec2 s, glm::ivec2 b, GLuint a);
};

#endif