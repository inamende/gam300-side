#include "AtlasData.h"

#include "../Gfx.h"

AtlasData::AtlasData() : MetaData::MetaData()
{
	graphics->AddAtlas(this);
}

AtlasData::~AtlasData()
{
	graphics->RemoveAtlas(this);
}

void AtlasData::CoreLoad(Json::Value & val)
{
	SetName(val.get("name", "Set_Name_Please").asString());
	width = val.get("width", 1.f).asFloat();
	height = val.get("height", 1.f).asFloat();

	std::vector<std::string> members = val.getMemberNames();

	for (std::vector<std::string>::iterator member = members.begin(); member != members.end(); member++)
	{
		if (*member == "name" || *member == "width" || *member == "height")
			continue;

		std::string idx = *member;

		float posX = val[(*member)]["position"].get("x", 0.0f).asFloat();
		float posY = val[(*member)]["position"].get("y", 0.0f).asFloat();

		float width = val[(*member)].get("width", 0.0f).asFloat();
		float height = val[(*member)].get("height", 0.0f).asFloat();

		atlasObj.insert(std::pair<std::string, Transform>(idx, Transform(glm::vec3(posX, posY, 1.f), 0.f, glm::vec3(width, height, 1.f))));
	}

	size = members.size() - 3;
}

void AtlasData::CoreSave(Json::Value & val) {} // don't modify the atlas