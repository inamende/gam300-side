#ifndef LINE_H
#define LINE_H

#include "../../EngineCommon.h"
#include <glm/glm.hpp>
#include <GL/glew.h>

class Line
{
public:

	GLuint vaoHandle;
	GLuint posBufferObj;
	GLuint colBufferObj;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec4> colors;

	std::string lineLayer;

	Line();
	~Line();

	virtual void InitLine();
	virtual void Draw();

	void DrawALine(glm::vec3 p1, glm::vec3 p2, glm::vec4 color);
};


#endif