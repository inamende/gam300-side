#ifndef PARTICLE_H
#define PARTICLE_H

#include "../../Gfx/Comp/Renderable.h"
#include <GL/glew.h>

struct Particle {
	glm::vec3 position, speed;
	unsigned char r, g, b ,a;
	float lifeTime, size;

	Particle() = default;
};

class ParticleEmiter : public Renderable
{
	enum Effect {def=0, dust, spooky, bubles} efects;
private:
	int FindUnusedParticle();
	void SortParticles();
	void SpreadFroimCenter(int index, float spreadFactor);
	void Dust(int index, float spreadFactor);
	void Spooky(int index, float spreadFactor);

public:
	ParticleEmiter();
	~ParticleEmiter();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	void Render();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	int particleRate;
	float paricleLifeTime;
	int particleLimit;
	float particleAlphaReducionRate;
	float particleSize;

	bool useRandomSize;
	bool useRandomColor;
	float spreadfactor;

private:
	unsigned maxParticles;

	GLuint vBuffer;
	GLuint pBuffer;
	GLuint cBuffer;

	GLuint handle;

	unsigned particleCount;
	float * posData;
	unsigned char * colData;
	Particle particleContainer[1000];
	std::vector<vec3> vertices;
};

#endif