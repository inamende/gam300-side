#include "Line.h"

#include "../Gfx.h"

Line::Line() {}

Line::~Line()
{
	vertices.clear();
	colors.clear();
}

void Line::InitLine()
{
	GLuint vao;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//1 buffer 
	glGenBuffers(1, &posBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, posBufferObj);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &colBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, colBufferObj);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(vao);
	 
	vaoHandle = vao;

	glLineWidth(3);
}

void Line::Draw()
{
	if (vertices.size() > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, posBufferObj);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW); //this is slow but should work for now
																										  //glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(glm::vec3), &vertices[0]);

		glBindBuffer(GL_ARRAY_BUFFER, colBufferObj);
		glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec4), &colors[0], GL_STATIC_DRAW);

		glBindVertexArray(vaoHandle);
		glDrawArrays(GL_LINES, 0, vertices.size());

		vertices.clear();
		colors.clear();
	}
}

void Line::DrawALine(glm::vec3 p1, glm::vec3 p2, glm::vec4 color)
{
	if (graphics->debugRendering)
	{
		vertices.push_back(p1);
		vertices.push_back(p2);

		colors.push_back(color);
		colors.push_back(color);
	}
}