#ifndef SOUND_H
#define SOUND_H

#include "../../ResourceManager/ResoManager.h"
#include "Parser.h"
struct Sound;
class SoundFile : public Resource
{
public:
	GLuint handle;
	std::string filePath;

	SoundFile();
	virtual ~SoundFile();
	virtual void LoadResource();
	Sound * ReturnSoundPointer();
private:
	Sound * mSoundPointer;
};
#endif