#include "Sprite.h"

Sprite::Sprite() : Resource::Resource() { wireFrame = false; }

Sprite::~Sprite()
{
	glDeleteBuffers(1, &posBufferObj);
	glDeleteBuffers(1, &texBufferObj);
	glDeleteBuffers(1, &normBufferObj);
	glDeleteBuffers(1, &tanBufferObj);
	glDeleteVertexArrays(1, &vaoHandle);

	Clear();
}

void Sprite::LoadResource()
{
	InitSprite(Sprite::filePath.c_str());
}

void Sprite::InitSprite(const char * name)
{
	LoadDataFromObj(name, vertices, uvs, normals);

	tangents = { glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f) };

	GLuint vao; //it doesn't work to use vao_ (the class memmber)

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//1 buffer 
	glGenBuffers(1, &posBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, posBufferObj);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &texBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, texBufferObj);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &normBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, normBufferObj);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &tanBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, tanBufferObj);
	glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &colBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, colBufferObj);
	if (!colors.empty()) {
		glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec4), &colors[0], GL_STATIC_DRAW);
	}
	
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(vao);

	vaoHandle = vao;
}

void Sprite::Draw()
{
	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	glBindVertexArray(0);
}

void Sprite::FlipVertical()
{
	for (unsigned i = 0; i < uvs.size(); i++)
		uvs[i].x = -uvs[i].x;

	glBindBuffer(GL_ARRAY_BUFFER, texBufferObj);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
}

void Sprite::FlipHorizontal()
{
	for (unsigned i = 0; i < uvs.size(); i++)
		uvs[i].y = -uvs[i].y;

	glBindBuffer(GL_ARRAY_BUFFER, texBufferObj);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
}

void Sprite::Clear()
{
	vertices.clear();
	uvs.clear();
	normals.clear();
	colors.clear();
	tangents.clear();
}