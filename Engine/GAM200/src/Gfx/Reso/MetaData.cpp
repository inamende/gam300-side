#include "MetaData.h"

#pragma warning( disable : 4996)

MetaData::MetaData() {}
void MetaData::LoadResource()
{
	Loader(MetaData::filePath.c_str());
}
void MetaData::Loader(std::string filePath)
{
	fileName = filePath;

	std::ifstream jsonFile(filePath.c_str());
	Json::Reader reader;

	bool ok = reader.parse(jsonFile, metaData);

	//if (!ok)
	//	std::cout << "ERROR READING JSON" << std::endl;

	CoreLoad(metaData);

	jsonFile.close();
}

void MetaData::Writer()
{
	CoreSave(metaData);

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();

	std::ofstream jsonFileWrite(fileName);

	writer->write(metaData, &jsonFileWrite);

	jsonFileWrite.close();
}

void MetaData::Writer(std::string filePath)
{
	CoreSave(metaData);

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();

	std::ofstream jsonFileWrite(filePath);

	writer->write(metaData, &jsonFileWrite);

	jsonFileWrite.close();
}