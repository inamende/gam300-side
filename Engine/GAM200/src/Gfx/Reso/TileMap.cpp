#include "TileMap.h"

#include "../../ResourceManager/ResoManager.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Input/Input.h"
#include "../Gfx.h"
#include "../Reso/Camera.h"
#include "../Reso/Shader.h"
#include "../Reso/Texture.h"
#include "../Reso/AnimationData.h"
#include "../Comp/Transform.h"
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL.h>
#include "../../Editor/ImGuiEditor.h"
#include "../../Physics/Collisions.h"
#include "../../Physics/Comp/BoxCollider.h"
#include "../../Level/Scene.h"
#include <glm/gtc/matrix_transform.hpp>

#include "Tile.h"

TileMap::TileMap() :
	Renderable(),
	tileModels(NULL),
	tileT(NULL),
	tileS(NULL),
	metaData(NULL),
	offset(100.f),
	edit(false),
	brush(0),
	width(5),
	height(5),
	isMoving(false)
{
	sprite = DBG_NEW Sprite();
	sprite->InitSprite("./Meshes/plane.obj");
}

TileMap::~TileMap() 
{
	CleanTiles();
	graphics->RemoveTileMap(this);
	data.clear();
	delete sprite;

	for (unsigned i = 0; i < colliders.size(); i++)
		RemoveCollider(i);
	colliders.clear();
	collisionData.clear();

	graphics->RemoveRenderable(this);
}

void TileMap::Initialize()
{
	renderableTransform = GetOwner()->mTransform;

	graphics->AddRenderable(this);
	
	if (data.empty())
		for (unsigned i = 0; i < width * height; i++)
			data.push_back(-1);

	if (collisionData.empty())
		for (unsigned i = 0; i < width * height; i++)
			collisionData.push_back(0);

	for (unsigned i = 0; i < collisionData.size(); i++)
		colliders.push_back(NULL);

	// if(LoadTiles())
	//	graphics->AddTileMap(this);

	LoadTiles();
	graphics->AddTileMap(this);
}

void TileMap::Update()
{		
	for (unsigned i = 0; i < colliders.size(); i++)
		if(colliders[i]) colliders[i]->Update();
}


void TileMap::Render()
{
	if (shader && texture && visible && metaData)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tiles[0]->GetTextureHandle());
		shader->setUniform("textureData", 0);

		glBindVertexArray(tiles[0]->GetVaoHandle());
		glDrawArraysInstanced(GL_TRIANGLES, 0, 6, width * height);
		glBindVertexArray(0);
	}
}

bool TileMap::LoadTiles()
{
	if (metaData == NULL)
		return false;

	const float x = renderableTransform->position.x - offset * (int)(width / 2);
	const float y = renderableTransform->position.y + offset * (int)(height / 2);
	const float z = renderableTransform->position.z;

	tileModels = DBG_NEW glm::mat4[width * height];
	tileT = DBG_NEW glm::mat4[width * height];
	tileS = DBG_NEW glm::mat4[width * height];

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			Transform transform(glm::vec3((float)(x + j * offset), (float)(y - i * offset), z), 0.0f, glm::vec3(offset, offset, 1.0f));
			tileModels[j * height + i] = transform.GetTransform();

			int tileType = data[i * width + j];

			int x_cor = tileType % metaData->GetRow(), y_cor = tileType / metaData->GetRow();

			glm::vec2 translation = glm::vec2(x_cor * 1.f / metaData->GetRow(), y_cor * 1.f / metaData->GetCol());

			tileT[j * height + i] = glm::translate(glm::mat4(1.f), glm::vec3(translation, 1.0f));
			tileS[j * height + i] = glm::scale(glm::mat4(1.f), glm::vec3(1.f / (float)metaData->GetRow(), 1.f / (float)metaData->GetCol(), 1.0f));
			tiles.push_back(DBG_NEW Tile(sprite, texture));
			tilePositions.push_back(glm::vec3((float)(x + j * offset), (float)(y - i * offset), z));

			if (tileType == -1)
				tileModels[j * height + i] = glm::mat4();
		}
	}

	for (unsigned i = 0; i < collisionData.size(); i++)
			RemoveCollider(i);

	for (unsigned i = 0; i < collisionData.size(); i++)
		if (collisionData[i] == 1)
			AddCollider(i);

	glGenBuffers(1, &insBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, insBufferObj);
	glBufferData(GL_ARRAY_BUFFER, width * height * sizeof(glm::mat4), &tileModels[0], GL_STATIC_DRAW);


	for (std::vector<Tile*>::iterator it = tiles.begin(); it != tiles.end(); it++)
	{
		GLuint vao = (*it)->GetVaoHandle();

		glBindVertexArray(vao);

		//tile model
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);

		glBindVertexArray(0);
	}

	glGenBuffers(1, &mBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, mBufferObj);
	glBufferData(GL_ARRAY_BUFFER, width * height * sizeof(glm::mat4), &tileT[0], GL_STATIC_DRAW);

	for (std::vector<Tile*>::iterator it = tiles.begin(); it != tiles.end(); it++)
	{
		GLuint vao = (*it)->GetVaoHandle();

		glBindVertexArray(vao);

		//tile texture transform
		glEnableVertexAttribArray(7);
		glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
		glEnableVertexAttribArray(8);
		glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
		glEnableVertexAttribArray(9);
		glVertexAttribPointer(9, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		glEnableVertexAttribArray(10);
		glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(7, 1);
		glVertexAttribDivisor(8, 1);
		glVertexAttribDivisor(9, 1);
		glVertexAttribDivisor(10, 1);

		glBindVertexArray(0);
	}

	glGenBuffers(1, &sBufferObj);
	glBindBuffer(GL_ARRAY_BUFFER, sBufferObj);
	glBufferData(GL_ARRAY_BUFFER, width * height * sizeof(glm::mat4), &tileS[0], GL_STATIC_DRAW);

	for (std::vector<Tile*>::iterator it = tiles.begin(); it != tiles.end(); it++)
	{
		GLuint vao = (*it)->GetVaoHandle();

		glBindVertexArray(vao);

		//tile texture model
		glEnableVertexAttribArray(11);
		glVertexAttribPointer(11, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
		glEnableVertexAttribArray(12);
		glVertexAttribPointer(12, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
		glEnableVertexAttribArray(13);
		glVertexAttribPointer(13, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		glEnableVertexAttribArray(14);
		glVertexAttribPointer(14, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(11, 1);
		glVertexAttribDivisor(12, 1);
		glVertexAttribDivisor(13, 1);
		glVertexAttribDivisor(14, 1);

		glBindVertexArray(0);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	return true;
}
	

void TileMap::CleanTiles()
{
	delete[] tileModels;
	delete[] tileT;
	delete[] tileS;

	glDeleteBuffers(1, &insBufferObj);
	glDeleteBuffers(1, &mBufferObj);
	glDeleteBuffers(1, &sBufferObj);

	for (auto it = tiles.begin(); it != tiles.end(); it++)
		delete (*it);
	tiles.clear();

	tilePositions.clear();
}

void TileMap::ChangeTexture()
{
	for (std::vector<Tile*>::iterator it = tiles.begin(); it != tiles.end(); it++)
	{
		(*it)->SetTexture(texture);
	}
}

void TileMap::Reload()
{
	CleanTiles();
	LoadTiles();
}

void TileMap::ReMake()
{
	CleanTiles();

	for (unsigned i = 0; i < colliders.size(); i++)
		RemoveCollider(i);
	colliders.clear();

	data.clear();
	collisionData.clear();

	for (unsigned i = 0; i < width * height; i++)
		data.push_back(-1);

	for (unsigned i = 0; i < width * height; i++)
		collisionData.push_back(0);

	for (unsigned i = 0; i < collisionData.size(); i++)
		colliders.push_back(NULL);

	LoadTiles();
}

void TileMap::load(Json::Value & val)
{
	shader = (resoManager->getReso<Shader>(val["TileMap"].get("shader", "Set_Shader_Please").asString()) != NULL) ?
		resoManager->getReso<Shader>(val["TileMap"].get("shader", "Set_Shader_Please").asString()) :
		graphics->GetDefaultShader();
	texture = resoManager->getReso<Texture>(val["TileMap"].get("texture", "Set_Texture_Please").asString());

	visible = val["TileMap"].get("visible", true).asBool();
	layer = val["TileMap"].get("layer", "Set_Layer_Please").asString();
	active = val["TileMap"].get("active", true).asBool();
	offset = val["TileMap"].get("offset", 100.f).asFloat();

	data.clear();
	for (unsigned i = 0; i < val["TileMap"]["data"].size(); i++)
		data.push_back(val["TileMap"]["data"][i].asInt());

	collisionData.clear();
	for (unsigned i = 0; i < val["TileMap"]["collision_data"].size(); i++)
		collisionData.push_back(val["TileMap"]["collision_data"][i].asBool());

	width = val["TileMap"].get("width", 5).asInt();
	height = val["TileMap"].get("height", 5).asInt();

	metaData = resoManager->getReso<AnimationData>(val["TileMap"].get("meta_data", "Set_Data_Please").asString());
}
void TileMap::save(Json::Value & val)
{
	if (shader)  val["TileMap"]["shader"] = resoManager->getResoName(shader);
	if (texture) val["TileMap"]["texture"] = resoManager->getResoName(texture);
	val["TileMap"]["visible"] = visible;
	val["TileMap"]["layer"] = layer;
	val["TileMap"]["active"] = active;
	val["TileMap"]["offset"] = offset;

	val["TileMap"]["data"].clear();
	for (unsigned i = 0; i < width * height; i++)
		val["TileMap"]["data"].append(data[i]);

	val["TileMap"]["collision_data"].clear();
	for (unsigned i = 0; i < width * height; i++)
		val["TileMap"]["collision_data"].append(collisionData[i]);

	val["TileMap"]["active"] = active;
	val["TileMap"]["offset"] = offset;

	val["TileMap"]["width"] = width;
	val["TileMap"]["height"] = height;

	if (metaData) val["TileMap"]["meta_data"] = resoManager->getResoName(metaData);

}
void TileMap::WriteToImguiWindow()
{
	static int selected_texture = 0;
	static int selected_shader = 0;
	static int selected_layer = 0;
	static int selected_animation = 0;

	if (ImGui::CollapsingHeader("TileMap"))
	{
		if (ImGui::Button("Texture##TileMap"))
			ImGui::OpenPopup("textures");

		ImGui::SameLine();
		if (texture) ImGui::Text(resoManager->getResoName(texture).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Shader##TileMap"))
			ImGui::OpenPopup("shaders");

		ImGui::SameLine();
		if (shader) ImGui::Text(resoManager->getResoName(shader).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Layer##TileMap"))
			ImGui::OpenPopup("layers");

		ImGui::SameLine();
		if (layer != "") ImGui::Text(layer.c_str());
		else ImGui::Text("NULL");

		ImGui::Checkbox("visible", &visible);
		if (sprite != NULL) ImGui::Checkbox("wire frame##TileMap", &sprite->wireFrame);

		if (ImGui::Button("Meta data##TileMap"))
			ImGui::OpenPopup("meta");

		ImGui::SameLine();
		if (metaData == NULL) ImGui::Text("NULL"); else ImGui::Text(resoManager->getResoName(metaData).c_str());

		if (ImGui::Button("Reload"))
			Reload();

		ImGui::NewLine();
		if (ImGui::Button("Detach##TileMap"))
			GetOwner()->detach(this);
	}

	if (ImGui::BeginPopup("textures"))
	{
		ImGui::Text("Textures");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->textureNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->textureNames[i].c_str()))
			{
				selected_texture = i;
				texture = resoManager->getReso<Texture>(imGuiEditor->textureNames[selected_texture]);
				ChangeTexture();
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("shaders"))
	{
		ImGui::Text("Shaders");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->shaderNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->shaderNames[i].c_str()))
			{
				selected_shader = i;
				shader = resoManager->getReso<Shader>(imGuiEditor->shaderNames[selected_shader]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("layers"))
	{
		ImGui::Text("Layers");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->layerNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->layerNames[i].c_str()))
			{
				Renderable * temp = this;

				graphics->RemoveRenderable(this);

				selected_layer = i;
				layer = imGuiEditor->layerNames[selected_layer];

				graphics->AddRenderable(temp);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("meta"))
	{
		ImGui::Text("Animations");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->animationNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->animationNames[i].c_str()))
			{
				selected_animation = i;

				metaData = resoManager->getReso<AnimationData>(imGuiEditor->animationNames[selected_animation]);

				graphics->RemoveTileMap(this);
				CleanTiles();
				if (LoadTiles())
					graphics->AddTileMap(this);

			}

		ImGui::EndPopup();
	}
}

void TileMap::Edit()
{
	const float x = renderableTransform->position.x - offset * (int)(width / 2);
	const float y = renderableTransform->position.y + offset * (int)(height / 2);
	const float z = renderableTransform->position.z;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int tileType = data[i * width + j];

			int x_cor = tileType % metaData->GetRow(), y_cor = tileType / metaData->GetRow();

			glm::vec2 translation = glm::vec2(x_cor * 1.f / metaData->GetRow(), y_cor * 1.f / metaData->GetRow());

			tileT[j * height + i] = glm::translate(glm::mat4(1.f), glm::vec3(translation, 1.0f));

			Transform transform(glm::vec3((float)(x + j * offset), (float)(y - i * offset), z), 0.0f, glm::vec3(offset, offset, 1.0f));
			tileModels[j * height + i] = transform.GetTransform();

			if (tileType == -1)
				tileModels[j * height + i] = glm::mat4();
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, mBufferObj);
	glBufferData(GL_ARRAY_BUFFER, width * height * sizeof(glm::mat4), &tileT[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, insBufferObj);
	glBufferData(GL_ARRAY_BUFFER, width * height * sizeof(glm::mat4), &tileModels[0], GL_STATIC_DRAW);

	glm::vec2 mouse = graphics->GetCameraByRenderLayer(layer)->ViewportToWorld(mousePosition);

	for (unsigned i = 0; i < tilePositions.size(); i++)
	{
		if (StaticPointToStaticRect(tilePositions[i], graphics->GetMainCamera()->position, graphics->GetMainCamera()->width, graphics->GetMainCamera()->height))
			DrawColorBox(tilePositions[i], offset * 0.5f, offset * 0.5f, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

		if (isMoving) continue;

		if (MousePressed(SDL_BUTTON_LEFT) && StaticPointToStaticRect(glm::vec3(mouse, 0.0f), tilePositions[i], offset, offset) && !imGuiEditor->HoveringImGui())
		{
			DrawColorBox(glm::vec3(tilePositions[i].x, tilePositions[i].y, 99), offset * 0.5f, offset * 0.5f, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			data[i] = brush;

			if (metaData->grid[brush] == 1)
			{
				RemoveCollider(i);
				AddCollider(i);
			}
			else
			{
				RemoveCollider(i);
				collisionData[i] = 0;
			}
		}

		if (MousePressed(SDL_BUTTON_RIGHT) && StaticPointToStaticRect(glm::vec3(mouse, 0.0f), tilePositions[i], offset, offset) && !imGuiEditor->HoveringImGui())
		{
			DrawColorBox(glm::vec3(tilePositions[i].x, tilePositions[i].y, 99), offset * 0.5f, offset * 0.5f, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			data[i] = -1;
			RemoveCollider(i);
			collisionData[i] = 0;
		}
	}
}

void TileMap::EditWindow()
{
	Edit();

	ImColor collisionColor; // pink if collision, blue if not
	ImColor collisionColorHover;

	if (texture && metaData)
	{
		//this is magic...
		for (unsigned i = 0; i < metaData->GetRow() * metaData->GetCol(); i++)
		{
			// I used grid in metadata to store collision insted hi hi hi...
			collisionColor = (metaData->grid[i] == 1) ? ImColor(255, 0, 255, 255) : ImColor(0, 255, 255, 255);
			collisionColorHover = (metaData->grid[i] == 1) ? ImColor(200, 0, 200, 255 ) : ImColor(0, 200, 200, 255);

			// the translations to get the uvs
			int uvTx = i % metaData->GetRow(); int uvTy = i / metaData->GetCol();

			ImGui::PushID(i); //this makes two buttons which are theoretically the same be different (I think...)
			ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)collisionColor);
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)collisionColorHover);

			if (ImGui::ImageButton((void*)(intptr_t)texture->texHandle, ImVec2(100, 100), ImVec2(uvTx * 1.f / metaData->GetRow(), uvTy * 1.f / metaData->GetCol()),
				ImVec2(uvTx  * 1.f / metaData->GetRow() + 1.f / metaData->GetRow(), uvTy * 1.f / metaData->GetRow() + 1.f / metaData->GetCol()),
				4, ImColor(0, 0, 0, 255)))
			{
				brush = i;
			}
				
			if (ImGui::IsItemHovered() && ImGui::GetIO().MouseClicked[1])
			{
				metaData->grid[i]++;
				if (metaData->grid[i] > 1) metaData->grid[i] = 0;
				metaData->Writer();
			}

			ImGui::PopStyleColor(2);
			ImGui::PopID();

			if ((i + 1) % metaData->GetCol() != 0) // just to indent it as in the sprite sheet
				ImGui::SameLine();
		}
	}

	ImGui::NewLine();
	if (ImGui::Button("Delete##TileMap"))
	{
		imGuiEditor->selectedTileMap = NULL;
		imGuiEditor->editState = ImGuiEditor::EditState::Idle;
		imGuiEditor->showTileMapWindow = false;
		scene->DeleteGameObject(GetOwner());
	}
}

void TileMap::AddCollider(unsigned idx)
{
	BoxCollider * col = DBG_NEW BoxCollider;
	Transform * tr = DBG_NEW Transform;

	tr->position = tilePositions[idx];
	col->SetOwnerTransform(tr);

	colliders[idx] = col;
	collisionData[idx] = 1;

	physics->AddCollider(col, true);
}

void TileMap::RemoveCollider(unsigned idx)
{
	Collider * col = colliders[idx];
	if (col != NULL)
	{
		delete col->GetOwnerTransform();
		delete col; // this calls RemoveCollider in physics
	}
	colliders[idx] = NULL;
}

bool TileMap::GetEdit() { return edit; }

void TileMap::SetWidth(unsigned w) { width = w; }

void TileMap::SetHeight(unsigned h) { height = h; }

bool TileMap::GetMoving() { return isMoving; }