#include "Parser.h"

#pragma warning(disable : 4996) //disble fopen and scanf warnings

void LoadDataFromObj(const char * file, std::vector<glm::vec3> & outvertices, std::vector<glm::vec2> & outtextures, std::vector<glm::vec3> & outnormals)
{
	FILE *in;
	if (!(in = fopen(file, "r")))
	{
		printf("Could not open input file\n");
		exit(0);
	}

	char aLine[200];
	char trash[1000]; //store comments here

	std::vector<glm::vec3>tempvertices;
	std::vector<glm::vec2>temptextures;
	std::vector<glm::vec3>tempnormals;
	std::vector<unsigned int>verticesIndx;
	std::vector<unsigned int>texturesIndx;
	std::vector<unsigned int>normalsIndx;
	glm::vec3 vertices;
	glm::vec2 textures;
	glm::vec3 normals;
	unsigned int indexV0, indexV1, indexV2;
	unsigned int indexT0, indexT1, indexT2;
	unsigned int indexN0, indexN1, indexN2;
	
	while (1)
	{
		if (fscanf(in, "%s", aLine) == EOF)
			break;
		
		if (strcmp(aLine, "v") == 0) //if line starts with v...
		{
			fscanf(in, "%f %f %f\n", &vertices.x, &vertices.y, &vertices.z);
			tempvertices.push_back(vertices);
		}
		//

		//
		else if (strcmp(aLine, "vt") == 0)
		{
			fscanf(in, "%f %f\n", &textures.s, &textures.t);
			temptextures.push_back(textures);
		}
		//

		//
		else if (strcmp(aLine, "vn") == 0)
		{	
			fscanf(in, "%f %f %f\n", &normals.x, &normals.y, &normals.z);
			tempnormals.push_back(normals);
		}
		//

		//
		else if (strcmp(aLine, "f") == 0)
		{
			fscanf(in, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &indexV0, &indexT0, &indexN0, &indexV1, &indexT1, &indexN1, &indexV2, &indexT2, &indexN2);
			verticesIndx.push_back(indexV0);
			verticesIndx.push_back(indexV1);
			verticesIndx.push_back(indexV2);

			texturesIndx.push_back(indexT0);
			texturesIndx.push_back(indexT1);
			texturesIndx.push_back(indexT2);

			normalsIndx.push_back(indexN0);
			normalsIndx.push_back(indexN1);
			normalsIndx.push_back(indexN2);
		}
		//

		//
		else //coments and wierd stuff
		{
			fgets(trash, 1000, in);
		}
	}

	for (unsigned i = 0; i < verticesIndx.size(); i++)
	{
		//fill the vertices, textures... with the repeated values to use drawarrays
		outvertices.push_back(tempvertices[verticesIndx[i] - 1]);
		outtextures.push_back(temptextures[texturesIndx[i] - 1]);
		outnormals.push_back(tempnormals[normalsIndx[i] - 1]);
	}

	fclose(in);
}