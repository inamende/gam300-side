#ifndef SPRITE_H
#define SPRITE_H

#include "../../ResourceManager/ResoManager.h"
#include "Parser.h"

class Sprite : public Resource
{
public:

	bool wireFrame;

	//handles
	GLuint vaoHandle;
	GLuint posBufferObj;
	GLuint texBufferObj;
	GLuint normBufferObj;
	GLuint tanBufferObj;
	GLuint colBufferObj;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> lines;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec4> colors;

	std::vector<glm::vec3> tangents;

	Sprite();
	virtual ~Sprite();

	virtual void LoadResource();
	virtual void InitSprite(const char * name);
	virtual void Draw();
	void FlipVertical();
	void FlipHorizontal();
	void Clear();
};


#endif