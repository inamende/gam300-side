#ifndef RENDERLAYER_H
#define RENDERLAYER_H

#include "../../Component/Entity.h"
#include "../../Gfx/Postprocessor.h"

class Camera;
class Renderable;

class RenderLayer : public Entity
{
public:
	std::list<Camera*> cameras;
	std::vector<Renderable*> renderables;
	Postprocessor postprocess;

	RenderLayer();
	RenderLayer(std::string & layerName);
	~RenderLayer();
	virtual void Render();

	void AddRenderable(Renderable * rend);
	void RemoveRenderable(Renderable * rend);
	void SortRenderables();
	void AddCamera(Camera * cam);
	void RemoveCamera(Camera * cam);
	bool IsInsideFrustum(Camera*, Renderable*);
};



#endif