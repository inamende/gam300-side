#include "Texture.h"

#include <SDL2/SDL_image.h>

Texture::Texture() : Resource::Resource(), surface(NULL), width(0.f), height(0.f) {}

Texture::~Texture()
{
	if (handle)
	{
		glBindTexture(GL_TEXTURE_2D, handle);
		glDeleteTextures(0, &handle);
		glDeleteTextures(1, &handle);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Texture::LoadResource()
{
	InitTex(Texture::filePath);
}

void Texture::InitTex(const std::string & filename)
{
	GLuint handle;
	surface = IMG_Load(filename.c_str());

	if (surface == NULL)
	{
		surface = IMG_Load("./Data/Texture/color_map.png"); //load a default texture

		//display a wrning
		std::string message = "Texture < " + filename + " > couldn't be loaded or does not exist";
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Texture", message.c_str(), NULL);
	}

	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);

	int pixelFormat = GL_RGB;

	if (surface->format->BytesPerPixel == 4)
		pixelFormat = GL_RGBA;

	float color[] = { 1.0f , 0.0f , 1.0f , 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);

	glTexImage2D(GL_TEXTURE_2D, 0, pixelFormat, surface->w, surface->h, 0, pixelFormat, GL_UNSIGNED_BYTE, surface->pixels);

	

	//filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	width = surface->w; height = surface->h;

	SDL_FreeSurface(surface);

	textureName = filename;
	texHandle = handle;
}