#ifndef ANIMATION_DATA_H
#define ANIMATION_DATA_H

#include "MetaData.h"

class Texture;

class AnimationData : public MetaData
{
public:
	AnimationData();
	AnimationData(std::string name, unsigned r, unsigned c);

	virtual void CoreLoad(Json::Value & val);
	virtual void CoreSave(Json::Value & val);

	unsigned GetRow();
	unsigned GetCol();

	std::vector<float> delays;
	std::vector<int> grid;

	Texture * texture;
	float speed;
	bool ping_pong;

private:
	Json::Value val;
	unsigned row;
	unsigned col;
};

#endif