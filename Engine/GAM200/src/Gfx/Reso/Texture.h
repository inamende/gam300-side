#ifndef TEXTURES_H
#define TEXTURES_H

#include "../../ResourceManager/ResoManager.h"

class SDL_Surface;

class Texture : public Resource
{
public:
	Texture();
	virtual ~Texture();

	virtual void LoadResource();
	void Texture::InitTex(const std::string & filename);

	GLuint texHandle;
	std::string textureName;
	SDL_Surface * surface;
	float width;
	float height;
};



#endif