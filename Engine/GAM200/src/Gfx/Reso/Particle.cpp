#include "Particle.h"
#include "ImGui/imgui.h"

#include "../Gfx.h"
#include "../../Gfx/Reso/Camera.h"
#include "Shader.h"
#include "Texture.h"
#include "../Comp/Transform.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Editor/ImGuiEditor.h"
#include "../../Utils/Utilities.h"
#include "Parser.h"

ParticleEmiter::ParticleEmiter() :
	Renderable::Renderable(),
	maxParticles(1000),
	particleCount(0.f),
	posData(NULL),
	colData(NULL),
	particleRate(100),
	paricleLifeTime(10.f),
	particleLimit(1000),
	particleAlphaReducionRate(1),
	particleSize(30),
	useRandomSize(false),
	useRandomColor(true),
	efects(dust),
	spreadfactor(40.f)
{}

ParticleEmiter::~ParticleEmiter() {}

void ParticleEmiter::Initialize()
{

	shader = resoManager->getReso<Shader>("particle");

	LoadDataFromObj("./Data/Meshes/plane.obj", vertices, std::vector<glm::vec2>(), std::vector<glm::vec3>());

	posData = DBG_NEW float [maxParticles * 4];
	colData = DBG_NEW unsigned char[maxParticles * 4];

	for (int i = 0; i < maxParticles; i++)
	{
		particleContainer[i].lifeTime = -1.0f;
	}

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &pBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticles * 4 * sizeof(float), NULL, GL_STREAM_DRAW);

	glGenBuffers(1, &cBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, cBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticles * 4 * sizeof(unsigned char), NULL, GL_STREAM_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	Renderable::Initialize();
	handle = vao;
}

void ParticleEmiter::Update() {}

void ParticleEmiter::Terminate()
{
	delete[] posData;
	delete[] colData;
	Renderable::Terminate();
}

void ParticleEmiter::Render()
{
	graphics->DrawBox(vec3(GetOwner()->mTransform->position.x, GetOwner()->mTransform->position.y, 400.f),100,100 );

	if (!active) {
		return;
	}

	int newParticles = particleRate;
	if (newParticles > 1000)
		newParticles = 1000;

	for (int i = 0; i < newParticles; i++)
	{
		int particleIndex = FindUnusedParticle();
		particleContainer[particleIndex].lifeTime = paricleLifeTime;

		switch (efects) {
		case def:		SpreadFroimCenter(particleIndex, spreadfactor); break;
		case dust:		Dust(particleIndex, spreadfactor);				break;
		case spooky:	Spooky(particleIndex, spreadfactor);			break;
		default:		Dust(particleIndex, spreadfactor);				break;
		}
		
		if (useRandomColor)
		{
			particleContainer[particleIndex].r = rand() % 256;
			particleContainer[particleIndex].g = rand() % 256;
			particleContainer[particleIndex].b = rand() % 256;
			particleContainer[particleIndex].a = 0;
		}
		else
		{
			particleContainer[particleIndex].r = 255.f * color.x;
			particleContainer[particleIndex].g = 255.f * color.y;
			particleContainer[particleIndex].b = 255.f * color.z;
			particleContainer[particleIndex].a = 255.f;
		}
	
		if (!useRandomSize)
			particleContainer[particleIndex].size = particleSize;
		else
			particleContainer[particleIndex].size = (10 + rand() % ((20 + 1) - 10));
	}

	particleCount = 0;
	for (int i = 0; i < particleLimit; i++) {

		Particle& p = particleContainer[i];

		if (p.lifeTime > 0.0f) {

			p.lifeTime -= ImGui::GetIO().DeltaTime;

			if (p.lifeTime > 0.0f) {

				p.speed += glm::vec3(0.0f, -9.81f, 0.0f) * (float)ImGui::GetIO().DeltaTime;
				p.position += p.speed * (float)ImGui::GetIO().DeltaTime;

				//if (p.a < 1)
				//	p.a += particleAlphaReducionRate / 10.f;
				//else
					p.a -= particleAlphaReducionRate / 10.f;

				posData[4 * particleCount + 0] = p.position.x;
				posData[4 * particleCount + 1] = p.position.y;
				posData[4 * particleCount + 2] = p.position.z;

				posData[4 * particleCount + 3] = p.size;

				colData[4 * particleCount + 0] = p.r;
				colData[4 * particleCount + 1] = p.g;
				colData[4 * particleCount + 2] = p.b;
				colData[4 * particleCount + 3] = p.a;
			}
			particleCount++;
		}
	}

	SortParticles();

	glBindVertexArray(handle);

	glBindBuffer(GL_ARRAY_BUFFER, pBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticles * 4 * sizeof(float), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * 4 * sizeof(float), posData);

	glBindBuffer(GL_ARRAY_BUFFER, cBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticles * 4 * sizeof(unsigned char), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * 4 * sizeof(unsigned char), colData);


	if (shader)
	{
		if (texture != NULL)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture->texHandle);
			shader->setUniform("textureData", 0);
		}
		else
			glBindTexture(GL_TEXTURE_2D, 0);
	}

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, pBuffer);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, cBuffer);
	glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, 0);

	glVertexAttribDivisor(0, 0);
	glVertexAttribDivisor(1, 1);
	glVertexAttribDivisor(2, 1);

	glBindVertexArray(handle);

	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 6, particleCount);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void ParticleEmiter::load(Json::Value & val)
{
	texture = resoManager->getReso<Texture>(val["ParticleEmiter"].get("texture", "Set_Texture_Please").asString());

	active = val["ParticleEmiter"].get("active", true).asBool();

	particleRate = val["ParticleEmiter"].get("particleRate", 100).asInt();
	paricleLifeTime = val["ParticleEmiter"].get("paricleLifeTime", 10.f).asFloat();
	particleAlphaReducionRate = val["ParticleEmiter"].get("particleAlphaReducionRate", 1.f).asFloat();
	particleSize = val["ParticleEmiter"].get("particleSize", 50.f).asFloat();

	useRandomSize = val["ParticleEmiter"].get("useRandomSize", false).asBool();
	useRandomColor = val["ParticleEmiter"].get("useRandomColor", true).asBool();

	efects = (Effect)(val["ParticleEmiter"].get("efect", 1).asInt());

	spreadfactor = val["ParticleEmiter"].get("spread", 40.f).asFloat();

	color.r = val["ParticleEmiter"]["color"].get("r", 1.f).asFloat();
	color.g = val["ParticleEmiter"]["color"].get("g", 1.f).asFloat();
	color.b = val["ParticleEmiter"]["color"].get("b", 1.f).asFloat();
	color.a = val["ParticleEmiter"]["color"].get("a", 1.f).asFloat();
}

void ParticleEmiter::save(Json::Value & val)
{
	val["ParticleEmiter"]["active"] = active;

	if (texture) val["ParticleEmiter"]["texture"] = resoManager->getResoName(texture);

	val["ParticleEmiter"]["particleRate"] = particleRate;
	val["ParticleEmiter"]["paricleLifeTime"] = paricleLifeTime;
	val["ParticleEmiter"]["particleAlphaReducionRate"] = particleAlphaReducionRate;
	val["ParticleEmiter"]["particleSize"] = particleSize;

	val["ParticleEmiter"]["useRandomSize"] = useRandomSize;
	val["ParticleEmiter"]["useRandomColor"] = useRandomColor;

	val["ParticleEmiter"]["efect"] = (int)efects;

	val["ParticleEmiter"]["spread"] = spreadfactor;

	val["ParticleEmiter"]["color"]["r"] = color.r;
	val["ParticleEmiter"]["color"]["g"] = color.g;
	val["ParticleEmiter"]["color"]["b"] = color.b;
	val["ParticleEmiter"]["color"]["a"] = color.a;
}

void ParticleEmiter::WriteToImguiWindow()
{
	static int selected_texture = 0;

	if (ImGui::CollapsingHeader("ParticleEmiter"))
	{
		ImGui::Checkbox("active##ParticleEmiter", &active);

		ImGui::InputInt("Particle Number##ParticleEmiter", &particleRate);
		ImGui::SliderFloat("Particle Life Time##ParticleEmiter", &paricleLifeTime, 0, 10);
		ImGui::SliderFloat("Particle Alpha Reduction##ParticleEmiter", &particleAlphaReducionRate, 0, 100);
		ImGui::SliderFloat("Particle Size##ParticleEmiter", &particleSize, 0, 1000);
		ImGui::SliderFloat("Spread##ParticleEmiter", &spreadfactor, 0 ,500);
		ImGui::NewLine();
		ImGui::Checkbox("Random Size##ParticleEmiter", &useRandomSize);
		ImGui::Checkbox("Random Color##ParticleEmiter", &useRandomColor);
		
		if (ImGui::Button("Texture##ParticleEmiter"))
			ImGui::OpenPopup("textures##ParticleEmiter");

		//ImGui::InputInt("Max Particles##ParticleEmiter", &particleLimit, 0, 1000);

		ImGuiEditor::ColorEdit4("Color##ParticleEmiter", color);
		static int i = (int)efects;
		if (ImGui::RadioButton("def", &i, 0)) {
			efects = (Effect)i;
		}
		if (ImGui::RadioButton("dust", &i, 1)) {
			efects = (Effect)i;
		}
		if (ImGui::RadioButton("spooky", &i, 2)) {
			efects = (Effect)i;
		}

		ImGui::NewLine();
		if (ImGui::Button("Detach##ParticleEmiter"))
			GetOwner()->detach(this);
	}

	if (ImGui::BeginPopup("textures##ParticleEmiter"))
	{
		ImGui::Text("Textures");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->textureNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->textureNames[i].c_str()))
			{
				selected_texture = i;
				texture = resoManager->getReso<Texture>(imGuiEditor->textureNames[selected_texture]);
				SetMetaData();
				ChangeTextureUvs(atlasIndex);
			}

		ImGui::EndPopup();
	}

}

int ParticleEmiter::FindUnusedParticle()
{
	static int lastUsed = 0;

	for (int i = lastUsed; i < maxParticles; i++)
	{
		if (particleContainer[i].lifeTime < 0)
		{
			lastUsed = i;
			return i;
		}
	}

	for (int i = 0; i < lastUsed; i++)
	{
		if (particleContainer[i].lifeTime < 0)
		{
			lastUsed = i;
			return i;
		}
	}
	return 0;
}

void ParticleEmiter::SortParticles()
{
	std::sort(&particleContainer[0], &particleContainer[1000],
		[](Particle first, Particle second)
		{
			return (first.position.z) < (second.position.z);
		}
	);
}

void ParticleEmiter::SpreadFroimCenter(int index, float spreadFactor)
{
	particleContainer[index].position = GetOwner()->mTransform->position;
	float spread = spreadFactor;
	glm::vec3 maindir = glm::vec3(0.0f, 4.0f, 0.0f);
	glm::vec3 randomdir = glm::vec3(
		(rand() % 2000 - 1000.0f) / 1000.0f,
		(rand() % 2000 - 1000.0f) / 1000.0f,
		0.f
	);
	particleContainer[index].speed = maindir + randomdir*spread;
}

void ParticleEmiter::Dust(int index, float spreadFactor) {
	particleContainer[index].position = glm::vec3(
		GetOwner()->mTransform->position.x + (10 + rand() % ((22000 + 1) - 10)),
		GetOwner()->mTransform->position.y + (10 + rand() % ((4000 + 1) - 10)),
		GetOwner()->mTransform->position.z
	);

	float spread = spreadFactor;
	glm::vec3 maindir = glm::vec3(0.0f, 4.0f, 0.0f);
	glm::vec3 randomdir = glm::vec3(
		(rand() % 2000 - 1000.0f) / 1000.0f,
		(rand() % 2000 - 1000.0f) / 1000.0f,
		0.f
	);
	particleContainer[index].speed = maindir + randomdir*spread;
}

void ParticleEmiter::Spooky(int index, float spreadFactor) {
	particleContainer[index].position = glm::vec3(
		GetOwner()->mTransform->position.x + RandomFloat(-100, 100),
		GetOwner()->mTransform->position.y + RandomFloat(-100, 100),
		GetOwner()->mTransform->position.z
	);

	float spread = spreadFactor;
	glm::vec3 maindir = glm::vec3(0.0f, 4.0f, 0.0f);
	glm::vec3 randomdir = glm::vec3(
		(rand() % 2000 - 1000.0f) / 1000.0f,
		(rand() % 2000 - 1000.0f) / 1000.0f,
		0.f
	);
	particleContainer[index].speed = maindir + randomdir * spread;
}