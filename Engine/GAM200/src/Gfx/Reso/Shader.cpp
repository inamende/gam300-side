#include "Shader.h"

#include <SDL2/SDL.h>

Shader::Shader() : Resource::Resource()
{
	shaderHandle = 0;
}

void Shader::LoadResource()
{
	compileFromFile(filePath + ".vert", VERTEX);
	compileFromFile(filePath + ".frag", FRAGMENT);
	link();
}

Shader::~Shader()
{
	if (shaderHandle >= 0)
		glDeleteProgram(shaderHandle);
}

bool Shader::compileFromFile(std::string & file, ShaderType type)
{
	std::string shaderContent;

	std::ifstream shaderFile(file, std::ios::in); //allow input operations (std::ios::in is added by default but adding it seems to be a good practice)

	if (!shaderFile)
	{
		//display a wrning
		std::string message = "Shader < " + file + " > couldn't be loaded or does not exist";
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Shader", message.c_str(), NULL);
		return false;
	}

	std::string line;

	while (!shaderFile.eof())
	{
		std::getline(shaderFile, line);
		shaderContent.append(line + "\n");
	}

	shaderFile.close();
	return compileFromString(shaderContent, type);
}

bool Shader::compileFromString(std::string & string, ShaderType type)
{
	GLuint handle;

	if (type == VERTEX)
	{
		handle = glCreateShader(GL_VERTEX_SHADER);
	}
	else if (type == FRAGMENT)
	{
		handle = glCreateShader(GL_FRAGMENT_SHADER);
	}
	else
	{
		//std::cout << "COULD NOT introduce that shader type" << std::endl;
		return false;
	}

	if (shaderHandle <= 0)
	{
		shaderHandle = glCreateProgram();
		if (shaderHandle == 0)
		{
			//std::cout << "COULD NOT CREATE SHADER PROGRAM" << std::endl;
			return false;
		}
	}

	const char * cnstString = string.c_str();
	glShaderSource(handle, 1, &cnstString, NULL);

	glCompileShader(handle);

	//CHECK ERRORS IN SHADER
	int result;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &result);
	if (GL_FALSE == result)
	{
		// Compile failed, store log and return false
		int length = 0;
		log_string_ = "";
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &length);
		if (length > 0)
		{
			char * c_log = new char[length];
			int written = 0;
			glGetShaderInfoLog(handle, length, &written, c_log);
			log_string_ = c_log;
			delete[] c_log; //<-BREAKPOINT 
		}

		return false;
	}
	else
	{
		glAttachShader(shaderHandle, handle);
		glDeleteShader(handle);
		return true;
	}
}

bool Shader::link()
{
	if (shaderHandle <= 0)
		return false;

	glLinkProgram(shaderHandle);
	return true;
}

void Shader::use()
{
	if (shaderHandle <= 0)
		return;

	glUseProgram(shaderHandle);
}

GLuint Shader::getHandle()
{
	return shaderHandle;
}

void Shader::setUniform(const char *name, float x, float y, float z) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniform3f(unifLocation, x, y, z);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}

void Shader::setUniform(const char *name, const glm::vec3 & v) const
{
	setUniform(name, v.x, v.y, v.z);
}

void Shader::setUniform(const char *name, const glm::vec4 & v) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniform4f(unifLocation, v.x, v.y, v.z, v.w);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}

void Shader::setUniform(const char *name, const glm::mat4 & m) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniformMatrix4fv(unifLocation, 1, GL_FALSE, &m[0][0]);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}

void Shader::setUniform(const char *name, const glm::mat3 & m) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniformMatrix3fv(unifLocation, 1, GL_FALSE, &m[0][0]);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}

void Shader::setUniform(const char *name, float val) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniform1f(unifLocation, val);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}

void Shader::setUniform(const char *name, int val) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniform1i(unifLocation, val);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}

void Shader::setUniform(const char *name, bool val) const
{
	GLuint unifLocation = glGetUniformLocation(shaderHandle, name);

	if (unifLocation >= 0)
	{
		glUniform1i(unifLocation, val);
	}
	else
	{
		//std::cout << "COULD NOT FIND UNIFORM : " << name << std::endl;
	}
}
