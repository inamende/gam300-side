#include "Character.h"

Character::Character()
{
	textureHandle = 0;
	size = glm::ivec2();
	bearing = glm::ivec2();
	advance = 0;
}

Character::Character(GLuint tex, glm::ivec2 s, glm::ivec2 b, GLuint a)
{
	textureHandle = tex; size = s; bearing = b; advance = a;
}