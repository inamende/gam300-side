#include "SoundFile.h"
#include "../../Audio/Audio.h"
SoundFile::SoundFile() : Resource::Resource()
{

}
SoundFile::~SoundFile()
{
	//free sound
	gAudioMgr.FreeSound(mSoundPointer);
}
//get the sound file from the path
void SoundFile::LoadResource()
{
	//mTotalSoundMap.insert(std::make_pair("Kalimba", gAudioMgr.CreateSound("./Data/Sounds/Kalimba.mp3")));
	mSoundPointer = gAudioMgr.CreateSound(Resource::filePath.c_str());
}
Sound * SoundFile::ReturnSoundPointer()
{
	return mSoundPointer;
}