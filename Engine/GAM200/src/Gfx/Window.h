#ifndef WINDOW_H
#define WINDOW_H

#include "../EngineCommon.h"
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

class Window
{
private:

	static Window * instance;
	Window();

public:

	virtual ~Window();

	const char * name;
	int prevWidth;
	int prevHeight;
	int width;
	int height;
	int screenWidth;
	int screenHeight;
	SDL_Window * windowHandle;
	SDL_GLContext context_;
	glm::vec4 frameBufferColor;
	glm::vec2 camChange;
	std::vector<glm::vec2> resolutions;
	unsigned currentResolution;
	bool fullScreen;
	bool quit;
	bool v_sync_active = true;
	unsigned window_state = 0;
	unsigned prev_window_state = 1;
	
	static Window * getInstance();
	Window(const char * name, int width, int height);

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	void Load();
	void Save();
	void WriteToImguiWindow();
	void FullScreen(bool full);
	bool GetFullScreen();
	void SetResolution();
	void UpdateResolution();
	glm::vec2 GetCurrentResolution();
	void v_sync(bool);
	void v_sync_toggle();
	bool window_is_missing();
	void set_ico(const char*);
};

#define mWindow Window::getInstance()

#endif