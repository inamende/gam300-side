#ifndef FONT_RENDERABLE_H
#define FONT_RENDERABLE_H

#include "Renderable.h"

class Font;

class FontRenderable : public Renderable
{
public:

	Font * font;
	std::string displayText;
	glm::vec3 color;
	float size;
	vec2 offset;

	FontRenderable();

	virtual void Initialize();
	virtual void Update();
	virtual void Render();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

};



#endif