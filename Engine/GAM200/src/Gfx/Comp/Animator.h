#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "Renderable.h"

class AnimationData;

class Animator : public Renderable
{
public:
	Animator();
	virtual ~Animator();

	virtual void Initialize();
	virtual void Update();
	void Render();

	void Play();
	void Pause();
	void Reset();
	void Loop(bool enable);
	void SetNewAnimationData(AnimationData * newData);
	void SetAnimationFrame(unsigned frameIndex);

	void ChangeTextureUvs(int ind, unsigned width, unsigned height);
	void ChangeTextureUvs(float x, float y, unsigned width, unsigned height);

	void ChangeAnimation(std::string name);
	void ChangeAnimationData();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	AnimationData * animData;
	float timer;
	unsigned currentFrame;
	float speed;
	bool loop;
	bool playing;
	bool finished;
	std::vector<float> delays;
	std::vector<int> frames;
	bool ping_pong;

private:
	glm::mat4 T, S;
	std::string animationName;
};


#endif