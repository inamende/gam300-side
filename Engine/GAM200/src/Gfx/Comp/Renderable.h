#ifndef RENDERABLE_H
#define RENDERABLE_H

#include "../../Component/Component.h"

class Shader;
class Transform;
class Texture;
class Sprite;
class AtlasData;

class Renderable : public Component
{
public:
	Renderable();
	virtual ~Renderable();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void Render();

	void ChangeTextureUvs(std::string ind);
	void SetMetaData();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void ApplyTextureScale();

	std::string layer;
	Shader * shader;
	Transform * renderableTransform;
	Texture * texture;
	Sprite * sprite;
	glm::vec4 color;
	bool visible;
	AtlasData * metaData;
	glm::mat4 T, S;
	std::string atlasIndex;
};


#endif