#ifndef LINE_RENDERABLE_H
#define LINE_RENDERABLE_H

#include "../../Gfx/Comp/Renderable.h"

class Line;

class LineRenderable : public Renderable
{
public:

	Line * line;

	LineRenderable();

	virtual void Initialize();
	virtual void Update();
	virtual void Render();
	virtual void Terminate();
};



#endif