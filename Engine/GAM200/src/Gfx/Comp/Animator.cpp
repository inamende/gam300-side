#include "Animator.h"

#include "../Gfx.h"
#include "../Comp/Light.h"
#include "../Reso/AnimationData.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Editor/ImGuiEditor.h"
#include "../../Gfx/Comp/Transform.h"
#include "../../Gfx/Reso/Shader.h"
#include "../../Gfx/Reso/Sprite.h"
#include "../../Gfx/Reso/Texture.h"
#include <glm/gtc/matrix_transform.hpp>

#define SPEED_REDUCTION 0.07f

Animator::Animator() : Renderable(), animData(NULL), timer(0.0f), currentFrame(0), speed(1.0f), loop(false), playing(false), finished(false), ping_pong(false) {}

Animator::~Animator() { graphics->RemoveRenderable(this); }

void Animator::Initialize()
{
	if (animData != NULL)
	{
		ping_pong = animData->ping_pong;
		speed = animData->speed;
		texture = animData->texture;
		delays = animData->delays;
		frames = animData->grid;
		ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
	}

	Renderable::renderableTransform = GetOwner()->mTransform;
	graphics->AddRenderable(this);
}

void Animator::Update()
{
	if (!animData) {
		return;
	}

	finished = false;

	if (!playing)
		return;

	timer += ImGui::GetIO().DeltaTime * speed * SPEED_REDUCTION;

	if (!ping_pong) {
		if (timer < animData->delays[currentFrame])
			return;

		else if (timer >= animData->delays[currentFrame])
		{
			timer -= animData->delays[currentFrame];
			if ((currentFrame == animData->grid.size() - 1) && loop)
			{
				currentFrame = 0;
				finished = true;
				ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
			}
			else if ((currentFrame == animData->grid.size() - 1) && !loop)
			{
				finished = true;
				playing = false;
			}
			else if (!finished)
			{
				currentFrame++;
				ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
			}
		}
		return;
	}

	// ping pong
	static int d = 1;
	if (timer < animData->delays[currentFrame])
		return;

	else if (timer >= animData->delays[currentFrame])
	{
		timer -= animData->delays[currentFrame];
		if ((currentFrame == 0) && loop && d < 0)
		{
			d = 1;
			currentFrame = 1;
			finished = true;
			ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
		}
		else if ((currentFrame == 0) && !loop && d < 0)
		{
			d = 1;
			finished = true;
			playing = false;
		}
		else if (!finished)
		{
			if ((currentFrame == animData->grid.size() - 1)) { // pong
				d = -1;
			}
			currentFrame += d;
			ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
		}
	}
}

void Animator::Render()
{
	glm::mat4 M = renderableTransform->GetTransform();

	if (shader)
	{
		shader->setUniform("M", M);
		shader->setUniform("Color", color);

		shader->setUniform("T", T);
		shader->setUniform("S", S);

		// light stuff
		shader->setUniform("lightNum", (int)graphics->lightNum);
		for (unsigned i = 0; i < Gfx::getInstance()->lightNum; i++) {
			shader->setUniform(("light[" + std::to_string(i) + "].ambient").c_str(), graphics->lights[i]->ambient);
			shader->setUniform(("light[" + std::to_string(i) + "].diffuse").c_str(), graphics->lights[i]->diffuse);
			shader->setUniform(("light[" + std::to_string(i) + "].specular").c_str(), graphics->lights[i]->specular);
			shader->setUniform(("light[" + std::to_string(i) + "].lightPosition").c_str(), graphics->lights[i]->GetOwner()->mTransform->position);
			shader->setUniform(("light[" + std::to_string(i) + "].spotDirection").c_str(), graphics->lights[i]->spotDirection);
			shader->setUniform(("light[" + std::to_string(i) + "].spotCosPhi").c_str(), graphics->lights[i]->spotCosPhi);
			shader->setUniform(("light[" + std::to_string(i) + "].spotCosTheta").c_str(), graphics->lights[i]->spotCosTheta);
			shader->setUniform(("light[" + std::to_string(i) + "].lightColor").c_str(), graphics->lights[i]->color);
		}
		shader->setUniform("ambient", graphics->global_ambient_lighting);

		if (texture != NULL)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture->texHandle);
			shader->setUniform("textureData", 0);
		}
		else
			glBindTexture(GL_TEXTURE_2D, 0); //no texture, just color (this has no effect, use another shader please)

		if (visible)
			if (sprite)
				sprite->Draw();
	}
}

void Animator::Play() { playing = true; }
void Animator::Pause() { playing = false; }
void Animator::Reset() { SetAnimationFrame(0); }
void Animator::Loop(bool enable) { loop = enable; }
void Animator::SetNewAnimationData(AnimationData * newData)
{
	animData = newData;
	Reset();
}


void Animator::SetAnimationFrame(unsigned frameIndex)
{
	if (animData == NULL)
		return;

	if (frameIndex < animData->GetCol() * animData->GetRow())
	{
		currentFrame = frameIndex;
		ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
		timer = 0.0f;
	}
}

void Animator::ChangeTextureUvs(int ind, unsigned width, unsigned height)
{
	ChangeTextureUvs((ind % width), (ind / width), width, height);
}

void Animator::ChangeTextureUvs(float x, float y, unsigned width, unsigned height)
{
	glm::vec2 translation(x * 1 / width, y * 1 / height);

	T = glm::translate(glm::mat4(1.f), glm::vec3(translation, 1.0f));
	S = glm::scale(glm::mat4(1.f), glm::vec3(1.f / animData->GetCol(), 1.f / animData->GetRow(), 1.0f));
}

void Animator::ChangeAnimation(std::string name)
{
	AnimationData * anim = graphics->FindAnimation(name);

	if (anim == NULL)
		return;

	SetNewAnimationData(anim);

	ping_pong = animData->ping_pong;
	speed = animData->speed;
	texture = animData->texture;
	delays = animData->delays;
	frames = animData->grid;
	SetAnimationFrame(0);
	ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
}

void Animator::ChangeAnimationData()
{
	if (animData == NULL)
		return;

	animData->ping_pong = ping_pong;
	animData->speed = speed;
	animData->delays = delays;
	animData->grid = frames;
	animData->Writer();
}

void Animator::load(Json::Value & val)
{
	texture = (resoManager->getReso<Texture>(val["Animator"].get("texture", "Set_Texture_Please").asString()) != NULL) ?
		resoManager->getReso<Texture>(val["Animator"].get("texture", "Set_Texture_Please").asString()) :
		graphics->GetDefaultTexture();

	sprite = (resoManager->getReso<Sprite>(val["Animator"].get("model", "Set_Model_Please").asString()) != NULL) ?
		resoManager->getReso<Sprite>(val["Animator"].get("model", "Set_Model_Please").asString()) :
		graphics->GetDefaultSprite();

	animData = resoManager->getReso<AnimationData>(val["Animator"].get("data", "Set_Data_Please").asString());

	shader = (resoManager->getReso<Shader>(val["Animator"].get("shader", "Set_Shader_Please").asString()) != NULL) ?
		resoManager->getReso<Shader>(val["Animator"].get("shader", "Set_Shader_Please").asString()) :
		graphics->GetDefaultShader();

	color.r = val["Animator"]["color"].get("r", 0.0f).asFloat();
	color.g = val["Animator"]["color"].get("g", 0.0f).asFloat();
	color.b = val["Animator"]["color"].get("b", 0.0f).asFloat();
	color.a = val["Animator"]["color"].get("a", 1.0f).asFloat();
	visible = val["Animator"].get("visible", true).asBool();
	layer = val["Animator"].get("layer", "Set_Layer_Please").asString();

	speed = val["Animator"].get("speed", 1.0f).asFloat();
	loop = val["Animator"].get("loop", true).asBool();
	playing = val["Animator"].get("playing", true).asBool();

	ping_pong = val["Animator"].get("ping_pong", false).asBool();
}

void Animator::save(Json::Value & val)
{
	if (sprite)  val["Animator"]["model"] = resoManager->getResoName(sprite);
	if (texture) val["Animator"]["texture"] = resoManager->getResoName(texture);
	if (animData) val["Animator"]["data"] = resoManager->getResoName(animData);
	if (shader)  val["Animator"]["shader"] = resoManager->getResoName(shader);

	val["Animator"]["color"]["r"] = color.r;
	val["Animator"]["color"]["g"] = color.g;
	val["Animator"]["color"]["b"] = color.b;
	val["Animator"]["color"]["a"] = color.a;
	val["Animator"]["visible"] = visible;
	val["Animator"]["layer"] = layer;

	val["Animator"]["speed"] = speed;
	val["Animator"]["loop"] = loop;
	val["Animator"]["playing"] = playing;

	val["Animator"]["ping_pong"] = ping_pong;
}

void Animator::WriteToImguiWindow()
{
	static int selected_texture = 0;
	static int selected_sprite = 0;
	static int selected_shader = 0;
	static int selected_layer = 0;
	static int selected_animation = 0;

	if (ImGui::CollapsingHeader("Animator"))
	{
		if (ImGui::Button("Animation##Animator"))
			ImGui::OpenPopup("animations");

		ImGui::SameLine();
		if (animData == NULL) ImGui::Text("NULL"); else ImGui::Text(resoManager->getResoName(animData).c_str());

		if (ImGui::Button("Shader##Animator"))
			ImGui::OpenPopup("shaders");

		ImGui::SameLine();
		if (shader) ImGui::Text(resoManager->getResoName(shader).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Sprite##Animator"))
			ImGui::OpenPopup("sprites");

		ImGui::SameLine();
		if (sprite) ImGui::Text(resoManager->getResoName(sprite).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Layer##Animator"))
			ImGui::OpenPopup("layers");

		ImGui::SameLine();
		if (layer != "") ImGui::Text(layer.c_str());
		else ImGui::Text("NULL");

		ImGuiEditor::ColorEdit4("Color##Animator", color);

		ImGui::Checkbox("visible##Animator", &visible);

		ImGui::Checkbox("ping pong##Animator", &ping_pong);

		ImGui::SliderFloat("speed##Animator", &speed, 0.0f, 100.0f);

		int curr = currentFrame;

		ImGui::InputFloat("timmer##Animator", &timer);
		if (ImGui::InputInt("current frame##Animator", &curr))
			SetAnimationFrame(curr);
		ImGui::Checkbox("loop##Animator", &loop);
		ImGui::Checkbox("playing##Animator", &playing);
		ImGui::Checkbox("finished##Animator", &finished);

		if (animData)
		{
			ImGui::NewLine();

			for (unsigned i = 0; i < frames.size(); i++)
			{
				std::string frameId = "Frame " + std::to_string(i);
				int f = frames[i];
				if (ImGui::InputInt(frameId.c_str(), &f))
				{
					frames[i] = f;
				}
			}

			ImGui::NewLine();

			for (unsigned i = 0; i < delays.size(); i++)
			{
				std::string delayId = "Delay " + std::to_string(i);
				float d = delays[i];
				if (ImGui::InputFloat(delayId.c_str(), &d))
				{
					delays[i] = d;
				}
			}
		}

		ImGui::NewLine();
		if (ImGui::Button("Save To Animation File##Animator"))
			ChangeAnimationData();

		ImGui::NewLine();
		if (ImGui::Button("Detach##Animator"))
			GetOwner()->detach(this);

	}

	if (ImGui::BeginPopup("shaders"))
	{
		ImGui::Text("Shaders");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->shaderNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->shaderNames[i].c_str()))
			{
				selected_shader = i;
				shader = resoManager->getReso<Shader>(imGuiEditor->shaderNames[selected_shader]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("sprites"))
	{
		ImGui::Text("Sprites");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->spriteNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->spriteNames[i].c_str()))
			{
				selected_sprite = i;
				sprite = resoManager->getReso<Sprite>(imGuiEditor->spriteNames[selected_sprite]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("layers"))
	{
		ImGui::Text("Layers");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->layerNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->layerNames[i].c_str()))
			{
				Renderable * temp = this;

				graphics->RemoveRenderable(this);

				selected_layer = i;
				layer = imGuiEditor->layerNames[selected_layer];

				graphics->AddRenderable(temp);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("animations"))
	{
		ImGui::Text("Animations");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->animationNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->animationNames[i].c_str()))
			{
				selected_animation = i;

				animData = resoManager->getReso<AnimationData>(imGuiEditor->animationNames[selected_animation]);

				ping_pong = animData->ping_pong;
				speed = animData->speed;
				texture = animData->texture;
				delays = animData->delays;
				frames = animData->grid;
				currentFrame = 0;
				ChangeTextureUvs(animData->grid[currentFrame], animData->GetCol(), animData->GetRow());
			}

		ImGui::EndPopup();
	}
}

