#include "Renderable.h"

#include "../Gfx.h"
#include "Light.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Input/Input.h"
#include "../Comp/Transform.h" 
#include "../Reso/Shader.h" 
#include "../Reso/Texture.h"
#include "../Reso/Sprite.h"
#include "../Reso/AtlasData.h"
#include "../../Editor/ImGuiEditor.h"
#include <glm/gtc/matrix_transform.hpp>

Renderable::Renderable() : layer("default"), shader(NULL), renderableTransform(NULL), texture(NULL), sprite(NULL), visible(true), metaData(NULL), atlasIndex("")
{
	color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	T = glm::mat4(1.f);
	S = glm::mat4(1.f);
}

Renderable::~Renderable() {}
void Renderable::Initialize()
{
	SetMetaData();

	if (GetOwner())
		renderableTransform = GetOwner()->GetComponent<Transform>();

	graphics->AddRenderable(this);
}

void Renderable::Update() {}

void Renderable::Terminate()
{
	graphics->RemoveRenderable(this);
}

void Renderable::Render()
{
	glm::mat4 M = renderableTransform->GetTransform();

	if (shader)
	{
		shader->setUniform("M", M);
		shader->setUniform("Color", color);

		shader->setUniform("T", T);
		shader->setUniform("S", S);

		// light stuff
		shader->setUniform("lightNum", (int)graphics->lightNum);
		for (unsigned i = 0; i < Gfx::getInstance()->lightNum; i++) {
			shader->setUniform(("light[" + std::to_string(i) + "].ambient").c_str(), graphics->lights[i]->ambient);
			shader->setUniform(("light[" + std::to_string(i) + "].diffuse").c_str(), graphics->lights[i]->diffuse);
			shader->setUniform(("light[" + std::to_string(i) + "].specular").c_str(), graphics->lights[i]->specular);
			shader->setUniform(("light[" + std::to_string(i) + "].lightPosition").c_str(), graphics->lights[i]->GetOwner()->mTransform->position);
			shader->setUniform(("light[" + std::to_string(i) + "].spotDirection").c_str(), graphics->lights[i]->spotDirection);
			shader->setUniform(("light[" + std::to_string(i) + "].spotCosPhi").c_str(), graphics->lights[i]->spotCosPhi);
			shader->setUniform(("light[" + std::to_string(i) + "].spotCosTheta").c_str(), graphics->lights[i]->spotCosTheta);
			shader->setUniform(("light[" + std::to_string(i) + "].lightColor").c_str(), graphics->lights[i]->color);
		}
		shader->setUniform("ambient", graphics->global_ambient_lighting);

		if (texture != NULL)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture->texHandle);
			shader->setUniform("textureData", 0);
		}
		else
			glBindTexture(GL_TEXTURE_2D, 0);


		if (visible)
			if (sprite)
				sprite->Draw();

		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Renderable::ChangeTextureUvs(std::string ind)
{
	if (metaData == NULL)
		return;

	T = glm::translate(glm::mat4(1.f), metaData->atlasObj[ind].position);
	S = glm::scale(glm::mat4(1.f), metaData->atlasObj[ind].scale);
}

void Renderable::SetMetaData()
{
	if (texture != NULL)
	{
		std::string textureName = resoManager->getResoName(texture);
		size_t lastindex = textureName.find_last_of(".");
		std::string noExtension = textureName.substr(0, lastindex);

		metaData = graphics->FindAtlas(noExtension);

		if (metaData != NULL)
			ChangeTextureUvs(atlasIndex);
		else
		{
			T = glm::mat4(1.f);
			S = glm::mat4(1.f);
		}
	}
}

void Renderable::load(Json::Value & val)
{
	texture = resoManager->getReso<Texture>(val["Renderable"].get("texture", "Set_Texture_Please").asString());

	sprite = (resoManager->getReso<Sprite>(val["Renderable"].get("model", "Set_Model_Please").asString()) != NULL) ?
		resoManager->getReso<Sprite>(val["Renderable"].get("model", "Set_Model_Please").asString()) :
		graphics->GetDefaultSprite();

	shader = (resoManager->getReso<Shader>(val["Renderable"].get("shader", "Set_Shader_Please").asString()) != NULL) ?
		resoManager->getReso<Shader>(val["Renderable"].get("shader", "Set_Shader_Please").asString()) :
		graphics->GetDefaultShader();

	color.r = val["Renderable"]["color"].get("r", 1.0f).asFloat();
	color.g = val["Renderable"]["color"].get("g", 1.0f).asFloat();
	color.b = val["Renderable"]["color"].get("b", 1.0f).asFloat();
	color.a = val["Renderable"]["color"].get("a", 1.0f).asFloat();
	visible = val["Renderable"].get("visible", true).asBool();
	layer = val["Renderable"].get("layer", "Set_Layer_Please").asString();
	atlasIndex = val["Renderable"].get("atlas_index", "Set_Index_Please").asString();
}

void Renderable::save(Json::Value & val)
{
	if(shader)   val["Renderable"]["shader"] = resoManager->getResoName(shader);
	if (sprite)  val["Renderable"]["model"] = resoManager->getResoName(sprite);
	if (texture) val["Renderable"]["texture"] = resoManager->getResoName(texture);
	val["Renderable"]["color"]["r"] = color.r;
	val["Renderable"]["color"]["g"] = color.g;
	val["Renderable"]["color"]["b"] = color.b;
	val["Renderable"]["color"]["a"] = color.a;
	val["Renderable"]["visible"] = visible;
	val["Renderable"]["layer"] = layer;
	val["Renderable"]["atlas_index"] = atlasIndex;
}

void Renderable::WriteToImguiWindow()
{
	static int selected_texture = 0;
	static int selected_sprite = 0;
	static int selected_shader = 0;
	static int selected_layer = 0;

	if (ImGui::CollapsingHeader("Renderable"))
	{		
		if (ImGui::Button("Texture##Renderable"))
			ImGui::OpenPopup("textures##Renderable");

		ImGui::SameLine();
		if (texture) ImGui::Text(resoManager->getResoName(texture).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Shader##Renderable"))
			ImGui::OpenPopup("shaders##Renderable");

		ImGui::SameLine();
		if (shader) ImGui::Text(resoManager->getResoName(shader).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Sprite##Renderable"))
			ImGui::OpenPopup("sprites##Renderable");

		ImGui::SameLine();
		if (sprite) ImGui::Text(resoManager->getResoName(sprite).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Layer##Renderable"))
			ImGui::OpenPopup("layers##Renderable");

		ImGui::SameLine();
		if (layer != "") ImGui::Text(layer.c_str());
		else ImGui::Text("NULL");

		ImGuiEditor::ColorEdit4("Color##Renderable", color);

		ImGui::Checkbox("visible##Renderable", &visible);
		if (sprite != NULL) ImGui::Checkbox("wire frame##Renderable", &sprite->wireFrame);

		if (metaData != NULL)
		{
			std::vector<std::string> atlasNames;
			for (auto it : metaData->atlasObj)
				atlasNames.push_back(it.first);

			static int iter = 0;
			if (ImGui::InputInt("Atlas Index##Renderable", &iter))
			{
				if (iter < 0) iter = 0;
				if (iter > metaData->size - 1) iter = 0;

				ChangeTextureUvs(atlasNames[iter]);
				atlasIndex = atlasNames[iter];
			}
		}
		
		if (ImGui::Button("Apply Texture Scale##Renderable"))
			ApplyTextureScale();

		ImGui::NewLine();
		if (ImGui::Button("Detach##Renderable"))
			GetOwner()->detach(this);
	}

	if (ImGui::BeginPopup("textures##Renderable"))
	{
		ImGui::Text("Textures");
		ImGui::Separator();

		static ImGuiTextFilter filter;
		filter.Draw("search##Texture");
		std::vector<std::string> textureNames = imGuiEditor->textureNames;
		for (unsigned i = 0; i < textureNames.size(); i++) {
			if (filter.PassFilter(textureNames[i].c_str())) {
				if (ImGui::Selectable(textureNames[i].c_str())) {
					selected_texture = i;
					texture = resoManager->getReso<Texture>(imGuiEditor->textureNames[selected_texture]);
					SetMetaData();
					ChangeTextureUvs(atlasIndex);
				}
			}
		}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("shaders##Renderable"))
	{
		ImGui::Text("Shaders");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->shaderNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->shaderNames[i].c_str()))
			{
				selected_shader = i;
				shader = resoManager->getReso<Shader>(imGuiEditor->shaderNames[selected_shader]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("sprites##Renderable"))
	{
		ImGui::Text("Sprites");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->spriteNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->spriteNames[i].c_str()))
			{
				selected_sprite = i;
				sprite = resoManager->getReso<Sprite>(imGuiEditor->spriteNames[selected_sprite]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("layers##Renderable"))
	{
		ImGui::Text("Layers");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->layerNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->layerNames[i].c_str()))
			{
				Renderable * temp = this;

				graphics->RemoveRenderable(this);

				selected_layer = i;
				layer = imGuiEditor->layerNames[selected_layer];

				graphics->AddRenderable(temp);
			}

		ImGui::EndPopup();
	}
}

void Renderable::ApplyTextureScale()
{
	if (metaData != NULL)
	{
		GetOwner()->mTransform->scale.x = metaData->atlasObj[atlasIndex].scale.x * metaData->width;
		GetOwner()->mTransform->scale.y = metaData->atlasObj[atlasIndex].scale.y * metaData->height;
	}
	else if (texture != NULL)
	{
		GetOwner()->mTransform->scale.x = texture->width;
		GetOwner()->mTransform->scale.y = texture->height;
	}
}