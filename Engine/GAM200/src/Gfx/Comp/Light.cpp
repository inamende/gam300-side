#include "Light.h"

#include "../Gfx.h"
#include "../Comp/Transform.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Editor/ImGuiEditor.h"

Light::Light()
{
	ambient = glm::vec3(0.0f, 0.0f, 0.0f);
	diffuse = glm::vec3(0.0f, 0.0f, 0.0f);
	specular = glm::vec3(0.0f, 0.0f, 0.0f);
	spotDirection = glm::vec3(0.f, 0.f, -1.f);
	spotCosPhi = cos(0.0f);
	spotCosTheta = cos(0.0f);
	color = glm::vec3(1.0f, 1.0f, 1.0f);
}

Light::~Light()
{
	graphics->RemoveLight(this);
}

void Light::Initialize()
{
	GetOwner()->mTransform->position.z = 1000.0f;
	graphics->AddLight(this);
}

void Light::Update() {}

void Light::load(Json::Value & val)
{
	ambient.x = val["Light"]["ambient"].get("x", 0.0f).asFloat();
	ambient.y = val["Light"]["ambient"].get("y", 0.0f).asFloat();
	ambient.z = val["Light"]["ambient"].get("z", 0.0f).asFloat();

	diffuse.x = val["Light"]["diffuse"].get("x", 0.0f).asFloat();
	diffuse.y = val["Light"]["diffuse"].get("y", 0.0f).asFloat();
	diffuse.z = val["Light"]["diffuse"].get("z", 0.0f).asFloat();

	specular.x = val["Light"]["specular"].get("x", 0.0f).asFloat();
	specular.y = val["Light"]["specular"].get("y", 0.0f).asFloat();
	specular.z = val["Light"]["specular"].get("z", 0.0f).asFloat();

	spotCosPhi = cos(val["Light"].get("spotCosPhi", 0.0f).asFloat());
	spotCosTheta = cos(val["Light"].get("spotCosTheta", 0.0f).asFloat());

	color.r = val["Light"]["color"].get("r", 1.0f).asFloat();
	color.g = val["Light"]["color"].get("g", 1.0f).asFloat();
	color.b = val["Light"]["color"].get("b", 1.0f).asFloat();
}

void Light::save(Json::Value & val)
{
	val["Light"]["ambient"]["x"] = ambient.x;
	val["Light"]["ambient"]["y"] = ambient.y;
	val["Light"]["ambient"]["z"] = ambient.z;

	val["Light"]["diffuse"]["x"] = diffuse.x;
	val["Light"]["diffuse"]["y"] = diffuse.y;
	val["Light"]["diffuse"]["z"] = diffuse.z;

	val["Light"]["specular"]["x"] = specular.x;
	val["Light"]["specular"]["y"] = specular.y;
	val["Light"]["specular"]["z"] = specular.z;

	val["Light"]["spotCosPhi"] = acos(spotCosPhi);
	val["Light"]["spotCosTheta"] = acos(spotCosTheta);

	val["Light"]["color"]["r"] = color.r;
	val["Light"]["color"]["g"] = color.g;
	val["Light"]["color"]["b"] = color.b;
}

void Light::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Light"))
	{
		ImGuiEditor::InputFloat3("Ambient", ambient);
		ImGuiEditor::InputFloat3("Diffuse", diffuse);
		ImGuiEditor::InputFloat3("Specular", specular);
		ImGuiEditor::InputFloat3("Spot Direction", spotDirection);
		ImGui::SliderFloat("spotCosPhi", &spotCosPhi, 0.0f, 1.0f);
		ImGui::SliderFloat("spotCosTheta", &spotCosTheta, 0.0f, 1.0f);
		ImGuiEditor::ColorEdit3("Color", color);

		ImGui::NewLine();
		if (ImGui::Button("Detach##Light"))
			GetOwner()->detach(this);
	}
}
