#ifndef SKELETON_RENDERABLE_H
#define SKELETON_RENDERABLE_H

#include "Renderable.h"

class spSkeleton;
class spAnimationState;
class spSkeletonData;
class spAnimationStateData;
class spEvent;
class spEventData;

class SkeletonRenderable : public Renderable
{
public:
	SkeletonRenderable();
	virtual ~SkeletonRenderable();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void Render();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void LoadData();
	void UploadData();
	void ChangeAnimation(std::string s);
	void ChangeAnimationWithOutLoop(std::string s);
	void AddAnimationToTrack(std::string s);
	void AddAnimationToQueue(std::string s, float Time = 0);
	void AddInfiniteAnimationToQueue(std::string s, float Time = 0);
	bool LoopBetweenEvents(const std::string Event1, const unsigned int Event1Num, const std::string Event2, const unsigned int Event2Num, const float TimeLooping = INFINITY);
	bool DoesThisEnd();
	void ClearAnimationData();
	void ToEndEventLoopBetweenEvents();

	int CheckEvent(std::string s);
	static void spineAnimStateHandler(spAnimationState* state, int trackIndex, int type, spEvent* event, int loopCount);
	void OnSpineAnimationStateEvent(int trackIndex, int type, spEvent* event, int loopCount);


	spSkeleton * skel;
	spAnimationState * animState;

	spSkeletonData * skelData;
	spAnimationStateData * animStateData;

	std::string skelFile;
	std::string animationState;
	std::string currentanimation;
	std::string skin;
	spEventData * eventData;

	int offset = 0;
	int	stride = 2;

	float worldVertices[1000];

	//loop between two events
	float Timer;
	float EventStart;
	float EventEnd;
	bool LoopingEvents;
	bool LoopNeed2End;
};	

#endif