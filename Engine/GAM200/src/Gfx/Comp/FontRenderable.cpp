#include "FontRenderable.h"

#include "../Gfx.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Gfx/Comp/Transform.h"
#include "../../Gfx/Reso/Shader.h"
#include "../../Gfx/Reso/Camera.h"
#include "../../Gfx/Reso/Font.h"
#include "../../Editor/ImGuiEditor.h"

FontRenderable::FontRenderable() : 
	Renderable::Renderable(),
	size(1.f),
	offset(vec2()) { 
	font = NULL;
	displayText = "default text";
}

void FontRenderable::Initialize()
{
	renderableTransform = GetOwner()->GetComponent<Transform>();
	Gfx::getInstance()->AddRenderable(this);
}

void FontRenderable::Update() {}

void FontRenderable::Render()
{
	if (font)
	{
		font->text = displayText;
		font->initPosition = GetOwner()->GetComponent<Transform>()->position.x + offset.x;
		font->textPosition.x = GetOwner()->GetComponent<Transform>()->position.x + offset.x;
		font->textPosition.y = GetOwner()->GetComponent<Transform>()->position.y + offset.y;
		font->textScale = size;

		Transform test(glm::vec3(0.0f, 0.0f, renderableTransform->position.z), 0.0f, glm::vec3(1.0f));
		//Transform test(glm::vec3(0.0f, 0.0f, GetOwner()->GetComponent<Transform>()->position.z), 0.0f, glm::vec3(1.0f));

		glm::mat4 modl = test.GetTransform();
		glm::mat4 view = (Gfx::getInstance()->GetMainCamera() != NULL) ? Gfx::getInstance()->GetMainCamera()->getViewMatrix() : glm::mat4();
		glm::mat4 proj = (Gfx::getInstance()->GetMainCamera() != NULL) ? Gfx::getInstance()->GetMainCamera()->getProjectionMatrix() : glm::mat4();

		//shader->use();
		if (shader) shader->setUniform("M", modl);
		//shader->setUniform("V", view);
		//shader->setUniform("P", proj);
		//shader->setUniform("Color", glm::vec3(1.0, 1.0, 1.0));
		if (shader) shader->setUniform("Color", color);

		if (visible)
			if (shader)
				font->Draw(shader);
	}
}

void FontRenderable::load(Json::Value & val)
{
	font = (resoManager->getReso<Font>(val["FontRenderable"].get("font", "Set_Font_Please").asString()) != NULL) ?
		resoManager->getReso<Font>(val["FontRenderable"].get("font", "Set_Font_Please").asString()) :
		graphics->GetDefaultFont();

	shader = (resoManager->getReso<Shader>(val["FontRenderable"].get("shader", "Set_Shader_Please").asString()) != NULL) ?
		resoManager->getReso<Shader>(val["FontRenderable"].get("shader", "Set_Shader_Please").asString()) :
		graphics->GetDefaultShader();

	displayText = val["FontRenderable"].get("text", "Set_Text_Please").asString();
	color.r = val["FontRenderable"]["color"].get("r", 0.0f).asFloat();
	color.g = val["FontRenderable"]["color"].get("g", 0.0f).asFloat();
	color.b = val["FontRenderable"]["color"].get("b", 0.0f).asFloat();
	visible = val["FontRenderable"].get("visible", true).asBool();
	layer = val["FontRenderable"].get("layer", "Set_Layer_Please").asString();

	size = val["FontRenderable"].get("size", 1.f).asFloat();
	offset.x = val["FontRenderable"]["offset"].get("x", 0.f).asFloat();
	offset.y = val["FontRenderable"]["offset"].get("y", 0.f).asFloat();
}

void FontRenderable::save(Json::Value & val)
{
	if(font) val["FontRenderable"]["font"] = resoManager->getResoName(font);
	val["FontRenderable"]["text"] = displayText;
	if(shader) val["FontRenderable"]["shader"] = resoManager->getResoName(shader);
	val["FontRenderable"]["color"]["r"] = color.r;
	val["FontRenderable"]["color"]["g"] = color.g;
	val["FontRenderable"]["color"]["b"] = color.b;
	val["FontRenderable"]["visible"] = visible;
	val["FontRenderable"]["layer"] = layer;

	val["FontRenderable"]["size"] = size;
	val["FontRenderable"]["offset"]["x"] = offset.x;
	val["FontRenderable"]["offset"]["y"] = offset.y;
}

void FontRenderable::WriteToImguiWindow()
{
	static int selected_font = 0;
	static int selected_shader = 0;
	static int selected_layer = 0;

	if (ImGui::CollapsingHeader("FontRenderable"))
	{
		if (ImGui::Button("Font##FontRenderable"))
			ImGui::OpenPopup("fonts##FontRenderable");

		ImGui::SameLine();
		if (font) ImGui::Text(resoManager->getResoName(font).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Shader##FontRenderable"))
			ImGui::OpenPopup("shaders##FontRenderable");

		ImGui::SameLine();
		if (shader) ImGui::Text(resoManager->getResoName(shader).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Layer##FontRenderable"))
			ImGui::OpenPopup("layers##FontRenderable");

		ImGui::SameLine();
		if (layer != "") ImGui::Text(layer.c_str());
		else ImGui::Text("NULL");

		ImGui::InputFloat("font size##FontRenderable", &size);
		ImGui::InputFloat("font offset x##FontRenderable", &offset.x);
		ImGui::InputFloat("font offset y##FontRenderable", &offset.y);
		ImGuiEditor::InputText("text##FontRenderable", displayText);
		ImGuiEditor::ColorEdit3("Color##FontRenderable", color);

		ImGui::NewLine();
		if (ImGui::Button("Detach##FontRenderable"))
			GetOwner()->detach(this);

	}

	if (ImGui::BeginPopup("fonts##FontRenderable"))
	{
		ImGui::Text("Fonts");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->fontNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->fontNames[i].c_str()))
			{
				selected_font = i;
				font = resoManager->getReso<Font>(imGuiEditor->fontNames[selected_font]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("shaders##FontRenderable"))
	{
		ImGui::Text("Shaders");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->shaderNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->shaderNames[i].c_str()))
			{
				selected_shader = i;
				shader = resoManager->getReso<Shader>(imGuiEditor->shaderNames[selected_shader]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("layers##FontRenderable"))
	{
		ImGui::Text("Layers");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->layerNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->layerNames[i].c_str()))
			{
				Renderable * temp = this;

				graphics->RemoveRenderable(this);

				selected_layer = i;
				layer = imGuiEditor->layerNames[selected_layer];

				graphics->AddRenderable(temp);
			}

		ImGui::EndPopup();
	}
}