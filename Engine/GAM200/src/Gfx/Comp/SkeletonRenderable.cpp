#include "SkeletonRenderable.h"

#include <spine/Animation.h>
#include <spine/extension.h>

#include "../../Editor/ImGuiEditor.h"

#include "../../ResourceManager/ResoManager.h"
#include "../../Gfx/Reso/Sprite.h"
#include "../../Gfx/Reso/Texture.h"
#include "../../Gfx/Reso/Shader.h"
#include "../../Gfx/Gfx.h"
#include "../../ObjectManager/GameObject.h"

#include "../../Level/Scene.h"

#include <spine/Animation.h>
#include <spine/extension.h>
#include <spine/SkeletonJson.h>
#include <spine/SkeletonClipping.h>

void world_v(spVertexAttachment* self, spSlot* slot, int start, int count, float* worldVertices, int offset, int stride) {
	spSkeleton* skeleton;
	int deformLength;
	float* deform;
	float* vertices;
	int* bones;

	count = offset + (count >> 1) * stride;
	skeleton = slot->bone->skeleton;
	deformLength = slot->attachmentVerticesCount;
	deform = slot->attachmentVertices;
	vertices = self->vertices;
	bones = self->bones;
	if (!bones) {
		spBone* bone;
		int v, w;
		float x, y;
		if (deformLength > 0) vertices = deform;
		bone = slot->bone;
		x = bone->worldX;
		y = bone->worldY;
		for (v = start, w = offset; w < count; v += 2, w += stride) {
			float vx = vertices[v], vy = vertices[v + 1];
			worldVertices[w] = vx * bone->a + vy * bone->b + x;
			worldVertices[w + 1] = vx * bone->c + vy * bone->d + y;
		}
	}
	else {
		int v = 0, skip = 0, i;
		spBone** skeletonBones;
		for (i = 0; i < start; i += 2) {
			int n = bones[v];
			v += n + 1;
			skip += n;
		}
		skeletonBones = skeleton->bones;
		if (deformLength == 0) {
			int w, b;
			for (w = offset, b = skip * 3; w < count; w += stride) {
				float wx = 0, wy = 0;
				int n = bones[v++];
				n += v;
				for (; v < n; v++, b += 3) {
					spBone* bone = skeletonBones[bones[v]];
					float vx = vertices[b], vy = vertices[b + 1], weight = vertices[b + 2];
					wx += (vx * bone->a + vy * bone->b + bone->worldX) * weight;
					wy += (vx * bone->c + vy * bone->d + bone->worldY) * weight;
				}
				worldVertices[w] = wx;
				worldVertices[w + 1] = wy;
			}
		}
		else {
			int w, b, f;
			for (w = offset, b = skip * 3, f = skip << 1; w < count; w += stride) {
				float wx = 0, wy = 0;
				int n = bones[v++];
				n += v;
				for (; v < n; v++, b += 3, f += 2) {
					spBone* bone = skeletonBones[bones[v]];
					float vx = vertices[b] + deform[f], vy = vertices[b + 1] + deform[f + 1], weight = vertices[b + 2];
					wx += (vx * bone->a + vy * bone->b + bone->worldX) * weight;///////////////
					wy += (vx * bone->c + vy * bone->d + bone->worldY) * weight;
				}
				worldVertices[w] = wx;
				worldVertices[w + 1] = wy;
			}
		}
	}
}

void _spAtlasPage_createTexture(spAtlasPage* self, const char* path)
{
	Texture * atlasTex = DBG_NEW Texture;

	atlasTex->filePath = path;
	atlasTex->LoadResource();

	self->rendererObject = atlasTex;
}

void _spAtlasPage_disposeTexture(spAtlasPage* self)
{
	if (self->rendererObject)
	{
		delete ((Texture*)(self->rendererObject));
	}
}

char* _spUtil_readFile(const char* path, int* length)
{
	return _spReadFile(path, length);
}

void SkeletonRenderable::spineAnimStateHandler(spAnimationState* state, int trackIndex, int type, spEvent* event, int loopCount)
{
	if (state && state->rendererObject) {
		int ok = type;

		spEventData data;
		data.intValue = (event) ? event->intValue : 0;
		//std::cout << "data : " << data.intValue << std::endl;

		SkeletonRenderable * skel = (SkeletonRenderable*)state->rendererObject;
		skel->OnSpineAnimationStateEvent(trackIndex, type, event, loopCount);
	}
}

void SkeletonRenderable::OnSpineAnimationStateEvent(int trackIndex, int type, spEvent* event, int loopCount) {
	eventData = (event) ? event->data : NULL;
	if (eventData) {
		eventData->intValue = event->intValue;
eventData->floatValue = event->floatValue;
	}
}

SkeletonRenderable::SkeletonRenderable() : Renderable::Renderable(), skel(NULL), animState(NULL), skelData(NULL),
animStateData(NULL), skin("default"), eventData(NULL), Timer(0.0f), LoopingEvents(false), EventStart(0.0f),
LoopNeed2End(false) {}
SkeletonRenderable::~SkeletonRenderable() {}

void SkeletonRenderable::Initialize()
{
	sprite = DBG_NEW Sprite;
	sprite->filePath = "./Data/Meshes/plane.obj";
	sprite->LoadResource();

	Renderable::Initialize();
}

void SkeletonRenderable::Update()
{
	spEvent * firedEvents = nullptr;
	int counter = 0;

	if (skel)
	{
		float dt = ImGui::GetIO().DeltaTime;
		spSkeleton_update(skel, dt);
		spAnimationState_update(animState, dt);
		spAnimationState_apply(animState, skel);
		spSkeleton_updateWorldTransform(skel);
		animState->rendererObject = (void*)this;
		animState->listener = (spAnimationStateListener)&SkeletonRenderable::spineAnimStateHandler;
	}
}

void SkeletonRenderable::Terminate()
{
	if (skel)
		spSkeleton_dispose(skel);

	if (animState)
		spAnimationState_dispose(animState);

	delete sprite;
	Renderable::Terminate();
}

void SkeletonRenderable::Render() {
	if (!skel || !animState) {
		return;
	}
	sprite->Clear();
	vec3 vtx[4];
	vec2 uvv[4];
	vec4 col[4];
	vec4 skelColor{ skel->color.r , skel->color.g, skel->color.b, skel->color.a };
	Texture *prev_tex = texture;
	spSkeletonClipping * clipping = spSkeletonClipping_create();
	spClippingAttachment * clipAttachment = nullptr;
	spSlot * clippingEndSlot = nullptr;
	spSkeleton_setSkinByName(skel, skin.c_str());
	float z = renderableTransform->position.z;
	for (int i = 0; i < skel->slotsCount; i++) {
		spSlot * slot = skel->drawOrder[i];
		if (!slot) continue;
		spAttachment * attachment = slot->attachment;
		if (!attachment) continue;
		if (attachment->type == SP_ATTACHMENT_MESH) {
			spMeshAttachment * mesh = (spMeshAttachment*)attachment;
			Texture * tex = (Texture *)((spAtlasRegion*)mesh->rendererObject)->page->rendererObject;
			if (!prev_tex) prev_tex = tex;
			if (tex) {
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, tex->texHandle);
			}
			world_v(&mesh->super, slot, 0, mesh->super.verticesCount, worldVertices, offset, stride);
			//spVertexAttachment_computeWorldVertices(&mesh->super, slot, 0, mesh->super.verticesCount, worldVertices, offset, stride);
			if (spSkeletonClipping_isClipping(clipping)) {
				spSkeletonClipping_clipTriangles(clipping, worldVertices, mesh->super.worldVerticesLength, mesh->triangles, mesh->trianglesCount, mesh->uvs, 2);
				for (int k = 0; k < clipping->clippedTriangles->size; k++) {
					int index = clipping->clippedTriangles->items[k] << 1;
					vtx[0].x = clipping->clippedVertices->items[index];
					vtx[0].y = clipping->clippedVertices->items[index + 1];
					vtx[0].z = z;
					uvv[0].x = clipping->clippedUVs->items[index];
					uvv[0].y = clipping->clippedUVs->items[index + 1];
					vec4 color = skelColor;
					color *= vec4(slot->color.r, slot->color.g, slot->color.b, slot->color.a);
					col[0] = color;

					sprite->vertices.push_back(vtx[0]);
					sprite->uvs.push_back(uvv[0]);
					sprite->colors.push_back(col[0]);
				}
			}
			else
			{
				for (unsigned k = 0; k < mesh->trianglesCount; k++) {
					int index = mesh->triangles[k] << 1;
					vtx[0].x = worldVertices[index];
					vtx[0].y = worldVertices[index + 1];
					vtx[0].z = z;
					uvv[0].x = mesh->uvs[index];
					uvv[0].y = mesh->uvs[index + 1];
					col[0].r = skelColor.r;
					vec4 color = skelColor;
					color *= vec4(slot->color.r, slot->color.g, slot->color.b, slot->color.a);
					col[0] = color;

					sprite->vertices.push_back(vtx[0]);
					sprite->uvs.push_back(uvv[0]);
					sprite->colors.push_back(col[0]);
				}
			}
			if (spSkeletonClipping_isClipping(clipping)) {
				unsigned short triangles[] = { 0,1,2,0,2,3 };
				spSkeletonClipping_clipTriangles(clipping, (float*)uvv, 4, triangles, 6, ((float*)uvv), 0);
				for (int k = 0; k < clipping->clippedTriangles->size; k++) {
					int index = clipping->clippedTriangles->items[k] << 1;
					vtx[0] = vec3(clipping->clippedVertices->items[index], clipping->clippedVertices->items[index + 1], z);
					uvv[0] = vec2(clipping->clippedUVs->items[index], clipping->clippedUVs->items[index + 1]);
					sprite->vertices.push_back(vtx[0]);
					sprite->uvs.push_back(uvv[0]);
					sprite->colors.push_back(col[0]);
				}
			}
		}
		if (attachment->type == SP_ATTACHMENT_REGION) {
			spRegionAttachment * region = (spRegionAttachment*)attachment;
			Texture * tex = (Texture*)((spAtlasRegion*)region->rendererObject)->page->rendererObject;
			if (!prev_tex) {
				prev_tex = tex;
			}
			if (tex) {
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, tex->texHandle);
			}
			spRegionAttachment_computeWorldVertices(region, slot->bone, (float*)worldVertices, 0, 2);
			vec4 color = skelColor;
			color *= vec4(slot->color.r, slot->color.g, slot->color.b, slot->color.a);
			uvv[0] = vec2{ region->uvs[BLX],region->uvs[BLY] };
			col[0] = color;
			uvv[1] = vec2{ region->uvs[BRX], region->uvs[BRY] };
			col[1] = color;
			uvv[2] = vec2{ region->uvs[URX], region->uvs[URY] };
			col[2] = color;
			uvv[3] = vec2{ region->uvs[ULX], region->uvs[ULY] };
			col[3] = color;

			if (spSkeletonClipping_isClipping(clipping)) {
				unsigned short triangles[] = { 0,1,2,0,2,3 };
				spSkeletonClipping_clipTriangles(clipping, (float*)uvv, 4, triangles, 6, ((float*)uvv), 0);
				for (int k = 0; k < clipping->clippedTriangles->size; k++) {
					int index = clipping->clippedTriangles->items[k] << 1;
					vtx[0] = vec3(clipping->clippedVertices->items[index], clipping->clippedVertices->items[index + 1], z);
					uvv[0] = vec2(clipping->clippedUVs->items[index], clipping->clippedUVs->items[index + 1]);
					sprite->vertices.push_back(vtx[0]);
					sprite->uvs.push_back(uvv[0]);
					sprite->colors.push_back(col[0]);
				}
			}
			else {
				vtx[0] = vec3(worldVertices[BLX], worldVertices[BLY], z);
				vtx[1] = vec3(worldVertices[BRX], worldVertices[BRY], z);
				vtx[2] = vec3(worldVertices[URX], worldVertices[URY], z);
				vtx[3] = vec3(worldVertices[ULX], worldVertices[ULY], z);

				sprite->vertices.push_back(vtx[0]);
				sprite->vertices.push_back(vtx[1]);
				sprite->vertices.push_back(vtx[2]);
				sprite->vertices.push_back(vtx[0]);
				sprite->vertices.push_back(vtx[2]);
				sprite->vertices.push_back(vtx[3]);

				sprite->uvs.push_back(uvv[0]);
				sprite->uvs.push_back(uvv[1]);
				sprite->uvs.push_back(uvv[2]);
				sprite->uvs.push_back(uvv[0]);
				sprite->uvs.push_back(uvv[2]);
				sprite->uvs.push_back(uvv[3]);
			}
		}
		else if (attachment->type == SP_ATTACHMENT_CLIPPING) {
			spClippingAttachment* clipAttachment = (spClippingAttachment *)attachment;
			spSkeletonClipping_clipStart(clipping, slot, clipAttachment);
			continue;
		}
		else {
			continue;
		}
	}
	spSkeletonClipping_dispose(clipping);
	if (shader) {
		glm::mat4 M = renderableTransform->GetTransform();
		shader->setUniform("M", M);
		shader->setUniform("textureData", 0);
	}
	if (sprite) {
		if (sprite->vertices.empty() || sprite->uvs.empty())
			return;

		glBindBuffer(GL_ARRAY_BUFFER, sprite->posBufferObj);
		glBufferData(GL_ARRAY_BUFFER, sprite->vertices.size() * sizeof(glm::vec3), &sprite->vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, sprite->texBufferObj);
		glBufferData(GL_ARRAY_BUFFER, sprite->uvs.size() * sizeof(glm::vec2), &sprite->uvs[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, sprite->colBufferObj);
		glBufferData(GL_ARRAY_BUFFER, sprite->colors.size() * sizeof(glm::vec4), &sprite->colors[0], GL_STATIC_DRAW);

		if (visible)
			sprite->Draw();
	}
}

void SkeletonRenderable::load(Json::Value & val)
{
	skelFile = val["SkeletonRenderable"].get("skelFile", "").asString();

	LoadData();

	active = val["SkeletonRenderable"].get("active", true).asBool();

	shader = (resoManager->getReso<Shader>(val["SkeletonRenderable"].get("shader", "Set_Shader_Please").asString()) != NULL) ?
		resoManager->getReso<Shader>(val["SkeletonRenderable"].get("shader", "Set_Shader_Please").asString()) :
		graphics->GetDefaultShader();

	color.r = val["SkeletonRenderable"]["color"].get("r", 1.0f).asFloat();
	color.g = val["SkeletonRenderable"]["color"].get("g", 1.0f).asFloat();
	color.b = val["SkeletonRenderable"]["color"].get("b", 1.0f).asFloat();
	color.a = val["SkeletonRenderable"]["color"].get("a", 1.0f).asFloat();
	visible = val["SkeletonRenderable"].get("visible", true).asBool();
	layer = val["SkeletonRenderable"].get("layer", "default").asString();

	animationState = val["SkeletonRenderable"].get("animationState", "").asString();

	UploadData();
}

void SkeletonRenderable::save(Json::Value & val)
{
	val["SkeletonRenderable"]["active"] = active;

	if (shader)   val["SkeletonRenderable"]["shader"] = resoManager->getResoName(shader);
	val["SkeletonRenderable"]["color"]["r"] = color.r;
	val["SkeletonRenderable"]["color"]["g"] = color.g;
	val["SkeletonRenderable"]["color"]["b"] = color.b;
	val["SkeletonRenderable"]["color"]["a"] = color.a;
	val["SkeletonRenderable"]["visible"] = visible;
	val["SkeletonRenderable"]["layer"] = layer;

	val["SkeletonRenderable"]["skelFile"] = skelFile;

	val["SkeletonRenderable"]["animationState"] = animationState;
}

void SkeletonRenderable::WriteToImguiWindow()
{
	static int selected_texture = 0;
	static int selected_sprite = 0;
	static int selected_shader = 0;
	static int selected_layer = 0;

	if (ImGui::CollapsingHeader("SkeletonRenderable"))
	{
		if (ImGui::Button("Shader##SkeletonRenderable"))
			ImGui::OpenPopup("shaders");

		ImGui::SameLine();
		if (shader) ImGui::Text(resoManager->getResoName(shader).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Layer##SkeletonRenderable"))
			ImGui::OpenPopup("layers");

		ImGui::SameLine();
		if (layer != "") ImGui::Text(layer.c_str());
		else ImGui::Text("NULL");

		ImGuiEditor::ColorEdit4("Color##SkeletonRenderable", color);

		ImGuiEditor::InputText("Skely Path", skelFile);

		if (ImGui::Button("Update Spine"))
		{
			LoadData();
			UploadData();
		}

		ImGui::Checkbox("visible##SkeletonRenderable", &visible);

		ImGui::InputInt("offset##SkeletonRenderable", &offset);
		ImGui::InputInt("stride##SkeletonRenderable", &stride);

		ImGui::NewLine();
		if (ImGui::Button("Detach##Renderable"))
			GetOwner()->detach(this);
	}

	if (ImGui::BeginPopup("shaders"))
	{
		ImGui::Text("Shaders");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->shaderNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->shaderNames[i].c_str()))
			{
				selected_shader = i;
				shader = resoManager->getReso<Shader>(imGuiEditor->shaderNames[selected_shader]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("layers"))
	{
		ImGui::Text("Layers");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->layerNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->layerNames[i].c_str()))
			{
				Renderable * temp = this;

				graphics->RemoveRenderable(this);

				selected_layer = i;
				layer = imGuiEditor->layerNames[selected_layer];

				graphics->AddRenderable(temp);
			}

		ImGui::EndPopup();
	}
}

void SkeletonRenderable::LoadData()
{
	std::string atF = "./Data/Spine/" + skelFile + "/" + skelFile + ".atlas";
	std::string skF = "./Data/Spine/" + skelFile + "/" + skelFile + ".json";

	static spSkeletonData * skelReso = NULL;
	static spAtlas * skellAtlasReso = NULL;
	static spAnimationStateData * skellAnimReso = NULL;

	skellAtlasReso = spAtlas_createFromFile(atF.c_str(), NULL);

	if (skellAtlasReso)
	{
		spSkeletonJson * json = spSkeletonJson_create(skellAtlasReso);
		json->scale = 0.001f;

		skelReso = spSkeletonJson_readSkeletonDataFile(json, skF.c_str());
		spSkeletonJson_dispose(json);

		skellAnimReso = spAnimationStateData_create(skelReso);
	}

	skelData = skelReso;
	animStateData = skellAnimReso;
}

void SkeletonRenderable::UploadData()
{
	if (skelData)
	{
		skel = spSkeleton_create(skelData);

		if (animStateData)
		{
			animState = spAnimationState_create(animStateData);
			ChangeAnimation(animationState);
			Update();
		}
	}
}

void SkeletonRenderable::ChangeAnimation(std::string s)
{
/*	if (animState) {
		currentanimation = "YoungToMedium";
    	spAnimationState_setAnimationByName(animState, 0, "YoungToMedium", true);
		ClearAnimationData();
	}*/
	if (animState)
		if (s != "")
		{
			//std::cout << "name of the string: " << s << "\n";
			currentanimation = s;
			spAnimationState_setAnimationByName(animState, 0, s.c_str(), true);
			ClearAnimationData();
		}
}

void SkeletonRenderable::ChangeAnimationWithOutLoop(std::string s)
{

	if (animState)
		if (s != "")
		{
			currentanimation = s;
			spAnimationState_setAnimationByName(animState, 0, s.c_str(), 0);
			ClearAnimationData();
		}
}
void SkeletonRenderable::AddAnimationToTrack(std::string s)
{

	if (animState)
		if (s != "")
		{
			currentanimation = s;
			spAnimationState_setAnimationByName(animState, 0, s.c_str(), 0);
			ClearAnimationData();
		}
}

void SkeletonRenderable::AddAnimationToQueue(std::string s, float Time)
{
	if (animState)
		if (s != "")
		{
			currentanimation = s;
			spAnimationState_addAnimationByName(animState, 0, s.c_str(), 0, Time);
			ClearAnimationData();
		}
}
void SkeletonRenderable::AddInfiniteAnimationToQueue(std::string s, float Time)
{
	if (animState)
		if (s != "")
		{
			currentanimation = s;
			spAnimationState_addAnimationByName(animState, 0, s.c_str(), 1, Time);
			ClearAnimationData();
		}
}

int SkeletonRenderable::CheckEvent(std::string s)
{
	if(eventData)
		if (eventData->name == s)
		{
			return eventData->intValue;
		}
	return -1;
}

bool SkeletonRenderable::LoopBetweenEvents(const std::string Event1, const unsigned int Event1Num, const std::string Event2, const unsigned int Event2Num, const float TimeLooping) 
{
	
	if (CheckEvent(Event1) == Event1Num && LoopingEvents == false && LoopNeed2End == false)
	{
		LoopingEvents = true;
		Timer = 0;
		// get the time in the animation, where the stun start event is. 
		EventStart = spTrackEntry_getAnimationTime(animState->tracks[0]);

		//std::cout << "stun start " << spTrackEntry_getAnimationTime(rend->animState->tracks[0])  << "\n";
	}

	if (LoopingEvents)
	{

		//std::cout << "stun loop " << spTrackEntry_getAnimationTime(rend->animState->tracks[0]) << "\n";

		// update timer for the loop
		Timer += ImGui::GetIO().DeltaTime;

		// Stun loop is finished, we should exit the state
		// but first wait for animation to finish
		if ((Timer > TimeLooping))
		{
			LoopingEvents = false;
			LoopNeed2End = true;
			//std::cout << "stun time over -> wait for animation to finish \n";
		}

		// make sure that we haven't gotten to the stun end event
		else
		{
			if (CheckEvent(Event2) == Event2Num)
			{
				EventEnd = animState->tracks[0]->trackTime;
				// assume that tracks[0] is the stun animation
				animState->tracks[0]->trackTime = EventStart;
				//std::cout << "stun end-> LOOOP \n";
			}
		}
	}

	else if (Timer != 0) // waiting for stun animation to finish
	{
		float currentTime = spTrackEntry_getAnimationTime(animState->tracks[0]);
		if (currentTime == animState->tracks[0]->animationEnd)
		{
			//we reset for the next time
			LoopNeed2End = false;
			Timer = 0;
			return true;//we finished
		}
	}
	return false;
	
	/*
	int algo = CheckEvent(Event1);
	if (algo == Event1Num && LoopingEvents == false)
	{
		LoopingEvents = true;
		Timer = 0;
		// get the time in the animation, where the stun start event is. 
		EventStart = spTrackEntry_getAnimationTime(animState->tracks[0]);

		//std::cout << "stun start " << spTrackEntry_getAnimationTime(rend->animState->tracks[0])  << "\n";
	}

	if (LoopingEvents)
	{

		//std::cout << "stun loop " << spTrackEntry_getAnimationTime(rend->animState->tracks[0]) << "\n";

		// update timer for the loop
		Timer += ImGui::GetIO().DeltaTime;

		// Stun loop is finished, we should exit the state
		// but first wait for animation to finish
		if ((Timer > TimeLooping))
		{
			LoopingEvents = false;
			//std::cout << "stun time over -> wait for animation to finish \n";
		}

		// make sure that we haven't gotten to the stun end event
		else
		{
			if (CheckEvent(Event2) == Event2Num)
			{
				// assume that tracks[0] is the stun animation
				animState->tracks[0]->trackTime = EventStart;
				//std::cout << "stun end-> LOOOP \n";
			}
		}
	}

	else if (Timer != 0) // waiting for stun animation to finish
	{
		float currentTime = spTrackEntry_getAnimationTime(animState->tracks[0]);
		if (currentTime == animState->tracks[0]->animationEnd)
		{
			return true;//we finished
		}
	}
	return false;
	*/
}

bool SkeletonRenderable::DoesThisEnd()
{
	return (animState->tracks[0]->animationEnd == spTrackEntry_getAnimationTime(animState->tracks[0]));
	/*
	if(eventData)
	if (eventData->name == s)
	{
	return eventData->intValue;
	}
	return -1;

	*/
}

void SkeletonRenderable::ClearAnimationData()
{
	//we dont want to bug the two events loop so here we initialize it again 
	//in case we dont end and the animation changes
	LoopingEvents = false;
	LoopNeed2End = false;
	Timer = 0;//just in case this one xddd
	EventEnd = 0;
}

void SkeletonRenderable::ToEndEventLoopBetweenEvents()
{
	if(EventEnd != 0)
		animState->tracks[0]->trackTime = EventEnd;
}
