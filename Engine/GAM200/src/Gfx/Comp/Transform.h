#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "../../Component/Component.h"

class Transform : public Component
{
public:
	glm::vec3 scale;
	float rotation;
	glm::vec3 position;

	glm::mat4 Rot;
	glm::mat4 Trans;
	glm::mat4 Sca;
	glm::mat4 transform;
	
	Transform();
	Transform(glm::vec3 translate, float rotate, glm::vec3 scaling);

	glm::mat4 GetTransform();

	virtual void Initialize();
	virtual void Update();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void FlipVertical();
	void FlipHorizontal();

	void FixY();
};

#endif