#ifndef LIGHT_H
#define LIGHT_H

#include "../../Component/Component.h"

class Light : public Component
{
public:

	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	glm::vec3 spotDirection;
	float spotCutoff;
	float spotCosPhi;
	float spotCosTheta;

	glm::vec3 color;

	Light();
	~Light();

	virtual void Initialize();
	virtual void Update();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

#endif