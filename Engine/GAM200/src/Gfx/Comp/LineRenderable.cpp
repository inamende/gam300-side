#include "LineRenderable.h"

#include "../Gfx.h"
#include "../../Gfx/Reso/Line.h"
#include "../../Gfx/Reso/Camera.h"
#include "../../Gfx/Reso/Shader.h"
#include "../../ResourceManager/ResoManager.h"

LineRenderable::LineRenderable() : Renderable::Renderable() {}

void LineRenderable::Initialize()
{
	shader = resoManager->getReso<Shader>("line");
	line = DBG_NEW Line;
	line->InitLine();

	layer = "default";
	visible = true;

	graphics->AddRenderable(this);
}

void LineRenderable::Update() {}

void LineRenderable::Render()
{ 
	glm::mat4 view = (graphics->GetCameraByRenderLayer(layer) != NULL) ? graphics->GetCameraByRenderLayer(layer)->getViewMatrix() : glm::mat4(1.0f);
	glm::mat4 proj = (graphics->GetCameraByRenderLayer(layer) != NULL) ? graphics->GetCameraByRenderLayer(layer)->getProjectionMatrix() : glm::mat4(1.0f);

	shader->use();
	shader->setUniform("V", view);
	shader->setUniform("P", proj);

	if (visible)
		line->Draw();	
}

void LineRenderable::Terminate()
{
	delete line;
}
