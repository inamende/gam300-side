#ifndef NORMAL_LIGHT_RENDERABLE_H
#define NORMAL_LIGHT_RENDERABLE_H

#include "../../Gfx/Comp/Renderable.h"

class Texture;

class NormalLightRenderable : public Renderable
{
public:

	Texture * normalTexture;

	NormalLightRenderable();

	virtual void Initialize();
	virtual void Update();
	virtual void Render();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};


#endif