#include "Transform.h"

#include <glm/gtc/matrix_transform.hpp>
#include "../../Editor/ImGuiEditor.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Actions/TransformAction.h"
#include "../../Actions/Actions.h"

std::vector<Action*> transformActions;

void Transform::Initialize() {}

void Transform::Update() {}

Transform::Transform()
{
	scale = glm::vec3(100.f, 100.f, 1.f);
	rotation = 0.0f;
	position = glm::vec3(0.f, 0.f, 0.f);

	glm::mat4 Translate = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, 0.f));
	glm::mat4 Rotate = glm::rotate(glm::mat4(1.f), 0.0f, glm::vec3(0.f, 0.f, 1.f));
	glm::mat4 Scale = glm::scale(glm::mat4(1.f), glm::vec3(1.f, 1.f, 1.f));
	transform = Translate * Rotate * Scale;
}

Transform::Transform(glm::vec3 translate, float rotate, glm::vec3 scaling)
{
	scale = scaling;
	rotation = rotate;
	position = translate;

	glm::mat4 Translate = glm::translate(glm::mat4(1.f), translate);
	glm::mat4 RotateZ = glm::rotate(glm::mat4(1.f), rotation, glm::vec3(0.f, 0.f, -1.f));
	glm::mat4 Scale = glm::scale(glm::mat4(1.f), scale);

	Rot = RotateZ;
	Trans = Translate;
	Sca = Scale;

	transform = Translate * RotateZ * Scale;
}

glm::mat4 Transform::GetTransform()
{
	glm::mat4 Translate = glm::translate(glm::mat4(1.f), position);
	glm::mat4 RotateZ = glm::rotate(glm::mat4(1.f), rotation, glm::vec3(0.f, 0.f, -1.f));
	glm::mat4 Scale = glm::scale(glm::mat4(1.f), scale);

	return Translate * RotateZ * Scale;
}

void Transform::load(Json::Value & val)
{
	position.x = val["Transform"]["position"].get("x", 0.0f).asFloat();
	position.y = val["Transform"]["position"].get("y", 0.0f).asFloat();
	position.z = val["Transform"]["position"].get("z", 0.0f).asFloat();

	scale.x = val["Transform"]["scale"].get("x", 100.0f).asFloat();
	scale.y = val["Transform"]["scale"].get("y", 100.0f).asFloat();
	scale.z = val["Transform"]["scale"].get("z", 1.0f).asFloat();

	rotation = val["Transform"].get("rotation", 0.0f).asFloat();
}

void Transform::save(Json::Value & val)
{
	val["Transform"]["position"]["x"] = position.x;
	val["Transform"]["position"]["y"] = position.y;
	val["Transform"]["position"]["z"] = position.z;

	val["Transform"]["scale"]["x"] = scale.x;
	val["Transform"]["scale"]["y"] = scale.y;
	val["Transform"]["scale"]["z"] = scale.z;

	val["Transform"]["rotation"] = rotation;
}

void Transform::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Transform"))
	{
		ImGui::Text("Translate :");

		float x = position.x;
		if (ImGui::InputFloat("x##Pos", &x))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->position.x = x;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}
			
		float y = position.y;
		if (ImGui::InputFloat("y##Pos", &y))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->position.y = y;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}

		float z = position.z;
		if (ImGui::InputFloat("z##Pos", &z))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->position.z = z;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}

		ImGui::NewLine();
		ImGui::Text("Scale :");

		float sx = scale.x;
		if (ImGui::InputFloat("x##Scl", &sx))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->scale.x = sx;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}

		float sy = scale.y;
		if (ImGui::InputFloat("y##Scl", &sy))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->scale.y = sy;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}

		float sz = scale.z;
		if (ImGui::InputFloat("z##Scl", &sz))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->scale.z = sz;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}

		ImGui::NewLine();
		
		float ro = rotation;
		if (ImGui::InputFloat("rotate##Transform", &ro))
		{
			transformActions.clear();
			for (auto it : imGuiEditor->selectedObjs)
			{
				TranslateAction * move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.push_back(move);
				it->mTransform->rotation = ro;
				move = DBG_NEW TranslateAction(it, it->mTransform);
				transformActions.insert(transformActions.begin(), move);
			}
			if (!transformActions.empty())
				mActions->PushAction(transformActions);
		}

		ImGui::NewLine();

		static bool vFlip = false;
		if (ImGui::Checkbox("Flip Vertical##Transform", &vFlip))
			FlipVertical();

		static bool hFlip = false;
		if (ImGui::Checkbox("Flip Horizontal##Transform", &hFlip))
			FlipHorizontal();

		if (ImGui::Button("Fix Y Position"))
			FixY();

		if (ImGui::Button("Half Scale"))
		{
			scale.x *= 0.5f; scale.y *= 0.5f;
		}
	}
}

void Transform::FlipVertical() { scale.x = -scale.x; }

void Transform::FlipHorizontal() { scale.y = -scale.y; }

void Transform::FixY()
{
	int fixer = 10;

	int fixY = round(position.y / fixer);
	int final = fixY * fixer;

	position.y = final;
}


