#include "NormalLightRenderable.h"

#include "../Gfx.h"
#include "../../ObjectManager/GameObject.h"
#include "../../Gfx/Comp/Light.h"
#include "../../Gfx/Comp/Transform.h"
#include "../../Gfx/Reso/Shader.h"
#include "../../Gfx/Reso/Texture.h"
#include "../../Gfx/Reso/Sprite.h"
#include "../../Editor/ImGuiEditor.h"

NormalLightRenderable::NormalLightRenderable() : Renderable() { normalTexture = NULL; }

void NormalLightRenderable::Initialize()
{
	Renderable::renderableTransform = static_cast<GameObject*>(GetOwner())->mTransform;

	graphics->AddRenderable(this);
}

void NormalLightRenderable::Update() {}


void NormalLightRenderable::Render()
{
	glm::mat4 M = renderableTransform->GetTransform();

	if (shader)
	{
		shader->setUniform("M", M);
		shader->setUniform("lightNum", (int)Gfx::getInstance()->lightNum);

		for (unsigned i = 0; i < Gfx::getInstance()->lightNum; i++)
		{
			shader->setUniform(("light[" + std::to_string(i) + "].ambient").c_str(), graphics->lights[i]->ambient);
			shader->setUniform(("light[" + std::to_string(i) + "].diffuse").c_str(), graphics->lights[i]->diffuse);
			shader->setUniform(("light[" + std::to_string(i) + "].specular").c_str(), graphics->lights[i]->specular);
			shader->setUniform(("light[" + std::to_string(i) + "].lightPosition").c_str(), graphics->lights[i]->GetOwner()->mTransform->position);
			shader->setUniform(("light[" + std::to_string(i) + "].spotDirection").c_str(), graphics->lights[i]->spotDirection);
			shader->setUniform(("light[" + std::to_string(i) + "].spotCosPhi").c_str(), graphics->lights[i]->spotCosPhi);
			shader->setUniform(("light[" + std::to_string(i) + "].spotCosTheta").c_str(), graphics->lights[i]->spotCosTheta);
			shader->setUniform(("light[" + std::to_string(i) + "].lightColor").c_str(), graphics->lights[i]->color);
		}

		if (texture)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture->texHandle);
		}

		shader->setUniform("textureData", 0);

		if (normalTexture)
		{
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, normalTexture->texHandle);
		}


		shader->setUniform("normalData", 1);

		if (visible)
			if (sprite)
				sprite->Draw();
	}
}

//SERIALIZER
void NormalLightRenderable::load(Json::Value & val)
{
	normalTexture = (resoManager->getReso<Texture>(val["NormalLightRenderable"].get("normal_texture", "Set_Normal_Texture_Please").asString()) != NULL) ?
		resoManager->getReso<Texture>(val["NormalLightRenderable"].get("normal_texture", "Set_Normal_Texture_Please").asString()) :
		graphics->GetDefaultTexture();

	texture = (resoManager->getReso<Texture>(val["NormalLightRenderable"].get("texture", "Set_Texture_Please").asString()) != NULL) ?
		resoManager->getReso<Texture>(val["NormalLightRenderable"].get("texture", "Set_Texture_Please").asString()) :
		graphics->GetDefaultTexture();

	sprite = (resoManager->getReso<Sprite>(val["NormalLightRenderable"].get("model", "Set_Model_Please").asString()) != NULL) ?
		resoManager->getReso<Sprite>(val["NormalLightRenderable"].get("model", "Set_Model_Please").asString()) :
		graphics->GetDefaultSprite();

	shader = (resoManager->getReso<Shader>(val["NormalLightRenderable"].get("shader", "Set_Shader_Please").asString()) != NULL) ?
		resoManager->getReso<Shader>(val["NormalLightRenderable"].get("shader", "Set_Shader_Please").asString()) :
		graphics->GetDefaultShader();

	visible = val["NormalLightRenderable"].get("visible", true).asBool();
	layer = val["NormalLightRenderable"].get("layer", "Set_Layer_Please").asString();
}

void NormalLightRenderable::save(Json::Value & val)
{
	if (shader)  val["NormalLightRenderable"]["shader"] = resoManager->getResoName(shader);
	if (sprite) val["NormalLightRenderable"]["model"] = resoManager->getResoName(sprite);
	if (texture) val["NormalLightRenderable"]["texture"] = resoManager->getResoName(texture);
	if (normalTexture) val["NormalLightRenderable"]["normal_texture"] = resoManager->getResoName(normalTexture);
	val["NormalLightRenderable"]["visible"] = visible;
	val["NormalLightRenderable"]["layer"] = layer;
}

void NormalLightRenderable::WriteToImguiWindow()
{
	static int selected_texture = 0;
	static int selected_normal = 0;
	static int selected_shader = 0;
	static int selected_sprite = 0;

	for (unsigned i = 0; i < imGuiEditor->textureNames.size(); i++)
	{
		if (texture)
			if (resoManager->getResoName(texture) == imGuiEditor->textureNames[i])
				selected_texture = i;
	}

	if (ImGui::CollapsingHeader("NormalLightRenderable"))
	{
		if (ImGui::Button("Texture##NormalLightRenderable"))
			ImGui::OpenPopup("textures");

		ImGui::SameLine();
		if (texture) ImGui::Text(resoManager->getResoName(texture).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Normal Map##NormalLightRenderable"))
			ImGui::OpenPopup("normals");

		ImGui::SameLine();
		if (normalTexture) ImGui::Text(resoManager->getResoName(normalTexture).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Shader##NormalLightRenderable"))
			ImGui::OpenPopup("shaders");

		ImGui::SameLine();
		if (shader) ImGui::Text(resoManager->getResoName(shader).c_str());
		else ImGui::Text("NULL");

		if (ImGui::Button("Sprite##NormalLightRenderable"))
			ImGui::OpenPopup("sprites");

		ImGui::SameLine();
		if (sprite) ImGui::Text(resoManager->getResoName(sprite).c_str());
		else ImGui::Text("NULL");

		ImGui::Checkbox("visible##NormalLightRenderable", &visible);

		if(sprite != NULL) ImGui::Checkbox("wire frame##NormalLightRenderable", &sprite->wireFrame);

		ImGui::NewLine();
		static bool vFlip = false;
		if (ImGui::Checkbox("Flip Vertical##NormalLightRenderable", &vFlip))
			sprite->FlipVertical();

		static bool hFlip = false;
		if (ImGui::Checkbox("Flip Horizontal##NormalLightRenderable", &hFlip))
			sprite->FlipHorizontal();

		ImGui::NewLine();
		if (ImGui::Button("Detach##NormalLightRenderable"))
			GetOwner()->detach(this);
	}

	if (ImGui::BeginPopup("textures"))
	{
		ImGui::Text("Textures");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->textureNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->textureNames[i].c_str()))
				selected_texture = i;

		texture = resoManager->getReso<Texture>(imGuiEditor->textureNames[selected_texture]);

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("normals"))
	{
		ImGui::Text("Normal Maps");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->textureNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->textureNames[i].c_str()))
			{
				selected_normal = i;
				normalTexture = resoManager->getReso<Texture>(imGuiEditor->textureNames[selected_normal]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("shaders"))
	{
		ImGui::Text("Shaders");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->shaderNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->shaderNames[i].c_str()))
			{
				selected_shader = i;
				shader = resoManager->getReso<Shader>(imGuiEditor->shaderNames[selected_shader]);
			}

		ImGui::EndPopup();
	}

	if (ImGui::BeginPopup("sprites"))
	{
		ImGui::Text("Sprites");
		ImGui::Separator();

		for (unsigned i = 0; i < imGuiEditor->spriteNames.size(); i++)
			if (ImGui::Selectable(imGuiEditor->spriteNames[i].c_str()))
			{
				selected_sprite = i;
				sprite = resoManager->getReso<Sprite>(imGuiEditor->spriteNames[selected_sprite]);
			}

		ImGui::EndPopup();
	}


}