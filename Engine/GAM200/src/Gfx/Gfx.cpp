#include "Gfx.h"

#include "../Gfx/Comp/LineRenderable.h"
#include "../Gfx/Comp/Renderable.h"
#include "../Gfx/Comp/FontRenderable.h"
#include "../Gfx/Comp/NormalLightRenderable.h"
#include "../Gfx/Comp/SkeletonRenderable.h"
#include "../Gfx/Comp/Animator.h"
#include "../Gfx/Comp/Light.h"

#include "../Gfx/Reso/Camera.h"
#include "../Gfx/Reso/RenderLayer.h"
#include "../Gfx/Reso/Font.h"
#include "../Gfx/Reso/AnimationData.h"
#include "../Gfx/Reso/AtlasData.h"
#include "../Gfx/Reso/Line.h"
#include "../Gfx/Reso/Sprite.h"
#include "../Gfx/Reso/Shader.h"
#include "../Gfx/Reso/TileMap.h"
#include "../Gfx/Reso/Texture.h"
#include "../Gfx/Reso/Particle.h"
#include "../Gfx/Window.h"

#include "../Factory/Factory.h"
#include "../Editor/ImGuiEditor.h"

#include "../Input/Input.h"

#include <windows.h>
#include <glm/gtc/matrix_transform.hpp>

Gfx * Gfx::getInstance()
{
	if(instance == NULL)
		instance = DBG_NEW Gfx();

	return instance;
}

Gfx::Gfx() : lightNum(0), debugRendering(true), postprocessor(nullptr), global_ambient_lighting(vec4(1.f))
{
	mainCamera = NULL;
	lineDraw = DBG_NEW LineRenderable;
}

Gfx::~Gfx()
{
	renderLayers.clear();
}

void Gfx::Initialize()
{
	mFactory->Register<Renderable>();
	mFactory->Register<NormalLightRenderable>();
	mFactory->Register<SkeletonRenderable>();
	mFactory->Register<FontRenderable>();
	mFactory->Register<Light>();
	mFactory->Register<Animator>();
	mFactory->Register<TileMap>();
	mFactory->Register<ParticleEmiter>();

	//DEFAULT RESOURCES   DO NOT DELETE
	//Texture *ls_fr_0 = DBG_NEW Texture, *ls_fr_1 = DBG_NEW Texture;
	Texture *ls[12] = { DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, DBG_NEW Texture, };
	resoManager->AddReso(ls[0], "l1.png", "./Data/Texture/l1.png");
	resoManager->AddReso(ls[1], "l2.png", "./Data/Texture/l2.png");
	resoManager->AddReso(ls[2], "l3.png", "./Data/Texture/l3.png");
	resoManager->AddReso(ls[3], "l4.png", "./Data/Texture/l4.png");
	resoManager->AddReso(ls[4], "l5.png", "./Data/Texture/l5.png");
	resoManager->AddReso(ls[5], "l6.png", "./Data/Texture/l6.png");
	resoManager->AddReso(ls[6], "l7.png", "./Data/Texture/l7.png");
	resoManager->AddReso(ls[7], "l8.png", "./Data/Texture/l8.png");
	resoManager->AddReso(ls[8], "l9.png", "./Data/Texture/l9.png");
	resoManager->AddReso(ls[9], "l10.png", "./Data/Texture/l10.png");
	resoManager->AddReso(ls[10], "l11.png", "./Data/Texture/l11.png");
	resoManager->AddReso(ls[11], "l12.png", "./Data/Texture/l12.png");

	resoManager->AddReso(DBG_NEW Texture, "color_map.png", "./Data/Texture/color_map.png");
	resoManager->AddReso(DBG_NEW Texture, "digipen.png", "./Data/Texture/digipen.png");
	resoManager->AddReso(DBG_NEW Texture, "black_duck.png", "./Data/Texture/black_duck.png");
	resoManager->AddReso(DBG_NEW Sprite, "plane.obj", "./Data/Meshes/plane.obj");
	resoManager->AddReso(DBG_NEW Shader, "basic", "./Data/Shaders/basic");
	resoManager->AddReso(DBG_NEW Font, "alterebro-pixel-font.ttf", "./Data/Texture/alterebro-pixel-font.ttf");
	resoManager->AddReso(DBG_NEW Font, "MorrisRomanBlack.ttf", "./Data/Texture/MorrisRomanBlack.ttf");
	imGuiEditor->textureNames.push_back("color_map.png");
	imGuiEditor->spriteNames.push_back("plane.obj");


	imGuiEditor->enemytypeNames.push_back("Attacker");
	imGuiEditor->enemytypeNames.push_back("BoxMover");
	imGuiEditor->enemytypeNames.push_back("Undertaker");

	ls_cam = DBG_NEW Camera;
	ls_rend = DBG_NEW Renderable;
	ls_sh = GetDefaultShader();

	ls_anim.push_back(ls[0]);
	ls_anim.push_back(ls[1]);
	ls_anim.push_back(ls[2]);
	ls_anim.push_back(ls[3]);
	ls_anim.push_back(ls[4]);
	ls_anim.push_back(ls[5]);
	ls_anim.push_back(ls[6]);
	ls_anim.push_back(ls[7]);
	ls_anim.push_back(ls[8]);
	ls_anim.push_back(ls[9]);
	ls_anim.push_back(ls[10]);
	ls_anim.push_back(ls[11]);

	LoadResources();

	lineDraw->Initialize();

	Load();

	postprocessor = DBG_NEW Postprocessor(resoManager->getReso<Shader>("postprocess"), resoManager->getReso<Sprite>("post_plane.obj"), mWindow->width, mWindow->height);
}

void Gfx::Update()
{
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE); //lol

	for (std::vector<RenderLayer*>::iterator layer = renderLayers.begin(); layer != renderLayers.end(); layer++) {
		(*layer)->postprocess.BeginRender();
		(*layer)->SortRenderables();
		(*layer)->Render();
	}
	postprocessor->BeginRender();
	for (std::vector<RenderLayer*>::iterator layer = renderLayers.begin(); layer != renderLayers.end(); layer++) {
		(*layer)->postprocess.Render();
	}

	postprocessor->EndRender();
	postprocessor->Render();

	// thats a lot of postprocessing

	if (debugRendering) { lineDraw->Update(); lineDraw->Render(); }

	for (unsigned i = 0; i < lightNum; i++)
		lights[i]->Update();
}

void Gfx::Terminate()
{
	Save();

	lineDraw->Terminate();
	delete lineDraw;

	for (std::vector<RenderLayer*>::iterator layer = renderLayers.begin(); layer != renderLayers.end(); layer++)
		delete (*layer);

	delete ls_cam;
	delete ls_rend;
	delete postprocessor;
	delete instance;
}

void Gfx::Load()
{
	std::ifstream jsonFile(editor_loader);
	Json::Reader reader;
	bool ok = reader.parse(jsonFile, commonValue);
	//if (!ok)
	//	std::cout << "ERROR READING JSON" << std::endl;

	debugRendering = commonValue.get("debug_rendering", true).asBool();

	jsonFile.close();
}

void Gfx::LoadLayers() {
	std::ifstream jsonFile(editor_loader);
	Json::Reader reader;
	bool ok = reader.parse(jsonFile, commonValue);
	//if (!ok)
	//	std::cout << "ERROR READING JSON" << std::endl;

//#ifndef EDITOR
		postprocessor->postVar.brightness = commonValue.get("brightness", 0.0f).asFloat();
//#endif
	for (auto layer : renderLayers) {
		layer->postprocess.postVar.hue = commonValue["layer_filters"][layer->GetName()].get("hue", 0.0f).asFloat();
		layer->postprocess.postVar.saturation = commonValue["layer_filters"][layer->GetName()].get("sat", 0.0f).asFloat();
		layer->postprocess.postVar.brightness = commonValue["layer_filters"][layer->GetName()].get("bri", 0.0f).asFloat();
		layer->postprocess.postVar.color.r = commonValue["layer_filters"][layer->GetName()]["col"].get("r", 1.0f).asFloat();
		layer->postprocess.postVar.color.g = commonValue["layer_filters"][layer->GetName()]["col"].get("g", 1.0f).asFloat();
		layer->postprocess.postVar.color.b = commonValue["layer_filters"][layer->GetName()]["col"].get("b", 1.0f).asFloat();
		layer->postprocess.postVar.density = commonValue["layer_filters"][layer->GetName()].get("den", 0.0f).asFloat();
	}

	jsonFile.close();
}

void Gfx::Save()
{
#ifdef EDITOR
	commonValue["debug_rendering"] = debugRendering;

	for (auto layer : renderLayers) {
		commonValue["layer_filters"][layer->GetName()]["hue"] = layer->postprocess.postVar.hue;
		commonValue["layer_filters"][layer->GetName()]["sat"] = layer->postprocess.postVar.saturation;
		commonValue["layer_filters"][layer->GetName()]["bri"] = layer->postprocess.postVar.brightness;
		commonValue["layer_filters"][layer->GetName()]["col"]["r"] = layer->postprocess.postVar.color.r;
		commonValue["layer_filters"][layer->GetName()]["col"]["g"] = layer->postprocess.postVar.color.g;
		commonValue["layer_filters"][layer->GetName()]["col"]["b"] = layer->postprocess.postVar.color.b;
		commonValue["layer_filters"][layer->GetName()]["den"] = layer->postprocess.postVar.density;
	}

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();
	std::ofstream jsonFileWrite(editor_loader);
	writer->write(commonValue, &jsonFileWrite);
	jsonFileWrite.close();
#endif
}

void Gfx::AddRenderable(Renderable * rend)
{
	RenderLayer * layer = GetRenderLayer(rend->layer);

	if (layer)
		layer->AddRenderable(rend);
}

void Gfx::AddCamera(Camera * cam, std::string layerName)
{
	RenderLayer * layer = GetRenderLayer(layerName);

	cameras.push_back(cam);

	cam->renderLayerName = layerName;
	layer->AddCamera(cam);
}

void Gfx::AddRenderLayer(std::string layerName)
{
	renderLayers.push_back(DBG_NEW RenderLayer(layerName));
	imGuiEditor->layerNames.push_back(layerName);
}

void Gfx::AddAnimation(AnimationData * animation)
{
	auto find = std::find_if(animations.begin(), animations.end(), [&](AnimationData * anim) { return anim == animation; });
	if (find == animations.end()) animations.push_back(animation);
}

void Gfx::AddAtlas(AtlasData * atlas)
{
	auto find = std::find_if(atlases.begin(), atlases.end(), [&](AtlasData * at) { return at == atlas; });
	if (find == atlases.end()) atlases.push_back(atlas);
}

void Gfx::AddTileMap(TileMap * tilemap)
{
	auto finder = std::find_if(tileMaps.begin(), tileMaps.end(), [&](TileMap * t) { return t == tilemap; });
	if (finder == tileMaps.end()) tileMaps.push_back(tilemap);
}

RenderLayer * Gfx::GetRenderLayer(std::string name)
{
	auto finder = std::find_if(renderLayers.begin(), renderLayers.end(), [&](RenderLayer * render) { return render->GetName() == name; });
	return (finder != renderLayers.end()) ? reinterpret_cast<RenderLayer*>(*finder) : NULL;
}

Camera * Gfx::FindCamera(std::string name)
{
	auto finder = std::find_if(cameras.begin(), cameras.end(), [&](Camera * cam) { return cam->GetName() == name; });
	return (finder != cameras.end()) ? reinterpret_cast<Camera*>(*finder) : NULL;
}

Camera * Gfx::GetCameraByRenderLayer(std::string name)
{
	auto finder = std::find_if(cameras.begin(), cameras.end(), [&](Camera * cam) { return cam->renderLayerName == name; });
	return (finder != cameras.end()) ? reinterpret_cast<Camera*>(*finder) : NULL;
}

void Gfx::SetMainCamera(std::string name)
{
	auto finder = std::find_if(cameras.begin(), cameras.end(), [&](Camera * cam) { return cam->GetName() == name; });
	if (finder != cameras.end()) mainCamera = (*finder);
}

void Gfx::SetMainCamera(Camera * camera)
{
	if (camera != NULL)
		mainCamera = camera;
	else
		mainCamera = NULL;
}

Camera * Gfx::GetMainCamera()
{
	return mainCamera;
}

void Gfx::Flush(std::string layer)
{
	lineDraw->layer = layer;
}

void Gfx::DrawALine(glm::vec3 p0, glm::vec3 p1, glm::vec4 color)
{
	lineDraw->line->DrawALine(p0, p1, color);
}

void Gfx::DrawABox(glm::vec3 pos, float width, float height, glm::vec4 color)
{
	// p0 ____ p1
	//  |      |
	//  |      |
	// p2 ____ p3

	glm::vec3 horizontal(width, 0.0f, 0.0f);
	glm::vec3 vertical(0.0f, height, 0.0f);

	glm::vec3 p0 = pos - horizontal + vertical;
	glm::vec3 p1 = pos + horizontal + vertical;
	glm::vec3 p2 = pos - horizontal - vertical;
	glm::vec3 p3 = pos + horizontal - vertical;

	DrawALine(p0, p1, color);
	DrawALine(p1, p3, color);
	DrawALine(p3, p2, color);
	DrawALine(p2, p0, color);
}

void Gfx::DrawABoxOriented(glm::vec3 pos, float width, float height, float rotation, glm::vec4 color)
{
	glm::mat3 Rot = glm::rotate(glm::mat4(1.f), rotation, glm::vec3(0.f, 0.f, -1.f));

	glm::vec3 rotHor = Rot * glm::vec3(width, 0.0f, 0.0f);
	glm::vec3 rotVer = Rot * glm::vec3(0.0f, height, 0.0f);

	glm::vec3 p0 = pos - rotHor + rotVer;
	glm::vec3 p1 = pos + rotHor + rotVer;
	glm::vec3 p2 = pos - rotHor - rotVer;
	glm::vec3 p3 = pos + rotHor - rotVer;

	DrawALine(p0, p1, color);
	DrawALine(p1, p3, color);
	DrawALine(p3, p2, color);
	DrawALine(p2, p0, color);
}

void Gfx::DrawACircle(glm::vec3 pos, float radius, int precission, glm::vec4 color)
{
	glm::vec3 prev = glm::vec3();
	glm::vec3 curr = glm::vec3();
	glm::vec3 star = glm::vec3();

	for (unsigned i = 0; i < precission; i++)
	{
		float angle = 2 * M_PI * i / precission;
		glm::vec3 p0(pos.x + radius * cos(angle), pos.y + radius * sin(angle), pos.z);
		if (i == 0)
		{
			prev = p0;
			star = p0;
		}
		curr = p0;
		DrawALine(prev, curr, color);
		prev = curr;
	}

	DrawALine(curr, star, color);
}

void Gfx::SortRenderables()
{
	for (std::vector<RenderLayer*>::iterator layer = renderLayers.begin(); layer != renderLayers.end(); layer++)
		(*layer)->SortRenderables();
}

Light * Gfx::AddLight(Light * light)
{
	if (lightNum > MAX_LIGHTS)
	{
		//std::cout << "MAX NUMBER OF LIGHTS REACHED" << std::endl;
		return NULL;
	}

	if (light == NULL)
		return NULL;

	auto finder = std::find_if(lights.begin(), lights.end(), [&](Light * lit) { return lit == light; });

	if (finder == lights.end())
	{
		lights.push_back(light);
		lightNum++;
		return light;
	}

	return NULL;
}

Light * Gfx::FindLight(std::string name)
{
	auto finder = std::find_if(lights.begin(), lights.end(), [&](Light * lit) { return lit->GetName() == name; });
	return (finder != lights.end()) ? reinterpret_cast<Light*>(*finder) : NULL;
}

AnimationData * Gfx::FindAnimation(std::string name)
{
	auto finder = std::find_if(animations.begin(), animations.end(), [&](AnimationData * anim) { return anim->GetName() == name; });
	return (finder != animations.end()) ? reinterpret_cast<AnimationData*>(*finder) : NULL;
}

AtlasData * Gfx::FindAtlas(std::string name)
{
	auto finder = std::find_if(atlases.begin(), atlases.end(), [&](AtlasData * at) { return at->GetName() == name; });
	return (finder != atlases.end()) ? reinterpret_cast<AtlasData*>(*finder) : NULL;
}

void Gfx::RemoveRenderable(Renderable * rend)
{
	RenderLayer * layer = GetRenderLayer(rend->layer);

	if (layer)
		layer->RemoveRenderable(rend);
}

void Gfx::RemoveLight(Light * light)
{
	if (light == NULL)
		return;

	lights.erase(std::remove(lights.begin(), lights.end(), light), lights.end());
	lightNum--;
}

void Gfx::RemoveTileMap(TileMap * tilemap)
{
	if (tilemap == NULL)
		return;

	tileMaps.erase(std::remove(tileMaps.begin(), tileMaps.end(), tilemap), tileMaps.end());
}

void Gfx::RemoveAtlas(AtlasData * atlas)
{
	if (atlas == NULL)
		return;

	atlases.erase(std::remove(atlases.begin(), atlases.end(), atlas), atlases.end());
}

void Gfx::ResetPostprocess(unsigned w, unsigned h)
{
	if (postprocessor)
		postprocessor->Reset(w, h);
}

Texture * Gfx::GetDefaultTexture()
{
	return resoManager->getReso<Texture>("color_map.png");
}

Shader * Gfx::GetDefaultShader()
{
	return resoManager->getReso<Shader>("basic");
}

Sprite * Gfx::GetDefaultSprite()
{
	return resoManager->getReso<Sprite>("plane.obj");
}

Font * Gfx::GetDefaultFont()
{
	return resoManager->getReso<Font>("alterebro");
}

void Gfx::LoadTexturesFromTxtFile(std::string filePath)
{
	std::ifstream file(filePath);

	std::string line;
	while (std::getline(file, line))
	{
		if (line[0] != 0)
		{
			std::string fullFilePath = "./Data/Texture/" + line;

			resoManager->AddReso(DBG_NEW Texture, line, fullFilePath.c_str());
			imGuiEditor->textureNames.push_back(line);

			// check if this texture has metadata
			size_t lastindex = line.find_last_of(".");
			std::string noExtension = line.substr(0, lastindex);
			
			std::string path = "./Data/Texture/" + noExtension + ".json";
			resoManager->AddReso(DBG_NEW AtlasData, noExtension, path.c_str());
			static float dt = 0;
			if (dt == ls_anim.size()) {
				dt = 0;
			}
			RenderAnimatedLoadingScreen(ls_cam, ls_sh, ls_rend, ls_anim, (int)dt);
			dt += 0.5;
		}
	}

	file.close();
}

void Gfx::LoadShadersFromFile(std::string filePath)
{
	std::vector<std::string> shaders = GetFiles(filePath);

	for (auto it : shaders)
	{
		size_t lastindex = it.find_last_of(".");
		std::string noExtension = it.substr(0, lastindex);
		std::string fullFilePath = filePath + noExtension;

		resoManager->AddReso(DBG_NEW Shader, noExtension, fullFilePath.c_str());
		imGuiEditor->shaderNames.push_back(noExtension);

		// no duplicates please
		std::sort(imGuiEditor->shaderNames.begin(), imGuiEditor->shaderNames.end());
		imGuiEditor->shaderNames.erase(std::unique(imGuiEditor->shaderNames.begin(), imGuiEditor->shaderNames.end()), imGuiEditor->shaderNames.end());
	}
}

void Gfx::LoadSpritesFromFile(std::string filePath)
{
	std::vector<std::string> sprites = GetFiles(filePath);

	for (auto it : sprites)
	{
		std::string fullFilePath = filePath + it;

		resoManager->AddReso(DBG_NEW Shader, it, fullFilePath.c_str());
		imGuiEditor->spriteNames.push_back(it);
	}
}

void Gfx::LoadAnimationsFromFile(std::string filePath)
{
	std::vector<std::string> anim = GetFiles(filePath);

	for (auto it : anim)
	{
		std::string fullFilePath = filePath + it;

		resoManager->AddReso(DBG_NEW AnimationData, it, fullFilePath.c_str());
		imGuiEditor->animationNames.push_back(it);
	}
}

std::vector<std::string> Gfx::GetFiles(std::string filePath)
{
	std::vector<std::string> files;
	std::string file = filePath + "//*";

	WIN32_FIND_DATA data;
	HANDLE handle;

	std::wstring temp = std::wstring(file.begin(), file.end());
	LPCWSTR lpcFile = temp.c_str();

	if ((handle = FindFirstFile(lpcFile, &data)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (data.cFileName[0] != '.') //ignore parent directories
			{
				std::wstring tempstr = data.cFileName;
				files.push_back(std::string(tempstr.begin(), tempstr.end()));
			}

		} while (FindNextFile(handle, &data) != 0);
		FindClose(handle);
	}

	return files;
}

void Gfx::WriteToImguiWindow()
{
	static int currentTileMap = 0;

	if (ImGui::CollapsingHeader("Gfx"))
	{
		ImGui::Checkbox("Debug Rendering", &debugRendering);
		ImGui::NewLine();
		ImGui::Text("TileMaps : %d", tileMaps.size()); ImGui::NewLine();

		std::vector<std::string> tilestr;

		for (auto it = tileMaps.begin(); it != tileMaps.end(); it++)
			tilestr.push_back((*it)->GetOwner()->GetName());

		if (ImGuiEditor::ListBox("tilemap list", &currentTileMap, tilestr) && !tilestr.empty())
		{
			imGuiEditor->editState = ImGuiEditor::EditState::TileMapPlease;
			imGuiEditor->showTileMapWindow = true;
			imGuiEditor->selectedTileMap = tileMaps[currentTileMap];
		}
		ImGui::NewLine();
		ImGui::SliderFloat("ambient light r", &global_ambient_lighting.x, 0.f, 1.f);
		ImGui::SliderFloat("ambient light g", &global_ambient_lighting.y, 0.f, 1.f);
		ImGui::SliderFloat("ambient light b", &global_ambient_lighting.z, 0.f, 1.f);
		ImGui::NewLine();
		// similar to photoshop (the boundaries)
		if (ImGui::CollapsingHeader("Global Postprocessing")) {
			ImGui::SliderFloat("Hue Global", &postprocessor->postVar.hue, -180.f, 180.f);
			ImGui::SliderFloat("Saturation Global", &postprocessor->postVar.saturation, -100.f, 100.f); // 0 to 4; default 1 was nice
			ImGui::SliderFloat("Brightness Global", &postprocessor->postVar.brightness, -100.f, 100.f);
			ImGui::NewLine();
			ImGuiEditor::ColorEdit3("Overlay Color Global", postprocessor->postVar.color);
			ImGui::SliderFloat("Density Global", &postprocessor->postVar.density, 0.f, 1.f);

			if (ImGui::Button("Reset Global"))
			{
				postprocessor->postVar.hue = postprocessor->postVar.saturation = postprocessor->postVar.brightness = postprocessor->postVar.density = 0.0f;
			}
		}

		for (auto layer : renderLayers) {
			std::string header = layer->GetName();
			if (ImGui::CollapsingHeader(header.c_str())) {
				std::string hue_ed = "Hue##" + layer->GetName();
				std::string sat_ed = "Saturation##" + layer->GetName();
				std::string bri_ed = "Brightness##" + layer->GetName();
				std::string ov_ed = "Overlay Color##" + layer->GetName();
				std::string den_ed = "Density##" + layer->GetName();
				std::string res_ed = "Reset##" + layer->GetName();

				ImGui::SliderFloat(hue_ed.c_str(), &layer->postprocess.postVar.hue, -180.f, 180.f);
				ImGui::SliderFloat(sat_ed.c_str(), &layer->postprocess.postVar.saturation, -100.f, 100.f); // 0 to 4; default 1 was nice
				ImGui::SliderFloat(bri_ed.c_str(), &layer->postprocess.postVar.brightness, -100.f, 100.f);
				ImGui::NewLine();
				ImGuiEditor::ColorEdit3(ov_ed.c_str(), layer->postprocess.postVar.color);
				ImGui::SliderFloat(den_ed.c_str(), &layer->postprocess.postVar.density, 0.f, 1.f);

				if (ImGui::Button(res_ed.c_str()))
				{
					layer->postprocess.postVar.hue = layer->postprocess.postVar.saturation = layer->postprocess.postVar.brightness = layer->postprocess.postVar.density = 0.0f;
				}
			}
		}
		ImGui::NewLine();
		tilestr.clear();
	}
}

void Gfx::LoadResources()
{
	imGuiEditor->fontNames.push_back("alterebro-pixel-font.ttf");
	imGuiEditor->fontNames.push_back("MorrisRomanBlack.ttf");

	resoManager->AddReso(DBG_NEW Sprite, "post_plane.obj", "./Data/Meshes/post_plane.obj");
	resoManager->AddReso(DBG_NEW Sprite, "sp.obj", "./Data/Meshes/plane.obj");

	LoadShadersFromFile("./Data/Shaders/");
	LoadTexturesFromTxtFile("./Data/Load/TextureLoader.txt");
	LoadAnimationsFromFile("./Data/Animations/");
}

void Gfx::RenderLoadingScreen(Camera * cam, Shader * shad, Renderable * rend)
{
	glm::mat4 view = cam->getViewMatrix();
	glm::mat4 proj = cam->getProjectionMatrix();

	glViewport(-(GLint)(cam->width / 2), (GLint)(cam->width / 2), -(GLint)(cam->height / 2), (GLint)(cam->height / 2));

	shad = GetDefaultShader();
	shad->use();
	shad->setUniform("V", view);
	shad->setUniform("P", proj);

	rend->shader = shad;
	rend->sprite = GetDefaultSprite();
	rend->texture = resoManager->getReso<Texture>("loading.png");
	if (rend->texture == NULL)
		rend->texture = GetDefaultTexture();
	rend->renderableTransform = DBG_NEW Transform(glm::vec3(), 0.f, glm::vec3(cam->width, cam->height, 1));

	glm::vec4 buff(0.f, 0.f, 0.f, 1.0f);

	glClearColor(buff.x, buff.y, buff.z, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	rend->Render();

	SDL_GL_SwapWindow(mWindow->windowHandle);
}

void Gfx::RenderAnimatedLoadingScreen(Camera * cam, Shader * shad, Renderable * rend, std::vector<Texture*> tex, int dt) {
	static glm::mat4 view = cam->getViewMatrix();
	static glm::mat4 proj = cam->getProjectionMatrix();

	glViewport(-(GLint)(cam->width / 2), (GLint)(cam->width / 2), -(GLint)(cam->height / 2), (GLint)(cam->height / 2));

	shad->use();
	shad->setUniform("V", view);
	shad->setUniform("P", proj);

	rend->shader = shad;
	rend->sprite = GetDefaultSprite();

	if (!tex.empty()) {
		rend->texture = tex[dt];
	}
	
	if (rend->texture == NULL)
		rend->texture = GetDefaultTexture();
	if (!rend->renderableTransform)
		rend->renderableTransform = DBG_NEW Transform(glm::vec3(610, -260, 0), 0.f, glm::vec3(400, 400, 1));

	glm::vec4 buff(0.f, 0.f, 0.f, 1.0f);
	glClearColor(buff.x, buff.y, buff.z, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	rend->Render();

	SDL_GL_SwapWindow(mWindow->windowHandle);
}

bool Gfx::FadeIn(float min, float dt) {
	if (postprocessor->postVar.brightness > min) {
		postprocessor->postVar.brightness -= dt;
		return false;
	}
	return true;
}

bool Gfx::FadeOut(float max, float dt) {
	if (postprocessor->postVar.brightness < max) {
		postprocessor->postVar.brightness += dt;
		return false;
	}
	return true;
}

void Gfx::InterpolateFilters(float h, float sat, float br, glm::vec3 col, float dens, float dt) {
	for (auto layer : renderLayers) {
		if (layer->GetName() == "dynamic" || layer->GetName() == "UI" || layer->GetName() == "PostUI" || layer->GetName() == "PostPostUI") continue;
		layer->postprocess.Interpolate(h, sat, br, col, dens, dt);
	}
}

Gfx * Gfx::instance = 0;