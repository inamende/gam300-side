#ifndef GFX_H
#define GFX_H

#include "../EngineCommon.h"
#include <glm/glm.hpp>

#define MAX_LIGHTS 30

class LineRenderable;
class Camera;
class RenderLayer;
class Light;
class Renderable;
class Texture;
class Shader;
class Sprite;
class Font;
class TileMap;
class AnimationData;
class AtlasData;
class Postprocessor;

class Gfx
{
private:

	static Gfx * instance;
	Gfx();

	Camera * mainCamera;

public:
	virtual ~Gfx();

	static Gfx * getInstance();

	bool debugRendering;

	std::vector<Camera*> cameras;
	std::vector<RenderLayer*> renderLayers;
	std::vector<Light*> lights; unsigned lightNum; //max defined in shader (I guess 8)
	std::vector<AnimationData*> animations;
	std::vector<AtlasData*> atlases;
	std::vector<TileMap*> tileMaps;

	LineRenderable * lineDraw;
	Postprocessor * postprocessor;
	vec4 global_ambient_lighting;

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	void Load();
	void LoadLayers();
	void Save();

	void AddRenderable(Renderable * rend);
	void AddCamera(Camera * cam, std::string layerName);
	void AddRenderLayer(std::string layerName = "defaultLayer");
	void AddAnimation(AnimationData * animation);
	void AddAtlas(AtlasData * atlas);
	void AddTileMap(TileMap * tilemap);

	RenderLayer * GetRenderLayer(std::string name);
	Camera * FindCamera(std::string name);
	Camera * GetCameraByRenderLayer(std::string name);
	void SetMainCamera(std::string name);
	void SetMainCamera(Camera * camera);
	Camera * GetMainCamera();
	void SortRenderables();
	Light * AddLight(Light * light);
	Light * FindLight(std::string name);
	AnimationData * FindAnimation(std::string name);
	AtlasData * FindAtlas(std::string name);

	void RemoveRenderable(Renderable * rend);
	void RemoveLight(Light * light);
	void RemoveTileMap(TileMap * tilemap);
	void RemoveAtlas(AtlasData * atlas);

	void ResetPostprocess(unsigned w, unsigned h);

	//DEFAULT RESOURCES
	Texture * GetDefaultTexture();
	Shader * GetDefaultShader();
	Sprite * GetDefaultSprite();
	Font * GetDefaultFont();

	void LoadTexturesFromTxtFile(std::string filePath);
	void LoadShadersFromFile(std::string filePath);
	void LoadSpritesFromFile(std::string filePath);
	void LoadAnimationsFromFile(std::string filePath);

	std::vector<std::string> GetFiles(std::string filePath);

	//RENDER LINES
	void Flush(std::string layer = "default");
	void DrawALine(glm::vec3 p0, glm::vec3 p1, glm::vec4 color = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
	void DrawABox(glm::vec3 pos, float width, float height, glm::vec4 color = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
	void DrawABoxOriented(glm::vec3 pos, float width, float height, float rotation, glm::vec4 color = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
	void DrawACircle(glm::vec3 pos, float radius, int precission = 12, glm::vec4 color = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));

	void WriteToImguiWindow();

	void LoadResources();
	
	bool FadeIn(float, float);
	bool FadeOut(float, float);

	void InterpolateFilters(float h, float sat, float br, glm::vec3 col, float dens, float dt);

	// LOADING SCREEN
	void RenderLoadingScreen(Camera * cam, Shader * shad, Renderable * rend);
	void RenderAnimatedLoadingScreen(Camera * cam, Shader * shad, Renderable * rend, std::vector<Texture*> tex, int dt);
	Camera *ls_cam;
	Renderable *ls_rend;
	Shader *ls_sh;
	std::vector<Texture*> ls_anim;
};

#define DrawLine(X,Y) Gfx::getInstance()->DrawALine(X,Y)
#define DrawColorLine(X,Y,Z) Gfx::getInstance()->DrawALine(X,Y,Z)

#define DrawBox(X,Y,Z) Gfx::getInstance()->DrawABox(X,Y,Z)
#define DrawBoxOriented(X, Y, Z, R) Gfx::getInstance()->DrawABoxOriented(X,Y,Z,R)
#define DrawColorBox(X,Y,Z,W) Gfx::getInstance()->DrawABox(X,Y,Z,W)

#define graphics Gfx::getInstance()

#endif