#ifndef GFXCOMMON_H
#define GFXCOMMON_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <vector>
#include <list>
#include <string>

#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL_image.h>

#include "json/json.h"
#include "../Utils/Input.h"

#endif