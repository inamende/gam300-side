#include "Postprocessor.h"

#include "../Gfx/Reso/Shader.h"
#include "../Gfx/Reso/Sprite.h"
#include "../Gfx/Window.h"
#include "../Utils/Utilities.h"
#include "ImGui/imgui.h"

Postprocessor::~Postprocessor()
{
	Clear();
}

Postprocessor::Postprocessor(Shader * sh, Sprite * sp, float w, float h) :
shader(sh),
sprite(sp),
width(w),
height(h)
{
	CreateProcess(sh, sp, w, h);
}


void Postprocessor::BeginRender()
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glViewport(0, 0, width, height);
	glColorMask(true, true, true, true);
	glClearColor(0.1, 0.1, 0.1, 0.f);
	glClear(GL_COLOR_BUFFER_BIT);
}

void Postprocessor::EndRender()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, width, height);
	glColorMask(true, true, true, true);
	glClearColor(mWindow->frameBufferColor.r, mWindow->frameBufferColor.g, mWindow->frameBufferColor.b, 0.f);
	glClear(GL_COLOR_BUFFER_BIT);
}

void Postprocessor::Render()
{
	shader->use();
	shader->setUniform("M", glm::mat4(1));
	shader->setUniform("V", glm::mat4(1));
	shader->setUniform("P", glm::mat4(1));

	shader->setUniform("hue", postVar.hue);
	shader->setUniform("saturation", postVar.saturation);
	shader->setUniform("brightness", postVar.brightness);

	shader->setUniform("overlay", postVar.color);
	shader->setUniform("density", postVar.density);

	glBindVertexArray(sprite->vaoHandle);
	glBindTexture(GL_TEXTURE_2D, THandle);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

void Postprocessor::CreateProcess(Shader * sh, Sprite * sp, float w, float h)
{
	// create frame buffer
	glGenFramebuffers(1, &FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);

	// generate texture
	GLuint handle;
	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	// attach texture to current framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, handle, 0);


	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "PREPROCESSOR FBO FAIL" << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	THandle = handle;
}

void Postprocessor::Clear()
{
	glDeleteFramebuffers(1, &FBO);
	glDeleteRenderbuffers(1, &RBO);

	glBindTexture(GL_TEXTURE_2D, THandle);
	glDeleteTextures(0, &THandle);
	glDeleteTextures(1, &THandle);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Postprocessor::Reset(unsigned w, unsigned h)
{
	Clear();
	CreateProcess(shader, sprite, w, h);
	width = w; height = h;
}

void Postprocessor::Interpolate(float h, float sat, float br, glm::vec3 col, float dens, float dt) {
	postVar.hue = Lerp<float>(postVar.hue, h, dt);
	postVar.saturation = Lerp<float>(postVar.saturation, sat, dt);
}