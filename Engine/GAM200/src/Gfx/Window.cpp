#include "Window.h"

#include <GL/glew.h>

#include "../Gfx/Gfx.h"
#include "../ObjectManager/ObjectManager.h"
#include "../Editor/ImGuiEditor.h"
#include "SDL2/SDL_image.h"
#include <windows.h>
#include "../../resource.h"

void Window::set_ico(const char *ico) {
	HICON icon = ::LoadIcon(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDI_ICON1));
	::SendMessage((HWND)windowHandle, WM_SETICON, 0, (LPARAM)icon);
}

Window * Window::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW Window();

	return instance;
}

Window::Window() : width(1920), height(1080), name("Strawss Berry"), windowHandle(NULL), quit(false), fullScreen(true), frameBufferColor(glm::vec4(0.9f, 0.6f, 0.9f, 1.0f)), currentResolution(0), prevWidth(1920), prevHeight(1080)
{
	resolutions = { { 1920,1080 },{ 1600, 900 },{ 1280,720 },{ 960, 540 } };
}

Window::Window(const char * windowName, int windoWidth, int windowHeight)
{
	windowHandle = NULL;
	name = windowName;
	width = windoWidth;
	height = windowHeight;
	prevWidth = width;
	prevHeight = height;
	fullScreen = true;
	frameBufferColor = glm::vec4(0.9f, 0.6f, 0.9f, 1.0f);
	currentResolution = 0;
	resolutions = { { 1920,1080 },{ 1600, 900 },{ 1280,720 },{ 960, 540 } };
}

Window::~Window() {}

void Window::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		//std::cout << "Could not initialize SDL: " << SDL_GetError() << std::endl;
		exit(1);
	}

	windowHandle = SDL_CreateWindow(name, 100, 100, width, height, SDL_WINDOW_OPENGL);

	if (windowHandle == NULL)
	{
		//std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	context_ = SDL_GL_CreateContext(windowHandle);
	if (context_ == nullptr)
	{
		SDL_DestroyWindow(windowHandle);
		//std::cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		SDL_GL_DeleteContext(context_);
		SDL_DestroyWindow(windowHandle);
		//std::cout << "GLEW Error: Failed to init" << std::endl;
		SDL_Quit();
		exit(1);
	} 

	SDL_GL_SetSwapInterval(true);

	imGuiEditor->InitImGui(windowHandle);

	glViewport(0, 0, width, height);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Load();
	set_ico("uwu");
	FullScreen(true);
}

void Window::Update()
{
	imGuiEditor->EditorWindow(windowHandle);
	graphics->Update();
	imGuiEditor->Render();

	// Unbind
	glBindVertexArray(0);
	glUseProgram(0);

	SDL_GL_SwapWindow(windowHandle);
}

void Window::Terminate()
{
	Save();

	ImGui_ImplSdlGL3_Shutdown();
	ImGui::DestroyContext();

	SDL_GL_DeleteContext(context_);
	SDL_DestroyWindow(windowHandle);
	SDL_Quit();

	delete instance;
}

void Window::Load()
{
	std::ifstream jsonFile(editor_loader);
	Json::Reader reader;
	bool ok = reader.parse(jsonFile, commonValue);
	//if (!ok)
	//	std::cout << "ERROR READING JSON" << std::endl;

	currentResolution = commonValue.get("resolution", 0).asInt();


	frameBufferColor.r = commonValue["window_color"].get("r", 0.f).asFloat();
	frameBufferColor.g = commonValue["window_color"].get("g", 0.f).asFloat();
	frameBufferColor.b = commonValue["window_color"].get("b", 0.f).asFloat();
	frameBufferColor.a = commonValue["window_color"].get("a", 1.f).asFloat();

	SetResolution();

	jsonFile.close();
}

void Window::Save()
{
#ifdef EDITOR
	//commonValue["full_screen"] = fullScreen;
	commonValue["resolution"] = currentResolution;

	commonValue["window_color"]["r"] = frameBufferColor.r;
	commonValue["window_color"]["g"] = frameBufferColor.g;
	commonValue["window_color"]["b"] = frameBufferColor.b;
	commonValue["window_color"]["a"] = frameBufferColor.a;

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();
	std::ofstream jsonFileWrite(editor_loader);
	writer->write(commonValue, &jsonFileWrite);
	jsonFileWrite.close();
#endif
}

void Window::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Window"))
	{
		ImGuiEditor::ColorEdit4("Frame Buffer Color", frameBufferColor);

		if (ImGui::Checkbox("Full Screen", &fullScreen))
			FullScreen(fullScreen);

		if (ImGui::TreeNode("Resolution"))
		{
			if (ImGui::Button("1920 x 1080"))
			{
				currentResolution = 0;
				SetResolution();
			}

			if (ImGui::Button("1600 x 900"))
			{
				currentResolution = 1;
				SetResolution();
			}

			if (ImGui::Button("1280 x 720"))
			{
				currentResolution = 2;
				SetResolution();
			}

			if (ImGui::Button("960 x 540"))
			{
				currentResolution = 3;
				SetResolution();
			}

			ImGui::TreePop();
		}
	}
}

void Window::FullScreen(bool full)
{
	if (!full)
	{
		SDL_SetWindowFullscreen(windowHandle, 0);
		SDL_SetWindowSize(windowHandle, (int)GetCurrentResolution().x, (int)GetCurrentResolution().y);

		width = (int)GetCurrentResolution().x;
		height = (int)GetCurrentResolution().y;

		fullScreen = full;

		graphics->ResetPostprocess(width, height);
	}
	else
	{
		int displayInd = SDL_GetWindowDisplayIndex(windowHandle);
		SDL_Rect screenRect;
		SDL_GetDisplayBounds(displayInd, &screenRect);
		SDL_SetWindowSize(windowHandle, screenRect.w, screenRect.h);
		prevWidth = width;
		prevHeight = height;
		width = screenRect.w;
		height = screenRect.h;

		int w, h;
		SDL_GetWindowSize(windowHandle, &w, &h);

		glViewport(0, 0, w, h);

		SDL_SetWindowFullscreen(windowHandle, SDL_WINDOW_FULLSCREEN_DESKTOP);

		fullScreen = full;

		graphics->ResetPostprocess(w, h);
	}
}

bool Window::GetFullScreen() { return fullScreen; }

void Window::SetResolution()
{
	fullScreen = false;

	GLsizei newWidth = (GLsizei)resolutions[currentResolution].x;
	GLsizei newHeight = (GLsizei)resolutions[currentResolution].y;

	SDL_DestroyWindow(windowHandle);

	windowHandle = SDL_CreateWindow(name, 0, 30, newWidth, newHeight, SDL_WINDOW_OPENGL);

	width = newWidth;
	height = newHeight;

	SDL_GL_MakeCurrent(windowHandle, context_);

	int w, h;
	SDL_GetWindowSize(windowHandle, &w, &h);

	glViewport(0, 0, w, h);

	if (SDL_GetRelativeMouseMode())
	{
		SDL_SetRelativeMouseMode(SDL_FALSE);
		SDL_SetRelativeMouseMode(SDL_TRUE);
	}
	graphics->ResetPostprocess(w, h);
}

void Window::UpdateResolution()
{
	currentResolution = ++currentResolution % resolutions.size();
}

glm::vec2 Window::GetCurrentResolution()
{
	return resolutions[currentResolution];
}

void Window::v_sync(bool b) { v_sync_active = b;  SDL_GL_SetSwapInterval(b); }
void Window::v_sync_toggle() { v_sync(!v_sync_active); }
bool Window::window_is_missing() {
	return window_state == 6 || window_state == 1030 || window_state == 70 || window_state == 4167;
}
Window * Window::instance = 0;