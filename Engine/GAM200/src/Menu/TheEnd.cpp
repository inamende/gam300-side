#include "TheEnd.h"
#include "../ObjectManager/GameObject.h"
#include "../Level/Scene.h"
#include "../Gfx/Gfx.h"
#include "../Gfx/Reso/Camera.h"
#include "imGui/imgui.h"

void TheEnd::Initialize() {
	start = false;
	dt = 0.f;
	Camera *cam = graphics->GetMainCamera();
	cam->position = {0.f,0.f,1.f};
	cam->view.x = cam->position.x;
	cam->view.y = cam->position.y;
}

void TheEnd::Update() {
	dt += ImGui::GetIO().DeltaTime;
	if (dt > 6.f && !start) {
		dt = 0.f;
		start = true;
	}
	if (start) {
		if (graphics->FadeIn(-100, dt * 0.5f)) {
			scene->currLevel = "Credits.json";
		}
	}
}

void TheEnd::Terminate() {}

void TheEnd::load(Json::Value & val) {
	active = val["TheEnd"].get("active", true).asBool();
}

void TheEnd::save(Json::Value & val) {
	val["TheEnd"]["active"] = active;
}

void TheEnd::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("TheEndOfEvangelion")) {
		if (ImGui::Button("Detach##TheEnd"))
			GetOwner()->detach(this);
	}
}