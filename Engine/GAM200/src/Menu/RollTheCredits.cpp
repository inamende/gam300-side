#include "RollTheCredits.h"
#include "../ObjectManager/GameObject.h"
#include "../Gfx/Comp/Transform.h"
#include "../Editor/ImGuiEditor.h"
#include "../Gfx/Comp/Renderable.h"
#include "../Level/Scene.h"
#include "../Gfx/Gfx.h"

void RollCredits::Initialize() {
	dt = 0.f;
	start = false;
	end = false;
}

void RollCredits::Update() {
	dt += ImGui::GetIO().DeltaTime;
	if (!start && !graphics->FadeOut(0, dt * 0.5f)) {
		return;
	}
	else {
		start = true;
	}

	if (GetOwner()->mTransform->position.y < limit) {
		dt = 0.f;
		GetOwner()->mTransform->position.y += speed;
	}
	else {
		if (!end && dt < 3.f) {
			return;
		}
		else {
			end = true;
		}
		if (end && graphics->FadeIn(-100, dt * 0.5f)) {
			scene->currLevel = "MainMenu.json";
		}
	}
}

void RollCredits::Terminate() {}

void RollCredits::load(Json::Value & val)
{
	active = val["RollCredits"].get("active", true).asBool();
	limit = val["RollCredits"].get("limit", 200.f).asFloat();
	speed = val["RollCredits"].get("speed", 2.f).asFloat();
}

void RollCredits::save(Json::Value & val)
{
	val["RollCredits"]["active"] = active;
	val["RollCredits"]["limit"] = limit;
	val["RollCredits"]["speed"] = speed;
}

void RollCredits::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("RollCredits"))
	{
		ImGui::InputFloat("y limit", &limit);
		ImGui::InputFloat("speed", &speed);
		ImGui::NewLine();
		if (ImGui::Button("Detach##RollCredits"))
			GetOwner()->detach(this);
	}
}
