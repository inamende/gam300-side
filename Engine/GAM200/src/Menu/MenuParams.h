#ifndef MENU_PARAMS_H_
#define MENU_PARAMS_H_

#include <vector>
#include "../glm/common.hpp"
#include "../Gfx/Window.h"
struct MenuParams
{
	static MenuParams & GetInstance()
	{
		static MenuParams MenuInstance;

		return MenuInstance;
	}
	std::vector<glm::vec2> Resolutions;
	bool IsFullScreen = mWindow->fullScreen;
	bool UsingMouse;
};

#endif