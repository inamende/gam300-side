#include "Cursor.h"
#include "../ObjectManager/GameObject.h"
#include "../Gfx/Gfx.h"
#include "../Gfx/Window.h"
#include "../Gfx/Reso/Camera.h"
#include "../Gfx/Comp/Transform.h"
#include "../Input/Input.h"
#include "imGui/imgui.h"

void Cursor::Initialize() {
	g = graphics;
	GetOwner()->mTransform->position = vec3();
	sensivility = 1000.f;
	cam = g->GetCameraByRenderLayer("PostPostUI");
}

void Cursor::Update() {
	if (mInput->using_controller) {
		vec2 dir = vec2(JoyStickDirection(SDL_CONTROLLER_AXIS_RIGHTX), JoyStickDirection(SDL_CONTROLLER_AXIS_RIGHTY)) / sensivility;
		float w = cam->width * 0.5f, h = cam->height * 0.5f;
		float x_next = GetOwner()->mTransform->position.x + dir.x;
		float y_next = GetOwner()->mTransform->position.y - dir.y;
		if (x_next > w || x_next < -w) {
			x_next = GetOwner()->mTransform->position.x;
		}
		if (y_next > h || y_next < -h) {
			y_next = GetOwner()->mTransform->position.y;
		}
		GetOwner()->mTransform->position.x = x_next;
		GetOwner()->mTransform->position.y = y_next;
	}
	else {
		mouse = cam->ViewportToWorld(mousePosition);
		GetOwner()->mTransform->position = { mouse.x, mouse.y, 9999.f };
	}
}

void Cursor::Terminate() {}

void Cursor::load(Json::Value & val) {
	active = val["Cursor"].get("active", true).asBool();
}

void Cursor::save(Json::Value & val) {
	val["Cursor"]["active"] = active;
}

void Cursor::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("Cursor")) {
		if (ImGui::Button("Detach##Cursor"))
			GetOwner()->detach(this);
	}
}

vec2 Cursor::get_camera_coord(const char *cam_name) {
	return graphics->GetCameraByRenderLayer(cam_name)->ViewportToWorld(cam->WorldToViewport(vec2(GetOwner()->mTransform->position)));
}