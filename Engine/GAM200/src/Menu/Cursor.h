#ifndef CURSOR_H
#define CURSOR_H

#include "../Component/Component.h"

class Gfx;
class Camera;
class Cursor : public Component {
public:
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	vec2 get_camera_coord(const char*);
private:
	glm::vec2 mouse;
	Gfx *g;
	float sensivility;
	Camera *cam;
};

#endif