#ifndef MENU_H_
#define MENU_H_

#include "../Component/Component.h"
#include "../StateMachine/StateMachine.h"
#include "../GAM200/src/Gfx/Gfx.h"


class Menu;
class Camera;

class Button : public Component
{
  public:
	  Button();
	  ~Button();

	  virtual void Initialize();
	  virtual void Update();
	  virtual void Terminate();

	  virtual void Execute() = 0;


	  bool Selected = false;
	  Menu* Owner;
	  std::string name = "Placeholder";
	  GameObject* MakeThing(std::string Name, glm::vec3 Pos);

	  virtual bool CheckMouseInside(glm::vec2 mousePos);
};

class Menu : public Component
{
  public:
	  Menu();
	  ~Menu();

	  virtual void Initialize();
	  virtual void Update();
	  virtual void Terminate();

	  virtual void load(Json::Value & val);
	  virtual void save(Json::Value & val);
	  virtual void WriteToImguiWindow();

	  void IterateUp();
	  void IterateDown();
	  void Action();

	  GameObject *CreateButton(std::string ArchetypeName, const glm::vec2 & Position);

	  std::vector<std::string> AvaileableButtons;
	  std::vector<std::string> OwnedButtons;
	  std::vector<Button*> Buttons;
	  std::vector<Button*> SureBoutThat;
	  std::vector<Button*>::iterator VectorIterator;
	  bool LockConfirm;
	  bool UsingMouse;
	  bool Lock;
	  unsigned index;
	  vec2 prev_pos;
};


class StartGame : public Button
{
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private :
	bool transition = false;
	float dt = 0.f;
};

class Options : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

private:
	Camera * cam;
};

class GraphiccOptions : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class SoundOptions : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class Credits : public Button
{
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	bool transition = false;
	float dt = 0.f;
};

class HowToPlay : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

class QuitToPause : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	enum which
	{
		Yes, No, ToMain
	};

	which current;

	GameObject* Confirmation;
	GameObject* YesB;
	GameObject* NoB;
	GameObject* BackToMain;
};

class FullScreenButton : public Button
{
	bool CheckMouseInside(glm::vec2 mousePos);
	virtual void Initialize();
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	GameObject* CheckBox;
};

class VSyncButton : public Button
{
	bool CheckMouseInside(glm::vec2 mousePos);
	virtual void Initialize();
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	GameObject* check_box;
};

class Controls : public Button
{
	virtual void Initialize();
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	GameObject* Mouse;
	GameObject* W;
	GameObject* A;
	GameObject* S;
	GameObject* D;
	GameObject* SpaceBar;
	GameObject* Q;
};

class BackToMain : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class YesToMain : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class YesToRestart : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class NoToPause : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class NoToPauseFromRestart : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class Quit : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class MasterVolumeButton : public Button
{
public:
	virtual bool CheckMouseInside(glm::vec2 mousePos);
	virtual void Initialize();
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	float volume;
	float TimeHeld;
	GameObject* Plus;
	GameObject* Minus;
	GameObject* Mute;
	bool Muted = false;
	bool moueonplus;
	bool mouseonminus;
	bool mouseonmute;
};

class MusicVolumeButton : public MasterVolumeButton
{
	virtual void Initialize();
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};


class SFXVolumeButton : public MasterVolumeButton
{
	virtual void Initialize();
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

class BackToPause : public Button
{
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
	bool transition = false;
	float dt = 0.f;
};

class BackToGraphicc : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class BackToSound : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};

class Resume : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

class Restart : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	Camera * cam;
};


class Resolutions : public Button
{
	virtual bool CheckMouseInside(glm::vec2 mousePos);
	virtual void Initialize();
	virtual void Execute();
	virtual void Update();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	
	std::string ResolutionsArray[4] = { "1920x1080" , "1600x900" , "1280x720" , "960x340"};
	GameObject* Left;
	GameObject* Right;
	bool mouseonleft;
	bool mouseonright;
};

class Apply : public Button
{
	virtual void Execute();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
	bool confirmation;
};

#endif