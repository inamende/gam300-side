#ifndef THE_END_H
#define THE_END_H

#include "../Component/Component.h"

class TheEnd : public Component {
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	bool start;
	float dt;
};

#endif