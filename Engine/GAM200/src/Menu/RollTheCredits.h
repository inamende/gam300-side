#ifndef ROLL_THE_CREDITS_H
#define ROLL_THE_CREDITS_H

#include "../../GAM200/src/Component/Component.h"

class RollCredits : public Component {
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
private:
	float timer;
	float limit;
	float speed;
	float dt;
	bool start;
	bool end;
};

#endif