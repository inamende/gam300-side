#include "FadeIntoMenu.h"
#include "Logo.h"
#include "G_Logo.h"
#include "BragAboutHowWeDidTheGame.h"
#include "LevelSwap.h"
#include "../../ObjectManager/GameObject.h"
#include <imGui/imgui.h>


FadeIntoMenu::FadeIntoMenu() {}
FadeIntoMenu::~FadeIntoMenu() {}

void FadeIntoMenu::Initialize() {
	m_actor = GetOwner();

	AddState(DBG_NEW DpLogo("1stLogo"));
	AddState(DBG_NEW GLogo("2ndLogo"));
	AddState(DBG_NEW BragAboutHowWeDidTheGame("Brag"));
	AddState(DBG_NEW Lvl_Swap("ChngLvl"));

	SetInitState("1stLogo");

	mInitialState->InternalEnter();
}

void FadeIntoMenu::Update() {
	StatusUpdate();
}

void FadeIntoMenu::Terminate() {}

void FadeIntoMenu::load(Json::Value & val) {
	active = val["FadeIntoMenu"].get("active", true).asBool();
}

void FadeIntoMenu::save(Json::Value & val) {
	val["FadeIntoMenu"]["active"] = active;
}

void FadeIntoMenu::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("FadeIntoMenu"))
	{
		ImGui::NewLine();
		if (ImGui::Button("Detach##FadeIntoMenu"))
			GetOwner()->detach(this);
	}
}
