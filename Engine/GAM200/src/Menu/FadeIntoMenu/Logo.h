#ifndef DP_LOGO_H
#define DP_LOGO_H

#include "../../StateMachine/StateMachine.h"
class DpLogo : public State
{
public:
	DpLogo(const char * name) : State(name) {}

	void Enter();
	void Update();
	float timer = 0.0f;
	float ScreenTime = 5.0f;
};

#endif
