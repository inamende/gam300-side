#ifndef LVL_SWAP_H
#define LVL_SWAP_H

#include "../../StateMachine/StateMachine.h"
class Lvl_Swap : public State
{
public:
	Lvl_Swap(const char * name) : State(name) {}

	void Enter();
	void Update();

};

#endif
