#include "BragAboutHowWeDidTheGame.h"
#include "../../ObjectManager/GameObject.h"
#include "../../ResourceManager/ResoManager.h"
#include "../../Gfx/Gfx.h"
#include "../../Gfx/Comp/Renderable.h"
#include "../../Gfx/Comp/FontRenderable.h"
#include "../../Gfx/Reso/Texture.h"
#include "../../Gfx/Reso/Shader.h"
#include "../../Gfx/Reso/Font.h"
#include "../../Factory/Factory.h"

void BragAboutHowWeDidTheGame::Enter() {
	m_actor->GetComponent<Renderable>("Renderable")->visible = false;
	FontRenderable* f = m_actor->GetComponent<FontRenderable>("FontRenderable"); 
	f->displayText = "        Everything you're about{        to see was created from{scratch using our custom C++ engine.";
}

void BragAboutHowWeDidTheGame::Update() {
	static bool fade = false;
	if (!fade) {
		fade = graphics->FadeOut(0, m_time_in_state * 0.3f);
	}
	else {
		if (m_time_in_state > 6.f) {
			if (graphics->FadeIn(-100, m_time_in_state * 0.3f)) {
				m_owner->ChangeState("ChngLvl");
			}
		}
	}
}
