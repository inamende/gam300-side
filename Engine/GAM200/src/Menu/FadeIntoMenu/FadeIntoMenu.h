#ifndef FADE_INTO_M_H
#define FADE_INTO_M_H

#include "../../StateMachine/StateMachine.h"

class FadeIntoMenu : public StateMachine
{
public:
	FadeIntoMenu();
	~FadeIntoMenu();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();
};

#endif