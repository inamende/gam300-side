#include "Logo.h"
#include "../../ObjectManager/GameObject.h"
#include "../../ResourceManager/ResoManager.h"
#include "../../Gfx/Gfx.h"
#include "../../Gfx/Comp/Renderable.h"
#include "../../Gfx/Reso/Texture.h"

void DpLogo::Enter() {
	Renderable* r = m_actor->GetComponent<Renderable>("Renderable");
	r->texture = resoManager->getReso<Texture>("digipen.png");
	r->ApplyTextureScale();
}

void DpLogo::Update() {
	static bool fade = false;
	if  (!fade) {
		fade = graphics->FadeOut(0, m_time_in_state * 0.3f);
	}
	else {
		if (m_time_in_state > 6.f) {
			if (graphics->FadeIn(-100, m_time_in_state * 0.3f)) {
				m_owner->ChangeState("2ndLogo");
			}
		}
	}
}
