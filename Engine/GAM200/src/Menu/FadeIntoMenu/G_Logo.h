#ifndef G_LOGO_H
#define G_LOGO_H

#include "../../StateMachine/StateMachine.h"

class GLogo : public State
{
public:
	GLogo(const char * name) : State(name) {}

	void Enter();
	void Update();
	float timer = 0.0f;
	float ScreenTime = 5.0f;
};

#endif
