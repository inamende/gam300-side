#ifndef BRAG_ABOUT_HOW_WE_DID_THE_GAME_H
#define BRAG_ABOUT_HOW_WE_DID_THE_GAME_H

#include "../../StateMachine/StateMachine.h"
class BragAboutHowWeDidTheGame : public State
{
public:
	BragAboutHowWeDidTheGame(const char * name) : State(name) {}

	void Enter();
	void Update();
};

#endif
