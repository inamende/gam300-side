#include "Menu.h"
#include "../Input/Input.h"
#include "../Editor/ImGuiEditor.h"
#include "../ObjectManager/GameObject.h"
#include "../ResourceManager/ResoManager.h"
#include "../Level/Scene.h"
#include "../Gfx/Comp/Renderable.h"
#include "../Gfx/Window.h"
#include "../Gfx/Comp/Transform.h"
#include "../Gfx/Reso/Camera.h"
#include "../Gfx/Comp/FontRenderable.h"
#include "../Physics/Collisions.h"
#include "../Gfx/Reso/Texture.h"
#include "Cursor.h"
//oscar additions
#include "../Audio/SoundEmitter.h"
Menu::Menu()
{
	AvaileableButtons.push_back("StartButton");
	AvaileableButtons.push_back("OptionsButton");
	AvaileableButtons.push_back("CreditsButton");
	AvaileableButtons.push_back("QuitButton");
	AvaileableButtons.push_back("FullScreenButton");
	AvaileableButtons.push_back("BackToMainButon");
	AvaileableButtons.push_back("MasterVolumeButton");
	AvaileableButtons.push_back("MusicVolumeButton");
	AvaileableButtons.push_back("SFXVolumeButton");
	AvaileableButtons.push_back("PlayButMM");
	AvaileableButtons.push_back("QuitButMM");
	AvaileableButtons.push_back("OptionsButMM");
	AvaileableButtons.push_back("CreditsButMM");
	AvaileableButtons.push_back("ControlsButMM");
	AvaileableButtons.push_back("ApplyReso");
	AvaileableButtons.push_back("ResolutionButton");

	index = mWindow->currentResolution;
}

Menu::~Menu()
{
	AvaileableButtons.clear();
	OwnedButtons.clear();
	Buttons.clear();
	SureBoutThat.clear();
	SoundEmitter * e = GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}

void Menu::Initialize()
{
	LockConfirm = false;
	UsingMouse = true;
	Lock = false;
	Buttons.clear();
	for (unsigned i = 0; i < OwnedButtons.size(); i++)
	{
		auto butt = scene->FindObject(OwnedButtons[i]);
		if (butt)
		{
			Buttons.push_back(butt->GetComponent<Button>());
			Buttons[i]->Owner = this;
		}
		else
		{
			glm::vec2 Pos;
			Pos.x = GetOwner()->mTransform->position.x;
			Pos.y = GetOwner()->mTransform->position.y;
			CreateButton(OwnedButtons[i], Pos);
		}
	}

	for (auto it : Buttons)
	{
		it->Selected = false;
	}

	VectorIterator = Buttons.begin();

	if (!Buttons.empty())
	{
		(*VectorIterator)->Selected = true;
	}

	Buttons.erase(unique(Buttons.begin(), Buttons.end()), Buttons.end());

	sort(OwnedButtons.begin(), OwnedButtons.end());
	OwnedButtons.erase(unique(OwnedButtons.begin(), OwnedButtons.end()), OwnedButtons.end());
	prev_pos = {};
}

void Menu::Update()
{	
	Camera* cam = graphics->GetCameraByRenderLayer("UI");
	glm::vec2 mousePos = mInput->using_controller ? scene->cursor->GetComponent<Cursor>()->get_camera_coord("UI") : cam->ViewportToWorld(Input::getInstance()->GetMousePosition());

	if (!mInput->using_controller) {
		if (mousePrevPosition != mousePosition || MouseDown(SDL_BUTTON_LEFT)) {
			UsingMouse = true;
		}
	}
	else {
		if (prev_pos != mousePos) {
			UsingMouse = true;
		}
	}

	if (!Lock)
	{
		if (!LockConfirm)
		{
			for (auto it : Buttons)
			{
				if (it->CheckMouseInside(mousePos) && UsingMouse)
				{
					(*VectorIterator)->Selected = false;
					for (VectorIterator = Buttons.begin(); VectorIterator != Buttons.end(); VectorIterator++)
					{
						if (*VectorIterator == it)
						{
							break;
						}
					}
					(*VectorIterator)->Selected = true;
					if (MouseDown(SDL_BUTTON_LEFT) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
					{
						(*VectorIterator)->Execute();
					}
					break;
				}
			}
			static bool down_up = false;
			if (!ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_UP)) {
				down_up = false;
			}
			if (KeyDown(Keyboard::UP) || KeyDown(Keyboard::W) || (ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_UP) && !down_up))
			{
				down_up = true;
				UsingMouse = false;
				SoundEmitter * e = NULL;
				e = this->GetOwner()->GetComponent<SoundEmitter>();
				if (e)
				{
					//if (e->IsInit)
					//	e->PlaySound("Menu_Click22.mp3", 1);
					if (e->IsInit)
					{
						e->PlayOnce("Menu_Click22.mp3", false);
					}
				}
				IterateUp();
			}
			static bool down_down = false;
			if (!ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_DOWN)) {
				down_down = false;
			}
			if (KeyDown(Keyboard::DOWN) || KeyDown(Keyboard::S) || (ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_DOWN) && !down_down))
			{
				down_down = true;
				UsingMouse = false;
				SoundEmitter * e = NULL;
				e = this->GetOwner()->GetComponent<SoundEmitter>();
				if (e)
				{
					//if (e->IsInit)
					//	e->PlaySound("Menu_Click22.mp3", 1);
					if (e->IsInit)
					{
						e->PlayOnce("Menu_Click22.mp3", false);
					}
				}
				IterateDown();
			}
			if ((KeyDown(Keyboard::SPACE) || KeyDown(Keyboard::RETURN) || ButtonDown(SDL_CONTROLLER_BUTTON_A)) && !UsingMouse)
			{
				UsingMouse = false;
				Action();
			}

		}
		else if (LockConfirm)
		{
			if (MouseDown(SDL_BUTTON_LEFT) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
			{
				if ((*VectorIterator)->CheckMouseInside(Input::getInstance()->GetMousePosition()))
				{

					(*VectorIterator)->Execute();
				}
			}
			if (KeyDown(Keyboard::SPACE) || KeyDown(Keyboard::RETURN) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
			{
				(*VectorIterator)->Execute();
			}
		}
	}
	else if (KeyDown(Keyboard::ESCAPE) || KeyDown(Keyboard::SPACE) || MouseDown(SDL_BUTTON_LEFT) || ButtonDown(SDL_CONTROLLER_BUTTON_A))
	{
		(*VectorIterator)->Execute();
	}

	prev_pos = mousePos;
}

void Menu::Terminate()
{
	 SoundEmitter * e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		e->StopVoice();
	}
}

void Menu::load(Json::Value & val)
{
	active = val["Menu"].get("active", true).asBool();
	OwnedButtons.clear();
	// load the button names
	// so that on initialize, you find them
	// and set their owner variable to this menu. 
	for (unsigned i = 0; i < val["Menu"]["Buttons"]["name"].size(); i++)
	{
		OwnedButtons.push_back(val["Menu"]["Buttons"]["name"][i].asString());
	}
}

void Menu::save(Json::Value & val)
{
	val["Menu"]["active"] = active;
	if (Buttons.empty())
	{
		return;
	}
	for (unsigned i = 0; i < Buttons.size(); i++)
	{
		val["Menu"]["Buttons"]["name"].append(Buttons[i]->name);
	}
}

void Menu::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Menu Options"))
	{
		if (ImGui::Button("Add Buttons"))
			ImGui::OpenPopup("Buttons");

		if (ImGui::BeginPopup("Buttons"))
		{
			ImGui::Text("Buttons");
			ImGui::Separator();

			for (unsigned i = 0; i < AvaileableButtons.size(); i++)
				if (ImGui::Selectable(AvaileableButtons[i].c_str()))
				{
					glm::vec2 Pos;
					Pos.x = GetOwner()->mTransform->position.x;
					Pos.y = GetOwner()->mTransform->position.y;
					CreateButton(AvaileableButtons[i], Pos);
					AvaileableButtons.erase(AvaileableButtons.begin() + i);
				}
			ImGui::EndPopup();
		}

		if (ImGui::Button("Owned Buttons"))
			ImGui::OpenPopup("Owned Buttons");

		if (ImGui::BeginPopup("Owned Buttons"))
		{
			ImGui::Text("Owned Buttons");
			ImGui::Separator();
			if (OwnedButtons.empty())
			{
			}
			else
			{
				for (unsigned i = 0; i < OwnedButtons.size(); i++)
					if (ImGui::Selectable(OwnedButtons[i].c_str()))
					{
					}
			}
			ImGui::EndPopup();
		}
	}
}

void Menu::IterateUp()
{
	(*VectorIterator)->Selected = false;
	if (VectorIterator != Buttons.begin())
		VectorIterator--;
	else
		VectorIterator = Buttons.end() - 1;
	(*VectorIterator)->Selected = true;
}

void Menu::IterateDown()
{
	(*VectorIterator)->Selected = false;
	if (VectorIterator != Buttons.end() - 1)
		VectorIterator++;
	else
		VectorIterator = Buttons.begin();
	(*VectorIterator)->Selected = true;
}

void Menu::Action()
{
	(*VectorIterator)->Execute();
}

GameObject *Menu::CreateButton(std::string ArchetypeName, const glm::vec2 & Position)
{
	static ImGuiEditor * ed = imGuiEditor;
	//float z = ed->GetLastZ();

	static Scene * sc = scene;
	GameObject * newEditorObj = sc->CreateGameObject();

	if (!newEditorObj)
		return nullptr;

	bool Found = newEditorObj->LoadJson("./Data/Jsons/Buttons/" + ArchetypeName + ".json");
	if (!Found)
	{
		sc->DeleteGameObject(newEditorObj);
		return nullptr;
	}

	Transform * tr = newEditorObj->mTransform;
	if (!tr)
	{
		sc->DeleteGameObject(newEditorObj);
		return nullptr;
	}

	tr->position.x = Position.x;
	tr->position.y = Position.y;
	tr->position.z = 1000.f;

	Button* but = newEditorObj->GetComponent<Button>();
	if (!but)
	{
		sc->DeleteGameObject(newEditorObj);
		return nullptr;
	}
	but->name = ArchetypeName;
	but->Owner = this;
	Buttons.push_back(but);
	//std::cout << Buttons.size() << std::endl;
	VectorIterator = Buttons.begin();
	return newEditorObj;
}


////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////


Button::Button()
{
	Owner = nullptr;
}

Button::~Button()
{
	//SoundEmitter * e = GetOwner()->GetComponent<SoundEmitter>();
	//if(e)
	//	e->StopVoice();
}

void Button::Initialize()
{
	name = GetOwner()->GetName();
}

void Button::Update()
{
	if (Selected)
	{
		GetOwner()->GetComponent<Renderable>("Renderable")->visible = true;
	}
	else
	{
		GetOwner()->GetComponent<Renderable>("Renderable")->visible = false;
	}
}

void Button::Terminate()
{

}

GameObject * Button::MakeThing(std::string Name, glm::vec3 Pos)
{
	GameObject *new_obj = scene->CreateGameObject();
	new_obj->LoadJson(Name);
	new_obj->mTransform->position.x = Pos.x;
	new_obj->mTransform->position.y = Pos.y;
	new_obj->mTransform->position.z = Pos.z;
	new_obj->save = false;
	new_obj->Initialize();
	return new_obj;
}

bool Button::CheckMouseInside(glm::vec2 mousePos)
{
	return StaticPointToStaticRect(mousePos, glm::vec2(GetOwner()->mTransform->position.x, GetOwner()->mTransform->position.y), GetOwner()->mTransform->scale.x * 0.6f, GetOwner()->mTransform->scale.y * 0.6f);
}


void StartGame::Execute()
{
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Accept.mp3", false);
		}
	}
	transition = true;
	Owner->Lock = true;
}

void StartGame::Update() {
	Button::Update();
	if (!transition) {
		return;
	}
	dt += ImGui::GetIO().DeltaTime;
	if (graphics->FadeIn(-100, dt * 0.5f)) {
		scene->currLevel = "TutorialLevel.json";
	}
}

void StartGame::load(Json::Value & val)
{
	active = val["StartGame"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["StartGame"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void StartGame::save(Json::Value & val)
{
	val["StartGame"]["active"] = active;
	if (Owner != nullptr)
	{
		val["StartGame"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void StartGame::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("StartGame"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##DoorReactor"))
			GetOwner()->detach(this);
	}
}

void Options::Execute()
{
	cam = graphics->GetCameraByRenderLayer("UI");
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}

	if (scene->currLevel == "MainMenu.json")
	{
		scene->currLevel = "Options.json";
	}
	else {
		cam->position.x = scene->options_position.x;
		cam->position.y = scene->options_position.y;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;
	}
}

void Options::load(Json::Value & val)
{
	active = val["Options"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Options"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Options::save(Json::Value & val)
{
	val["Options"]["active"] = active;
	if (Owner != nullptr)
	{
		val["Options"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Options::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("OptionsButton"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Options"))
			GetOwner()->detach(this);
	}
}

void GraphiccOptions::Execute() {
	cam = graphics->GetCameraByRenderLayer("UI");
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}

	cam->position.x = scene->graphicc_options_position.x;
	cam->position.y = scene->graphicc_options_position.y;
	cam->view.x = cam->position.x;
	cam->view.y = cam->position.y;
}

void GraphiccOptions::load(Json::Value & val) {
	active = val["GraphiccOptions"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["GraphiccOptions"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void GraphiccOptions::save(Json::Value & val) {
	val["GraphiccOptions"]["active"] = active;
	if (Owner != nullptr)
	{
		val["GraphiccOptions"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void GraphiccOptions::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("GraphiccOptions"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##GraphiccOptions"))
			GetOwner()->detach(this);
	}
}

void SoundOptions::Execute() {
	cam = graphics->GetCameraByRenderLayer("UI");
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}

	cam->position.x = scene->sound_options_position.x;
	cam->position.y = scene->sound_options_position.y;
	cam->view.x = cam->position.x;
	cam->view.y = cam->position.y;
}

void SoundOptions::load(Json::Value & val) {
	active = val["SoundOptions"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["SoundOptions"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void SoundOptions::save(Json::Value & val) {
	val["SoundOptions"]["active"] = active;
	if (Owner != nullptr)
	{
		val["SoundOptions"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void SoundOptions::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("SoundOptions"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##SoundOptions"))
			GetOwner()->detach(this);
	}
}

void Credits::Execute()
{
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}
	transition = true;
	Owner->Lock = true;
}

void Credits::Update() {
	Button::Update();
	if (!transition) {
		return;
	}
	dt += ImGui::GetIO().DeltaTime;
	if (graphics->FadeIn(-100, dt * 0.5f)) {
		scene->currLevel = "Credits.json";
	}
}

void Credits::load(Json::Value & val)
{
	active = val["Credits"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Credits"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Credits::save(Json::Value & val)
{
	val["Credits"]["active"] = active;
	if (Owner != nullptr)
	{
		val["Credits"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Credits::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("CreditsButton"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##DoorReactor"))
			GetOwner()->detach(this);
	}
}

void HowToPlay::Execute() {
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}
	scene->currLevel = "HowToPlay.json";
}

void HowToPlay::load(Json::Value & val) {
	active = val["HowToPlay"].get("active", true).asBool();
}

void HowToPlay::save(Json::Value & val) {
	val["HowToPlay"]["active"] = active;
}

void HowToPlay::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("HowToPlayButton"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##HowToPlay"))
			GetOwner()->detach(this);
	}
}

bool FullScreenButton::CheckMouseInside(glm::vec2 mousePos)
{
	if (StaticPointToStaticRect(mousePos, glm::vec2(CheckBox->mTransform->position.x, CheckBox->mTransform->position.y), CheckBox->mTransform->scale.x, CheckBox->mTransform->scale.y))
	{
		return true;
	}
	return false;
}

void FullScreenButton::Initialize()
{
	Button::Initialize();
	glm::vec3 CheckBoxPos = GetOwner()->mTransform->position;
	CheckBoxPos.x += 200;
	CheckBoxPos.y += 20;
	CheckBox = MakeThing("./Data/Jsons/Buttons/checkbox.json", CheckBoxPos);
	CheckBox->mRenderable->texture = (mWindow->fullScreen) ? (resoManager->getReso<Texture>("Checkbox.png")) : (resoManager->getReso<Texture>("Checkbox vacia.png"));
	GetOwner()->GetComponent<Renderable>("Renderable")->visible = true;
}

void FullScreenButton::Execute()
{
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}

	mWindow->FullScreen(!mWindow->fullScreen);
	CheckBox->GetComponent<Renderable>()->texture = (mWindow->fullScreen) ? (resoManager->getReso<Texture>("Checkbox.png")) : (resoManager->getReso<Texture>("Checkbox vacia.png"));
}

void FullScreenButton::Update()
{
	Button::Update();
	CheckBox->GetComponent<Renderable>()->texture = (mWindow->fullScreen) ? (resoManager->getReso<Texture>("Checkbox.png")) : (resoManager->getReso<Texture>("Checkbox vacia.png"));
}

void FullScreenButton::load(Json::Value & val)
{
	active = val["FullScreenButton"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["FullScreenButton"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void FullScreenButton::save(Json::Value & val)
{
	val["FullScreenButton"]["active"] = active;
	if (Owner != nullptr)
	{
		val["FullScreenButton"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void FullScreenButton::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("FullScreenButton"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
	}
	ImGui::NewLine();
	if (ImGui::Button("Detach##DoorReactor"))
		GetOwner()->detach(this);
}

bool VSyncButton::CheckMouseInside(glm::vec2 mousePos) {
	return StaticPointToStaticRect(mousePos, glm::vec2(check_box->mTransform->position.x, check_box->mTransform->position.y), check_box->mTransform->scale.x, check_box->mTransform->scale.y);
}

void VSyncButton::Initialize() {
	Button::Initialize();
	glm::vec3 CheckBoxPos = GetOwner()->mTransform->position;
	CheckBoxPos.x += 200;
	CheckBoxPos.y += 20;
	check_box = MakeThing("./Data/Jsons/Buttons/checkbox.json", CheckBoxPos);
	check_box->mRenderable->texture = (mWindow->v_sync_active) ? (resoManager->getReso<Texture>("Checkbox.png")) : (resoManager->getReso<Texture>("Checkbox vacia.png"));
}

void VSyncButton::Execute() {
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}

	mWindow->v_sync_toggle();
	check_box->GetComponent<Renderable>()->texture = (mWindow->v_sync_active) ? (resoManager->getReso<Texture>("Checkbox.png")) : (resoManager->getReso<Texture>("Checkbox vacia.png"));
}

void VSyncButton::Update() {
	Button::Update();
	check_box->GetComponent<Renderable>()->texture = (mWindow->v_sync_active) ? (resoManager->getReso<Texture>("Checkbox.png")) : (resoManager->getReso<Texture>("Checkbox vacia.png"));
}

void VSyncButton::load(Json::Value & val) {
	active = val["VSyncButton"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["VSyncButton"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void VSyncButton::save(Json::Value & val) {
	val["VSyncButton"]["active"] = active;
	if (Owner != nullptr)
	{
		val["VSyncButton"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void VSyncButton::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("VSyncButton"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
	}
	ImGui::NewLine();
	if (ImGui::Button("Detach##VSyncButton"))
		GetOwner()->detach(this);
}

void BackToMain::Execute()
{
	cam = graphics->GetCameraByRenderLayer("UI");
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}

	if (scene->currLevel == "MainMenu.json")
	{
		scene->currLevel = "Options.json";
	}
	else {
		cam->position.x = scene->main_sure_position.x;
		cam->position.y = scene->main_sure_position.y;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;
	}
}

void BackToMain::load(Json::Value & val)
{
	active = val["BackToMain"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["BackToMain"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void BackToMain::save(Json::Value & val)
{
	val["BackToMain"]["active"] = active;
	if (Owner != nullptr)
	{
		val["BackToMain"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void BackToMain::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("BackToMainButton"))
	{
		ImGui::Checkbox("active##BackToMain", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##BackToMain"))
			GetOwner()->detach(this);
	}
}

void YesToMain::Execute() {
	if (scene->currLevel != "AreYouSure.json") {
		cam = graphics->GetMainCamera();
		cam->position.x = 0.f;
		cam->position.y = 0.f;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;
		scene->isPaused = !scene->isPaused;
		scene->pause_logic();
		scene->currLevel = "MainMenu.json";
		return;
	}
	mWindow->quit = true;
}

void YesToMain::load(Json::Value & val) {
	active = val["YesToMain"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["YesToMain"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void YesToMain::save(Json::Value & val) {
	val["YesToMain"]["active"] = active;
	if (Owner != nullptr)
	{
		val["YesToMain"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void YesToMain::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("YesToMainButton"))
	{
		ImGui::Checkbox("active##YesToMain", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##YesToMain"))
			GetOwner()->detach(this);
	}
}

void YesToRestart::Execute() {
	scene->restartSignal = true;
	scene->isPaused = !scene->isPaused;
	scene->pause_logic();
}

void YesToRestart::load(Json::Value & val) {
	active = val["YesToRestart"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["YesToRestart"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void YesToRestart::save(Json::Value & val) {
	val["YesToRestart"]["active"] = active;
	if (Owner != nullptr)
	{
		val["YesToRestart"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void YesToRestart::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("YesToRestart"))
	{
		ImGui::Checkbox("active##YesToRestart", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##YesToRestart"))
			GetOwner()->detach(this);
	}
}

void NoToPause::Execute() {
	if (scene->currLevel != "AreYouSure.json") {
		cam = graphics->GetCameraByRenderLayer("UI");
		cam->position.x = scene->pause_position.x;
		cam->position.y = scene->pause_position.y;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;
		return;
	}
	scene->currLevel = "MainMenu.json";
}

void NoToPause::load(Json::Value & val) {
	active = val["NoToPause"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["NoToPause"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void NoToPause::save(Json::Value & val) {
	val["NoToPause"]["active"] = active;
	if (Owner != nullptr)
	{
		val["NoToPause"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void NoToPause::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("NoToPauseButton"))
	{
		ImGui::Checkbox("active##NoToPause", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##NoToPause"))
			GetOwner()->detach(this);
	}
}

void NoToPauseFromRestart::Execute() {
	cam = graphics->GetCameraByRenderLayer("UI");
	cam->position.x = scene->pause_position.x;
	cam->position.y = scene->pause_position.y;
	cam->view.x = cam->position.x;
	cam->view.y = cam->position.y;
}

void NoToPauseFromRestart::load(Json::Value & val) {
	active = val["NoToPauseFromRestart"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["NoToPauseFromRestart"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void NoToPauseFromRestart::save(Json::Value & val) {
	val["NoToPauseFromRestart"]["active"] = active;
	if (Owner != nullptr)
	{
		val["NoToPauseFromRestart"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void NoToPauseFromRestart::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("NoToPauseFromRestart"))
	{
		ImGui::Checkbox("active##NoToPauseFromRestart", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##NoToPauseFromRestart"))
			GetOwner()->detach(this);
	}
}

void Quit::Execute() {
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Accept.mp3", false);
		}
	}
	scene->currLevel = "AreYouSure.json";
}

void Quit::load(Json::Value & val) {
	active = val["Quit"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Quit"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Quit::save(Json::Value & val) {
	val["Quit"]["active"] = active;
	if (Owner != nullptr)
	{
		val["Quit"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Quit::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("QuitButton"))
	{
		ImGui::Checkbox("active##Quit", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Quit"))
			GetOwner()->detach(this);
	}
}

bool MasterVolumeButton::CheckMouseInside(glm::vec2 mousePos)
{
	if (StaticPointToStaticRect(mousePos, glm::vec2(Plus->mTransform->position.x, Plus->mTransform->position.y), Plus->mTransform->scale.x, Plus->mTransform->scale.y))
	{
		moueonplus = true;
		return true;
	}
	else
	{
		moueonplus = false;
	}
	if (StaticPointToStaticRect(mousePos, glm::vec2(Minus->mTransform->position.x, Minus->mTransform->position.y), Minus->mTransform->scale.x, Minus->mTransform->scale.y))
	{
		mouseonminus = true;
		return true;
	}
	else
	{
		mouseonminus = false;
	}
	if (StaticPointToStaticRect(mousePos, glm::vec2(Mute->mTransform->position.x, Mute->mTransform->position.y), Mute->mTransform->scale.x, Mute->mTransform->scale.y))
	{
		mouseonmute = true;
		return true;
	}
	else 
	{
		mouseonmute = false;
	}
	return false;
}

void MasterVolumeButton::Initialize()
{
	Button::Initialize();
	volume = MasterVolume::GetMasterVolume();

	glm::vec3 PLUSPOS = glm::vec3(GetOwner()->mTransform->position.x + 75.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.1f);
	glm::vec3 MINUSPOS = glm::vec3(GetOwner()->mTransform->position.x - 75.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.11f);
	glm::vec3 MutePOS = glm::vec3(GetOwner()->mTransform->position.x - 3.0f * 60.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.12f);

	Plus = MakeThing(std::string("./Data/Jsons/Buttons/plus.json"), PLUSPOS);
	Minus = MakeThing(std::string("./Data/Jsons/Buttons/minus.json"), MINUSPOS);
	Mute = MakeThing(std::string("./Data/Jsons/Buttons/mute.json"), MutePOS);


	if (GetOwner()->GetComponent<FontRenderable>())
	{
		int ActualVolume = static_cast<int>(volume * 100);
		GetOwner()->GetComponent<FontRenderable>()->displayText = std::to_string(ActualVolume);
	}
	
	Muted = (MasterVolume::GetMasterVolume() == 0) ? true : false;

	if (!Muted)
	{
		if (volume != MasterVolume::GetMasterVolume())
		{
			MasterVolume::SetMasterVolume(volume);
		}
	}
	mouseonminus = false;
	mouseonmute = false;
	moueonplus = false;
}

void MasterVolumeButton::Execute()
{
	if (!Owner->UsingMouse)
	{
		Muted = !Muted;
		if (Muted)
		{
			MasterVolume::SetMasterVolume(0.0f);
		}
	}
	else
	{
		if (moueonplus)
		{
			if (volume <= 1.0f)
			{
				volume += 0.01f;
			}
		}
		if (mouseonminus)
		{
			if (volume > 0.0f)
			{
				volume -= 0.01f;
			}
		}
		if (mouseonmute)
		{
			Muted = !Muted;
			if (Muted)
			{
				MasterVolume::SetMasterVolume(0.0f);
			}
		}
	}
}

void MasterVolumeButton::Update()
{
	Button::Update();
	if (Selected)
	{
		if (!Muted)
		{
			if (volume != MasterVolume::GetMasterVolume())
			{
				MasterVolume::SetMasterVolume(volume);
			}
		}
		if (KeyDown(Keyboard::LEFT) || KeyDown(Keyboard::A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT))
		{
			if (volume > 0.0f)
			{
				volume -= 0.01f;
			}
			else
			{
				volume = 0.0f;
			}
			Minus->mTransform->scale *= 1.3f;
		}
		if (ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT)) {
			std::cout << "press" << std::endl;
		}
		if (KeyPressed(Keyboard::LEFT) || KeyPressed(Keyboard::A) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT))
		{
			TimeHeld += ImGui::GetIO().DeltaTime;
			if (TimeHeld >= 0.4f)
			{
				if (volume > 0.0f)
				{
					volume -= 0.01f;
				}
				else
				{
					volume = 0.0f;
				}
			}
		}
		if (KeyDown(Keyboard::RIGHT) || KeyDown(Keyboard::D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
		{
			if (volume <= 1.0f)
			{
				volume += 0.01f;
			}
			else
			{
				volume = 1.0f;
			}
			Plus->mTransform->scale *= 1.3f;
		}
		if (KeyPressed(Keyboard::RIGHT) || KeyPressed(Keyboard::D) || ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
		{
			TimeHeld += ImGui::GetIO().DeltaTime;
			if (TimeHeld >= 0.4f)
			{
				if (volume <= 1.0f)
				{
					volume += 0.01f;
				}
				else
				{
					volume = 1.0f;
				}
			}
		}
		if (KeyUp(Keyboard::LEFT) || KeyUp(Keyboard::RIGHT) || KeyUp(Keyboard::A) || KeyUp(Keyboard::D) || ButtonUp(SDL_CONTROLLER_BUTTON_DPAD_LEFT) || ButtonUp(SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
		{
			TimeHeld = 0.0f;
		}
		if (KeyUp(Keyboard::LEFT) || KeyUp(Keyboard::A) || ButtonUp(SDL_CONTROLLER_BUTTON_DPAD_LEFT))
		{
			Minus->mTransform->scale /= 1.3f;
		}
		if (KeyUp(Keyboard::RIGHT) || KeyUp(Keyboard::D) || ButtonUp(SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
		{
			Plus->mTransform->scale /= 1.3f;
		}
		int ActualVolume = static_cast<int>(volume * 100);
		if (ActualVolume > 100)
		{
			ActualVolume = 100;
		}
		GetOwner()->GetComponent<FontRenderable>()->displayText = std::to_string(ActualVolume);
	}
	else
	{
		if (Plus->mTransform->scale.x > 32.0f)
		{
			Plus->mTransform->scale /= 1.3f;
		}
		if (Minus->mTransform->scale.x > 32.0f)
		{
			Minus->mTransform->scale /= 1.3f;
		}
	}
	Mute->mRenderable->texture = (Muted) ? (resoManager->getReso<Texture>("Volume OFF.png")) : (resoManager->getReso<Texture>("Volume ON.png"));

}

void MasterVolumeButton::load(Json::Value & val)
{
	active = val["MasterVolumeButton"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["MasterVolumeButton"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void MasterVolumeButton::save(Json::Value & val)
{
	val["MasterVolumeButton"]["active"] = active;
	if (Owner != nullptr)
	{
		val["MasterVolumeButton"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void MasterVolumeButton::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("MasterVolumeButton"))
	{
		ImGui::Checkbox("active##MasterVolumeButton", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##DoorReactor"))
			GetOwner()->detach(this);
	}
}

void BackToPause::Execute() {
	if (scene->currLevel != "Options.json" && scene->currLevel != "Credits.json") {
		cam = graphics->GetCameraByRenderLayer("UI");
		cam->position.x = scene->pause_position.x;
		cam->position.y = scene->pause_position.y;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;
		return;
	}
	if (scene->currLevel != "Credits.json") {
		scene->currLevel = "MainMenu.json";
	}
	else {
		transition = true;
	}
}

void BackToPause::Update() {
	Button::Update();
	if (scene->currLevel != "Credits.json" || !transition) {
		return;
	}
	dt += ImGui::GetIO().DeltaTime;
	if (graphics->FadeIn(-100, dt * 0.5f)) {
		scene->currLevel = "MainMenu.json";
	}
}

void BackToPause::load(Json::Value & val) {
	active = val["BackToPause"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["BackToPause"].get("OwnerName", "OwnerName").asString());
	if (Owner_) {
		Owner = Owner_->GetComponent<Menu>();
	}
}

void BackToPause::save(Json::Value & val) {
	val["BackToPause"]["active"] = active;
	if (Owner != nullptr) {
		val["BackToPause"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void BackToPause::WriteToImguiWindow() 
{
	if (ImGui::CollapsingHeader("BackToPause"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr) {
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##BackToPause"))
			GetOwner()->detach(this);
	}
}

void BackToGraphicc::Execute() {
	cam = graphics->GetCameraByRenderLayer("UI");
	cam->position.x = scene->options_position.x;
	cam->position.y = scene->options_position.y;
	cam->view.x = cam->position.x;
	cam->view.y = cam->position.y;
}

void BackToGraphicc::load(Json::Value & val) {
	active = val["BackToGraphicc"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["BackToGraphicc"].get("OwnerName", "OwnerName").asString());
	if (Owner_) {
		Owner = Owner_->GetComponent<Menu>();
	}
}

void BackToGraphicc::save(Json::Value & val) {
	val["BackToGraphicc"]["active"] = active;
	if (Owner != nullptr) {
		val["BackToGraphicc"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void BackToGraphicc::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("BackToGraphicc"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr) {
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##BackToGraphicc"))
			GetOwner()->detach(this);
	}
}

void BackToSound::Execute() {
	if (scene->currLevel != "HowToPlay.json") {
		cam = graphics->GetCameraByRenderLayer("UI");
		cam->position.x = scene->options_position.x;
		cam->position.y = scene->options_position.y;
		cam->view.x = cam->position.x;
		cam->view.y = cam->position.y;
		return;
	}
	scene->currLevel = "MainMenu.json";
}

void BackToSound::load(Json::Value & val) {
	active = val["BackToSound"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["BackToSound"].get("OwnerName", "OwnerName").asString());
	if (Owner_) {
		Owner = Owner_->GetComponent<Menu>();
	}
}

void BackToSound::save(Json::Value & val) {
	val["BackToSound"]["active"] = active;
	if (Owner != nullptr) {
		val["BackToSound"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void BackToSound::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("BackToSound"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr) {
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##BackToSound"))
			GetOwner()->detach(this);
	}
}

void Resume::Execute() 
{
	scene->isPaused = !scene->isPaused;
	scene->pause_logic();
}

void Resume::load(Json::Value & val) {
	active = val["Resume"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Resume"].get("OwnerName", "OwnerName").asString());
	if (Owner_) {
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Resume::save(Json::Value & val) {
	val["Resume"]["active"] = active;
	if (Owner != nullptr) {
		val["Resume"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Resume::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("Resume"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr) {
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Resume"))
			GetOwner()->detach(this);
	}
}

void Restart::Execute() {
	cam = graphics->GetCameraByRenderLayer("UI");
	SoundEmitter * e = NULL;
	e = this->GetOwner()->GetComponent<SoundEmitter>();
	if (e)
	{
		if (e->IsInit)
		{
			e->PlayOnce("Menu_Select16.mp3", false);
		}
	}
	cam->position.x = scene->restart_sure_position.x;
	cam->position.y = scene->restart_sure_position.y;
	cam->view.x = cam->position.x;
	cam->view.y = cam->position.y;
}

void Restart::load(Json::Value & val) {
	active = val["Restart"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Restart"].get("OwnerName", "OwnerName").asString());
	if (Owner_) {
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Restart::save(Json::Value & val) {
	val["Restart"]["active"] = active;
	if (Owner != nullptr) {
		val["Restart"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Restart::WriteToImguiWindow() {
	if (ImGui::CollapsingHeader("Restart"))
	{
		ImGui::Text(name.c_str());
		if (Owner != nullptr) {
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Restart"))
			GetOwner()->detach(this);
	}
}
	
void MusicVolumeButton::Initialize()
{
	Button::Initialize();
	volume = MasterVolume::GetMusicMasterVolume();

	glm::vec3 PLUSPOS = glm::vec3(GetOwner()->mTransform->position.x + 75.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.1f);
	glm::vec3 MINUSPOS = glm::vec3(GetOwner()->mTransform->position.x - 75.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.11f);
	glm::vec3 MutePOS = glm::vec3(GetOwner()->mTransform->position.x - 3.0f * 60.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.12f);

	Plus = MakeThing(std::string("./Data/Jsons/PlusButton.json"), PLUSPOS);
	Minus = MakeThing(std::string("./Data/Jsons/MinusButton.json"), MINUSPOS);
	Mute = MakeThing(std::string("./Data/Jsons/Mute.json"), MutePOS);

	if (GetOwner()->GetComponent<FontRenderable>())
	{
		int ActualVolume = static_cast<int>(volume * 100);
		GetOwner()->GetComponent<FontRenderable>()->displayText = std::to_string(ActualVolume);
	}
	Muted = (MasterVolume::GetMusicMasterVolume() == 0) ? true : false;
	if (!Muted)
	{
		if (volume != MasterVolume::GetMusicMasterVolume())
		{
			MasterVolume::SetMusicVolume(volume);
		}
	}
}

void MusicVolumeButton::Execute()
{
	if (!Owner->UsingMouse)
	{
		Muted = !Muted;
		if (Muted)
		{
			MasterVolume::SetMusicVolume(0.0f);
		}
	}
	else
	{
		if (moueonplus)
		{
			if (volume <= 1.0f)
			{
				volume += 0.01f;
			}
		}
		if (mouseonminus)
		{
			if (volume > 0.0f)
			{
				volume -= 0.01f;
			}
		}
		if (mouseonmute)
		{
			Muted = !Muted;
			if (Muted)
			{
				MasterVolume::SetMusicVolume(0.0f);
			}
		}
	}
}

void MusicVolumeButton::Update()
{
	Button::Update();
	if (Selected)
	{
		if (!Muted)
		{
			if (volume != MasterVolume::GetMusicMasterVolume())
			{
				MasterVolume::SetMusicVolume(volume);
			}
		}
		if (KeyDown(Keyboard::LEFT))
		{
			if (volume > 0.0f)
			{
				volume -= 0.01f;
			}
			else
			{
				volume = 0.0f;
			}
			Minus->mTransform->scale *= 1.3f;
		}
		if (KeyPressed(Keyboard::LEFT))
		{
			TimeHeld += ImGui::GetIO().DeltaTime;
			if (TimeHeld >= 0.4f)
			{
				if (volume > 0.0f)
				{
					volume -= 0.01f;
				}
				else
				{
					volume = 0.0f;
				}
			}
		}
		if (KeyDown(Keyboard::RIGHT))
		{
			if (volume <= 1.0f)
			{
				volume += 0.01f;
			}
			else
			{
				volume = 1.0f;
			}
			Plus->mTransform->scale *= 1.3f;
		}
		if (KeyPressed(Keyboard::RIGHT))
		{
			TimeHeld += ImGui::GetIO().DeltaTime;
			if (TimeHeld >= 0.4f)
			{
				if (volume <= 1.0f)
				{
					volume += 0.01f;
				}
				else
				{
					volume = 1.0f;
				}
			}
		}
		if (KeyUp(Keyboard::LEFT) || KeyUp(Keyboard::RIGHT))
		{
			TimeHeld = 0.0f;
		}
		if (KeyUp(Keyboard::LEFT))
		{
			Minus->mTransform->scale /= 1.3f;
		}
		if (KeyUp(Keyboard::RIGHT))
		{
			Plus->mTransform->scale /= 1.3f;
		}

		int ActualVolume = static_cast<int>(volume * 100);
		if (ActualVolume > 100)
		{
			ActualVolume = 100;
		}
		GetOwner()->GetComponent<FontRenderable>()->displayText = std::to_string(ActualVolume);
	}
	else
	{
		if (Plus->mTransform->scale.x > 32.0f)
		{
			Plus->mTransform->scale /= 1.3f;
		}
		if (Minus->mTransform->scale.x > 32.0f)
		{
			Minus->mTransform->scale /= 1.3f;
		}
	}
	Mute->mRenderable->texture = (Muted) ? (resoManager->getReso<Texture>("Volume OFF.png")) : (resoManager->getReso<Texture>("Volume ON.png"));
}

void MusicVolumeButton::load(Json::Value & val)
{
	active = val["MusicVolumeButton"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["MusicVolumeButton"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void MusicVolumeButton::save(Json::Value & val)
{
	val["MusicVolumeButton"]["active"] = active;
	if (Owner != nullptr)
	{
		val["MusicVolumeButton"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void MusicVolumeButton::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("MusicVolumeButton"))
	{
		ImGui::Checkbox("active##MusicVolumeButton", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##MusicVolumeButton"))
			GetOwner()->detach(this);
	}
}

void SFXVolumeButton::Initialize()
{
	Button::Initialize();
	volume = MasterVolume::GetEffectsMasterVolume();

	glm::vec3 PLUSPOS = glm::vec3(GetOwner()->mTransform->position.x + 75.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.1f);
	glm::vec3 MINUSPOS = glm::vec3(GetOwner()->mTransform->position.x - 75.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.11f);
	glm::vec3 MutePOS = glm::vec3(GetOwner()->mTransform->position.x - 3.0f * 60.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.12f);

	Plus = MakeThing(std::string("./Data/Jsons/PlusButton.json"), PLUSPOS);
	Minus = MakeThing(std::string("./Data/Jsons/MinusButton.json"), MINUSPOS);
	Mute = MakeThing(std::string("./Data/Jsons/Mute.json"), MutePOS);

	if (GetOwner()->GetComponent<FontRenderable>())
	{
		int ActualVolume = static_cast<int>(volume * 100);
		GetOwner()->GetComponent<FontRenderable>()->displayText = std::to_string(ActualVolume);
	}
	
	Muted = (MasterVolume::GetEffectsMasterVolume() == 0) ? true : false;

	if (!Muted)
	{
		if (volume != MasterVolume::GetEffectsMasterVolume())
		{
			MasterVolume::SetEffectsVolume(volume);
		}
	}
	Mute->mRenderable->texture = (Muted) ? (resoManager->getReso<Texture>("Volume OFF.png")) : (resoManager->getReso<Texture>("Volume ON.png"));
}

void SFXVolumeButton::Execute()
{
	if (!Owner->UsingMouse)
	{
		Muted = !Muted;
		if (Muted)
		{
			MasterVolume::SetEffectsVolume(0.0f);
		}
	}
	else
	{
		if (moueonplus)
		{
			if (volume <= 1.0f)
			{
				volume += 0.01f;
			}
		}
		if (mouseonminus)
		{
			if (volume > 0.0f)
			{
				volume -= 0.01f;
			}
		}
		if (mouseonmute)
		{
			Muted = !Muted;
			if (Muted)
			{
				MasterVolume::SetEffectsVolume(0.0f);
			}
		}
	}
}

void SFXVolumeButton::Update()
{
	Button::Update();
	if (Selected)
	{
		if (!Muted)
		{
			if (volume != MasterVolume::GetEffectsMasterVolume())
			{
				MasterVolume::SetEffectsVolume(volume);
			}
		}
		if (KeyDown(Keyboard::LEFT))
		{
			if (volume > 0.0f)
			{
				volume -= 0.01f;
			}
			else
			{
				volume = 0.0f;
			}
			Minus->mTransform->scale *= 1.3f;
		}
		if (KeyPressed(Keyboard::LEFT))
		{
			TimeHeld += ImGui::GetIO().DeltaTime;
			if (TimeHeld >= 0.4f)
			{
				if (volume > 0.0f)
				{
					volume -= 0.01f;
				}
				else
				{
					volume = 0.0f;
				}
			}
		}
		if (KeyDown(Keyboard::RIGHT))
		{
			if (volume <= 1.0f)
			{
				volume += 0.01f;
			}
			else
			{
				volume = 1.0f;
			}
			Plus->mTransform->scale *= 1.3f;
		}
		if (KeyPressed(Keyboard::RIGHT))
		{
			TimeHeld += ImGui::GetIO().DeltaTime;
			if (TimeHeld >= 0.4f)
			{
				if (volume <= 1.0f)
				{
					volume += 0.01f;
				}
				else
				{
					volume = 1.0f;
				}
			}
		}
		if (KeyUp(Keyboard::LEFT) || KeyUp(Keyboard::RIGHT))
		{
			TimeHeld = 0.0f;
		}
		if (KeyUp(Keyboard::LEFT))
		{
			Minus->mTransform->scale /= 1.3f;
		}
		if (KeyUp(Keyboard::RIGHT))
		{
			Plus->mTransform->scale /= 1.3f;
		}

		int ActualVolume = static_cast<int>(volume * 100);
		if (ActualVolume > 100)
		{
			ActualVolume = 100;
		}
		GetOwner()->GetComponent<FontRenderable>()->displayText = std::to_string(ActualVolume);
	}
	else
	{
		if (Plus->mTransform->scale.x > 32.0f)
		{
			Plus->mTransform->scale /= 1.3f;
		}
		if (Minus->mTransform->scale.x > 32.0f)
		{
			Minus->mTransform->scale /= 1.3f;
		}
	}
	Mute->mRenderable->texture = (Muted) ? (resoManager->getReso<Texture>("Volume OFF.png")) : (resoManager->getReso<Texture>("Volume ON.png"));
}

void SFXVolumeButton::load(Json::Value & val)
{
	active = val["SFXVolumeButton"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["SFXVolumeButton"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void SFXVolumeButton::save(Json::Value & val)
{
	val["SFXVolumeButton"]["active"] = active;
	if (Owner != nullptr)
	{
		val["SFXVolumeButton"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void SFXVolumeButton::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("SFXVolumeButton"))
	{
		ImGui::Checkbox("active##SFXVolumeButton", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##SFXVolumeButton"))
			GetOwner()->detach(this);
	}
}

void Apply::Execute()
{
	mWindow->currentResolution = Owner->index;
	mWindow->SetResolution();
}

void Apply::load(Json::Value & val)
{
	active = val["Apply"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Apply"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Apply::save(Json::Value & val)
{
	val["Apply"]["active"] = active;
	if (Owner != nullptr)
	{
		val["Apply"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Apply::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Apply"))
	{
		ImGui::Checkbox("active##Apply", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Apply"))
			GetOwner()->detach(this);
	}
}

bool Resolutions::CheckMouseInside(glm::vec2 mousePos)
{
	if (StaticPointToStaticRect(mousePos, glm::vec2(Right->mTransform->position.x, Right->mTransform->position.y), Right->mTransform->scale.x, Right->mTransform->scale.y))
	{
		mouseonright = true;
		return true;
	}
	else
	{
		mouseonright = false;
	}
	if (StaticPointToStaticRect(mousePos, glm::vec2(Left->mTransform->position.x, Left->mTransform->position.y), -Left->mTransform->scale.x, Left->mTransform->scale.y))
	{
		mouseonleft = true;
		return true;
	}
	else
	{
		mouseonleft = false;
	}
	return false;
}

void Resolutions::Initialize()
{
	Button::Initialize();
	glm::vec3 RightAPos = glm::vec3(GetOwner()->mTransform->position.x + 170.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.1f);
	glm::vec3 LeftAPos = glm::vec3(GetOwner()->mTransform->position.x - 150.0f, GetOwner()->mTransform->position.y, GetOwner()->mTransform->position.z + 0.1f);
	Left = MakeThing("./Data/Jsons/Buttons/left_arrow.json", LeftAPos);
	Right = MakeThing("./Data/Jsons/Buttons/left_arrow.json", RightAPos);
	float ini = Left->mTransform->scale.x;
	Left->mTransform->scale.x = -ini;
	if (Owner)
	{
		Owner->index = mWindow->currentResolution;
		GetOwner()->GetComponent<FontRenderable>()->displayText = ResolutionsArray[Owner->index];
	}
	
}

void Resolutions::Execute()
{
	if (Owner->UsingMouse)
	{
		if (mouseonleft)
		{
			if (Owner->index != 0)
			{
				Owner->index -= 1;
				GetOwner()->GetComponent<FontRenderable>()->displayText = ResolutionsArray[Owner->index];
			}
			else
			{
				Owner->index = 3;
				GetOwner()->GetComponent<FontRenderable>()->displayText = ResolutionsArray[Owner->index];
			}
		}
		else if (mouseonright)
		{
			if (Owner->index != 3)
			{
				Owner->index += 1;
				GetOwner()->GetComponent<FontRenderable>()->displayText = ResolutionsArray[Owner->index];
			}
			else
			{
				Owner->index = 0;
				GetOwner()->GetComponent<FontRenderable>()->displayText = ResolutionsArray[Owner->index];
			}
		}
	}
	mWindow->currentResolution = Owner->index;
	mWindow->SetResolution();
}

void Resolutions::Update()
{
	Button::Update();
	if (Selected)
	{
		if (KeyDown(Keyboard::LEFT) || KeyDown(Keyboard::A) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT))
		{
			if (Owner->index != 0)
			{
				Owner->index -= 1;	
			}
			else
			{
				Owner->index = 3;
			}
			mWindow->currentResolution = Owner->index;
			mWindow->SetResolution();
		}
		if (KeyDown(Keyboard::RIGHT) || KeyDown(Keyboard::D) || ButtonDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
		{
			if (Owner->index != 3)
			{
				Owner->index += 1;
			}
			else
			{
				Owner->index = 0;
			}
			mWindow->currentResolution = Owner->index;
			mWindow->SetResolution();
		}
	}
	GetOwner()->GetComponent<FontRenderable>()->displayText = ResolutionsArray[Owner->index];
}

void Resolutions::load(Json::Value & val)
{
	active = val["Resolutions"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Resolutions"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Resolutions::save(Json::Value & val)
{
	val["Resolutions"]["active"] = active;
	if (Owner != nullptr)
	{
		val["Resolutions"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Resolutions::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Resolutions"))
	{
		ImGui::Checkbox("active##Resolutions", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Resolutions"))
			GetOwner()->detach(this);
	}
}

void Controls::Initialize()
{
	Button::Initialize();
	//Mouse->GetComponent<Renderable>()->visible = true;
	//W->GetComponent<Renderable>()->visible = true;
	//A->GetComponent<Renderable>()->visible = true;
	//S->GetComponent<Renderable>()->visible = true;
	//D->GetComponent<Renderable>()->visible = true;
	//SpaceBar->GetComponent<Renderable>()->visible = true;
	//Q->GetComponent<Renderable>()->visible = true;
}

void Controls::Execute()
{
	if (!Owner->Lock)
	{
		Owner->Lock = true;
		Mouse->GetComponent<Renderable>()->visible = true;
		W->GetComponent<Renderable>()->visible = true;
		A->GetComponent<Renderable>()->visible = true;
		S->GetComponent<Renderable>()->visible = true;
		D->GetComponent<Renderable>()->visible = true;
		SpaceBar->GetComponent<Renderable>()->visible = true;
		Q->GetComponent<Renderable>()->visible = true;
	}
	else
	{
		Owner->Lock = false;
		Mouse->GetComponent<Renderable>()->visible = false;
		W->GetComponent<Renderable>()->visible = false;
		A->GetComponent<Renderable>()->visible = false;
		S->GetComponent<Renderable>()->visible = false;
		D->GetComponent<Renderable>()->visible = false;
		SpaceBar->GetComponent<Renderable>()->visible = false;
		Q->GetComponent<Renderable>()->visible = false;
	}
}

void Controls::load(Json::Value & val)
{
	active = val["Controls"].get("active", true).asBool();
	auto Owner_ = scene->FindObject(val["Controls"].get("OwnerName", "OwnerName").asString());
	if (Owner_)
	{
		Owner = Owner_->GetComponent<Menu>();
	}
}

void Controls::save(Json::Value & val)
{
	val["Controls"]["active"] = active;
	if (Owner != nullptr)
	{
		val["Controls"]["OwnerName"] = Owner->GetOwner()->GetName();
	}
}

void Controls::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Controls"))
	{
		ImGui::Checkbox("active##Button", &active);
		if (Owner != nullptr)
		{
			ImGui::Text(Owner->GetOwner()->GetName().c_str());
		}
		ImGui::NewLine();
		if (ImGui::Button("Detach##Button"))
			GetOwner()->detach(this);
	}
}

void QuitToPause::Execute()
{
}

void QuitToPause::load(Json::Value & val)
{
}

void QuitToPause::save(Json::Value & val)
{
}

void QuitToPause::WriteToImguiWindow()
{
}
