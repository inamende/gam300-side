#ifndef ENTITY_H
#define ENTITY_H

#include "../EngineCommon.h"

class Entity
{
	friend class Factory;
public:
	Entity();
	virtual ~Entity() {}

	void SetName(std::string n);
	std::string GetName();
	unsigned GetId();

	//RTTI
	template<typename T>
	static std::string GetType()
	{
		std::string class_className = typeid(T).name();
		//typeid will return a string like "class ActualClass" but we omit "class"
		std::string className = class_className.erase(0, 6); //get rid of class

		return className;
	}

	bool operator==(Entity & other);

private:
	std::string name;
	unsigned uniqueId;
};

#endif