#include "Entity.h"

Entity::Entity() {}

void Entity::SetName(std::string n) { name = n; }
std::string Entity::GetName() { return name; }
unsigned Entity::GetId() { return uniqueId;  }

bool Entity::operator==(Entity & other)
{
	return uniqueId == other.uniqueId;
}
