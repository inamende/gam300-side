#include "Component.h"

GameObject * Component::GetOwner() const
{
	return _owner;
}

void Component::SetOwner(GameObject * other)
{
	_owner = other;
}

void Component::Initialize() { /*std::cout << "oops..." << std::endl;*/ }

void Component::Update() {/* std::cout << "oops..." << std::endl; */}

void Component::Terminate() {}
