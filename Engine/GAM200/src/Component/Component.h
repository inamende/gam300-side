#ifndef COMPONENT_H
#define COMPONENT_H

#include "../Component/Entity.h"

class GameObject;

class Component : public Entity
{
	friend class Physics;
 public:
	Component() : active(true)
	{
		//std::cout << "compp[ppppppp" << std::endl;

		//std::cout << typeid(*this).name() << std::endl;
	}
	virtual ~Component() {}
	virtual void load(Json::Value & val) = 0;
	virtual void save(Json::Value & val) = 0;
	virtual void WriteToImguiWindow() = 0;

	GameObject * GetOwner() const;
	void SetOwner(const GameObject *);
	void SetOwner(GameObject *);
	
	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	friend class GameObject;

//protected:
	bool active;

private:
	GameObject * _owner = NULL;
};

#endif