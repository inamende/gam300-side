#include "GameObject.h"

#include "../Component/Component.h"
#include "../Factory/Factory.h"

GameObject::GameObject() : componentNum(0), alive(false), active(false), save(true) {}

GameObject::~GameObject() {}

void GameObject::attach(Component * obj)
{
	components.push_back(obj);
	obj->_owner = this;
	componentNum++;
}

void GameObject::detach(Component * obj)
{
	std::vector<Component*>::iterator it = std::find(components.begin(), components.end(), obj);
	if (it != components.end())
	{
		(*it)->Terminate();
		delete (*it);
		components.erase(it);
	}

	componentNum--;
}

unsigned GameObject::component_count()
{
	return componentNum;
}

void GameObject::Initialize()
{
	for (std::vector<Component*>::iterator it = components.begin(); it != components.end(); it++)
		(*it)->Initialize();
}

void GameObject::Update()
{
	for (std::vector<Component*>::iterator it = components.begin(); it != components.end(); it++)
		if((*it)->active) (*it)->Update();
}

void GameObject::Terminate()
{
	for (std::vector<Component*>::iterator it = components.begin(); it != components.end(); it++)
	{
		(*it)->Terminate();
		delete (*it);
	}
	components.clear();
}

void GameObject::SaveJson(std::string jsonName)
{
	value["name"] = GetName();

	SaveComponents(value);

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();

	std::ofstream jsonFileWrite(jsonName);

	writer->write(value, &jsonFileWrite);

	jsonFileWrite.close();
}

bool GameObject::LoadJson(std::string jsonName)
{
	std::ifstream jsonFile(jsonName);
	Json::Reader reader;

	bool ok = reader.parse(jsonFile, value);

	if (!ok)
	{
		//std::cout << "ERROR READING JSON : " << jsonName << std::endl;
		return false;
	}
		

	SetName(value.get("name", "unnamed").asString());
	AttachComponents(value);
	LoadComponents(value);

	jsonFile.close();

	return true;
}

void GameObject::AttachComponents(Json::Value & val)
{
	value = val;

	std::vector<std::string> members = val.getMemberNames();

	std::string compType; // actual name of the component ie. "BoxCollider"
	std::string compName; // name of the component ie. "BoxCollider_9"

	for (std::vector<std::string>::iterator it = members.begin(); it != members.end(); it++)
	{
		if ((*it) != "name")
		{
			// same component twice logic...
			size_t lastindex = (*it).find_last_of("_");

			std::string noExtension = (*it).substr(0, lastindex);

			compType = noExtension; 
			compName = (*it);

			Component * comp = dynamic_cast<Component*>(mFactory->Create(compType.c_str()));

			comp->SetName(compName);

			if (GetComponent<Component>((*it).c_str()) == NULL) //if the game object doesn't have the component -> attach it
				attach(comp);
		}
	}
}

void GameObject::LoadComponents(Json::Value & val)
{
	for (std::vector<Component*>::iterator it = components.begin(); it != components.end(); it++)
		(*it)->load(val);
}

void GameObject::SaveComponents(Json::Value & val)
{
	for (std::vector<Component*>::iterator it = components.begin(); it != components.end(); it++)
		(*it)->save(val);
}

void GameObject::OpenComponentWindows()
{
	for (unsigned i = 0; i < component_count(); i++)
		if (components[i]) components[i]->WriteToImguiWindow();
}

Json::Value & GameObject::GetValue() { return value; }

void GameObject::set_parent(GameObject * parent)
{
	_parent = parent;
}

GameObject * GameObject::get_parent()
{
	return _parent;
}

void GameObject::set_child(GameObject * child)
{
	_child = child;
}

GameObject * GameObject::get_child()
{
	return _child;
}
