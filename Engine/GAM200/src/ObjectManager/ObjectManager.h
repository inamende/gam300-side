#ifndef OBJECT_MANAGER_H
#define OBJECT_MANAGER_H

#include "../Component/Entity.h"
#include "../Events/event.h"

class GameObject;

class ObjectManager : Entity
{
private:

	static ObjectManager * instance;
	ObjectManager();

	bool autoSave;

public:
	~ObjectManager();
	static ObjectManager * getInstance();

	GameObject * AddGameObject(GameObject * obj, const char * name = NULL);
	void RemoveGameObject(GameObject * obj);

	void LoadObjectsFromFile(std::string filePath);
	void SaveObjectsToFile(std::string filePath);

	void UnloadObjects();

	void FreeDeadObjects();

	GameObject * FindObjectByName(std::string name);
	GameObject * FindObjectById(unsigned id);
	void FindObjectByName(std::string name, std::list<GameObject*> & obj);

	std::list<GameObject*> aliveObjects;
	std::list<GameObject*> deadObjects;

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void Load();
	virtual void Save();

	bool AutosaveEnabled();
	void SetAutoSave(bool enable);
	void AutoSave(float frequency);

	void WriteToImguiWindow();
};

class DeletedObjectEvent : public Event {};

#define objectManager ObjectManager::getInstance()

#endif