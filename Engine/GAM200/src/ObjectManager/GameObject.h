#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "../Component/Entity.h"

#include "../Physics/Comp/RigidBody.h"
#include "../Events/event.h"

class Component;
class Transform;
class Renderable;
class Animator;
class PlayerController;
//class Forms;
class ArrowDirection;
class BoxCollider;

#define mTransform GetComponent<Transform>()
#define mRenderable GetComponent<Renderable>()
#define mAnimator GetComponent<Animator>()
#define mRigidBody GetComponent<RigidBody>()
#define mController GetComponent<PlayerController>()
//#define mForms GetComponent<Forms>()
#define mArrowDi GetComponent<ArrowDirection>()


class GameObject : public Entity
{
public:
	bool alive;
	bool active;

	bool save;

	GameObject();
	~GameObject();

	void attach(Component * obj);
	void detach(Component * obj);
	unsigned component_count();

	template<typename T>
	unsigned component_count()
	{
		unsigned count = 0;

		for (auto it : components)
			if (dynamic_cast<T*>(it))
				count++;

		return count;
	}

	template<typename T>
	T * GetComponent(const char * compName)
	{
		auto find = std::find_if(components.begin(), components.end(), [&](Component * c) { return c->GetName() == compName; });
		return (find != components.end()) ? reinterpret_cast<T*>(*find) : NULL;
	}

	template<typename T>
	T * GetComponent()
	{
		auto find = std::find_if(components.begin(), components.end(), [](Component * c) { return dynamic_cast<T*>(c); });
		return (find != components.end()) ? dynamic_cast<T*>(*find) : NULL;
	}

	template<typename T>
	std::vector<T*> GetComponents()
	{
		std::vector<T*> comp;
		for (auto it : components)
			if (dynamic_cast<T*>(it))
				comp.push_back(dynamic_cast<T*>(it));
		return comp;
	}

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	void LoadComponents(Json::Value & val);
	void SaveComponents(Json::Value & val);

	void SaveJson(std::string jsonName);
	bool LoadJson(std::string jsonName);

	void AttachComponents(Json::Value & val);

	//ImGui
	void OpenComponentWindows();

	//Event handler
	EventHandler local_handler;

	template<typename E, typename O>
	void register_handler(O & obj, void(O::*fp)(const E&))
	{
		// if there are no handlres for this event yet, we subscribe the handle to the global_handler
		if (local_handler.get_map().find(type_of<E>().get_name()) == local_handler.get_map().end())
			global_handler.register_handler(local_handler, &EventHandler::handle<E>);

		local_handler.register_handler(obj, fp);
	}

	template<typename E, typename O>
	void unregister_handler(O & obj, void(O::*fp)(const E&))
	{
		short int result = local_handler.unregister_handler(obj, fp);

		// Check if the local handler's subscribers for this event is empty
		if (result == -1)
			global_handler.unregister_handler(obj, fp);
	}

	Json::Value & GetValue();

	// Hierarchy
	void set_parent(GameObject* parent);
	GameObject* get_parent();

	void set_child(GameObject* child);
	GameObject* get_child();

	std::vector<Component*> components;

private:
	
	unsigned componentNum;
	Json::Value value;

	GameObject* _parent;
	GameObject* _child;
};

#endif