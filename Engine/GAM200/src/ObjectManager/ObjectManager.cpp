#include "ObjectManager.h"

#include "../Editor/ImGuiEditor.h"
#include "../Factory/Factory.h"
#include "../Gfx/Gfx.h"
#include "../Gfx/Reso/Camera.h"
#include "../Level/Scene.h"
#include "../Events/event.h"

ObjectManager * ObjectManager::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW ObjectManager();

	return instance;
}

ObjectManager::ObjectManager() : autoSave(true) {}

ObjectManager::~ObjectManager() {}

GameObject * ObjectManager::AddGameObject(GameObject * obj, const char * name)
{
	if (obj != NULL)
	{
		for (std::list<GameObject*>::iterator it = aliveObjects.begin(); it != aliveObjects.end(); it++)
		{
			if ((*it) == obj)
				return NULL;
		}
		obj->alive = obj->active = true;
		if (name)
			obj->SetName(name);

		aliveObjects.push_back(obj);
		return obj;
	}
	else
		return NULL;
}

void ObjectManager::RemoveGameObject(GameObject * obj)
{
	if (obj != NULL)
	{
		obj->local_handler.handle(DeletedObjectEvent());

		obj->alive = obj->active = false;

		deadObjects.push_back(obj);

#if 0
		if (obj == imGuiEditor->GetSelected())
		{
			
			//auto find = std::find_if(imGuiEditor->selectedObjs.begin(), imGuiEditor->selectedObjs.end(), [&](GameObject * g) {return g == obj; });
			//if (find != imGuiEditor->selectedObjs.end()) imGuiEditor->selectedObjs.erase(find);

			imGuiEditor->selectedObjs.remove(obj);

			/*for (auto it : imGuiEditor->selectedObjs)
				if (it == obj)
					it = NULL;*/

			imGuiEditor->SetSelected(NULL);
		}
#endif	
	}
}

void ObjectManager::FreeDeadObjects()
{
	while (!deadObjects.empty())
	{
		deadObjects.sort();
		deadObjects.unique();

		GameObject * del_object = deadObjects.back();

		del_object->local_handler.handle(DeletedObjectEvent());

		aliveObjects.remove(del_object);
		
		del_object->Terminate();

		delete del_object;

		deadObjects.pop_back();
	}
}

void ObjectManager::LoadObjectsFromFile(std::string filePath)
{
	std::ifstream jsonFile(filePath);
	Json::Reader reader;

	Json::Value objVal;

	bool ok = reader.parse(jsonFile, objVal);

	//if (!ok)
	//	std::cout << "Error reading : " << filePath  << std::endl;

	std::vector<std::string> parentMembers = objVal.getMemberNames(); //parent members are "0", "1"...

	for (std::vector<std::string>::iterator parent = parentMembers.begin(); parent != parentMembers.end(); parent++)
	{
		GameObject * newEditorObj = mFactory->Create<GameObject>();
		objectManager->AddGameObject(newEditorObj);
	
		newEditorObj->SetName(objVal[*parent].get("name", "unnamed").asString());
		newEditorObj->AttachComponents(objVal[*parent]);
		newEditorObj->LoadComponents(objVal[*parent]);
	}

	jsonFile.close();
}

void ObjectManager::SaveObjectsToFile(std::string filePath)
{
	unsigned counter = 0;

	Json::Value objVal;

	for (std::list<GameObject*>::iterator obj = aliveObjects.begin(); obj != aliveObjects.end(); obj++)
	{
		if ((*obj)->save)
		{
			objVal[std::to_string(counter)]["name"] = (*obj)->GetName();
			//for some reason, I can't read unnamed objects. Instead, asign names like "0", "1"... to the objects that should be unnamed
			(*obj)->SaveComponents(objVal[std::to_string(counter)]);
			counter++;
		}
	}
		
	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();
	std::ofstream jsonFileWrite(filePath);

	writer->write(objVal, &jsonFileWrite);

	jsonFileWrite.close();
}

void ObjectManager::UnloadObjects()
{
	FreeDeadObjects();

	for (std::list<GameObject*>::iterator it = aliveObjects.begin(); it != aliveObjects.end(); it++)
	{
		(*it)->Terminate();
		delete (*it);
	}

	aliveObjects.clear();
}

GameObject * ObjectManager::FindObjectByName(std::string name)
{
	std::list<GameObject*>::iterator it = std::find_if(aliveObjects.begin(), aliveObjects.end(), [&](GameObject * obj) {return obj->GetName() == name; }); //gotta go fast
	return (it != aliveObjects.end()) ? reinterpret_cast<GameObject*>(*it) : NULL;
}

GameObject * ObjectManager::FindObjectById(unsigned id)
{
	std::list<GameObject*>::iterator it = std::find_if(aliveObjects.begin(), aliveObjects.end(), [&](GameObject * obj) {return obj->GetId() == id; });
	return (it != aliveObjects.end()) ? reinterpret_cast<GameObject*>(*it) : NULL;
}

void ObjectManager::FindObjectByName(std::string name, std::list<GameObject*> & obj)
{
	for (std::list<GameObject*>::iterator it = aliveObjects.begin(); it != aliveObjects.end(); it++)
		if ((*it)->GetName() == name)
			obj.push_back((*it));
}

void ObjectManager::Initialize()
{
	Load();

	for (std::list<GameObject*>::iterator it = aliveObjects.begin(); it != aliveObjects.end(); it++)
		(*it)->Initialize();	
}

void ObjectManager::Update()
{
	FreeDeadObjects();

	for (std::list<GameObject*>::iterator it = aliveObjects.begin(); it != aliveObjects.end(); it++)
	{
		if ((*it)->alive && (*it)->active)
			(*it)->Update();

		if (!(*it)->alive)
		{
			//aliveObjects.erase(it);
			//RemoveGameObject((*it));

			deadObjects.push_back((*it));
		}
	}
}

void ObjectManager::Terminate()
{
	// save the level when we exit it
	if (autoSave)
		if (scene->isEditing)
			SaveObjectsToFile("./Data/Levels/" + scene->currLevel);

	Save();

	FreeDeadObjects();

	for (std::list<GameObject*>::iterator it = aliveObjects.begin(); it != aliveObjects.end(); it++)
	{
		(*it)->Terminate();
		delete (*it);
	}

	aliveObjects.clear();

	delete instance;
}

void ObjectManager::Load()
{
	std::ifstream jsonFile(editor_loader);
	Json::Reader reader;
	bool ok = reader.parse(jsonFile, commonValue);
	//if (!ok)
	//	std::cout << "ERROR READING JSON" << std::endl;

	autoSave = commonValue.get("auto_save", true).asBool();

	jsonFile.close();
}

void ObjectManager::Save()
{
#ifdef EDITOR
	commonValue["auto_save"] = autoSave;

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();
	std::ofstream jsonFileWrite(editor_loader);
	writer->write(commonValue, &jsonFileWrite);
	jsonFileWrite.close();
#endif
}

bool ObjectManager::AutosaveEnabled()
{
	return autoSave;
}

void ObjectManager::SetAutoSave(bool enable)
{
	autoSave = enable;
}

void ObjectManager::AutoSave(float frequency)
{
	static float timer = 0.0f;

	timer += ImGui::GetIO().DeltaTime;

	if (autoSave && timer > frequency)
	{
		timer = 0.0f;
		SaveObjectsToFile("./Data/Levels/" + scene->currLevel);
	}
}

void ObjectManager::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Object Manager"))
	{
		static int selected_component = -1;

		ImGui::Text("Game Objects in the scene : %d", aliveObjects.size()); ImGui::NewLine();

		static int currentObj = 0;

		std::vector<std::string> objstr;
		std::vector<unsigned> objId;

		for (auto it = aliveObjects.begin(); it != aliveObjects.end(); it++)
		{
			objstr.push_back((*it)->GetName());
			objId.push_back((*it)->GetId());
		}
		
		static ImGuiTextFilter filter;
		filter.Draw("search##Object");
		for (unsigned i = 0; i < objstr.size(); i++) {
			if (filter.PassFilter(objstr[i].c_str())) {
				if (ImGui::Selectable(objstr[i].c_str())) {
					imGuiEditor->DumpSelected();
					imGuiEditor->SetSelected(FindObjectById(objId[i]));
					imGuiEditor->editState = ImGuiEditor::EditState::TransformPlease;
					graphics->GetMainCamera()->position.x = FindObjectById(objId[i])->mTransform->position.x;
					graphics->GetMainCamera()->position.y = FindObjectById(objId[i])->mTransform->position.y;
				}
			}
		}

		/*if (ImGuiEditor::ListBoxHeader("object list", &currentObj, objstr, "search##ObjManager"))
		{
			imGuiEditor->DumpSelected();
			imGuiEditor->SetSelected(FindObjectById(objId[currentObj]));
			imGuiEditor->editState = ImGuiEditor::EditState::TransformPlease;
		}
		if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
		{
			graphics->GetMainCamera()->position.x = FindObjectById(objId[currentObj])->mTransform->position.x;
			graphics->GetMainCamera()->position.y = FindObjectById(objId[currentObj])->mTransform->position.y;
		}*/
		objstr.clear();
		objId.clear();
	}
}

ObjectManager * ObjectManager::instance = 0;