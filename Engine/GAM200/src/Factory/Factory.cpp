#include "Factory.h"

Factory::Factory() {}

Factory::~Factory()
{
	creators.clear();
}

Factory * Factory::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW Factory();

	return instance;
}

void Factory::AddCreator(const char * type, ICreator * creator)
{
	creators[type] = creator;
}

void Factory::UnregisterCreators()
{
	for (std::map<std::string, ICreator*>::iterator it = creators.begin(); it != creators.end(); it++)
	{
		delete it->second;
	}
}


Entity * Factory::Create(const char * type)
{
	static unsigned id = 0;
	ICreator * creator = GetCreator(type);
	Entity * comp = creator->Create();
	comp->uniqueId = id; id++;
	return comp;
}


ICreator * Factory::GetCreator(const char * type)
{
	std::map<std::string, ICreator*>::iterator it;
	it = creators.find(type);
	if (it != creators.end())
		return it->second;
	else
	{
		//std::cout << "CREATOR NOT FOUND" << std::endl;
		return NULL;
	}
}

void Factory::Terminate()
{
	UnregisterCreators();
	delete instance;
}

Factory * Factory::instance = 0;


