#ifndef FACTORY_H
#define FACTORY_H

#include "../Component/Entity.h"
#include "../Editor/ImGuiEditor.h"

#include "../Gfx/Comp/Transform.h"
#include "../ObjectManager/GameObject.h"

class ICreator : public Entity
{
public:
	ICreator(){}
	virtual Entity * Create() = 0;
};

template<typename T>
class TCreator : public ICreator
{
public:
	TCreator() : ICreator() {}

	virtual Entity * Create()
	{
		Entity * comp = dynamic_cast<Entity*>(DBG_NEW T);

		if(comp != NULL)
			return comp;
		else
		{
			//std::cout << "FACTORY ERROR IN CREATE" << std::endl;
			return NULL;
		}
	}
};

class Factory
{
private:

	static Factory * instance;
	Factory();

public:

	virtual ~Factory();

	std::map<std::string, ICreator*> creators;

	static Factory * getInstance();

	Entity * Create(const char * type);

	template<typename T>
	T * Create()
	{
		Entity * newEntity = Create((Entity::GetType<T>()).c_str());

		return reinterpret_cast<T*>(newEntity);
	}

	void AddCreator(const char * type, ICreator * creator);

	ICreator * GetCreator(const char * type);  

	template<typename T>
	void Register()
	{
		std::string className = Entity::GetType<T>();

		if(className != "GameObject")
			ImGuiEditor::getInstance()->componentNames.push_back(className);

		AddCreator(className.c_str(), DBG_NEW TCreator<T>());
	}

	void UnregisterCreators();

	void Terminate();
};

#define mFactory Factory::getInstance()

#endif