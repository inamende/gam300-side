#include "Input.h"

#include "../Gfx/Window.h"
#include "../Editor/ImGuiEditor.h"

Input * Input::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW Input();

	return instance;
}

Input::Input() : mousePos(0.0f, 0.0f), mouseRDown(false), mouseLDown(false), prevMouseRDown(false), prevMouseLDown(false), mouseLUp(false), mouseRUp(false),
mouseMiddleDown(false), mouseMiddleUp(false), prevMouseMiddleDown(false), editorIsActive(false), mouseWheelDirection(0), controller(nullptr), which(-1), using_controller(false) {
	memset(axis_states, 0, sizeof(float)*SDL_CONTROLLER_AXIS_MAX);
}

Input::~Input() {}


void Input::Initialize()
{
	for (unsigned i = 0; i < SDL_NUM_SCANCODES; i++)
	{
		keysDown[i] = false;
		prevKeyDown[i] = false;
		keysUp[i] = false;
	}
	mousePrevPos = mousePos;

	for (unsigned i = 0; i < SDL_CONTROLLER_BUTTON_MAX; i++) {
		buttonsDown[i] = false;
		prevButtonDown[i] = false;
		buttonsUp[i] = false;
	}

	SDL_Init(SDL_INIT_GAMECONTROLLER);
	SDL_GameControllerAddMappingsFromFile("./Data/Load/game_controller_db.txt");
}

void Input::Update()
{
	for (unsigned i = 0; i < SDL_NUM_SCANCODES; i++)
	{
		prevKeyDown[i] = keysDown[i];
		keysUp[i] = false;

		prevMouseRDown = mouseRDown;
		prevMouseLDown = mouseLDown;
		prevMouseMiddleDown = mouseMiddleDown;

		mouseRUp = false;
		mouseLUp = false;
		mouseMiddleUp = false;

		mouseWheelDirection = 0;

		mousePrevPos = mousePos;
	}
	for (unsigned i = 0; i < SDL_CONTROLLER_BUTTON_MAX; i++)
	{
		prevButtonDown[i] = buttonsDown[i];
		buttonsUp[i] = false;
	}
}

void Input::controller_update() {
	SDL_GameControllerUpdate();
	for (int i = 0; i < SDL_CONTROLLER_AXIS_MAX; i++) {
		axis_states[i] = SDL_GameControllerGetAxis(controller, (SDL_GameControllerAxis)i);
	}
}

void Input::PollInputEvents()
{
	while (SDL_PollEvent(&event)) //if (SDL_PollEvent(&event)) whil make the events slower
	{
		ImGui_ImplSdlGL3_ProcessEvent(&event); //allow imgui to handdle input too
		
		switch (event.type)
		{
		case SDL_KEYDOWN:
			keysDown[event.key.keysym.scancode] = true;
			break;
		case SDL_KEYUP:
			keysDown[event.key.keysym.scancode] = false;
			keysUp[event.key.keysym.scancode] = true;
			break;
		case SDL_CONTROLLERBUTTONDOWN:
			buttonsDown[event.cbutton.button] = true;
			break;
		case SDL_CONTROLLERBUTTONUP:
			buttonsDown[event.cbutton.button] = false;
			buttonsUp[event.cbutton.button] = true;
			break;
		case SDL_MOUSEMOTION:
			SDL_GetMouseState(&mousePos.x, &mousePos.y);
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (event.button.button == SDL_BUTTON_LEFT)
				mouseLDown = true;
			else if (event.button.button == SDL_BUTTON_MIDDLE)
				mouseMiddleDown = true;
			else
				mouseRDown = true;
			break;
		case SDL_MOUSEWHEEL:
			mouseWheelDirection = event.wheel.y;
			break;
		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				mouseLUp = true;
				mouseLDown = false;
			}
			else if (event.button.button == SDL_BUTTON_MIDDLE)
			{
				mouseMiddleUp = true;
				mouseMiddleDown = false;
			}
			else //i hate sdl2
			{
				mouseRUp = true;
				mouseRDown = false;
			}
			break;
		case SDL_QUIT:
			mWindow->quit = true;
			break;
		case SDL_CONTROLLERDEVICEADDED:
			if (!controller) {
				using_controller = true;
				which = event.cdevice.which;
				controller = SDL_GameControllerOpen(which);
				memset(axis_states, 0, sizeof(float)*SDL_CONTROLLER_AXIS_MAX);
			}
			if (!controller) {
				using_controller = false;
			}
			break;
		case SDL_CONTROLLERDEVICEREMOVED:
			if (which == event.cdevice.which) {
				which = -1;
				controller = nullptr;
				using_controller = false;
			}
			break;
		}
	}
}

bool Input::KeyIsDown(SDL_Scancode pressedKey, bool editor)
{
	if (using_controller) return false;
	return editor ? keysDown[pressedKey] && prevKeyDown[pressedKey] == false && !editorIsActive :
		keysDown[pressedKey] && prevKeyDown[pressedKey] == false;
}

bool Input::KeyIsDown(Keyboard pressedKey, bool editor)
{
	if (using_controller) return false;
	return editor ? keysDown[(SDL_Scancode)pressedKey] && prevKeyDown[(SDL_Scancode)pressedKey] == false && !editorIsActive :
		keysDown[(SDL_Scancode)pressedKey] && prevKeyDown[(SDL_Scancode)pressedKey] == false;
}

bool Input::KeyIsUp(SDL_Scancode pressedKey, bool editor)
{
	if (using_controller) return false;
	return editor ? keysUp[pressedKey] && !editorIsActive :
		keysUp[pressedKey];
}

bool Input::KeyIsUp(Keyboard pressedKey, bool editor)
{
	if (using_controller) return false;
	return editor ? keysUp[(SDL_Scancode)pressedKey] && !editorIsActive :
		keysUp[(SDL_Scancode)pressedKey];
}

bool Input::KeyIsPressed(SDL_Scancode pressedKey, bool editor)
{
	if (using_controller) return false;
	return editor ? keysDown[pressedKey] && !editorIsActive :
		keysDown[pressedKey];
}

bool Input::KeyIsPressed(Keyboard pressedKey, bool editor)
{
	if (using_controller) return false;
	return editor ? keysDown[(SDL_Scancode)pressedKey] && !editorIsActive :
		keysDown[(SDL_Scancode)pressedKey];
}

bool Input::MouseIsDown(Uint8 pressedButton)
{
	if (using_controller) return false;
	if(pressedButton == SDL_BUTTON_LEFT)
		return mouseLDown && !prevMouseLDown;
	else if (pressedButton == SDL_BUTTON_MIDDLE)
		return mouseMiddleDown && !prevMouseMiddleDown;
	else
		return mouseRDown && !prevMouseRDown;
}

bool Input::MouseIsUp(Uint8 pressedButton)
{
	if (using_controller) return false;
	if (pressedButton == SDL_BUTTON_LEFT)
		return mouseLUp;
	else if (pressedButton == SDL_BUTTON_MIDDLE)
		return mouseMiddleUp;
	else
		return mouseRUp;
}

bool Input::MouseIsPressed(Uint8 pressedButton)
{
	if (using_controller) return false;
	if (pressedButton == SDL_BUTTON_LEFT)
		return mouseLDown;
	else if (pressedButton == SDL_BUTTON_MIDDLE)
		return mouseMiddleDown;
	else
		return mouseRDown;
}

bool Input::MouseWheelIsActive(Sint32 & intensity)
{
	if (using_controller) return false;
	intensity = mouseWheelDirection;
	return mouseWheelDirection > 0 || mouseWheelDirection < 0;
}

bool Input::ButtonIsDown(SDL_GameControllerButton button) {
	return buttonsDown[button] && !prevButtonDown[button];
}

bool Input::ButtonIsUp(SDL_GameControllerButton button) {
	return buttonsUp[(SDL_GameControllerButton)button];
}

bool Input::ButtonIsPressed(SDL_GameControllerButton button) {
	return buttonsDown[(SDL_Scancode)button];
}

float Input::GetJoystickDirection(SDL_GameControllerAxis axis) {
	return fabs(axis_states[axis]) < JOYSTICK_DEADZONE ? 0.f : axis_states[axis];
}

glm::vec2 Input::GetMousePosition()
{
	return mousePos;
}

glm::vec2 Input::GetMousePrevPosition()
{
	return mousePrevPos;
}

void Input::SetMousePosition(glm::vec2 pos)
{
	mousePos = pos;
}

void Input::Terminate()
{
	delete instance;
}

Input * Input::instance = 0;