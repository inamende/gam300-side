#ifndef INPUT_H
#define INPUT_H

#include "../Component/Entity.h"
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

enum class Keyboard;

class Input : public Entity
{
private:
	static Input * instance;
	Input();
	
public:
	bool mouseRUp;
	bool mouseLUp;
	bool mouseRDown;
	bool mouseLDown;
	bool prevMouseRDown;
	bool prevMouseLDown;

	bool mouseMiddleUp;
	bool mouseMiddleDown;
	bool prevMouseMiddleDown;

	int mouseWheelDirection;

	bool editorIsActive;

	SDL_Event event;

	~Input();

	static Input * getInstance();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	void PollInputEvents();
	void controller_update();

	bool KeyIsDown(SDL_Scancode pressedKey, bool editor = true);
	bool KeyIsDown(Keyboard pressedKey, bool editor = true);

	bool KeyIsUp(SDL_Scancode pressedKey, bool editor = true);
	bool KeyIsUp(Keyboard pressedKey, bool editor = true);

	bool KeyIsPressed(SDL_Scancode pressedKey, bool editor = true);
	bool KeyIsPressed(Keyboard pressedKey, bool editor = true);

	bool MouseIsDown(Uint8 pressedButton);
	bool MouseIsUp(Uint8 pressedButton);
	bool MouseIsPressed(Uint8 pressedButton);

	bool MouseWheelIsActive(Sint32 & intensity);

	bool ButtonIsDown(SDL_GameControllerButton button);
	bool ButtonIsUp(SDL_GameControllerButton button);
	bool ButtonIsPressed(SDL_GameControllerButton button);

	float GetJoystickDirection(SDL_GameControllerAxis axis);

	glm::vec2 GetMousePosition();
	glm::vec2 GetMousePrevPosition();
	void SetMousePosition(glm::vec2 pos);

	bool using_controller;
private:
	SDL_GameController *controller;
	glm::ivec2 mousePos;
	glm::ivec2 mousePrevPos;

	bool keysUp[SDL_NUM_SCANCODES];
	bool keysDown[SDL_NUM_SCANCODES];
	bool prevKeyDown[SDL_NUM_SCANCODES];

	bool buttonsUp[SDL_CONTROLLER_BUTTON_MAX];
	bool buttonsDown[SDL_CONTROLLER_BUTTON_MAX];
	bool prevButtonDown[SDL_CONTROLLER_BUTTON_MAX];

	float axis_states[SDL_CONTROLLER_BUTTON_MAX];

	int which;
};

/*	IMPORTANT: Ask Ignacio before changing this, or you may break the game.

NOTE: if for some reason, you don't find the key you need in here, notify Ignacio
and by now just use it's SDL code.
*/
enum class Keyboard
{
	A = 4, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
	ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO,
	RETURN, ESCAPE, BACKSPACE, TAB, SPACE, MINUS, EQUALS, LEFTBACKET, RIGHTBRACKET, BACKSLASH, NONUSHASH,
	SEMICOLON, APOSTROPHE, GRAVE, COMMA, PERIOD, SLASH, CAPSLOCK,
	F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
	DEL = 76, RIGHT = 79, LEFT, DOWN, UP,
	REDO = 121, UNDO, CUT, COPY, PASTE,
	LEFFT_CTRL = 224, LEFT_SHIFT, LEFT_ALT, LEFT_WINDOWS,
	RIGHT_CTRL, RIGHT_SHIFT, RIGHT_ALT, RIGHT_WINDOWS
};

#define KeyPressed(X) Input::getInstance()->KeyIsPressed(X) 
#define KeyDown(X) Input::getInstance()->KeyIsDown(X) 
#define KeyUp(X) Input::getInstance()->KeyIsUp(X) 

#define MousePressed(X) Input::getInstance()->MouseIsPressed(X) 
#define MouseDown(X) Input::getInstance()->MouseIsDown(X) 
#define MouseUp(X) Input::getInstance()->MouseIsUp(X) 

#define MouseWheel(X) Input::getInstance()->MouseWheelIsActive(X)

#define ButtonPressed(X) Input::getInstance()->ButtonIsPressed(X) 
#define ButtonDown(X) Input::getInstance()->ButtonIsDown(X) 
#define ButtonUp(X) Input::getInstance()->ButtonIsUp(X)
#define JoyStickDirection(X) Input::getInstance()-> GetJoystickDirection(X)

#define mousePosition Input::getInstance()->GetMousePosition() 
#define mousePrevPosition Input::getInstance()->GetMousePrevPosition() 

#define mInput Input::getInstance()


#endif