#include "ContactCollisions.h"

#include "Collisions.h"
#include "../Gfx/Comp/Transform.h"

bool StaticCircleToStaticCircleEx(glm::vec2 & center0, float rad0, glm::vec2 & center1, float rad1, Contact * contact)
{
	if (contact != NULL)
	{
		contact->normal = (center1 - center0) / glm::length(center1 - center0);
		contact->penetration = (rad0 + rad1) - glm::length((center1 - center0));
		contact->intersection = center0 + (contact->normal * rad0);
	}
	return StaticCircleToStaticCircle(center0, rad0, center1, rad1);
}

bool StaticRectToStaticCircleEx(glm::vec2 & rectPos, float rectWidth, float rectHeight, glm::vec2 & circlePos, float rad, Contact * contact)
{
	if (contact != NULL)
	{
		float L = rectPos.x - rectWidth * 0.5f;
		float R = rectPos.x + rectWidth * 0.5f;
		float B = rectPos.y - rectHeight * 0.5f;
		float T = rectPos.y + rectHeight * 0.5f;

		contact->intersection = { glm::clamp(circlePos.x, L, R), glm::clamp(circlePos.y, B, T) };

		if (circlePos.x != contact->intersection.x || circlePos.y != contact->intersection.y)
		{
			contact->normal = (circlePos - contact->intersection) / glm::length(circlePos - contact->intersection);
			contact->penetration = rad - glm::length(circlePos - contact->intersection);
		}
		else
		{
			glm::vec2 D(circlePos - rectPos);
			float penx = rectWidth * 0.5f - abs(D.x) + rad;
			float peny = rectHeight * 0.5f - abs(D.y) + rad;
			contact->penetration = std::min(penx, peny);

			if (penx < peny)
				contact->normal = { 1.0f, 0.0f };
			else
				contact->normal = { 0.0f, 1.0f };
			if (glm::dot(contact->normal, D) < 0.0f)
				contact->normal = -contact->normal;
		}
	}

	return StaticRectToStaticCirlce(rectPos, rectWidth, rectHeight, circlePos, rad);
}

bool StaticOBBToStaticCircleEx(Transform * OBB, glm::vec2 & center, float rad, Contact * contact)
{
	if (contact != NULL)
	{
		Contact mycontact;

		Transform trans(glm::vec3(OBB->position.x, OBB->position.y, 1.f), OBB->rotation, glm::vec3(1.f, 1.f, 1.f));
		glm::vec4 newCenter = glm::inverse(trans.GetTransform()) * glm::vec4(center, 0.f, 1.f); // (Rinv * Tinv) * Pos 	
		StaticRectToStaticCircleEx(glm::vec2(), OBB->scale.x, OBB->scale.y, glm::vec2(newCenter), rad, &mycontact);

		contact->normal = glm::mat3(trans.GetTransform()) * glm::vec3(mycontact.normal, 0.f);
		contact->penetration = mycontact.penetration;
		contact->intersection = glm::mat3(trans.GetTransform()) * glm::vec3(mycontact.intersection, 1.f);
	}

	return OrientedRectToStaticCirlce(glm::vec2(OBB->position.x, OBB->position.y), OBB->scale.x, OBB->scale.y, OBB->rotation, center, rad);
}

bool StaticRectToStaticRectEx(glm::vec2 & pos0, glm::vec2 & size0, glm::vec2 & pos1, glm::vec2 & size1, Contact * contact)
{
	if (contact != NULL) 
	{
		float L1 = pos0.x - size0.x * 0.5f; float L2 = pos1.x - size1.x * 0.5f;
		float R1 = pos0.x + size0.x * 0.5f; float R2 = pos1.x + size1.x * 0.5f;
		float B1 = pos0.y - size0.y * 0.5f; float B2 = pos1.y - size1.y * 0.5f;
		float T1 = pos0.y + size0.y * 0.5f; float T2 = pos1.y + size1.y * 0.5f;

		glm::vec2 D(pos1 - pos0);
		float penx = (float)(((size0.x + size1.x) * 0.5) - abs(D.x));
		float peny = (float)(((size0.y + size1.y) * 0.5) - abs(D.y));
		contact->penetration = std::min(penx, peny);
		contact->normal = (penx < peny) ? glm::vec2(1.f, 0.f) : glm::vec2(0.f, 1.f);
		contact->normal = (glm::dot(D, contact->normal) < 0) ? -contact->normal : contact->normal;
																						  
		if (StaticPointToStaticRect(glm::vec2(L2, T2), pos0, size0.x, size0.y))
			contact->intersection = { L2, T2 };
		else if (StaticPointToStaticRect(glm::vec2(R2, B2), pos0, size0.x, size0.y))
			contact->intersection = { R2, B2 };
		else if (StaticPointToStaticRect(glm::vec2(R2, T2), pos0, size0.x, size0.y))
			contact->intersection = { R2, T2 };
		else if (StaticPointToStaticRect(glm::vec2(L2, B2), pos0, size0.x, size0.y))
			contact->intersection = { L2, B2 };
		else if (StaticPointToStaticRect(glm::vec2(L1, T1), pos1, size1.x, size1.y))
			contact->intersection = { L1, T1 };
		else if (StaticPointToStaticRect(glm::vec2(R1, B1), pos1, size1.x, size1.y))
			contact->intersection = { R1, B1 };
		else if (StaticPointToStaticRect(glm::vec2(R1, T1), pos1, size1.x, size1.y))
			contact->intersection = { R1, T1 };
		else
			contact->intersection = { L1, B1 };
	}

	return StaticRectToStaticRect(pos0, size0.x, size0.y, pos1, size1.x, size1.y);
}

bool OrientedRectToOrientedRectEx(Transform * OBB0, Transform * OBB1, Contact * contact)
{
	if (contact != NULL)
	{
		glm::vec2 axis1 = { cos(OBB0->rotation), sin(OBB0->rotation) };
		glm::vec2 axis2 = { cos(OBB1->rotation), sin(OBB1->rotation) };

		glm::vec2 X1 = { (OBB0->scale.x * 0.5f) * cos(OBB0->rotation), (OBB0->scale.x * 0.5f) * sin(OBB0->rotation) };
		glm::vec2 X2 = { (OBB1->scale.x * 0.5f) * cos(OBB1->rotation), (OBB1->scale.x * 0.5f) * sin(OBB1->rotation) };
		glm::vec2 Y1 = { (OBB0->scale.y * 0.5f) * cos(OBB0->rotation), (OBB0->scale.y * 0.5f) * sin(OBB0->rotation) };
		Y1 = glm::vec2(-Y1.y, Y1.x);
		glm::vec2 Y2 = { (OBB1->scale.y * 0.5f) * cos(OBB1->rotation), (OBB1->scale.y * 0.5f) * sin(OBB1->rotation) };
		Y2 = glm::vec2(-Y2.y, Y2.x);

		//compute all the corners
		glm::vec2 top_left1 = { OBB0->position.x + (-OBB0->scale.x * 0.5f  * cos(OBB0->rotation)) - (OBB0->scale.y * 0.5f * sin(OBB0->rotation)) ,
			OBB0->position.y + (-OBB0->scale.x * 0.5f  * sin(OBB0->rotation)) + (OBB0->scale.y * 0.5f * cos(OBB0->rotation)) };
		glm::vec2 top_right1 = { OBB0->position.x + (OBB0->scale.x * 0.5f  * cos(OBB0->rotation)) - (OBB0->scale.y * 0.5f * sin(OBB0->rotation)) ,
			OBB0->position.y + (OBB0->scale.x * 0.5f  * sin(OBB0->rotation)) + (OBB0->scale.y * 0.5f * cos(OBB0->rotation)) };
		glm::vec2 bot_left1 = { OBB0->position.x + (-OBB0->scale.x * 0.5f  * cos(OBB0->rotation)) - (-OBB0->scale.y * 0.5f * sin(OBB0->rotation)) ,
			OBB0->position.y + (-OBB0->scale.x * 0.5f  * sin(OBB0->rotation)) + (-OBB0->scale.y * 0.5f * cos(OBB0->rotation)) };
		glm::vec2 bot_right1 = { OBB0->position.x + (OBB0->scale.x * 0.5f  * cos(OBB0->rotation)) - (-OBB0->scale.y * 0.5f * sin(OBB0->rotation)) ,
			OBB0->position.y + (OBB0->scale.x * 0.5f  * sin(OBB0->rotation)) + (-OBB0->scale.y * 0.5f * cos(OBB0->rotation)) };

		glm::vec2 top_left2 = { OBB1->position.x + (-OBB1->scale.x * 0.5f  * cos(OBB1->rotation)) - (OBB1->scale.y * 0.5f * sin(OBB1->rotation)) ,
			OBB1->position.y + (-OBB1->scale.x * 0.5f  * sin(OBB1->rotation)) + (OBB1->scale.y * 0.5f * cos(OBB1->rotation)) };
		glm::vec2 top_right2 = { OBB1->position.x + (OBB1->scale.x * 0.5f  * cos(OBB1->rotation)) - (OBB1->scale.y * 0.5f * sin(OBB1->rotation)) ,
			OBB1->position.y + (OBB1->scale.x * 0.5f  * sin(OBB1->rotation)) + (OBB1->scale.y * 0.5f * cos(OBB1->rotation)) };
		glm::vec2 bot_left2 = { OBB1->position.x + (-OBB1->scale.x * 0.5f  * cos(OBB1->rotation)) - (-OBB1->scale.y * 0.5f * sin(OBB1->rotation)) ,
			OBB1->position.y + (-OBB1->scale.x * 0.5f  * sin(OBB1->rotation)) + (-OBB1->scale.y * 0.5f * cos(OBB1->rotation)) };
		glm::vec2 bot_right2 = { OBB1->position.x + (OBB1->scale.x * 0.5f  * cos(OBB1->rotation)) - (-OBB1->scale.y * 0.5f * sin(OBB1->rotation)) ,
			OBB1->position.y + (OBB1->scale.x * 0.5f  * sin(OBB1->rotation)) + (-OBB1->scale.y * 0.5f * cos(OBB1->rotation)) };

		glm::vec2 D(OBB1->position - OBB0->position);

		float pen1 = abs(glm::dot(D, axis1)) - (abs(glm::dot(X1, axis1)) + abs(glm::dot(X2, axis1)) + abs(glm::dot(Y1, axis1)) + abs(glm::dot(Y2, axis1)));
		float pen2 = abs(glm::dot(D, axis2)) - (abs(glm::dot(X1, axis2)) + abs(glm::dot(X2, axis2)) + abs(glm::dot(Y1, axis2)) + abs(glm::dot(Y2, axis2)));
		float pen1_p = abs(glm::dot(D, glm::vec2(-axis1.y, axis1.x))) - (abs(glm::dot(X1, glm::vec2(-axis1.y, axis1.x))) + abs(glm::dot(X2, glm::vec2(-axis1.y, axis1.x))) + abs(glm::dot(Y1, glm::vec2(-axis1.y, axis1.x))) + abs(glm::dot(Y2, glm::vec2(-axis1.y, axis1.x))));
		float pen2_p = abs(glm::dot(D, glm::vec2(-axis2.y, axis2.x))) - (abs(glm::dot(X1, glm::vec2(-axis2.y, axis2.x))) + abs(glm::dot(X2, glm::vec2(-axis2.y, axis2.x))) + abs(glm::dot(Y1, glm::vec2(-axis2.y, axis2.x))) + abs(glm::dot(Y2, glm::vec2(-axis2.y, axis2.x))));

		contact->penetration = std::max(pen1, std::max(pen2, std::max(pen1_p, pen2_p)));

		if (contact->penetration == pen1)
			contact->normal = axis1;
		else if (contact->penetration == pen2)
			contact->normal = axis2;
		else if (contact->penetration == pen1_p)
			contact->normal = glm::vec2(-axis1.y, axis1.x);
		else
			contact->normal = glm::vec2(-axis2.y, axis2.x);

		contact->normal = (glm::dot(D, contact->normal) < 0) ? -contact->normal : contact->normal;
		contact->normal = -contact->normal;

		if (StaticPointToOrientedRect(top_right2, glm::vec2(OBB0->position), OBB0->scale.x, OBB0->scale.y, OBB0->rotation))
			contact->intersection = top_right2;
		else if (StaticPointToOrientedRect(bot_right2, glm::vec2(OBB0->position), OBB0->scale.x, OBB0->scale.y, OBB0->rotation))
			contact->intersection = bot_right2;
		else if (StaticPointToOrientedRect(top_left2, glm::vec2(OBB0->position), OBB0->scale.x, OBB0->scale.y, OBB0->rotation))
			contact->intersection = top_left2;
		else if (StaticPointToOrientedRect(bot_left2, glm::vec2(OBB0->position), OBB0->scale.x, OBB0->scale.y, OBB0->rotation))
			contact->intersection = bot_left2;
		else if (StaticPointToOrientedRect(top_left1, glm::vec2(OBB1->position), OBB1->scale.x, OBB1->scale.y, OBB1->rotation))
			contact->intersection = top_left1;
		else if (StaticPointToOrientedRect(bot_right1, glm::vec2(OBB1->position), OBB1->scale.x, OBB1->scale.y, OBB1->rotation))
			contact->intersection = bot_right1;
		else if (StaticPointToOrientedRect(top_right1, glm::vec2(OBB1->position), OBB1->scale.x, OBB1->scale.y, OBB1->rotation))
			contact->intersection = top_right1;
		else
			contact->intersection = bot_left1;

		return (contact->penetration < 0);

	}

	return StaticRectToStaticRect(glm::vec2(OBB0->position), OBB0->scale.x, OBB0->scale.y, glm::vec2(OBB1->position), OBB1->scale.x, OBB1->scale.y);
}

bool Contact::operator==(const Contact & other)
{
	return (*body0 == *other.body0 && *body1 == *other.body1) || (*body0 == *other.body1 && *body1 == *other.body0);;
}
