#include "Collisions.h"

#include "../Gfx/Comp/Transform.h"

bool StaticPointToStaticCircle(glm::vec2 & p, glm::vec2 & center, float rad)
{
	return glm::distance(p, center) <= rad;
}

bool StaticPointToStaticRect(glm::vec2 & p, glm::vec2 & rect, float width, float height)
{
	float L = rect.x - width * 0.5f;
	float R = rect.x + width * 0.5f;
	float B = rect.y - height * 0.5f;
	float T = rect.y + height * 0.5f;
	return ((L <= p.x) && (p.x <= R)) && ((B <= p.y) && (p.y <= T));
}

bool StaticPointToStaticRect(glm::vec3 & p, glm::vec3 & rect, float width, float height)
{
	return StaticPointToStaticRect(glm::vec2(p), glm::vec2(rect), width, height);
}

bool StaticPointToOrientedRect(glm::vec2 & p, glm::vec2 & rect, float width, float height, float angle)
{
	Transform trans(glm::vec3(rect.x, rect.y, 1.f), angle, glm::vec3(1.f, 1.f, 1.f));
	glm::vec3 newpoint = glm::mat3(glm::inverse(trans.GetTransform())) * glm::vec3(p, 1.f); // (Rinv * Tinv) * Pos 											 
	return StaticPointToStaticRect(glm::vec2(newpoint), glm::vec2(), width, height);
}

bool StaticCircleToStaticCircle(glm::vec2 & center0, float rad_0, glm::vec2 & center1, float rad_1)
{
	return StaticPointToStaticCircle(center0, center1, (rad_0 + rad_1));
}

bool StaticRectToStaticRect(glm::vec2 & rect0, float width0, float height0, glm::vec2 & rect1, float width1, float height1)
{
	glm::vec2 D(fabsf(rect1.x - rect0.x), fabsf(rect1.y - rect0.y));
	return ((D.x < ((width0 + width1) * 0.5f)) && (D.y < ((height0 + height1) * 0.5f)));
}

bool StaticRectToStaticCirlce(glm::vec2 & rect, float width, float height, glm::vec2 & center, float rad)
{
	float L = rect.x - width * 0.5f;
	float R = rect.x + width * 0.5f;
	float B = rect.y - height * 0.5f;
	float T = rect.y + height * 0.5f;

	return StaticPointToStaticCircle(glm::vec2(glm::clamp(center.x, L, R), glm::clamp(center.y, B, T)), center, rad);
}

bool OrientedRectToStaticCirlce(glm::vec2 & rect, float width, float height, float angle, glm::vec2 & center, float rad)
{
	Transform trans(glm::vec3(rect.x, rect.y, 1.f), angle, glm::vec3(1.f, 1.f, 1.f));
	glm::vec4 newpoint = glm::inverse(trans.GetTransform()) * glm::vec4(center.x, center.y, 0.f, 1.f); // (Rinv * Tinv) * Pos
	return StaticRectToStaticCirlce(glm::vec2(newpoint), width, height, glm::vec2(), rad);
}
