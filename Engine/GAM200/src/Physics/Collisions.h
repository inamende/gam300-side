#ifndef COLLISIONS_H
#define COLLISIONS_H

#include "../EngineCommon.h"
#include <glm/glm.hpp>

bool StaticPointToStaticCircle(glm::vec2 & p, glm::vec2 & center, float rad);

bool StaticPointToStaticRect(glm::vec2 & p, glm::vec2 & rect, float width, float height);
bool StaticPointToStaticRect(glm::vec3 & p, glm::vec3 & rect, float width, float height);

bool StaticPointToOrientedRect(glm::vec2 & p, glm::vec2 & rect, float width, float height, float angle);

bool StaticCircleToStaticCircle(glm::vec2 & center0, float rad_0, glm::vec2 & center1, float rad_1);

bool StaticRectToStaticRect(glm::vec2 & rect0, float width0, float height0, glm::vec2 & rect1, float width1, float height1);

bool StaticRectToStaticCirlce(glm::vec2 & rect, float width, float height, glm::vec2 & center, float rad);

bool OrientedRectToStaticCirlce(glm::vec2 & rect, float width, float height, float angle, glm::vec2 & center, float rad);


#endif
