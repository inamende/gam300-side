#ifndef CONTACT_COLLISIONS_H
#define CONTACT_COLLISIONS_H

#include "../EngineCommon.h"
#include <glm/glm.hpp>
#include "Comp\Collider.h"

struct Contact
{
	glm::vec2 intersection;
	glm::vec2 normal;
	float penetration;

	Collider * body0;
	Collider * body1;

	bool operator==(const Contact & other);

	bool has_sent_collisiosn_event = false;
};

class Transform;

bool StaticCircleToStaticCircleEx(glm::vec2 & center0, float rad_0, glm::vec2 & center1, float rad_1, Contact * contact);

bool StaticRectToStaticCircleEx(glm::vec2 & rectPos, float rectWidth, float rectHeight, glm::vec2 & circlePos, float rad, Contact * contact);

bool StaticOBBToStaticCircleEx(Transform * OBB, glm::vec2 & center, float rad, Contact * contact);

bool StaticRectToStaticRectEx(glm::vec2 & pos0, glm::vec2 & size0, glm::vec2 & pos1, glm::vec2 & size1, Contact * contact);

bool OrientedRectToOrientedRectEx(Transform * OBB0, Transform * OBB1, Contact * contact);

#endif
