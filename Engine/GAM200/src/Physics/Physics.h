#ifndef PHYSICS_H
#define PHYSICS_H

#include "../Component/Entity.h"
#include "../Events/event.h"
#include "../ObjectManager/GameObject.h"
#include "CollisionTable.h"
#include "Comp/Collider.h"

class RigidBody;
class Collider;
struct Contact;

#define DFLT_RESTITUTION 0.908f;

bool CollideCircles(Collider * b0, Collider * b1, Contact * c);
bool CollideAABBs(Collider* b0, Collider* b1, Contact * c);
bool CollideOBBs(Collider* b0, Collider* b1, Contact * c);
bool CollideAABBToCircle(Collider* b0, Collider* b1, Contact * c);
bool CollideOBBToCircle(Collider* b0, Collider* b1, Contact * c);

typedef bool(*CollisionFn)(Collider*, Collider*, Contact *);

typedef std::vector<Contact> CONTACT_CONTAINER;

enum ResolutionType { resolve, detect, none};

class Physics : Entity
{
private:

	static Physics * instance;
	Physics();

public:
	virtual ~Physics();

	static Physics * getInstance();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	void Render();
	void Load();
	void Save();

	void WriteToImguiWindow();

	void CollideAllBodies();
	void ClearBodies();

	void AddCollider(Collider * b, bool isStatic);
	void RemoveCollider(Collider * b);
	void SwapContainer(Collider * b, bool isStatic);

	CollisionFn GetCollisionFn(Collider * b0, Collider * b1);

	void ResolveContactPenetration(RigidBody * r0, RigidBody * r1, Contact * contact);
	void ResolveContactVelocity(RigidBody * r0, RigidBody * r1, Contact * contact);

	void EnablePhysics(bool enable);
	unsigned StaticObjectCount();
	unsigned DynamicObjectCount();

	// Collisiosn event handling
	void UpdateContactEvents();

	// Collision Group stuff
	ResolutionType check_if_they_should_collide(Collider* b0, Collider* b1, const TABLE_FORMAT & collision_table);

	CONTACT_CONTAINER prev_contacts;
	CONTACT_CONTAINER curr_contacts;

private:
	std::list<Collider*> staticBodies;
	std::list<Collider*> dynamicBodies;

	unsigned precission;
	bool active;

	CollisionFn collisionTests[(4 | 2) + 1];

};

#define physics Physics::getInstance()

class CollisionPersistedEvent : public Event
{
public:

	CollisionPersistedEvent(GameObject *  other) : other_object(other){}
	GameObject * other_object;
};

class CollisionStartedEvent : public Event
{
public:

	CollisionStartedEvent(GameObject *  other) : other_object(other) {}
	GameObject * other_object;
};

class CollisionEndedEvent : public Event
{
public:

	CollisionEndedEvent(GameObject *  other) : other_object(other) {}
	GameObject * other_object;
};

//#define mRigidBody GetOwner()->GetComponent<RigidBody>()

#endif