#include "CollisionTable.h"
#include "Comp\Collider.h"


TABLE_FORMAT CollissionTable::table = 0;// { 1 };

//TABLE_FORMAT CollissionTable::table;

// INSTRUCTIONS
// 1 means detect and resolve collision
// 2 means only detection, no resolution
// 3 means no detection

// Row/Column 0 = default
// Row/Column 1 = test

TABLE_FORMAT & CollissionTable::getInstance()
{
	if (table == 0) // lazy init
	{
		table = DBG_NEW int*[COLLISION_TABLE_SIZE];

		for (int i = 0; i < COLLISION_TABLE_SIZE; ++i)
			table[i] = DBG_NEW int[COLLISION_TABLE_SIZE];

		//Set the matrix values
		table[arrow][knightbox] = 2;
		table[knightbox][arrow] = 2;
		table[player][slept_body] = 2;
		table[slept_body][player] = 2;
		//walls and spectre
		table[player][SpctreWalls] = 1;
		table[SpctreWalls][player] = 1;
		table[Specter][JuiceBox] = 3;
		table[JuiceBox][Specter] = 3;
		table[Specter][slept_body] = 2;
		table[slept_body][Specter] = 2;
		//scene_event
		table[default][scene_event] = 3;
		table[scene_event][default] = 3;
		table[player][scene_event] = 2;
		table[scene_event][player] = 2;
		table[arrow][scene_event] = 3;
		table[scene_event][arrow] = 3;
		//enemy
		table[enemy][default] = 1;
		table[default][enemy] = 1;
		table[player][enemy] = 2;
		table[enemy][player] = 2;
		table[arrow][enemy] = 2;
		table[enemy][arrow] = 2;
		table[knightbox][enemy] = 3;
		table[enemy][knightbox] = 3;
		table[slept_body][enemy] = 3;
		table[enemy][slept_body] = 3;
		table[Specter][enemy] = 2;
		table[enemy][Specter] = 2;

	}
	return table;
}
