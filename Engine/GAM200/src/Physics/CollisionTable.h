#pragma once

#include "../EngineCommon.h"
#define COLLISION_TABLE_SIZE count
typedef int** TABLE_FORMAT;

// This file will only contain a Matrix that
// represents the collision table.

// INSTRUCTIONS
// 1 means detect and resolve collision
// 2 means only detection, no resolution
// 3 means no detection

class CollissionTable
{
public:

	static TABLE_FORMAT & getInstance();

private:
	static TABLE_FORMAT table;
	CollissionTable();
};