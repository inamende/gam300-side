#include "Collider.h"

#include "../../Gfx/Gfx.h"
#include "../../Gfx/Comp/Transform.h"

#include "../../ObjectManager/GameObject.h"

#include "../../Editor/ImGuiEditor.h"

Collider::Collider() : isTrigger(false), ownerTransform(NULL), transform(NULL), has_been_deleted(false), colliderId(-1) {}

Collider::~Collider() {}

void Collider::Initialize() {}

void Collider::Update() {} 

void Collider::Terminate() {}

void Collider::Render() {}

CollisionShape & Collider::GetShape() { return shape; }

Transform * Collider::GetTransform() { return transform; }

Transform * Collider::GetOwnerTransform() { return ownerTransform; }

void Collider::SetTransform(Transform * tr) { transform = tr; }

void Collider::SetOwnerTransform(Transform * tr) {ownerTransform = tr; }

bool Collider::IsTrigger() { return isTrigger; }

void Collider::ChangeTriggerState(bool s) { isTrigger = s; }

void Collider::SwitchTriggerState() { isTrigger != isTrigger; }

void Collider::OnDeletedObject(const DeletedObjectEvent &)
{
	has_been_deleted = true;
}

bool Collider::has_been_deleted_in_this_frame()
{
	return has_been_deleted;
}
