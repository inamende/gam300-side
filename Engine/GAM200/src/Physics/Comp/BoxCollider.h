#ifndef BOX_COLLIDER_H
#define BOX_COLLIDER_H

#include "Collider.h"

class Transform;

class BoxCollider : public Collider
{
public:
	BoxCollider();
	~BoxCollider();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	virtual void Initialize();
	virtual void Update();
	virtual void Render();
};


#endif