#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "../../Component/Component.h"
#include <glm/glm.hpp>

enum Integrator
{
	INTEGRATOR_EULER,
	INTEGRATOR_NEWTON,
	INTEGRATOR_VERLET
};

class Transform;

class RigidBody : public Component
{
	friend class Physics;
	friend class PlayerController;
	friend class Forms;

public:
	RigidBody();
	~RigidBody();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate() override;

	void Integrate(float dt);
	void IntegrateEuler(float dt);
	void IntegrateNewton(float dt);
	void IntegrateVerlet(float dt);

	void AddForce(glm::vec2 force);
	void AddForce(float fx, float fy);

	void ComputeAABB(glm::vec2 & pos, glm::vec2 & size);

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	void AutoComputeBodyInvMass();

	Transform * GetTransform();
	bool GetStaticness();
	void SetStaticness(bool change);

	glm::vec2 velocity;
	glm::vec2 gravity;

	Transform * transform;
	glm::vec3 prevPosition;
	glm::vec2 acceleration;
	float invMass;
	float drag;

	Integrator integrator;

	bool isStatic;
	bool autoComputeMass;
	//oscars additions
	//a function to set the velocity and acceleration of this Rigidbody to 0, used by the specter dash logic in PlayerController.cpp
	void ForceFullStop();
	//this will add a velocity to the rigidbody defined by temp
	void SetInstantaeneousVelocity(glm::vec2 temp);
};

#endif