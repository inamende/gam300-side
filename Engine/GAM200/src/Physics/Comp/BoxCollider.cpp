#include "BoxCollider.h"

#include "../../Gfx/Gfx.h"
#include "../../Gfx/Comp/Transform.h"

#include "../../ObjectManager/GameObject.h"

#include "../../Editor/ImGuiEditor.h"

BoxCollider::BoxCollider() : Collider::Collider()
{
	shape = CollisionShape::CSHAPE_AABB;
	transform = DBG_NEW Transform;
}

BoxCollider::~BoxCollider() 
{ 
	delete transform;
	physics->RemoveCollider(this);
}

void BoxCollider::Initialize()
{
	if (colliderId == -1)
		colliderId = GetOwner()->component_count<BoxCollider>() - 1;

	SetName("BoxCollider_" + std::to_string(colliderId));

	ownerTransform = GetOwner()->mTransform;

	RigidBody * rb = GetOwner()->GetComponent<RigidBody>();
	if(rb == NULL)
		physics->AddCollider(this, true);
	else
		physics->AddCollider(this, rb->GetStaticness());
}

void BoxCollider::Update() {} 
 
void BoxCollider::Render()
{
	int direction = (GetOwnerTransform()->scale.x > 0) ? 1 : -1;

	glm::vec3 pos = glm::vec3(GetOwnerTransform()->position.x + direction * transform->position.x, GetOwnerTransform()->position.y + transform->position.y, 99.f);
	DrawBox(pos, transform->scale.x * 0.5f, transform->scale.y * 0.5f);
}

void BoxCollider::load(Json::Value & val)
{
	active = val[GetName()].get("active", true).asBool();
	
	colliderId = val[GetName()].get("colliderId", -1).asInt();

	transform->position.x = val[GetName()]["position"].get("x", 0.0).asFloat();
	transform->position.y = val[GetName()]["position"].get("y", 0.0).asFloat();
	transform->position.z = val[GetName()]["position"].get("z", 0.0).asFloat();
	transform->rotation = val[GetName()].get("rotation", 0.0).asFloat();
	transform->scale.x = val[GetName()]["scale"].get("x", 0.0).asFloat();
	transform->scale.y = val[GetName()]["scale"].get("y", 0.0).asFloat();
	transform->scale.z = val[GetName()]["scale"].get("z", 0.0).asFloat();
	isTrigger = val[GetName()].get("isTrigger", true).asBool();
	collision_group = static_cast<CollisionGroup>(val[GetName()].get("collision_group", default).asInt());
	
}

void BoxCollider::save(Json::Value & val)
{
	active = val[GetName()].get("active", true).asBool();

	val[GetName()]["colliderId"] = colliderId;

	val[GetName()]["position"]["x"] = transform->position.x;
	val[GetName()]["position"]["y"] = transform->position.y;
	val[GetName()]["position"]["z"] = transform->position.z;
	val[GetName()]["rotation"] = transform->rotation;
	val[GetName()]["scale"]["x"] = transform->scale.x;
	val[GetName()]["scale"]["y"] = transform->scale.y;
	val[GetName()]["scale"]["z"] = transform->scale.z;
	val[GetName()]["scale"]["z"] = transform->scale.z;
	val[GetName()]["isTrigger"] = isTrigger;
	val[GetName()]["collision_group"] = collision_group;
}

void BoxCollider::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader(GetName().c_str()))
	{
		std::string activeId = "active##" + GetName();
		std::string positionId = "Position##" + GetName();
		std::string scaleId = "Scale##" + GetName();
		std::string triggerId = "Is Trigger##" + GetName();
		std::string detachId = "Detach##" + GetName();

		ImGui::Checkbox(activeId.c_str(), &active);
		ImGuiEditor::InputFloat3(positionId.c_str(), transform->position);
		ImGuiEditor::InputFloat3(scaleId.c_str(), transform->scale);
		ImGui::Checkbox(triggerId.c_str(), &isTrigger);

		ImGui::NewLine();

		if (ImGui::Button("CollisionGroup##BoxCollider"))
			ImGui::OpenPopup("CollisionGroup");
		ImGui::SameLine();
		ImGui::Text("CollisionGroup %i", (int)collision_group);

		ImGui::SameLine();

		switch (collision_group)
		{
		case default:
			ImGui::Text("Default");
			break;

		case arrow:
			ImGui::Text("Arrow");
			break;

		case knightbox:
			ImGui::Text("KnightBox");
			break;
		case player:
			ImGui::Text("Player");
			break;
		case slept_body:
			ImGui::Text("Slept Body");
			break;
		case SpctreWalls:
			ImGui::Text("Specter Wall");
			break;
		}

		ImGui::NewLine();
		if (ImGui::Button(detachId.c_str()))
			GetOwner()->detach(this);
	}

	if (ImGui::BeginPopup("CollisionGroup"))
	{
		ImGui::Text("CollisionGroups");
		ImGui::Separator();

		//lol
		if (ImGui::Selectable("Default"))
		{
			collision_group = default;
		}

		if (ImGui::Selectable("arrow"))
		{
			collision_group = arrow;
		}

		if (ImGui::Selectable("KnightBox"))
		{
			collision_group = knightbox;
		}

		if (ImGui::Selectable("Player"))
		{
			collision_group = player;
		}

		if (ImGui::Selectable("Slept Body"))
		{
			collision_group = slept_body;
		}

		if (ImGui::Selectable("Specter Wall"))
		{
			collision_group = SpctreWalls;
		}
		if (ImGui::Selectable("JuiceBox"))
		{
			collision_group = JuiceBox;
		}
		if (ImGui::Selectable("scene_event")) {
			collision_group = scene_event;
		}
		if (ImGui::Selectable("enemy")) {
			collision_group = enemy;
		}
		ImGui::EndPopup();
	}
}