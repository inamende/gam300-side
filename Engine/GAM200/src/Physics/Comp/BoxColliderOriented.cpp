#include "BoxColliderOriented.h"

#include "../../Gfx/Gfx.h"
#include "../../Gfx/Comp/Transform.h"

#include "../../ObjectManager/GameObject.h"

#include "../../Editor/ImGuiEditor.h"

BoxColliderOriented::BoxColliderOriented() : Collider::Collider()
{
	shape = CollisionShape::CSHAPE_OBB;
	transform = DBG_NEW Transform;
}

BoxColliderOriented::~BoxColliderOriented()
{ 
	delete transform;
	physics->RemoveCollider(this);
}

void BoxColliderOriented::Initialize()
{
	if (colliderId == -1)
		colliderId = GetOwner()->component_count<BoxColliderOriented>() - 1;

	SetName("BoxColliderOriented_" + std::to_string(colliderId));

	ownerTransform = GetOwner()->mTransform;

	RigidBody * rb = GetOwner()->GetComponent<RigidBody>();
	if(rb == NULL)
		physics->AddCollider(this, true);
	else
		physics->AddCollider(this, rb->GetStaticness());
}

void BoxColliderOriented::Update() {} 
 
void BoxColliderOriented::Render()
{
	int direction = (GetOwnerTransform()->scale.x > 0) ? 1 : -1;

	glm::vec3 pos = glm::vec3(GetOwnerTransform()->position.x + direction * transform->position.x, GetOwnerTransform()->position.y + transform->position.y, 99.f);
	float rot = GetOwnerTransform()->rotation + transform->rotation;
	DrawBoxOriented(pos, transform->scale.x * 0.5f, transform->scale.y * 0.5f, rot);
}

void BoxColliderOriented::load(Json::Value & val)
{
	active = val[GetName()].get("active", true).asBool();
	
	colliderId = val[GetName()].get("colliderId", -1).asInt();

	transform->position.x = val[GetName()]["position"].get("x", 0.0).asFloat();
	transform->position.y = val[GetName()]["position"].get("y", 0.0).asFloat();
	transform->position.z = val[GetName()]["position"].get("z", 0.0).asFloat();
	transform->rotation = val[GetName()].get("rotation", 0.0).asFloat();
	transform->scale.x = val[GetName()]["scale"].get("x", 0.0).asFloat();
	transform->scale.y = val[GetName()]["scale"].get("y", 0.0).asFloat();
	transform->scale.z = val[GetName()]["scale"].get("z", 0.0).asFloat();
	isTrigger = val[GetName()].get("isTrigger", true).asBool();
	
}

void BoxColliderOriented::save(Json::Value & val)
{
	active = val[GetName()].get("active", true).asBool();

	val[GetName()]["colliderId"] = colliderId;

	val[GetName()]["position"]["x"] = transform->position.x;
	val[GetName()]["position"]["y"] = transform->position.y;
	val[GetName()]["position"]["z"] = transform->position.z;
	val[GetName()]["rotation"] = transform->rotation;
	val[GetName()]["scale"]["x"] = transform->scale.x;
	val[GetName()]["scale"]["y"] = transform->scale.y;
	val[GetName()]["scale"]["z"] = transform->scale.z;
	val[GetName()]["scale"]["z"] = transform->scale.z;
	val[GetName()]["isTrigger"] = isTrigger;
}

void BoxColliderOriented::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader(GetName().c_str()))
	{
		std::string activeId = "active##" + GetName();
		std::string positionId = "Position##" + GetName();
		std::string rotationId = "Rotation##" + GetName();
		std::string scaleId = "Scale##" + GetName();
		std::string triggerId = "Is Trigger##" + GetName();
		std::string detachId = "Detach##" + GetName();

		ImGui::Checkbox(activeId.c_str(), &active);
		ImGuiEditor::InputFloat3(positionId.c_str(), transform->position);
		ImGui::InputFloat(rotationId.c_str(), &transform->rotation);
		ImGuiEditor::InputFloat3(scaleId.c_str(), transform->scale);
		ImGui::Checkbox(triggerId.c_str(), &isTrigger);

		ImGui::NewLine();
		if (ImGui::Button(detachId.c_str()))
			GetOwner()->detach(this);
	}
}