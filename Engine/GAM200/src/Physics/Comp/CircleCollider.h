#ifndef CIRCLE_COLLIDER_H
#define CIRCLE_COLLIDER_H

#include "Collider.h"

class Transform;

class CircleCollider : public Collider
{
public:
	CircleCollider();
	~CircleCollider();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	virtual void Initialize();
	virtual void Update();
	virtual void Render();
};


#endif