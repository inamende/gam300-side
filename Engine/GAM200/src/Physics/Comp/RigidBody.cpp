#include "RigidBody.h"

#include "../../ObjectManager/GameObject.h"
#include "../../Gfx/Comp/Transform.h"
#include "../../Editor/ImGuiEditor.h"
#include <glm/gtc/matrix_transform.hpp>

#include "../Physics.h"

#include "Collider.h"

RigidBody::RigidBody() :
integrator(INTEGRATOR_EULER),
invMass(0.f),
drag(0.99f),
acceleration(glm::vec2()),
velocity(glm::vec2()),
isStatic(true),
autoComputeMass(false),
gravity(0.f, -1.f)
{}

RigidBody::~RigidBody() {}

void RigidBody::Initialize()
{
	transform = GetOwner()->mTransform;
	if(autoComputeMass) AutoComputeBodyInvMass();


	std::vector<Collider*> colliders = GetOwner()->GetComponents<Collider>();
	for (auto it : colliders)
		physics->SwapContainer(it, isStatic);
}

void RigidBody::Update()
{	
	if (autoComputeMass) AutoComputeBodyInvMass();

	if (invMass)
		AddForce(gravity / invMass);

	Integrate(ImGui::GetIO().DeltaTime); // this should go before collide all bodies, not here
}

void RigidBody::Terminate() {}

void RigidBody::Integrate(float dt)
{
	if (dt != 0.f && invMass != 0.f && !isStatic)
	{

		switch (integrator)
		{
		case INTEGRATOR_EULER:  IntegrateEuler(dt);  break;
		case INTEGRATOR_NEWTON: IntegrateNewton(dt); break;
		case INTEGRATOR_VERLET: IntegrateVerlet(dt); break;
		}

		acceleration = glm::vec2();
	}
}

void RigidBody::IntegrateEuler(float dt)
{
	velocity += acceleration * dt;
	velocity *= drag;
	prevPosition = transform->position;
	transform->position.x += velocity.x * dt;
	transform->position.y += velocity.y * dt;
}

void RigidBody::IntegrateNewton(float dt)
{
	velocity += acceleration * dt;
	velocity *= drag;													 
	prevPosition = transform->position;
	transform->position.x += velocity.x + acceleration.x * 0.5f * dt * dt;
	transform->position.y += velocity.y + acceleration.y * 0.5f * dt * dt;
}

void RigidBody::IntegrateVerlet(float dt)
{
	glm::vec2 v = transform->position - prevPosition;
	prevPosition = transform->position;
	transform->position.x += v.x * drag + acceleration.x * invMass * dt * dt;
	transform->position.y += v.y * drag + acceleration.y * invMass * dt * dt;
}

void RigidBody::AddForce(glm::vec2 force)
{
	acceleration += force;
}

void RigidBody::AddForce(float fx, float fy)
{
	AddForce(glm::vec2(fx, fy));
}

void RigidBody::ComputeAABB(glm::vec2 & pos, glm::vec2 & size)
{
	// this was done by Ignacio
#if 0
	switch (collisionShape)
	{
	case CSHAPE_AABB:
		pos = transform->position;
		size = transform->scale;
		break;
	case CSHAPE_CIRCLE:

		pos = transform->position;
		size = { transform->scale.x * 2, transform->scale.y * 2 };
		break;
	case CSHAPE_OBB:
		glm::mat3 rot = glm::mat3(glm::rotate(glm::mat4(1.f), transform->rotation, glm::vec3(0.f, 0.f, 1.f)));
		float width = transform->scale.x;
		float height = transform->scale.y;

		glm::vec3 top_left, top_right, bot_left, bot_right;
													
		top_left.x = -width;
		top_left.y = height;
		top_left.z = 1.f;

		top_left = (rot * top_left) + transform->position;

		//top_right  as an aabb
		top_right.x = width;
		top_right.y = height;

		//transformed aabb
		top_right = (rot * top_right) + transform->position;

		//bot_left as an aabb
		bot_left.x = -width;
		bot_left.y = -height;

		//transformed aabb
		bot_left = (rot * bot_left) + transform->position;

		//bot_right as an aabb
		bot_right.x = +width;
		bot_right.y = -height;

		//transformed aabb
		bot_right = (rot * bot_right) + transform->position;

		float max_x = std::max(top_left.x, std::max(bot_left.x, std::max(top_right.x, bot_right.x))) - transform->position.x;
		float max_y = std::max(top_left.y, std::max(bot_left.y, std::max(top_right.y, bot_right.y))) - transform->position.y;

		pos = transform->position;
		size = { max_x, max_y };
		break;
	}
#endif
}

void RigidBody::load(Json::Value & val)
{
	active = val["RigidBody"].get("active", true).asBool();

	invMass = val["RigidBody"].get("inverse_mass", 1.f).asFloat();
	drag = val["RigidBody"].get("drag", 0.f).asFloat();
	gravity.x = val["RigidBody"]["gravity"].get("x", 0.f).asFloat();
	gravity.y = val["RigidBody"]["gravity"].get("y", -1.f).asFloat();

	isStatic = val["RigidBody"].get("static", true).asBool();
	autoComputeMass = val["RigidBody"].get("autoMass", true).asBool();
}

void RigidBody::save(Json::Value & val)
{
	val["RigidBody"]["active"] = active;

	val["RigidBody"]["inverse_mass"] = invMass;
	val["RigidBody"]["drag"] = drag;
	val["RigidBody"]["gravity"]["x"] = gravity.x;
	val["RigidBody"]["gravity"]["y"] = gravity.y;

	val["RigidBody"]["static"] = isStatic;
	val["RigidBody"]["autoMass"] = autoComputeMass;
}

void RigidBody::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("RigidBody"))
	{
		ImGui::Checkbox("active##RigidBody", &active);
		ImGui::InputFloat("inverse mass##RigidBody", &invMass);
		ImGui::InputFloat("drag##RigidBody", &drag);
		ImGuiEditor::InputFloat2("gravity##RigidBody", gravity);
		ImGuiEditor::InputFloat2("ACCELERATION##RigidBody", acceleration);
		ImGuiEditor::InputFloat2("VELOCITY##RigidBody", velocity);

		if (ImGui::Checkbox("static##RigidBody", &isStatic))
		{
			acceleration = glm::vec2();
			velocity = glm::vec2();

			std::vector<Collider*> colliders = GetOwner()->GetComponents<Collider>();
			for (auto it : colliders)
				physics->SwapContainer(it, isStatic);
		}
			
		ImGui::Checkbox("auto mass##RigidBody", &autoComputeMass);

		ImGuiEditor::InputFloat3("trans##RigidBody", transform->position);

		ImGui::NewLine();
		if (ImGui::Button("Detach##RigidBody"))
			GetOwner()->detach(this);
	}
}

void RigidBody::AutoComputeBodyInvMass()
{
	float magic_density = 0.00658f;
	invMass = 1.0f / fabs(transform->scale.x * transform->scale.y * magic_density);
}

Transform * RigidBody::GetTransform() { return transform; }

bool RigidBody::GetStaticness() { return isStatic; }

void RigidBody::SetStaticness(bool change)
{
	isStatic = change;
}
void RigidBody::ForceFullStop()
{
	this->acceleration = glm::vec2(0.0f, 0.0f);
	this->velocity = glm::vec2(0.0f, 0.0f);
}
void RigidBody::SetInstantaeneousVelocity(glm::vec2 temp)
{
	this->velocity = temp;
}
