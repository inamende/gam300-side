#ifndef BOX_COLLIDER_ORIENTED_H
#define BOX_COLLIDER_ORIENTED_H

#include "Collider.h"

class Transform;

class BoxColliderOriented : public Collider
{
public:
	BoxColliderOriented();
	~BoxColliderOriented();

	virtual void load(Json::Value & val);
	virtual void save(Json::Value & val);
	virtual void WriteToImguiWindow();

	virtual void Initialize();
	virtual void Update();
	virtual void Render();
};


#endif