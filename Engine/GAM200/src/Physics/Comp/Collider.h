#ifndef COLLIDER_H
#define COLLIDER_H

#include "../../Component/Component.h"
#include "../Physics.h"
#include "../../ObjectManager/ObjectManager.h"

enum CollisionShape
{
	CSHAPE_AABB = 1,	// 001
	CSHAPE_CIRCLE = 2,	// 010
	CSHAPE_OBB = 4,	// 100
	CSHAPE_INDEX_MAX = (CSHAPE_OBB | CSHAPE_CIRCLE) + 1
};

enum CollisionGroup
{
	default = 0,
	arrow,
	knightbox,
	player,
	slept_body,
	SpctreWalls,
	JuiceBox,
	Specter,
	scene_event,
	enemy,
	count
};

class Transform;

class Collider : public Component
{
	friend class Forms;
public:
	Collider();
	~Collider();

	virtual void load(Json::Value & val) = 0;
	virtual void save(Json::Value & val) = 0;
	virtual void WriteToImguiWindow() = 0;

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();
	virtual void Render();

	CollisionShape & GetShape();
	Transform * GetTransform();
	Transform * GetOwnerTransform();
	void SetTransform(Transform * tr);
	void SetOwnerTransform(Transform * tr);
	bool IsTrigger();

	//oscars additions, just some setters for the isTrigger variable
	void ChangeTriggerState(bool s);
	void SwitchTriggerState();

	// Collision Evnets hacks
	void OnDeletedObject(const DeletedObjectEvent &);
	bool has_been_deleted_in_this_frame();

	// CollisionGroups
	CollisionGroup collision_group = default;

protected:
	Transform * transform;
	Transform * ownerTransform;
	CollisionShape shape;
	bool isTrigger;
	int colliderId;
	bool aplyOwnerScale;

	// Collision Events hacks
	bool has_been_deleted = false;
};


#endif