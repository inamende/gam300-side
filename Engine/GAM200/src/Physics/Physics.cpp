#include "Physics.h"

#include "../Gfx/Gfx.h"
#include "../Gfx/Comp/Transform.h"
#include "../Factory/Factory.h"
#include "../Level/Scene.h"
#include "../Physics/Comp/RigidBody.h"
#include "Collisions.h"
#include "ContactCollisions.h"

Physics::Physics() : active(true), precission(2) {}

Physics::~Physics() {}

const TABLE_FORMAT collision_table = CollissionTable::getInstance();

Physics * Physics::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW Physics();

	return instance;
}

void Physics::Initialize()
{
	Load();

	mFactory->Register<RigidBody>();

	for (unsigned i = 0; i < CSHAPE_INDEX_MAX; ++i)
		collisionTests[i] = NULL;

	collisionTests[CSHAPE_CIRCLE | CSHAPE_CIRCLE] = CollideCircles;
	collisionTests[CSHAPE_AABB | CSHAPE_AABB] = CollideAABBs;
	collisionTests[CSHAPE_OBB | CSHAPE_OBB] = CollideOBBs;
	collisionTests[CSHAPE_CIRCLE | CSHAPE_AABB] = CollideAABBToCircle;
	collisionTests[CSHAPE_OBB | CSHAPE_AABB] = CollideOBBs;
	collisionTests[CSHAPE_CIRCLE | CSHAPE_OBB] = CollideOBBToCircle;
}

void Physics::Update()
{
#ifdef EDITOR
	if (active && !scene->isEditing) {
		CollideAllBodies();
	}
#endif
#ifndef EDITOR
	if (active)
		CollideAllBodies();
#endif
}

void Physics::Terminate()
{
	Save();

	ClearBodies();

	delete instance;
}

void Physics::UpdateContactEvents()
{
	// for each contact in the curr
	for (auto i : curr_contacts)
	{
		i.has_sent_collisiosn_event = false;

		for (auto t = prev_contacts.begin(); t != prev_contacts.end(); t++)
		{
			if (i.has_sent_collisiosn_event)
				continue;

			t->has_sent_collisiosn_event = false;

			// see if the same contact exists in prev
			if ((i.body0 == t->body0 && i.body1 == t->body1) || (i.body0 == t->body1 && i.body1 == t->body0))
			{
				if (i.body0->has_been_deleted_in_this_frame() || i.body1->has_been_deleted_in_this_frame())
					continue;

				// you are welcome mende
				if (i.body0->GetOwner()->alive == false || i.body1->GetOwner()->alive == false) {
					continue;
				}

				// disaptch collision persists event.
				i.body0->GetOwner()->local_handler.handle(CollisionPersistedEvent(i.body1->GetOwner()));
				i.body1->GetOwner()->local_handler.handle(CollisionPersistedEvent(i.body0->GetOwner()));

				i.has_sent_collisiosn_event = true;

				// remove the contact in prev to now check the rest
				t = prev_contacts.erase(t); // here it assigns the iterator t to its previous value

				if (prev_contacts.empty() || t == prev_contacts.end())
					break;

			}
		} // prev_contacts

		if(!i.has_sent_collisiosn_event)
		{
			// as it wasn't on the prev list,
			// dispatch collision started event

			if (i.body0->has_been_deleted_in_this_frame() || i.body1->has_been_deleted_in_this_frame())
				continue;

			if (i.body0->GetOwner()->alive == false || i.body1->GetOwner()->alive == false) {
				continue;
			}

			i.body0->GetOwner()->local_handler.handle(CollisionStartedEvent(i.body1->GetOwner()));
			i.body1->GetOwner()->local_handler.handle(CollisionStartedEvent(i.body0->GetOwner()));
			i.has_sent_collisiosn_event = true;
		}

	} // curr_contacts

	//for the remaining contacts in prev
	for (auto t : prev_contacts)
	{
		if (t.body0 == nullptr || t.body1 == nullptr)
			continue;

		if (!t.has_sent_collisiosn_event)
		{
			auto owner0 = t.body0->GetOwner();
			auto owner1 = t.body1->GetOwner();

			if (owner0 == nullptr || owner1 == nullptr)
				continue;

			if (t.body0->has_been_deleted_in_this_frame() || t.body1->has_been_deleted_in_this_frame())
				continue;

			if (owner0->alive == false || owner0->alive == false) {
				continue;
			}
			//std::cout << owner0->GetName() << "and" << owner1->GetName() << std::endl;
			//std::cout <<"********************************************** \n";
			owner0->local_handler.handle(CollisionEndedEvent(t.body1->GetOwner()));
			owner1->local_handler.handle(CollisionEndedEvent(t.body0->GetOwner()));
		}						
	} // prev_contacts

	prev_contacts = curr_contacts;
	curr_contacts.clear();
}

ResolutionType Physics::check_if_they_should_collide(Collider * b0, Collider * b1, const TABLE_FORMAT & collision_table)
{
	unsigned result = collision_table[b0->collision_group][b1->collision_group];

	ResolutionType type;

	switch (result)
	{
	case 1:
		type = resolve;
		break;

	case 2:
		type = detect;
		break;

	case 3:
		type = none;
		break;

	default:
		type = resolve;
		break;
	}

	return type;
}

void Physics::Render()
{
	for (auto it = staticBodies.begin(); it != staticBodies.end(); it++)
		(*it)->Render();

	for (auto it = dynamicBodies.begin(); it != dynamicBodies.end(); it++)
		(*it)->Render();
}

void Physics::Load()
{
	std::ifstream jsonFile(editor_loader);
	Json::Reader reader;
	bool ok = reader.parse(jsonFile, commonValue);
	if (!ok)
		std::cout << "ERROR READING JSON" << std::endl;

	precission = commonValue.get("physics_precission", 2).asInt();
	active = commonValue.get("physics_active", true).asBool();

	jsonFile.close();
}

void Physics::Save()
{
#ifdef EDITOR
	commonValue["physics_precission"] = precission;
	commonValue["physics_active"] = active;

	Json::StreamWriterBuilder builder;
	Json::StreamWriter * writer = builder.newStreamWriter();
	std::ofstream jsonFileWrite(editor_loader);
	writer->write(commonValue, &jsonFileWrite);
	jsonFileWrite.close();
#endif
}

void Physics::WriteToImguiWindow()
{
	if (ImGui::CollapsingHeader("Physics"))
	{
		//static bool apply = true;
		//ImGui::Checkbox("Apply Phisics##Physics", &apply);
		//physics->EnablePhysics(apply);

		ImGuiEditor::InputInt("Prcission##Physics", precission);

		unsigned statics = physics->StaticObjectCount();
		ImGui::Text("Static objects : %d", statics);

		unsigned dynamics = physics->DynamicObjectCount();
		ImGui::Text("Dynamic objects : %d", dynamics);
	}
}

void Physics::CollideAllBodies()
{
	RigidBody * r0 = NULL, * r1 = NULL;

	Contact contact;

	for (int n = precission; n > 0; n--)
	{
		for (auto b0 = dynamicBodies.begin(); b0 != dynamicBodies.end(); b0++) 
		{
			auto b1 = b0; b1++;
			for (; b1 != dynamicBodies.end(); b1++)
			{
				contact.body0 = *b0;
				contact.body1 = *b1;

				CollisionFn fn = GetCollisionFn((*b0), (*b1));
				if (fn && fn((*b0), (*b1), &contact))
				{
					// Check the Collision Table to know whether this objects should detect the collision,
					// and resolve it or not
					unsigned result = check_if_they_should_collide(*b0, *b1, collision_table);

					if (result == none) // means we shouldnt even send the event
						continue;

					else if (result == detect) // means we should only send the event
					{
						curr_contacts.push_back(contact);
						continue;
					}
					else // means we should send the event and resolve the collision
						curr_contacts.push_back(contact);			

					if ((*b0)->IsTrigger() || (*b1)->IsTrigger())
					{
						continue;
					}

					r0 = (*b0)->GetOwner()->mRigidBody; r1 = (*b1)->GetOwner()->mRigidBody;

					if (r0 == NULL || r1 == NULL) continue;

					ResolveContactPenetration(r0, r1, &contact);

					//std::cout << "COLLISION DYNAMIC" << std::endl;
					//if (n == 0) // resolve velocity just once no matter the precission
					ResolveContactVelocity(r0, r1, &contact);
				}
			}
		}

		RigidBody temp = RigidBody();
		for (auto b0 = dynamicBodies.begin(); b0 != dynamicBodies.end(); b0++)
			for (auto b1 = staticBodies.begin(); b1 != staticBodies.end(); b1++)
			{
				contact.body0 = *b0;
				contact.body1 = *b1;

				CollisionFn fn = GetCollisionFn((*b0), (*b1));
				if (fn && fn((*b0), (*b1), &contact))
				{
					// Check the Collision Table to know whether this objects should detect the collision,
					// and resolve it or not
					unsigned result = check_if_they_should_collide(*b0, *b1, collision_table);

					if (result == none) // means we shouldnt even send the event
						continue;

					else if (result == detect) // means we should only send the event
					{
						curr_contacts.push_back(contact);
						continue;
					}
					else // means we should send the event and resolve the collision
						curr_contacts.push_back(contact);

					if ((*b0)->IsTrigger() || (*b1)->IsTrigger())
					{
						// collision event logic goes here?
						// std::cout << "COLLISION STATIC" << std::endl;
						continue;
					}

					if ((*b0)->GetOwner()->GetName() == "Strawss" || (*b1)->GetOwner()->GetName() == "Strawss")
						std::cout << "";

					if((*b0)->GetOwner())
						r0 = (*b0)->GetOwner()->mRigidBody; 
					if ((*b1)->GetOwner())
						r1 = (*b1)->GetOwner()->mRigidBody;

					if (r0 != NULL && r1 == NULL)
					{
						// prevent static to static coillision
						if (r0->invMass == 0.0f)
							continue;

						
						temp.transform = (*b1)->GetOwnerTransform();

						ResolveContactPenetration(r0, &temp, &contact);
						//if (n == precission)
						ResolveContactVelocity(r0, &temp, &contact);

						continue;
					}

					if (r0 == NULL && r1 != NULL)
					{
						// prevent static to static coillision
						if (r1->invMass == 0.0f)
							continue;

						temp.transform = (*b0)->GetOwnerTransform();

						ResolveContactPenetration(&temp, r1, &contact);
						//if (n == precission)
						ResolveContactVelocity(&temp, r1, &contact);

						continue;
					}

					if (r0 == NULL && r1 == NULL) continue;

					ResolveContactPenetration(r0, r1, &contact);

					//if (n == 0) // resolve the velocity only once
						ResolveContactVelocity(r0, r1, &contact);
				}
			}
	}

	UpdateContactEvents();
}

void Physics::AddCollider(Collider * b, bool isStat)
{
	if (b == NULL)
		return;

	if (isStat)
	{
		if(b->GetOwner())
			if(b->GetOwner()->mRigidBody != NULL)
				b->GetOwner()->mRigidBody->isStatic = true;

		staticBodies.push_back(b);
	}
	else
	{
		if (b->GetOwner()->mRigidBody != NULL)
			b->GetOwner()->mRigidBody->isStatic = false;
		dynamicBodies.push_back(b);
	}
		
}

void Physics::RemoveCollider(Collider * b)
{
	staticBodies.remove(b);
	dynamicBodies.remove(b);
}

void Physics::SwapContainer(Collider * b, bool isStatic)
{
	if (isStatic)
	{
		RemoveCollider(b);
		AddCollider(b, true);
	}
	else
	{
		RemoveCollider(b);
		AddCollider(b, false);
	}
}

void Physics::ClearBodies()
{
	staticBodies.clear();
	dynamicBodies.clear();
}

CollisionFn Physics::GetCollisionFn(Collider * b0, Collider * b1)
{
	unsigned collision_index = b0->GetShape() | b1->GetShape();
	return collisionTests[collision_index];
}

void Physics::ResolveContactPenetration(RigidBody * r0, RigidBody * r1, Contact * contact)
{
	float total_inv_mass = r0->invMass + r1->invMass;
	float mass_inf1 = r0->invMass / total_inv_mass;
	float mass_inf2 = r1->invMass / total_inv_mass;

	r0->transform->position.x = r0->transform->position.x - contact->normal.x * contact->penetration * mass_inf1;
	r0->transform->position.y = r0->transform->position.y - contact->normal.y * contact->penetration * mass_inf1;

	r1->transform->position.x = r1->transform->position.x + contact->normal.x * contact->penetration * mass_inf2;
	r1->transform->position.y = r1->transform->position.y + contact->normal.y * contact->penetration * mass_inf2;
}

void Physics::ResolveContactVelocity(RigidBody * r0, RigidBody * r1, Contact * contact)
{
	glm::vec2 r = r1->velocity - r0->velocity;
	float sep = glm::dot(r, contact->normal);
	//float sep_1 = -sep * DFLT_RESTITUTION;
	float sep_1 = 0; // for now
	float Asep = sep_1 - sep;
	float total_inv_mass = r0->invMass + r1->invMass;
	float mass_inf1 = r0->invMass / total_inv_mass;
	float mass_inf2 = r1->invMass / total_inv_mass;

	r0->velocity = r0->velocity - contact->normal * Asep * mass_inf1 * mass_inf1;
	r1->velocity = r1->velocity + contact->normal * Asep * mass_inf2 * mass_inf2;

}

void Physics::EnablePhysics(bool enable) { active = enable; }

unsigned Physics::StaticObjectCount() { return staticBodies.size(); }

unsigned Physics::DynamicObjectCount() { return dynamicBodies.size(); }

Physics * Physics::instance = 0;

bool CollideCircles(Collider * b0, Collider* b1, Contact * c)
{
	glm::vec2 pos0 = b0->GetOwnerTransform()->position + b0->GetTransform()->position;
	glm::vec2 pos1 = b1->GetOwnerTransform()->position + b1->GetTransform()->position;

	return StaticCircleToStaticCircleEx(pos0, b0->GetTransform()->scale.x, pos1, b1->GetTransform()->scale.x, c);
}

bool CollideAABBs(Collider * b0, Collider * b1, Contact * c)
{ 
	int direction_0 = (b0->GetOwnerTransform()->scale.x > 0) ? 1 : -1;
	int direction_1 = (b1->GetOwnerTransform()->scale.x > 0) ? 1 : -1;

	glm::vec2 tr_0(b0->GetTransform()->position.x * direction_0, b0->GetTransform()->position.y);
	glm::vec2 tr_1(b1->GetTransform()->position.x * direction_1, b1->GetTransform()->position.y);

	glm::vec2 p0 = glm::vec2(b0->GetOwnerTransform()->position) + tr_0, s0 = b0->GetTransform()->scale,
			  p1 = glm::vec2(b1->GetOwnerTransform()->position) + tr_1, s1 = b1->GetTransform()->scale;
	return StaticRectToStaticRectEx(p0, s0, p1, s1, c);
}

bool CollideOBBs(Collider * b0, Collider * b1, Contact * c)
{
	Transform obb0( b0->GetOwnerTransform()->position + b0->GetTransform()->position,
					-(b0->GetOwnerTransform()->rotation) + (-b0->GetTransform()->rotation),
					b0->GetTransform()->scale);
	Transform obb1( b1->GetOwnerTransform()->position + b1->GetTransform()->position,
					-(b1->GetOwnerTransform()->rotation) + (-b1->GetTransform()->rotation),
					b1->GetTransform()->scale);

	return OrientedRectToOrientedRectEx(&obb0, &obb1, c);
}

bool CollideAABBToCircle(Collider * b0, Collider * b1, Contact * c)
{

	Collider * rect = b0->GetShape() == CSHAPE_AABB ? b0 : b1;
	Collider * circle = b1->GetShape() == CSHAPE_CIRCLE ? b1 : b0;

	if (StaticRectToStaticCircleEx( 
		glm::vec2(rect->GetOwnerTransform()->position + rect->GetTransform()->position),
		rect->GetTransform()->scale.x, rect->GetTransform()->scale.y,
		glm::vec2(circle->GetOwnerTransform()->position + circle->GetTransform()->position),
		circle->GetTransform()->scale.x, c)
		)
	{
		if (circle == b0) // flip normal to match our convention
			c->normal = -c->normal;
		return true;
	}
	return false;
}

bool CollideOBBToCircle(Collider * b0, Collider * b1, Contact * c)
{
	Transform obb = (b0->GetShape() == CSHAPE_OBB)
		? Transform(b0->GetOwnerTransform()->position + b0->GetTransform()->position,
			b0->GetOwnerTransform()->rotation + b0->GetTransform()->rotation,
			b0->GetTransform()->scale)
		: Transform(b1->GetOwnerTransform()->position + b1->GetTransform()->position,
			b1->GetOwnerTransform()->rotation + b1->GetTransform()->rotation,
			b1->GetTransform()->scale);

	Collider * circle = b0->GetShape() == CSHAPE_CIRCLE ? b0 : b1;

	if (StaticOBBToStaticCircleEx(
		&obb, glm::vec2(circle->GetOwnerTransform()->position + circle->GetTransform()->position),
		circle->GetTransform()->scale.x, c))
	{
		if (circle == b0) // flip normal to match our convention
			c->normal = -c->normal;
		return true;
	}
	return false;
}

