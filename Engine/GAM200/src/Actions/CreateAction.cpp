#include "CreateAction.h"

#include "../ObjectManager/GameObject.h"
#include "../Gfx/Comp/Transform.h"
#include "../Gfx/Comp/Renderable.h"
#include "../Level/Scene.h"
#include "../Editor/ImGuiEditor.h"
#include "Actions.h"

CreateAction::~CreateAction() {}
CreateAction::CreateAction(Json::Value & jsonVal, glm::vec3 & pos, std::string rl, bool useJsonPos) : theJson(jsonVal), createPosition(pos), createRenderLayer(rl), jsonPos(useJsonPos) { theActor = NULL; }
void CreateAction::ExecuteAction()
{
	copyOfActor = theActor;

	theActor = scene->CreateGameObject();

	theActor->SetName(theJson.get("name", "unnamed").asString());
	theActor->AttachComponents(theJson);
	theActor->LoadComponents(theJson);

	if (!jsonPos)
	{
		theActor->mTransform->position.x = createPosition.x;
		theActor->mTransform->position.y = createPosition.y;
		theActor->mTransform->position.z = createPosition.z;
	}
	else
		theActor->mTransform->position.z = createPosition.z + 0.1f;

	Renderable * rend = theActor->GetComponent<Renderable>();
	if (rend)
		rend->layer = createRenderLayer;

	theActor->Initialize();

	RestoreDependencies(copyOfActor, theActor);
}

void CreateAction::UndoAction()
{
	imGuiEditor->SetSelected(NULL);
	imGuiEditor->DumpSelected();
	scene->DeleteGameObject(theActor);
}

void CreateAction::RestoreDependencies(GameObject * ob0, GameObject * ob1)
{
	int remaining = mActions->counter + 1;

	while (remaining < mActions->undo.size())
	{
		for (auto it = mActions->undo[remaining].begin(); it != mActions->undo[remaining].end(); it++)
				if ((*it)->GetActor() == ob0)
					(*it)->SetActor(ob1);

		remaining++;
	}
}