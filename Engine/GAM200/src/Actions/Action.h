#ifndef ACTION_H
#define ACTION_H

#include "../Component/Entity.h"

class GameObject;

class Action : Entity
{
	friend class Actions;
public:
	Action();
	virtual ~Action();

	virtual void ExecuteAction() = 0;
	virtual void UndoAction() = 0;

	GameObject * GetActor();
	void SetActor(GameObject*);

protected:
	GameObject * theActor;
};

#endif