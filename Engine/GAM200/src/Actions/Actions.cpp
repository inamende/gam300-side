#include "Actions.h"

#include "Action.h"

Actions * Actions::getInstance()
{
	if (instance == NULL)
		instance = DBG_NEW Actions();

	return instance;
}

Actions::Actions() : counter(0) {}

Actions::~Actions()
{
	/* for (auto it = undo.begin(); it != undo.end(); it++)
	 	for (auto is = (*it).begin(); is != (*it).end(); is++)
	 		delete *is;
	 undo.clear();*/
}

void Actions::Terminate()
{
	delete instance;
}

void Actions::PushAction(std::vector<Action*> & action)
{
	while (counter < undo.size())
	{
		for (unsigned i = 0; i < undo.back().size(); i++)
			delete undo.back()[i];
		undo.pop_back();
	}
		  
	counter++;
	undo.push_back(action);

	// this is very nice for debuging
	//std::cout << "mem : ";
	//for (unsigned i = 0; i < undo.size(); i++)
	//	std::cout << i << " ";
	//std::cout<<std::endl;
}

void Actions::Undo()
{
	if (counter > 0)
		counter--;
	else
		return;

	for(unsigned i = 0; i < undo[counter].size(); i++)
		undo[counter][i]->UndoAction();
	
	//std::cout << "lel : ";
	//for (unsigned i = 0; i < counter; i++)
	//	std::cout << i << " ";
	//std::cout << std::endl;
}

void Actions::Redo()
{
	if (counter > undo.size() - 1 || undo.size() == 0)
		return;

	for (unsigned i = undo[counter].size(); i > 0; i--)
		undo[counter][i - 1]->ExecuteAction();

	counter++;

	//std::cout << "lel : ";
	//for (unsigned i = 0; i < counter; i++)
	//	std::cout << i << " ";
	//std::cout << std::endl;
}

Actions * Actions::instance = 0;