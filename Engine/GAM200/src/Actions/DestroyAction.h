#ifndef DESTROY_ACTION_H
#define DESTROY_ACTION_H

#include "Action.h"

class DestroyAction : public Action
{
public:
	~DestroyAction();
	DestroyAction(GameObject * actor);

	virtual void ExecuteAction();
	virtual void UndoAction();

	void RestoreDependencies(GameObject * ob0, GameObject * ob1);

private:
	Json::Value theJson;
	std::vector<std::vector<GameObject*>> dependencies;
};

#endif