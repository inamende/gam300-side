#ifndef CREATE_ACTION_H
#define CREATE_ACTION_H

#include "Action.h"

class CreateAction : public Action
{
public:
	~CreateAction();
	CreateAction(Json::Value & jsonVal, glm::vec3 & pos, std::string rl, bool useJsonPos = false);

	virtual void ExecuteAction();
	virtual void UndoAction();

	void RestoreDependencies(GameObject * ob0, GameObject * ob1);

private:
	Json::Value theJson;
	glm::vec3 createPosition;
	std::string createRenderLayer;
	bool jsonPos;

	GameObject * copyOfActor;
	std::vector<std::vector<GameObject*>> dependencies;
};

#endif