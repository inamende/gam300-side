#include "TransformAction.h"

#include "../ObjectManager/GameObject.h"
#include "../Gfx/Comp/Transform.h"

TranslateAction::~TranslateAction() {}
TranslateAction::TranslateAction(GameObject * actor, Transform * tr) : newPosition(tr->position), newScale(tr->scale), newRotation(tr->rotation) { theActor = actor; }
void TranslateAction::ExecuteAction()
{
	theActor->mTransform->position = newPosition;
	theActor->mTransform->scale = newScale;
	theActor->mTransform->rotation = newRotation;
}
void TranslateAction::UndoAction()
{
	theActor->mTransform->position = newPosition;
	theActor->mTransform->scale = newScale;
	theActor->mTransform->rotation = newRotation;
}