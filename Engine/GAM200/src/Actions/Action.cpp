#include "Action.h"

#include "Actions.h"
#include "../ObjectManager/GameObject.h"
#include "../Gfx/Comp/Transform.h"
#include "../Editor/ImGuiEditor.h"
#include "../Level/Scene.h"
#include "../Gfx/Gfx.h"
#include "../Gfx/Reso/Camera.h"
#include "../Factory/Factory.h"
#include "../ObjectManager/ObjectManager.h"

Action::Action() {}
Action::~Action() {}

GameObject * Action::GetActor() { return theActor; }

void Action::SetActor(GameObject * actor) { theActor = actor; }