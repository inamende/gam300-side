#ifndef ACTIONS_H
#define ACTIONS_H

#include "../Component/Entity.h"

class Action;

class Actions : Entity
{
	friend class CreateAction;
	friend class DestroyAction;
private:
	static Actions * instance;
	Actions();

public:
	~Actions();
	static Actions * getInstance();

	void Terminate();

	void PushAction(std::vector<Action*> &);
	void Undo();
	void Redo();

private:
	std::vector<std::vector<Action*>> undo;
	unsigned counter;
};

#define mActions Actions::getInstance()

#endif