#include "DestroyAction.h"

#include "../ObjectManager/GameObject.h"
#include "../Level/Scene.h"
#include "Actions.h"

DestroyAction::~DestroyAction() {}
DestroyAction::DestroyAction(GameObject * actor) { theActor = actor; }

void DestroyAction::ExecuteAction()
{
	Json::Value v = theActor->GetValue();
	v["name"] = theActor->GetName();
	theActor->SaveComponents(v);
	theJson = v;

	scene->DeleteGameObject(theActor);
}

void DestroyAction::UndoAction()
{
	GameObject * restoreObj = scene->CreateGameObject();
	restoreObj->GetValue() = theJson;
	restoreObj->SetName(theJson.get("name", "unnamed").asString());
	restoreObj->AttachComponents(theJson);
	restoreObj->LoadComponents(theJson);
	restoreObj->Initialize();

	RestoreDependencies(theActor, restoreObj);

	theActor = restoreObj;
}

void DestroyAction::RestoreDependencies(GameObject * ob0, GameObject * ob1)
{
	int remaining = mActions->counter - 1;

	while (remaining >= 0)
	{
		for (auto it = mActions->undo[remaining].begin(); it != mActions->undo[remaining].end(); it++)
			if ((*it)->GetActor() == ob0)
				(*it)->SetActor(ob1);

		remaining--;
	}
}