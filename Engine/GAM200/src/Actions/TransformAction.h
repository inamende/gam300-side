#ifndef TRANSFORM_ACTION_H
#define TRANSFORM_ACTION_H

#include "Action.h"

class Transform;

class TranslateAction : public Action
{
public:
	~TranslateAction();
	TranslateAction(GameObject * actor, Transform * tr);

	virtual void ExecuteAction();
	virtual void UndoAction();

private:
	glm::vec3 newPosition;
	glm::vec3 newScale;
	float newRotation;
};

#endif