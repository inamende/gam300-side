#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_

#include "../Component/Component.h"

class StateMachine;
class GameObject;

class State
{
public:
	State(const char * name);

	State() {}

	virtual void Enter() {}
	virtual void Exit() {}
	virtual void Update() {}

	virtual void InternalEnter();
	virtual void InternalExit();
	virtual void InternalUpdate();

	std::string m_name;
	StateMachine * m_owner;
	GameObject * m_actor;
	float m_time_in_state;
};

class StateMachine : public Component
{
public:
	StateMachine();
	StateMachine(GameObject * actor);
	virtual ~StateMachine();

	virtual void Initialize();
	virtual void Update();
	virtual void Terminate();

	virtual void load(Json::Value & val) = 0;
	virtual void save(Json::Value & val) = 0;
	virtual void WriteToImguiWindow() = 0;

	void AddState(State * state);
	void RemoveState(State * state);
	void Clear();
	State* GetState(const char * stateName);
	void SetInitState(State * state);
	void SetInitState(const char * stateName);
	void ChangeState(State * state);
	void ChangeState(const char * stateName);
	void StatusUpdate();
	void Reset();

	std::vector<State*> mStates;
	GameObject * m_actor;
	State * mPreviousState;
	State * mCurrentState;
	State * mNextState;
	State * mInitialState;
};

#endif