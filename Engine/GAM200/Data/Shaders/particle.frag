#version 400

in vec2 UV;
in vec4 particleColor;
out vec4 color;

uniform sampler2D textureData;

void main()
{	
	vec4 texColor = texture2D(textureData, vec2(UV)).rgba;
	color = texColor * particleColor;
}	
