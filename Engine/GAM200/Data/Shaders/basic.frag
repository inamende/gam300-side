#version 400

in vec2 UV;
out vec4 color;
in vec3 position; 
in vec3 normal;

struct Light
{
	vec3 ambient;
	vec3 diffuse;  
	vec3 specular; 
	vec3 lightPosition;
	vec3 halfVector;
	vec3 spotDirection;
	float spotCutoff; 
	float spotCosPhi; 
	float spotCosTheta;
	vec3 lightColor;
	float spotDelta;
};

uniform sampler2D textureData;
uniform vec4 Color;

uniform Light light[30];
uniform mat4 V;
uniform mat4 M;
uniform int lightNum;
uniform vec4 ambient;

//Compute diffuse
vec3 comp_dif(inout vec3 L, inout vec3 N, int i) {
	L = normalize(light[i].lightPosition - position);	
	N = vec3(normalize(M * vec4(normal, 0.0)));
	return light[i].diffuse * light[i].lightColor * max(dot(N, L), 0.0);
}

//Compute specular
vec3 comp_spec(vec3 L, vec3 N, int i) {
	//vec3 R = normalize(reflect(-L,N));
	//vec3 V = normalize(light[i].viewerPos - position);
	//return light[i].specular * light[i].lightColor * pow(max(dot(R, V), 0.0), mat.n);
	return vec3(0.0);
}

//Compute spotlight
float comp_spot(inout vec4 diff, inout vec4 spec, vec3 L, int i) {
	if(dot(normalize(light[i].spotDirection), -L) < light[i].spotCosPhi) {
		diff = vec4(0.0);
		spec = vec4(0.0);
		return 1.0;
	}
	else if(dot(normalize(light[i].spotDirection), -L) > light[i].spotCosTheta) {
		return 1.0;
	}
	else {
		float Alpha = dot(light[i].spotDirection, -L);
		return pow(((Alpha - light[i].spotCosPhi) / (light[i].spotCosTheta -  light[i].spotCosPhi)), 2);	  
	}
}
	
void main() {
	vec4 outLight = vec4(0.0);
	
	vec3 l, n;
	
	for(int i = 0; i < lightNum; i++) {
		//vec4 ambient = vec4(light[i].ambient, 1.0);
		vec4 diffuse = vec4(comp_dif(l, n, i), 1.0);
		vec4 specular = vec4(comp_spec(l, n, i), 1.0);
		float spot = comp_spot(diffuse, specular, l, i);
	
		outLight += spot * (diffuse + specular);
	}
	vec4 light_color = texture2D(textureData, UV).rgba * (ambient + outLight);
	color.rgb = light_color.rgb * Color.rgb;
	color.a = light_color.a * Color.a;
}
