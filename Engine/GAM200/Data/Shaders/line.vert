#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 vertexColor;

out vec4 colorVert;

uniform mat4 P;
uniform mat4 V;

void main()
{
	gl_Position = P * V * vec4(vertexPosition,1.0);
	colorVert = vertexColor;
}