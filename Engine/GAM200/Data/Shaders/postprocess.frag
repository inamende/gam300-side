#version 400

in vec2 UV;

uniform sampler2D textureData;

uniform float brightness;
uniform float saturation;
uniform float hue;
uniform vec3 overlay;
uniform float density;

out vec4 fragColor;

void Brightness(inout vec4 color, float b)
{
    color.rgb += b;
}

mat4 Saturation(float saturation)
{
    vec3 luminance = vec3( 0.3086, 0.6094, 0.0820 );
    float oneMinusSat = 1.0 - saturation;
    vec3 red = vec3(luminance.x * oneMinusSat);
    red.r += saturation;
    
    vec3 green = vec3(luminance.y * oneMinusSat);
    green.g += saturation;
    
    vec3 blue = vec3(luminance.z * oneMinusSat);
    blue.b += saturation;
    
    return mat4( 
        red,     0,
        green,   0,
        blue,    0,
        0, 0, 0, 1 );
}

vec4 Hue(in vec3 col, in float Shift)
{
    vec3 P = vec3(0.55735) * dot(vec3(0.55735), col);
    vec3 U = col - P;
    vec3 V = cross(vec3(0.55735), U);    
    col = U * cos(Shift * 6.2832) + V * sin(Shift * 6.2832) + P;
    return vec4(col, 1.0);
}

void main()
{	
	vec4 color = texture2D(textureData, UV);
	vec4 finalColor = color;
	
	Brightness(color, brightness / 100.0);
	finalColor = Saturation((saturation + 100.0) / 100.0) * color;
	finalColor = Hue(finalColor.rgb, hue / 360.0);
	finalColor.a = 1.0;
	
	fragColor = vec4(mix(finalColor.rgb, overlay.rgb, density), color.a);
}
