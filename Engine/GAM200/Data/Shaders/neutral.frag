#version 400

in vec2 UV;
out vec4 color;

uniform sampler2D textureData;
uniform vec4 Color;
uniform vec4 ambient;

	
void main() {
	vec4 texture_color = texture2D(textureData, UV).rgba * ambient;
	color.rgb = texture_color.rgb * Color.rgb;
	color.a = texture_color.a * Color.a;
}
