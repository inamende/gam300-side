#version 400

in vec2 UV;
out vec4 color;

uniform sampler2D textureData;
uniform vec3 Color;

void main()
{	
	vec4 sampled = vec4(1.0, 1.0, 1.0, texture(textureData, UV).r);
	color = vec4(Color, 1.0) * sampled;
}	
