#version 400

in vec3 UV;
out vec4 color;

uniform sampler2D textureData;

void main()
{	
	vec4 texColor = texture2D(textureData, vec2(UV)).rgba;
	color = texColor;
	
	//vec4 texColor = texture2D(textureData, UV).rgba;
	//color.rgb = texColor.rgb * Color.rgb;
	//color.a = texColor.a * Color.a;
}	
