#version 400

out vec4 color;

uniform vec4 Color;

void main()
{	
	color.rgba = Color.rgba;
}	
