#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;
layout(location = 4) in vec4 vertexColor;

out vec2 UV;
out vec3 position;
out vec3 normal;
out vec4 vColor;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;


void main()
{
   gl_Position = P * V * M * vec4(vertexPosition,1.0);
   position = vec3(M * vec4(vertexPosition, 1.0));
   normal = vertexNormal;
   UV = vertexUV;
   vColor = vertexColor;
}