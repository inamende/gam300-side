#version 400

layout(location = 0) in vec3 vertexPosition;

out vec3 position;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void main()
{
   gl_Position = P * V * M * vec4(vertexPosition,1.0);
   position = vertexPosition;
}