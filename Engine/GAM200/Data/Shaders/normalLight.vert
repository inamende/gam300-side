#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;
layout(location = 3) in vec3 vertexTangent;

out vec2 UV;
out vec3 position;
out vec3 normal;
out vec3 tangent;
out vec3 bitangent;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;


void main()
{
    mat3 MV3x3 = mat3(V * M);
    
   gl_Position = P * V * M * vec4(vertexPosition,1.0);
   position = vec3(V * M * vec4(vertexPosition, 1.0));
   normal = MV3x3 * vertexNormal;
   tangent = vec3(normalize(MV3x3 * vertexTangent));
   bitangent = normalize(cross( normal, tangent));
   
   //UV.x = 1 - vertexUV.x;
   //UV.y = 1 - vertexUV.y;
   
   UV = vertexUV;
}