#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

out vec2 UV;
out vec3 position;
out vec3 normal;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;


void main()
{
   gl_Position = P * V * M * vec4(vertexPosition,1.0);
   UV = vertexUV;
}