#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

out vec2 UV;
out vec3 position;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void main()
{
   gl_Position = M * V * P * vec4(vertexPosition,1.0);
   position = vertexPosition;

   // UV = vertexUV;
   UV.x = vertexUV.x;
   UV.y = 1 - vertexUV.y;
}