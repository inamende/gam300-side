#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

out vec3 UV;
out vec3 position;
out vec3 normal;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

uniform mat4 T;
uniform mat4 S;

void main()
{
   gl_Position = P * V * M * vec4(vertexPosition,1.0);
   position = vec3(M  * vec4(vertexPosition, 1.0));
   normal = vertexNormal;
   UV = vec3(T * S * vec4(vertexUV, 1.0, 1.0));
}