#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 3) in mat4 modelMatrix;
layout(location = 7) in mat4 T;
layout(location = 11) in mat4 S;

out vec3 UV;
out vec3 position;

uniform mat4 V;
uniform mat4 P;

void main()
{
   gl_Position = P * V * modelMatrix * vec4(vertexPosition,1.0);
   position = vertexPosition;

   UV = vec3(T * S * vec4(vertexUV, 1.0, 1.0));
}