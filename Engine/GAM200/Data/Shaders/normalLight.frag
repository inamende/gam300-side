#version 400

in vec2 UV;
in vec3 position; 
in vec3 normal;
in vec3 tangent;
in vec3 bitangent;

out vec4 color;

struct Light
{
	vec3 ambient;
	vec3 diffuse;  
	vec3 specular; 
	vec3 lightPosition;
	vec3 halfVector;
	vec3 spotDirection;
	float spotCutoff; 
	float spotCosPhi; 
	float spotCosTheta;
	vec3 lightColor;
	float spotDelta;
};

uniform sampler2D textureData;
uniform sampler2D normalData;
uniform Light light[8];
uniform mat4 V;
uniform mat4 M;
uniform int lightNum;

void main()
{	
    mat3 CT = mat3(tangent.x, bitangent.x, normal.x,
				   tangent.y, bitangent.y, normal.y,
				   tangent.z, bitangent.z, normal.z);
				   
	vec2 tempUv;
	tempUv.x = 1 - UV.x;
	tempUv.y = 1 - UV.y;
	vec3 normalMapper =  2.0 * texture( normalData, tempUv ).xyz - 1.0;
 
	vec4 outLight = vec4(0.0,0.0,0.0,0.0);
 
 for(int i = 0; i < lightNum; i++)
 {
	 vec3 Direction = vec3(V *  vec4(light[i].spotDirection, 0.0));
    
 
//Compute diffuse
	vec3 L = normalize(CT * (vec3(V * vec4(light[i].lightPosition, 1.0)) - position));	
	vec3 N = normalMapper;
	
	vec3 IDiffuse = light[i].lightColor * light[i].diffuse * max(dot(N, L), 0.0);
    
//Compute specular
	vec3 R = normalize(reflect(-L,N));
	vec3 V = normalize(CT * (-position)); //camera position is (0,0,0) in view space

	vec3 ISpeccular = light[i].lightColor * light[i].specular * vec3(5,5,5) * pow(max(dot(R, V), 0.0), 100);

//Compute spot light
    float SpotlightEffect = 1.0;

	vec3 spotter = normalize(CT * (Direction));
	
	if(dot(spotter, -L) < light[i].spotCosPhi) //totally out
	{
		IDiffuse = vec3(0,0,0);
		ISpeccular = vec3(0,0,0);
	}
	if(dot(spotter, -L) > light[i].spotCosTheta) //inside inner cone
	{
		SpotlightEffect = 1.0;
	}
	else
	{
		float Alpha = dot(spotter, -L);
		SpotlightEffect = pow(((Alpha - light[i].spotCosPhi) / (light[i].spotCosTheta -  light[i].spotCosPhi)), 2);

	}
	
    outLight += vec4(light[i].ambient, 1.0) + SpotlightEffect * (vec4(IDiffuse, 1.0) + vec4(ISpeccular, 1.0));

 }
   
    color = texture2D(textureData, UV).rgba * outLight;
}	
