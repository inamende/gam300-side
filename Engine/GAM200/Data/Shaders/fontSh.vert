#version 400

layout(location = 0) in vec4 vertexPosition;

out vec2 UV;

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

void main()
{
   gl_Position = P * V * M * vec4(vertexPosition.xy, 0.0, 1.0);
   UV = vertexPosition.zw;
}