#version 400

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 xyzs;
layout(location = 2) in vec4 color; 

out vec2 UV;
out vec4 particleColor;

uniform mat4 V;
uniform mat4 P;

void main()
{

	float particleSize = xyzs.w;
	vec3 particleCenter_wordspace = xyzs.xyz;
	
	vec3 vertexPosition_worldspace = 
		particleCenter_wordspace
		+ vec3(1.0, 0.0, 0.0) * vertexPosition.x * particleSize
		+ vec3(0.0, 1.0, 0.0) * vertexPosition.y * particleSize;


	gl_Position = P * V * vec4(vertexPosition_worldspace, 1.0f);

	UV = vertexPosition.xy + vec2(0.5, 0.5);

	UV.y = -UV.y;
	
	particleColor = color;



	
   //gl_Position = P * V * modelMatrix * vec4(vertexPosition,1.0);

   //UV = vec3(T * S * vec4(vertexUV, 1.0, 1.0));
}