#version 400

in vec4 colorVert;
out vec4 color;

void main()
{	
	color = colorVert.rgba;
}	
